(require-macros :macros.vim)

(set-local-opt tabstop 2)
(set-local-opt shiftwidth 2)
(set-local-opt foldmethod :marker)
