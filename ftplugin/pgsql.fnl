(require-macros :macros.vim)

(set-local-opt tabstop 4)
(set-local-opt softtabstop 4)
(set-local-opt shiftwidth 4)
