(require-macros :macros.vim)
(local kulala (require :kulala))

(def-keys/this-buffer
  "execute request" n <CR> (kulala.run)
  "jump to the previous request" n <localleader>j (kulala.jump_prev)
  "jump to the next request" n <localleader>k (kulala.jump_next)
  "toggle between body and headers" n <localleader>t (kulala.toggle_view)
  "inspect current request" n <localleader>i (kulala.inspect)
  "download graphql schema" n <localleader>g (kulala.download_graphql_schema))
