-- [nfnl] Compiled from ftplugin/pgsql.fnl by https://github.com/Olical/nfnl, do not edit.
vim["opt_local"]["tabstop"] = 4
vim["opt_local"]["softtabstop"] = 4
vim["opt_local"]["shiftwidth"] = 4
return nil
