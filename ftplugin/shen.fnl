(require-macros :macros.vim)

(append-local-opt iskeyword [:- :? :! :.])
(set-local-opt commentstring "\\\\ %s")
