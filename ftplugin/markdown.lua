-- [nfnl] Compiled from ftplugin/markdown.fnl by https://github.com/Olical/nfnl, do not edit.
vim["opt_local"]["wrap"] = true
vim["opt_local"]["list"] = false
vim["opt_local"]["linebreak"] = true
local function _1_()
  local markdown = require("rz.lib.markdown")
  return markdown["preview-current"]()
end
return vim.keymap.set({"n"}, "<leader>r", _1_, {buffer = true, silent = true})
