-- [nfnl] Compiled from ftplugin/vim.fnl by https://github.com/Olical/nfnl, do not edit.
vim["opt_local"]["tabstop"] = 2
vim["opt_local"]["shiftwidth"] = 2
vim["opt_local"]["foldmethod"] = "marker"
return nil
