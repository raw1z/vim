-- [nfnl] Compiled from ftplugin/query.fnl by https://github.com/Olical/nfnl, do not edit.
vim["opt_local"]["wrap"] = true
local function _1_()
  local treesitter_query = require("nvim-treesitter.query")
  return treesitter_query.invalidate_query_cache()
end
return vim.api.nvim_create_autocmd({"BufWrite"}, {buffer = 0, callback = _1_})
