(require-macros :macros.core)
(require-macros :macros.vim)
(local {: autoload} (require :nfnl.module))
(local path (autoload :rz.lib.path))
(local mermaid (autoload :rz.lib.mermaid))

(fn src []
  (path.expand :%:p))

(fn compile-and-show []
  (let [output-file :/tmp/mermaid-output.png]
    (mermaid.compile (src)
                     output-file
                     #(path.preview output-file))))

(fn save-compile-and-show []
  (%> update)
  (compile-and-show))

(set-local-opt commentstring "%% %s")

(define-command MermaidPreview []
  (compile-and-show))

(define-command MermaidCompile [dest]
  (mermaid.compile (src) dest))

(define-key/this-buffer n <leader>r save-compile-and-show)
