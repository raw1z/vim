-- [nfnl] Compiled from ftplugin/fugitive.fnl by https://github.com/Olical/nfnl, do not edit.
vim["opt_local"]["number"] = false
vim["opt_local"]["relativenumber"] = false
local function _1_()
  return vim.cmd("Git commit --quiet")
end
local function _2_()
  return vim.cmd("Git commit --quiet --amend")
end
local function _3_()
  return vim.cmd("Git commit --quiet --amend --no-edit")
end
return {vim.keymap.set({"n"}, "cc", _1_, {buffer = true, desc = "git commit", silent = true}), vim.keymap.set({"n"}, "ca", _2_, {buffer = true, desc = "git amend", silent = true}), vim.keymap.set({"n"}, "ce", _3_, {buffer = true, desc = "git amend without edit", silent = true})}
