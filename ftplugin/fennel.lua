-- [nfnl] Compiled from ftplugin/fennel.fnl by https://github.com/Olical/nfnl, do not edit.
vim["opt_local"]["foldmethod"] = "marker"
do
  local opt_value_11_auto = vim.opt_local.iskeyword
  opt_value_11_auto:append({"?", "!"})
end
if ("colors/rz.fnl" == vim.fn.expand("<afile>")) then
  local colorizer = require("colorizer")
  local function _1_()
    return colorizer.detach_from_buffer(bufnr)
  end
  return vim.schedule(_1_)
else
  return nil
end
