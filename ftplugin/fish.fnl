(require-macros :macros.vim)

(set-opt shiftwidth 4)
(set-local-opt textwidth 79)
(set-local-opt foldmethod :marker)

(lint-code BufWritePost)
