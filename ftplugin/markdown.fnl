(require-macros :macros.core)
(require-macros :macros.vim)

(set-local-opt wrap true)
(set-local-opt list false)
(set-local-opt linebreak true)

(define-key/this-buffer n <leader>r
  (let [markdown (require :rz.lib.markdown)]
    (markdown.preview-current)))
