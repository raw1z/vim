-- [nfnl] Compiled from ftplugin/http.fnl by https://github.com/Olical/nfnl, do not edit.
local kulala = require("kulala")
local function _1_()
  return kulala.run()
end
local function _2_()
  return kulala.jump_prev()
end
local function _3_()
  return kulala.jump_next()
end
local function _4_()
  return kulala.toggle_view()
end
local function _5_()
  return kulala.inspect()
end
local function _6_()
  return kulala.download_graphql_schema()
end
return {vim.keymap.set({"n"}, "<CR>", _1_, {buffer = true, desc = "execute request", silent = true}), vim.keymap.set({"n"}, "<localleader>j", _2_, {buffer = true, desc = "jump to the previous request", silent = true}), vim.keymap.set({"n"}, "<localleader>k", _3_, {buffer = true, desc = "jump to the next request", silent = true}), vim.keymap.set({"n"}, "<localleader>t", _4_, {buffer = true, desc = "toggle between body and headers", silent = true}), vim.keymap.set({"n"}, "<localleader>i", _5_, {buffer = true, desc = "inspect current request", silent = true}), vim.keymap.set({"n"}, "<localleader>g", _6_, {buffer = true, desc = "download graphql schema", silent = true})}
