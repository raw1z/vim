(require-macros :macros.vim)

(set-local-opt foldmethod :marker)
(append-local-opt iskeyword [:? :!])
