-- [nfnl] Compiled from ftplugin/typescript.fnl by https://github.com/Olical/nfnl, do not edit.
local function _1_()
  local lsp_41_auto = require("rz.lib.lsp")
  return lsp_41_auto["autoformat-code"]()
end
return vim.api.nvim_create_autocmd({"BufWritePre"}, {buffer = 0, callback = _1_})
