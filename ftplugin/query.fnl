(require-macros :macros.core)
(require-macros :macros.vim)

(set-local-opt wrap true)

(define-autocmd/this-buffer BufWrite
  (let [treesitter-query (require :nvim-treesitter.query)]
    (treesitter-query.invalidate_query_cache)))
