-- [nfnl] Compiled from ftplugin/fish.fnl by https://github.com/Olical/nfnl, do not edit.
vim["opt"]["shiftwidth"] = 4
vim["opt_local"]["textwidth"] = 79
vim["opt_local"]["foldmethod"] = "marker"
local function _1_()
  local lint_42_auto = require("lint")
  return lint_42_auto.try_lint()
end
return vim.api.nvim_create_autocmd({"BufWritePost"}, {buffer = 0, callback = _1_})
