-- [nfnl] Compiled from ftplugin/mermaid.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local path = autoload("rz.lib.path")
local mermaid = autoload("rz.lib.mermaid")
local function src()
  return path.expand("%:p")
end
local function compile_and_show()
  local output_file = "/tmp/mermaid-output.png"
  local function _2_()
    return path.preview(output_file)
  end
  return mermaid.compile(src(), output_file, _2_)
end
local function save_compile_and_show()
  vim.cmd("update")
  return compile_and_show()
end
vim["opt_local"]["commentstring"] = "%% %s"
local function _3_()
  return compile_and_show()
end
vim.api.nvim_create_user_command("MermaidPreview", _3_, {force = true})
local function _4_(_241)
  local function _5_(dest)
    return mermaid.compile(src(), dest)
  end
  return _5_(_241.args)
end
vim.api.nvim_create_user_command("MermaidCompile", _4_, {force = true, nargs = 1})
return vim.keymap.set({"n"}, "<leader>r", save_compile_and_show, {buffer = true, silent = true})
