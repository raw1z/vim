-- [nfnl] Compiled from ftplugin/ruby.fnl by https://github.com/Olical/nfnl, do not edit.
local function _1_()
  local lsp_41_auto = require("rz.lib.lsp")
  return lsp_41_auto["autoformat-code"]()
end
vim.api.nvim_create_autocmd({"BufWritePre"}, {buffer = 0, callback = _1_})
do
  local opt_value_11_auto = vim.opt_local.iskeyword
  opt_value_11_auto:append({"?", "!", "@-@", "$"})
end
local gemfile = require("rz.lib.gemfile")
return gemfile.setup()
