-- [nfnl] Compiled from ftplugin/beancount.fnl by https://github.com/Olical/nfnl, do not edit.
local function _1_()
  local lsp_41_auto = require("rz.lib.lsp")
  return lsp_41_auto["autoformat-code"]()
end
vim.api.nvim_create_autocmd({"BufWritePre"}, {buffer = 0, callback = _1_})
local lazy_40_auto = require("lazy")
return lazy_40_auto.load({plugins = {"vim-speeddating"}})
