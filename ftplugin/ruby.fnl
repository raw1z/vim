(require-macros :macros.vim)

(autoformat-code)

(append-local-opt iskeyword [:? :! "@-@" :$])

(let [gemfile (require :rz.lib.gemfile)]
  (gemfile.setup))
