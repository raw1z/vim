-- [nfnl] Compiled from ftplugin/shen.fnl by https://github.com/Olical/nfnl, do not edit.
do
  local opt_value_11_auto = vim.opt_local.iskeyword
  opt_value_11_auto:append({"-", "?", "!", "."})
end
vim["opt_local"]["commentstring"] = "\\\\ %s"
return nil
