(require-macros :macros.core)
(require-macros :macros.vim)

(set-local-opt number false)
(set-local-opt relativenumber false)

(def-keys/this-buffer
  "git commit" n cc (%> Git commit --quiet)
  "git amend" n ca (%> Git commit --quiet --amend)
  "git amend without edit" n ce (%> Git commit --quiet --amend --no-edit))
