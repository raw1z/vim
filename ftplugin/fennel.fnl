(require-macros :macros.vim)

(set-local-opt foldmethod :marker)
(append-local-opt iskeyword [:? :!])

(when (= :colors/rz.fnl
         (vim.fn.expand :<afile>))
  (let [colorizer (require :colorizer)]
    (vim.schedule #(colorizer.detach_from_buffer bufnr))))
