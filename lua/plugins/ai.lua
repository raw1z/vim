-- [nfnl] Compiled from lua/plugins/ai.fnl by https://github.com/Olical/nfnl, do not edit.
local plugins_1_auto = require("rz.lib.plugins")
local _1_
do
  local plugins_1_auto0 = require("rz.lib.plugins")
  _1_ = plugins_1_auto0.declare("stevearc/dressing.nvim", {input = {enabled = false}})
end
local _2_
do
  local plugins_1_auto0 = require("rz.lib.plugins")
  _2_ = plugins_1_auto0.declare("HakonHarnes/img-clip.nvim", {event = "VeryLazy", opts = {default = {drag_and_drop = {insert_mode = true}, use_absolute_path = true, embed_image_as_base64 = false, prompt_for_file_name = false}}})
end
local function _3_(...)
  local plugins_1_auto0 = require("rz.lib.plugins")
  return plugins_1_auto0.declare("MeanderingProgrammer/render-markdown.nvim", {dependencies = {"nvim-treesitter/nvim-treesitter", "nvim-tree/nvim-web-devicons"}, ft = {"Avante"}, opts = {file_types = {"Avante"}, heading = {icons = {"\239\138\146 ", "\226\151\143 ", "* "}, signs = {}}, bullet = {icons = {"\226\150\170 ", "\226\150\171 "}}, checkbox = {unchecked = {icon = "\226\150\161 "}, checked = {icon = "\226\150\163 "}}, latex = {enabled = false}, link = {image = "\239\128\190 ", email = "@ ", hyperlink = "\239\145\140 ", custom = {web = {icon = "\239\146\132 "}}}}})
end
return plugins_1_auto.declare("yetone/avante.nvim", {build = "make", dependencies = {"nvim-treesitter/nvim-treesitter", _1_, "nvim-lua/plenary.nvim", "MunifTanjim/nui.nvim", "nvim-tree/nvim-web-devicons", _2_, _3_(...)}, event = "VeryLazy", opts = {behaviour = {auto_suggestions = true, auto_set_keymaps = true}, mappings = {suggestion = {accept = "<localleader><space>", next = "<localleader>j", previous = "<localleader>k"}}, suggestion = {debounce = 300, throttle = 300}, windows = {width = 50}}, lazy = false, version = false})
