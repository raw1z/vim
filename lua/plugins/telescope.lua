-- [nfnl] Compiled from lua/plugins/telescope.fnl by https://github.com/Olical/nfnl, do not edit.
local function opts(...)
  local actions = require("telescope.actions")
  local layout_actions = require("telescope.actions.layout")
  local trouble = require("trouble.sources.telescope")
  return {defaults = {entry_prefix = "    ", prompt_prefix = " \244\128\129\181  ", selection_caret = " \244\128\176\147  ", borderchars = {prompt = {"\226\150\128", "\226\150\144", "\226\150\132", "\226\150\140", "\226\150\155", "\226\150\156", "\226\150\159", "\226\150\153"}, results = {"\226\150\128", "\226\150\144", "\226\150\132", "\226\150\140", "\226\150\155", "\226\150\156", "\226\150\159", "\226\150\153"}, preview = {"\226\150\128", "\226\150\144", "\226\150\132", "\226\150\140", "\226\150\155", "\226\150\156", "\226\150\159", "\226\150\153"}}, mappings = {i = {["<esc>"] = actions.close, ["<c-t>"] = trouble.open, ["<C-n>"] = actions.cycle_history_next, ["<C-p>"] = actions.cycle_history_prev, ["<C-f>"] = actions.preview_scrolling_down, ["<C-b>"] = actions.preview_scrolling_up, ["<c-u>"] = false}, n = {s = actions.select_horizontal, v = actions.select_vertical, t = actions.select_tab, q = actions.close, ["<tab>"] = actions.smart_send_to_qflist, ["<c-t>"] = trouble.open, ["<space>"] = actions.toggle_selection, K = actions.preview_scrolling_up, J = actions.preview_scrolling_down, L = actions.preview_scrolling_right, H = actions.preview_scrolling_left, R = layout_actions.cycle_layout_next, ["<C-d>"] = actions.results_scrolling_down, ["<C-u>"] = actions.results_scrolling_up}}, ["file_ignore_patterns:"] = {"%.git"}, vimgrep_arguments = {"rg", "--color=never", "--no-heading", "--with-filename", "--line-number", "--column", "--smart-case", "--hidden", "-g", "!.git"}, show_line = false}, pickers = {find_files = {find_command = {"rg", "--files", "--hidden", "-g", "!.git"}}}}
end
local function load_extension(name)
  local telescope = require("telescope")
  return telescope.load_extension(name)
end
local function keys(...)
  local _1_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _2_()
      local builtin_2_auto = require("telescope.builtin")
      local method_3_auto = builtin_2_auto.current_buffer_fuzzy_find
      local themes_4_auto = require("telescope.themes")
      local opts_5_auto = themes_4_auto.get_ivy((nil or {}))
      return method_3_auto(opts_5_auto)
    end
    _1_ = plugins_2_auto.key({"n"}, "<space>B", _2_, {desc = "fuzzy search in current buffer"})
  end
  local _3_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _4_()
      local builtin_2_auto = require("telescope.builtin")
      local method_3_auto = builtin_2_auto.grep_string
      local themes_4_auto = require("telescope.themes")
      local opts_5_auto = themes_4_auto.get_ivy((nil or {}))
      return method_3_auto(opts_5_auto)
    end
    _3_ = plugins_2_auto.key({"n"}, "<space>F", _4_, {desc = "search for the word under the cursor"})
  end
  local _5_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _6_()
      local builtin_2_auto = require("telescope.builtin")
      local method_3_auto = builtin_2_auto.buffers
      local themes_4_auto = require("telescope.themes")
      local opts_5_auto = themes_4_auto.get_ivy((nil or {}))
      return method_3_auto(opts_5_auto)
    end
    _5_ = plugins_2_auto.key({"n"}, "<space>b", _6_, {desc = "list buffers"})
  end
  local _7_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _8_()
      local builtin_2_auto = require("telescope.builtin")
      local method_3_auto = builtin_2_auto.find_files
      local themes_4_auto = require("telescope.themes")
      local opts_5_auto = themes_4_auto.get_ivy((nil or {}))
      return method_3_auto(opts_5_auto)
    end
    _7_ = plugins_2_auto.key({"n"}, "<space>f", _8_, {desc = "find a file in current folder"})
  end
  local _9_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _10_()
      local builtin_2_auto = require("telescope.builtin")
      local method_3_auto = builtin_2_auto.help_tags
      local themes_4_auto = require("telescope.themes")
      local opts_5_auto = themes_4_auto.get_ivy((nil or {}))
      return method_3_auto(opts_5_auto)
    end
    _9_ = plugins_2_auto.key({"n"}, "<space>h", _10_, {desc = "search vim help"})
  end
  local _11_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _12_()
      local builtin_2_auto = require("telescope.builtin")
      local method_3_auto = builtin_2_auto.live_grep
      local themes_4_auto = require("telescope.themes")
      local opts_5_auto = themes_4_auto.get_ivy((nil or {}))
      return method_3_auto(opts_5_auto)
    end
    _11_ = plugins_2_auto.key({"n"}, "<space>k", _12_, {desc = "live grep in current folder"})
  end
  local _13_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _14_()
      local builtin_2_auto = require("telescope.builtin")
      local method_3_auto = builtin_2_auto.lsp_document_symbols
      local themes_4_auto = require("telescope.themes")
      local opts_5_auto = themes_4_auto.get_ivy(({symbol_width = 100} or {}))
      return method_3_auto(opts_5_auto)
    end
    _13_ = plugins_2_auto.key({"n"}, "<space>o", _14_, {desc = "list document symbols"})
  end
  local _15_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _16_()
      local builtin_2_auto = require("telescope.builtin")
      local method_3_auto = builtin_2_auto.quickfix
      local themes_4_auto = require("telescope.themes")
      local opts_5_auto = themes_4_auto.get_ivy((nil or {}))
      return method_3_auto(opts_5_auto)
    end
    _15_ = plugins_2_auto.key({"n"}, "<space>q", _16_, {desc = "show quickfix entries"})
  end
  local function _17_(...)
    local plugins_2_auto = require("rz.lib.plugins")
    local function _18_()
      local builtin = require("telescope.builtin")
      return builtin.git_status()
    end
    return plugins_2_auto.key({"n"}, "<leader>g/", _18_, {desc = "search files in current git status"})
  end
  return {_1_, _3_, _5_, _7_, _9_, _11_, _13_, _15_, _17_(...)}
end
local function define_autocmds()
  local _19_ = vim.api.nvim_create_augroup("rz_telescope_custom", {clear = true})
  local function _20_()
    vim["opt_local"]["wrap"] = false
    return nil
  end
  local function _21_()
    if (("TelescopePrompt" == vim.bo.ft) and ("i" == vim.fn.mode())) then
      return vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<Esc>", true, false, true), "i", false)
    else
      return nil
    end
  end
  return {vim.api.nvim_create_autocmd({"User"}, {callback = _20_, group = _19_, pattern = {"TelescopePreviewerLoaded"}}), vim.api.nvim_create_autocmd({"WinLeave"}, {callback = _21_, group = _19_, pattern = {"*"}})}
end
local function config(_, opts0)
  local telescope = require("telescope")
  telescope.setup(opts0)
  return define_autocmds()
end
local plugins_1_auto = require("rz.lib.plugins")
local _23_
do
  local plugins_1_auto0 = require("rz.lib.plugins")
  local function _24_()
    return load_extension("fzf")
  end
  _23_ = plugins_1_auto0.declare("nvim-telescope/telescope-fzf-native.nvim", {build = "make", config = _24_})
end
local _25_
do
  local plugins_1_auto0 = require("rz.lib.plugins")
  local function _26_()
    return load_extension("changes")
  end
  _25_ = plugins_1_auto0.declare("linarcx/telescope-changes.nvim", {config = _26_})
end
local _27_
do
  local plugins_1_auto0 = require("rz.lib.plugins")
  local function _28_()
    return load_extension("glyph")
  end
  _27_ = plugins_1_auto0.declare("ghassan0/telescope-glyph.nvim", {config = _28_})
end
local _29_
do
  local plugins_1_auto0 = require("rz.lib.plugins")
  local function _30_()
    return load_extension("emoji")
  end
  _29_ = plugins_1_auto0.declare("xiyaowong/telescope-emoji.nvim", {config = _30_})
end
local function _31_(...)
  local plugins_1_auto0 = require("rz.lib.plugins")
  local function _32_()
    return load_extension("gitmoji")
  end
  return plugins_1_auto0.declare("olacin/telescope-gitmoji.nvim", {config = _32_})
end
return plugins_1_auto.declare("nvim-telescope/telescope.nvim", {cmd = {"Telescope"}, config = config, dependencies = {"nvim-lua/plenary.nvim", "nvim-tree/nvim-web-devicons", "folke/trouble.nvim", _23_, _25_, _27_, _29_, _31_(...)}, keys = keys, opts = opts})
