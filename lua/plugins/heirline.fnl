(require-macros :macros.core)
(require-macros :macros.plugins)

(fn winbar-disabled? [buf]
  (let [conditions (require :heirline.conditions)
        buffer (require :rz.lib.buffer)]
    (or (buffer.floating? buf)
        (conditions.buffer_matches {:buftype (%w prompt quickfix)
                                    :filetype ["^git.*"
                                               :alpha
                                               :fugitive
                                               :TelescopeResults
                                               :Trouble]}
                                   buf))))

(fn config [...]
  (let [heirline (require :heirline)
        conditions (require :heirline.conditions)]
    (heirline.setup {:statusline (require :rz.lib.statusline)
                     :winbar (require :rz.lib.winbar)
                     :tabline (require :rz.lib.tabline)
                     ; :statuscolumn (require :rz.lib.statuscolumn)
                     :opts {:disable_winbar_cb #(winbar-disabled? $1.buf)}})))

(plug rebelot/heirline.nvim
      {: config
       :dependencies [:nvim-tree/nvim-web-devicons]
       :lazy false})
