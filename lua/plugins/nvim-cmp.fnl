(require-macros :macros.core)
(require-macros :macros.plugins)
(local {: autoload} (require :nfnl.module))
(local cmp (autoload :cmp))
(local luasnip (autoload :luasnip))
(local lspkind (autoload :lspkind))

(fn sources []
  (cmp.config.sources [{:name :nvim_lsp}
                       {:name :luasnip}
                       {:name :path}
                       {:name :buffer}]))

(fn expand [{: body}]
  (luasnip.lsp_expand body))

(fn scroll-back []
  (cmp.mapping (cmp.mapping.scroll_docs -4)
               [:i :c]))

(fn scroll-forward []
  (cmp.mapping (cmp.mapping.scroll_docs 4)
               [:i :c]))

(fn i-complete []
  (cmp.mapping.confirm {:behavior cmp.ConfirmBehavior.Replace
                        :select false}))

(fn c-complete [fallback]
  (if (cmp.visible)
    (cmp.confirm {:behavior cmp.ConfirmBehavior.Replace
                  :select false})
    (fallback)))

(fn complete []
  (cmp.mapping {:i (i-complete)
                :c c-complete}))

(fn close []
  (cmp.mapping {:i (cmp.mapping.abort)
                :c (cmp.mapping.close)}))

(fn confirm []
  (cmp.mapping.confirm {:behavior cmp.ConfirmBehavior.Replace
                        :select true}))

(fn i-control-n [fallback]
  (if (cmp.visible)
    (cmp.select_next_item {:behavior cmp.SelectBehavior.Replace})
    (fallback)))

(fn control-n []
  (cmp.mapping {:i i-control-n}))

(fn i-control-p [fallback]
  (if (cmp.visible)
    (cmp.select_prev_item {:behavior cmp.SelectBehavior.Replace})
    (fallback)))

(fn control-p []
  (cmp.mapping {:i i-control-p}))

(fn mapping []
  (cmp.mapping.preset.insert {:<C-n>     (control-n)
                              :<C-p>     (control-p)
                              :<C-b>     (scroll-back)
                              :<C-f>     (scroll-forward)
                              :<C-Space> (complete)
                              :<C-e>     (close)
                              :<CR>      (confirm)}))

(fn format []
  (lspkind.cmp_format {:mode :symbol_text
                       :menu {:buffer "[Buffer]"
                              :nvim_lsp "[LSP]"
                              :luasnip "[Snippet]"
                              :path "[Path]"}}))

(fn opts [...]
  {:snippet {:expand expand}
   :mapping (mapping)
   :sources (sources)
   :formatting {:format (format)}
   :view {:entries :custom}
   :preselect cmp.PreselectMode.Item
   :experimental {:ghost_text true}})

(fn config [_ opts]
  (cmp.setup opts)
  (cmp.setup.cmdline "/" {:mapping (cmp.mapping.preset.cmdline)
                          :sources [{:name :buffer}]})
  (cmp.setup.cmdline ":" {:mapping (cmp.mapping.preset.cmdline)
                          :sources (cmp.config.sources [{:name :path}]
                                                       [{:name :cmdline
                                                         :option {:ignore_cmds [:Man :!]}}])})
  (cmp.setup.filetype :http {:sources (cmp.config.sources [{:name :kulala-cmp-graphql}]
                                                          [{:name :buffer}])}))

(plug hrsh7th/nvim-cmp
      {:dependencies (%w hrsh7th/cmp-buffer
                         hrsh7th/cmp-cmdline
                         hrsh7th/cmp-nvim-lsp
                         hrsh7th/cmp-path
                         onsails/lspkind-nvim
                         saadparwaiz1/cmp_luasnip
                         PaterJason/cmp-conjure
                         mistweaverco/kulala-cmp-graphql.nvim)
       :event (%w CmdlineEnter
                  InsertEnter)
       : opts
       : config})
