-- [nfnl] Compiled from lua/plugins/git.fnl by https://github.com/Olical/nfnl, do not edit.
local function checkout_trunk()
  local git = require("rz.lib.git")
  local trunk_branch = git["guess-trunk-branch"]()
  return vim.cmd(string.format("Git checkout %s", trunk_branch))
end
local function show_branches()
  local lazy = require("lazy")
  lazy.load({plugins = {"telescope.nvim"}})
  local telescope_builtin = require("telescope.builtin")
  local telescope_themes = require("telescope.themes")
  return telescope_builtin.git_branches(telescope_themes.get_dropdown({previewer = false}))
end
local function show_folder_history()
  local git = require("rz.lib.git")
  local current_branch = git["current-branch"]()
  local trunk_branch = git["guess-trunk-branch"]()
  local current_folder = vim.fn.getcwd()
  if (current_branch == trunk_branch) then
    return vim.cmd(string.format("DiffviewFileHistory --no-merges %s", current_folder))
  else
    return vim.cmd(string.format("DiffviewFileHistory --range=%s..HEAD --no-merges %s", trunk_branch, current_folder))
  end
end
local function show_file_history()
  return vim.cmd("DiffviewFileHistory --no-merges %")
end
local function at_top(func)
  func()
  local function _2_()
    return vim.cmd("wincmd K")
  end
  return vim.schedule(_2_)
end
local function amend()
  return vim.cmd("Git commit --amend --quiet --no-edit")
end
local function stage_all_and_amend()
  vim.cmd("Git add .")
  return amend()
end
local function commit()
  return vim.cmd("Git commit --quiet")
end
local function stage_all_and_commit()
  vim.cmd("Git add .")
  return commit()
end
local _3_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local _4_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _5_()
      return vim.cmd("Git")
    end
    _4_ = plugins_2_auto.key({"n"}, "<leader>gg", _5_, {desc = "open git status"})
  end
  local _6_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _7_()
      return vim.cmd("Gw")
    end
    _6_ = plugins_2_auto.key({"n"}, "<leader>gw", _7_, {desc = "git stage current buffer's file"})
  end
  local _8_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _9_()
      return at_top(commit)
    end
    _8_ = plugins_2_auto.key({"n"}, "<leader>gc", _9_, {desc = "git commit"})
  end
  local _10_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _11_()
      return at_top(stage_all_and_commit)
    end
    _10_ = plugins_2_auto.key({"n"}, "<leader>gC", _11_, {desc = "stage all changes and git commit"})
  end
  local _12_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _13_()
      return at_top(amend)
    end
    _12_ = plugins_2_auto.key({"n"}, "<leader>ge", _13_, {desc = "git amend without edit"})
  end
  local _14_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _15_()
      return at_top(stage_all_and_amend)
    end
    _14_ = plugins_2_auto.key({"n"}, "<leader>gE", _15_, {desc = "stage all changes and git amend without edit"})
  end
  local _16_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _17_()
      return vim.cmd("Git commit --amend --quiet")
    end
    _16_ = plugins_2_auto.key({"n"}, "<leader>ga", _17_, {desc = "git amend"})
  end
  local _18_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _19_()
      return Snacks.git.blame_line()
    end
    _18_ = plugins_2_auto.key({"n"}, "<leader>gb", _19_, {desc = "show git blame for current line"})
  end
  local _20_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _21_()
      return vim.cmd("Git! push")
    end
    _20_ = plugins_2_auto.key({"n"}, "<leader>gp", _21_, {desc = "git push"})
  end
  local _22_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _23_()
      return vim.cmd("Git! enforce")
    end
    _22_ = plugins_2_auto.key({"n"}, "<leader>gP", _23_, {desc = "git force push"})
  end
  local _24_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _25_()
      return vim.cmd("Git! pull")
    end
    _24_ = plugins_2_auto.key({"n"}, "<leader>gu", _25_, {desc = "git pull"})
  end
  local _26_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _27_()
      return vim.cmd("Git! fetch")
    end
    _26_ = plugins_2_auto.key({"n"}, "<leader>gf", _27_, {desc = "git fetch"})
  end
  local _28_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _29_()
      return vim.cmd("Gvdiff")
    end
    _28_ = plugins_2_auto.key({"n"}, "<leader>gd", _29_, {desc = "git diff current buffer"})
  end
  local _30_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _30_ = plugins_2_auto.key({"n"}, "<leader>gT", checkout_trunk, {desc = "checkout trunk"})
  end
  local _31_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _31_ = plugins_2_auto.key({"n"}, "<leader>gh", show_folder_history, {desc = "show git history for the current folder"})
  end
  local _32_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _33_()
      return vim.cmd("Lazygit")
    end
    _32_ = plugins_2_auto.key({"n"}, "<leader>gz", _33_, {desc = "open Lazygit"})
  end
  local _34_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _34_ = plugins_2_auto.key({"n"}, "<leader>gH", show_file_history, {desc = "show git history for the current file"})
  end
  local _35_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _35_ = plugins_2_auto.key({"n"}, "<leader>gB", show_branches, {desc = "show git branches"})
  end
  local _36_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _37_()
      vim.cmd("Gread")
      return vim.cmd("update")
    end
    _36_ = plugins_2_auto.key({"n"}, "<leader>gR", _37_, {desc = "git restore current file"})
  end
  local _38_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _39_()
      return vim.cmd("Git add .")
    end
    _38_ = plugins_2_auto.key({"n"}, "<leader>gS", _39_, {desc = "stage all changes"})
  end
  local _40_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _41_()
      return vim.cmd("MergetoolToggle")
    end
    _40_ = plugins_2_auto.key({"n"}, "<leader>gmm", _41_, {desc = "open git conflict resolution view"})
  end
  local _42_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _43_()
      return vim.cmd("G checkout main")
    end
    _42_ = plugins_2_auto.key({"n"}, "<leader>gmtc", _43_, {desc = "checkout trunk"})
  end
  local _44_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _45_()
      return vim.cmd("RebaseTrunk")
    end
    _44_ = plugins_2_auto.key({"n"}, "<leader>gmtr", _45_, {desc = "rebase trunk"})
  end
  local _46_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _47_()
      return vim.cmd("BranchTrunk")
    end
    _46_ = plugins_2_auto.key({"n"}, "<leader>gmtb", _47_, {desc = "create new branch from trunk"})
  end
  local _48_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _49_()
      return vim.cmd("UpdateTrunk")
    end
    _48_ = plugins_2_auto.key({"n"}, "<leader>gmtu", _49_, {desc = "update the trunk"})
  end
  local _50_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _51_()
      return vim.cmd("RebaseContinue")
    end
    _50_ = plugins_2_auto.key({"n"}, "<leader>gmrc", _51_, {desc = "git rebase continue"})
  end
  local _52_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _53_()
      return vim.cmd("RebaseAbort")
    end
    _52_ = plugins_2_auto.key({"n"}, "<leader>gmra", _53_, {desc = "git rebase abort"})
  end
  local _54_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _55_()
      return vim.cmd("SaveChanges")
    end
    _54_ = plugins_2_auto.key({"n"}, "<leader>gmss", _55_, {desc = "stash current changes"})
  end
  local function _56_(...)
    local plugins_2_auto = require("rz.lib.plugins")
    local function _57_()
      return vim.cmd("RestoreChanges")
    end
    return plugins_2_auto.key({"n"}, "<leader>gmsp", _57_, {desc = "stash pop"})
  end
  _3_ = plugins_1_auto.declare("tpope/vim-fugitive", {cmd = {"G", "Git", "Gw", "Gread", "Gsplit", "Gvsplit", "Gvdiff"}, keys = {_4_, _6_, _8_, _10_, _12_, _14_, _16_, _18_, _20_, _22_, _24_, _26_, _28_, _30_, _31_, _32_, _34_, _35_, _36_, _38_, _40_, _42_, _44_, _46_, _48_, _50_, _52_, _54_, _56_(...)}})
end
local _58_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _58_ = plugins_1_auto.declare("sindrets/diffview.nvim", {cmd = {"DiffviewFileHistory", "DiffviewOpen"}, opts = {keymaps = {file_panel = {q = "<Cmd>DiffviewClose<CR>"}, file_history_panel = {q = "<Cmd>DiffviewClose<CR>"}}}})
end
local _59_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local function _60_(_241)
    local function _61_()
      return vim.cmd("Gitsigns next_hunk")
    end
    vim.keymap.set({"n"}, "<leader>gj", _61_, {buffer = _241, desc = "go to next hunk", silent = true})
    local function _62_()
      return vim.cmd("Gitsigns prev_hunk")
    end
    vim.keymap.set({"n"}, "<leader>gk", _62_, {buffer = _241, desc = "go to previous hunk", silent = true})
    local function _63_()
      return vim.cmd("Gitsigns stage_hunk")
    end
    vim.keymap.set({"n"}, "<leader>gs", _63_, {buffer = _241, desc = "stage hunk", silent = true})
    local function _64_()
      return vim.cmd("Gitsigns reset_hunk")
    end
    return vim.keymap.set({"n"}, "<leader>gr", _64_, {buffer = _241, desc = "reset hunk", silent = true})
  end
  _59_ = plugins_1_auto.declare("lewis6991/gitsigns.nvim", {event = {"BufReadPre", "BufNewFile"}, opts = {on_attach = _60_, signs = {add = {text = "+"}, change = {text = "\226\150\140"}, delete = {text = "_"}, topdelete = {text = "\226\128\190"}, changedelete = {text = "~"}, untracked = {text = "\226\150\140"}}}})
end
local _65_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local _66_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _66_ = plugins_2_auto.key({"n"}, "<leader>gy", "<cmd>lua require'gitlinker'.get_buf_range_url('n')<cr>", {desc = "copy a permalink for current line"})
  end
  local function _67_(...)
    local plugins_2_auto = require("rz.lib.plugins")
    return plugins_2_auto.key({"v"}, "<leader>gy", "<cmd>lua require'gitlinker'.get_buf_range_url('v')<cr>", {desc = "copy a permalink for selected line range"})
  end
  _65_ = plugins_1_auto.declare("ruifm/gitlinker.nvim", {dependencies = {"nvim-lua/plenary.nvim"}, keys = {_66_, _67_(...)}, opts = {mapping = nil, print_url = true}})
end
local function _68_(...)
  local plugins_1_auto = require("rz.lib.plugins")
  local function _69_(...)
    local plugins_2_auto = require("rz.lib.plugins")
    return plugins_2_auto.key({"n", "v"}, "<leader>gl", "<cmd>lua require'git-log'.check_log()<cr>", {desc = "check git log of the selected code"})
  end
  return plugins_1_auto.declare("niuiic/git-log.nvim", {dependencies = {"niuiic/core.nvim"}, keys = {_69_(...)}, opts = {}})
end
return {_3_, _58_, _59_, _65_, _68_(...)}
