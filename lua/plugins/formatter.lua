-- [nfnl] Compiled from lua/plugins/formatter.fnl by https://github.com/Olical/nfnl, do not edit.
local opts = {default_format_opts = {timeout_ms = 3000, lsp_format = "fallback", async = false, quiet = false}, formatters = {erb_lint = {command = "erb_lint", args = {"-a", "$FILENAME"}, inherit = false, stdin = false}}, formatters_by_ft = {eruby = {"erb_lint"}, fish = {"fish_indent"}, javascript = vim.tbl_extend("force", {stop_after_first = true}, {"prettier"}), lua = {"stylua"}, rust = vim.tbl_extend("force", {lsp_format = "fallback"}, {"rustfmt"})}, format_on_save = {timeout_ms = 5000, lsp_format = "fallback"}}
local function _1_(...)
  local plugins_1_auto = require("rz.lib.plugins")
  return plugins_1_auto.declare("stevearc/conform.nvim", {event = {"BufWritePre"}, cmd = {"ConformInfo"}, opts = opts})
end
return {_1_(...)}
