-- [nfnl] Compiled from lua/plugins/autopairs.fnl by https://github.com/Olical/nfnl, do not edit.
local function config(_, opts)
  local nvim_autopairs = require("nvim-autopairs")
  local Rule = require("nvim-autopairs.rule")
  nvim_autopairs.setup(opts)
  nvim_autopairs.remove_rule("`")
  nvim_autopairs.add_rule(Rule("`", "`", {"-shen", "-fennel", "-lisp", "-lfe", "-racket", "-scheme"}))
  nvim_autopairs.remove_rule("'")
  nvim_autopairs.add_rule(Rule("'", "'", {"-shen", "-fennel", "-lisp", "-lfe", "-racket", "-scheme"}))
  nvim_autopairs.add_rule(Rule("|", "|", "rust"))
  return nvim_autopairs.add_rule(Rule("|", "|", "ruby"))
end
local plugins_1_auto = require("rz.lib.plugins")
return plugins_1_auto.declare("windwp/nvim-autopairs", {config = config, event = {"BufReadPost", "BufNewFile"}, opts = {disable_filetype = {"TelescopePrompt"}, map_c_h = true, enable_check_bracket_line = false}})
