(require-macros :macros.core)
(require-macros :macros.plugins)

(fn install-parsers []
  (let [parsers (require :nvim-treesitter.parsers)
        parser-config (parsers.get_parser_configs)]
    (tset parser-config :lfe {:install_info {:url :https://github.com/raw1z/tree-sitter-lfe.git
                                             :files [:src/parser.c]}})
    (tset parser-config :shen {:install_info {:url :https://github.com/raw1z/tree-sitter-shen.git
                                              :branch :main
                                              :files [:src/parser.c]}})
    (tset parser-config :grpcnvim {:install_info {:url :https://github.com/antosha417/tree-sitter-grpc-nvim
                                                  :branch :master
                                                  :files [:src/parser.c
                                                          :src/scanner.cc]
                                                  :filetype :grpcnvim}})))

(fn config [_ opts]
  (install-parsers)
  (let [configs (require :nvim-treesitter.configs)]
    (configs.setup opts)))

(fn installed-languages []
  [:bash
   :c
   :clojure
   :css
   :dart
   :diff
   :eex
   :elixir
   :elm
   :fish
   :fennel
   :git_config
   :git_rebase
   :gitattributes
   :gitcommit
   :gitignore
   :go
   :graphql
   :heex
   :html
   :http
   :janet_simple
   :javascript
   :json
   :lua
   :make
   :markdown
   :nix
   :nu
   :proto
   :python
   :query
   :racket
   :ruby
   :rust
   :scheme
   :sql
   :starlark
   :tsx
   :typescript
   :vim
   :vue
   :vimdoc
   :xml
   :yaml])

[(plug nvim-treesitter/playground
       {:cmd :TSPlaygroundToggle})

 (plug nvim-treesitter/nvim-treesitter
      {:build ":TSUpdate"
       :cmd [:TSUpdateSync]
       : config
       :dependencies [:JoosepAlviste/nvim-ts-context-commentstring
                      :nvim-treesitter/nvim-treesitter-textobjects]
       :event (%w BufReadPost BufNewFile)
       :opts {:ensure_installed (installed-languages)
              :highlight {:enable true}
              :indent {:enable true}
              :textobjects {:enable true
                            :select {:enable true
                                     :lookahead true
                                     :keymaps {:af "@function.outer"
                                               :if "@function.inner"
                                               :ac "@class.outer"
                                               :ic "@class.inner"
                                               :ad "@context.outer"
                                               :ic "@context.inner"}}
                            :move {:enable true
                                   :set_jumps true
                                   :goto_next_start     {"]f" {:query "@function.outer"
                                                               :desc "Next function start"}
                                                         "]c" {:query "@class.outer"
                                                               :desc "Next class start"}
                                                         "]d" {:query "@context.outer"
                                                               :desc "Next context start"}}
                                   :goto_next_end       {"]F" {:query "@function.outer"
                                                               :desc "Next function start"}
                                                         "]C" {:query "@class.outer"
                                                               :desc "Next class start"}
                                                         "]D" {:query "@context.outer"
                                                               :desc "Next context start"}}
                                   :goto_previous_start {"[f" {:query  "@function.outer"
                                                               :desc "Previous function start"}
                                                         "[c" {:query "@class.outer"
                                                              :desc "Previous class start"}
                                                         "[d" {:query "@context.outer"
                                                               :desc "Previous context start"}}
                                  :goto_previous_end    {"[F" {:query  "@function.outer"
                                                               :desc "Previous function start"}
                                                         "[C" {:query "@class.outer"
                                                              :desc "Previous class start"}
                                                         "[D" {:query "@context.outer"
                                                               :desc "Previous context start"}}}
                            :ls_interop {:enable false}}}})

 (plug JoosepAlviste/nvim-ts-context-commentstring
       {:config true})]
