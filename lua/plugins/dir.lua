-- [nfnl] Compiled from lua/plugins/dir.fnl by https://github.com/Olical/nfnl, do not edit.
local plugins_1_auto = require("rz.lib.plugins")
local _1_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _2_()
    local dir_2_auto = require("rz.lib.dir")
    return dir_2_auto["open-current-buffer-file-dirname"]("no")
  end
  _1_ = plugins_2_auto.key({"n"}, "--", _2_, {desc = "browse current file's folder in same buffer"})
end
local _3_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _4_()
    local dir_2_auto = require("rz.lib.dir")
    return dir_2_auto["open-current-buffer-file-dirname"]("horizontal")
  end
  _3_ = plugins_2_auto.key({"n"}, "-s", _4_, {desc = "browse current file's folder in horizontal split"})
end
local _5_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _6_()
    local dir_2_auto = require("rz.lib.dir")
    return dir_2_auto["open-current-buffer-file-dirname"]("vertical")
  end
  _5_ = plugins_2_auto.key({"n"}, "-v", _6_, {desc = "browse current file's folder in same vertical split"})
end
local _7_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _8_()
    local dir_2_auto = require("rz.lib.dir")
    return dir_2_auto["open-current-buffer-file-dirname"]("tab")
  end
  _7_ = plugins_2_auto.key({"n"}, "-t", _8_, {desc = "browse current file's folder in new tab"})
end
local _9_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _10_()
    local dir_2_auto = require("rz.lib.dir")
    return dir_2_auto["open-current-buffer-file-dirname"]("float")
  end
  _9_ = plugins_2_auto.key({"n"}, "-f", _10_, {desc = "browse current file's folder in a floating window"})
end
local _11_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _12_()
    local dir_2_auto = require("rz.lib.dir")
    return dir_2_auto["open-project-root-directory"]("no")
  end
  _11_ = plugins_2_auto.key({"n"}, "<leader>--", _12_, {desc = "browse current project's root folder in same buffer"})
end
local _13_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _14_()
    local dir_2_auto = require("rz.lib.dir")
    return dir_2_auto["open-project-root-directory"]("horizontal")
  end
  _13_ = plugins_2_auto.key({"n"}, "<leader>-s", _14_, {desc = "browse current project's root folder in horizontal split"})
end
local _15_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _16_()
    local dir_2_auto = require("rz.lib.dir")
    return dir_2_auto["open-project-root-directory"]("vertical")
  end
  _15_ = plugins_2_auto.key({"n"}, "<leader>-v", _16_, {desc = "browse current project's root folder in same vertical split"})
end
local _17_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _18_()
    local dir_2_auto = require("rz.lib.dir")
    return dir_2_auto["open-project-root-directory"]("tab")
  end
  _17_ = plugins_2_auto.key({"n"}, "<leader>-t", _18_, {desc = "browse current project's root folder in new tab"})
end
local function _19_(...)
  local plugins_2_auto = require("rz.lib.plugins")
  local function _20_()
    local dir_2_auto = require("rz.lib.dir")
    return dir_2_auto["open-project-root-directory"]("float")
  end
  return plugins_2_auto.key({"n"}, "<leader>-f", _20_, {desc = "browse current project's root folder in a floating window"})
end
return plugins_1_auto.declare("stevearc/oil.nvim", {dependencies = {"nvim-tree/nvim-web-devicons"}, cmd = "Oil", event = "CmdlineEnter", keys = {_1_, _3_, _5_, _7_, _9_, _11_, _13_, _15_, _17_, _19_(...)}, opts = {default_file_explorer = true, keymaps = {["<CR>"] = "actions.select", ["<C-p>"] = "actions.preview", q = "actions.close", ["<C-r>"] = "actions.refresh", ["<S-U>"] = "actions.parent", ["<C-o>"] = "actions.open_external", ["<C-y>"] = "actions.copy_entry_path"}, skip_confirm_for_simple_edit = true, watch_for_changes = true, view_options = {show_hidden = true}, prompt_save_on_select_new_entry = false, use_default_keymaps = false}})
