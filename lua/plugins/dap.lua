-- [nfnl] Compiled from lua/plugins/dap.fnl by https://github.com/Olical/nfnl, do not edit.
local plugins_1_auto = require("rz.lib.plugins")
local _1_
do
  local plugins_1_auto0 = require("rz.lib.plugins")
  local _2_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _3_()
      local dap_go_2_auto = require("dap-go")
      return dap_go_2_auto.debug_test()
    end
    _2_ = plugins_2_auto.key({"n"}, "<leader>dt", _3_, {desc = "debug go test"})
  end
  local function _4_(...)
    local plugins_2_auto = require("rz.lib.plugins")
    local function _5_()
      local dap_go_2_auto = require("dap-go")
      return dap_go_2_auto.debug_last_test()
    end
    return plugins_2_auto.key({"n"}, "<leader>dl", _5_)
  end
  _1_ = plugins_1_auto0.declare("leoluz/nvim-dap-go", {config = true, keys = {_2_, _4_(...)}})
end
local function _6_(...)
  local plugins_1_auto0 = require("rz.lib.plugins")
  return plugins_1_auto0.declare("suketa/nvim-dap-ruby", {config = true})
end
local _7_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _8_()
    local dap_2_auto = require("dap")
    return dap_2_auto.continue()
  end
  _7_ = plugins_2_auto.key({"n"}, "<leader>dd", _8_, {desc = "start the debugger or continue if already up"})
end
local _9_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _10_()
    local dap_2_auto = require("dap")
    return dap_2_auto.terminate()
  end
  _9_ = plugins_2_auto.key({"n"}, "<leader>dx", _10_, {desc = "stop the debugger"})
end
local _11_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _12_()
    local widgets_2_auto = require("dap.ui.widgets")
    return widgets_2_auto.hover()
  end
  _11_ = plugins_2_auto.key({"n", "v"}, "<leader>dh", _12_, {desc = "show value for local under the cursor"})
end
local _13_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _14_()
    local widgets_2_auto = require("dap.ui.widgets")
    return widgets_2_auto.preview()
  end
  _13_ = plugins_2_auto.key({"n", "v"}, "<leader>dp", _14_, {desc = "preview value for local under the cursor"})
end
local _15_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _16_()
    local dap_2_auto = require("dap")
    return dap_2_auto.toggle_breakpoint()
  end
  _15_ = plugins_2_auto.key({"n"}, "<leader>db", _16_, {desc = "toggle breakpoint"})
end
local _17_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _18_()
    local dap_2_auto = require("dap")
    return dap_2_auto.step_over()
  end
  _17_ = plugins_2_auto.key({"n"}, "<leader>do", _18_, {desc = "debugger step over"})
end
local _19_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _20_()
    local dap_2_auto = require("dap")
    return dap_2_auto.step_into()
  end
  _19_ = plugins_2_auto.key({"n"}, "<leader>di", _20_, {desc = "debugger step into"})
end
local _21_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _22_()
    local dap_2_auto = require("dap")
    return dap_2_auto.repl.open()
  end
  _21_ = plugins_2_auto.key({"n"}, "<leader>dr", _22_, {desc = "open debugger repl"})
end
local _23_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _24_()
    local widgets_2_auto = require("dap.ui.widgets")
    return widgets_2_auto.centered_float(widgets_2_auto.frames)
  end
  _23_ = plugins_2_auto.key({"n"}, "<leader>df", _24_, {desc = "show debugger frames"})
end
local function _25_(...)
  local plugins_2_auto = require("rz.lib.plugins")
  local function _26_()
    local widgets_2_auto = require("dap.ui.widgets")
    return widgets_2_auto.centered_float(widgets_2_auto.scopes)
  end
  return plugins_2_auto.key({"n"}, "<leader>ds", _26_, {desc = "show debugger scopes"})
end
return plugins_1_auto.declare("mfussenegger/nvim-dap", {dependencies = {_1_, _6_(...)}, keys = {_7_, _9_, _11_, _13_, _15_, _17_, _19_, _21_, _23_, _25_(...)}})
