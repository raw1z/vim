-- [nfnl] Compiled from lua/plugins/luasnip.fnl by https://github.com/Olical/nfnl, do not edit.
local function load_snippets()
  local path = require("rz.lib.path")
  local from_vscode = require("luasnip.loaders.from_vscode")
  local from_lua = require("luasnip.loaders.from_lua")
  from_vscode.lazy_load({exclude = {"clojure", "fennel", "gitcommit", "go", "ruby", "yaml"}})
  return from_lua.lazy_load({paths = path.join(vim.fn.stdpath("config"), "snippets")})
end
local function setup(opts)
  local ls = require("luasnip")
  return ls.config.set_config(opts)
end
local function _1_(_241)
  local function _2_(filetype_3f)
    local snippets = require("rz.lib.snippets")
    local buffer = require("rz.lib.buffer")
    local filetype
    if (1 == vim.fn.empty(filetype_3f)) then
      filetype = buffer.filetype(buffer.current())
    else
      filetype = filetype_3f
    end
    local event = snippets.watch(filetype)
    snippets.edit(filetype)
    vim.api.nvim_buf_set_option(0, "bufhidden", "delete")
    local function _4_()
      return event:stop()
    end
    return vim.api.nvim_create_autocmd({"BufUnload"}, {buffer = 0, callback = _4_})
  end
  return _2_(_241.args)
end
vim.api.nvim_create_user_command("SnipEdit", _1_, {complete = "filetype", force = true, nargs = "?"})
local function _5_(_241)
  local function _6_(filetype_3f)
    local snippets = require("rz.lib.snippets")
    local buffer = require("rz.lib.buffer")
    local filetype
    if (1 == vim.fn.empty(filetype_3f)) then
      filetype = buffer.filetype(buffer.current())
    else
      filetype = filetype_3f
    end
    return snippets.reload(filetype)
  end
  return _6_(_241.args)
end
vim.api.nvim_create_user_command("SnipReload", _5_, {complete = "filetype", force = true, nargs = "?"})
local function config(_, opts)
  setup(opts)
  return load_snippets()
end
local function jump_to_next()
  local ls = require("luasnip")
  if ls.expand_or_jumpable() then
    return ls.expand_or_jump()
  else
    return nil
  end
end
local function jump_to_prev()
  local ls = require("luasnip")
  if ls.jumpable(-1) then
    return ls.jump(-1)
  else
    return nil
  end
end
local function previous_choice()
  local ls = require("luasnip")
  if ls.choice_active() then
    return ls.change_choice(-1)
  else
    return nil
  end
end
local function next_choice()
  local ls = require("luasnip")
  if ls.choice_active() then
    return ls.change_choice(1)
  else
    return nil
  end
end
local function keys(...)
  local _12_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _12_ = plugins_2_auto.key({"i", "s"}, "<C-j>", jump_to_next, {desc = "expand snippet or jump to next slot"})
  end
  local _13_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _13_ = plugins_2_auto.key({"i", "s"}, "<C-k>", jump_to_prev, {desc = "jump to previous slot in snippet"})
  end
  local _14_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _14_ = plugins_2_auto.key({"i", "s"}, "<C-h>", previous_choice, {desc = "select previous slot choice in snippet"})
  end
  local function _15_(...)
    local plugins_2_auto = require("rz.lib.plugins")
    return plugins_2_auto.key({"i", "s"}, "<C-l>", next_choice, {desc = "select next slot choice in snippet"})
  end
  return {_12_, _13_, _14_, _15_(...)}
end
local plugins_1_auto = require("rz.lib.plugins")
return plugins_1_auto.declare("L3MON4D3/LuaSnip", {config = config, dependencies = {"rafamadriz/friendly-snippets"}, keys = keys, opts = {history = true, updateevents = "TextChanged,TextChangedI", enable_autosnippets = false}, version = "v1.*"})
