(require-macros :macros.plugins)

(fn config [_ opts]
  (let [nvim-autopairs (require :nvim-autopairs)
        Rule (require :nvim-autopairs.rule)]
    (nvim-autopairs.setup opts)
    ; lisps
    (nvim-autopairs.remove_rule "`")
    (nvim-autopairs.add_rule (Rule "`" "`" [:-shen :-fennel :-lisp :-lfe :-racket :-scheme]))
    (nvim-autopairs.remove_rule "'")
    (nvim-autopairs.add_rule (Rule "'" "'" [:-shen :-fennel :-lisp :-lfe :-racket :-scheme]))
    ; rust
    (nvim-autopairs.add_rule (Rule "|" "|" :rust))
    ; ruby
    (nvim-autopairs.add_rule (Rule "|" "|" :ruby))))

(plug windwp/nvim-autopairs
      {: config
       :event [:BufReadPost :BufNewFile]
       :opts {:disable_filetype [:TelescopePrompt]
              :map_c_h true
              :enable_check_bracket_line false}})
