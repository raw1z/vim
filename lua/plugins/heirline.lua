-- [nfnl] Compiled from lua/plugins/heirline.fnl by https://github.com/Olical/nfnl, do not edit.
local function winbar_disabled_3f(buf)
  local conditions = require("heirline.conditions")
  local buffer = require("rz.lib.buffer")
  local or_1_ = buffer["floating?"](buf)
  if not or_1_ then
    or_1_ = conditions.buffer_matches({buftype = {"prompt", "quickfix"}, filetype = {"^git.*", "alpha", "fugitive", "TelescopeResults", "Trouble"}}, buf)
  end
  return or_1_
end
local function config(...)
  local heirline = require("heirline")
  local conditions = require("heirline.conditions")
  local function _2_(_241)
    return winbar_disabled_3f(_241.buf)
  end
  return heirline.setup({statusline = require("rz.lib.statusline"), winbar = require("rz.lib.winbar"), tabline = require("rz.lib.tabline"), opts = {disable_winbar_cb = _2_}})
end
local plugins_1_auto = require("rz.lib.plugins")
return plugins_1_auto.declare("rebelot/heirline.nvim", {config = config, dependencies = {"nvim-tree/nvim-web-devicons"}, lazy = false})
