(require-macros :macros.core)
(require-macros :macros.plugins)
(require-macros :macros.vim)
(local lsp (require :rz.lib.lsp))

(fn lua-ls-handler []
  (lsp.configure :lua_ls {:settings {:Lua {:runtime {:version :LuaJIT}
                                           :format {:enable false}
                                           :diagnostics {:globals [:vim :hs]}
                                           :workspace {:library (vim.api.nvim_get_runtime_file "" true)}
                                           :telemetry {:enable false}}}}))

(fn fennel-ls-handler []
  (lsp.configure :fennel_language_server {:settings {:fennel {:workspace {:library (vim.api.nvim_list_runtime_paths)}
                                                              :diagnostics {:globals [:vim]}}}}))

(fn beancount-ls-handler []
  (lsp.configure :beancount {:init_options {:journal_file (vim.fn.expand :<afile>:p)}}))

(fn rust-analyzer-ls-handler []
  (lsp.configure :rust_analyzer {:settings {:rust-analyzer {:checkOnSave {:command :clippy
                                                                          :extraArgs [:--
                                                                                      :-W :clippy::pedantic
                                                                                      :-W :clippy::nursery
                                                                                      :-W :clippy::unwrap_used
                                                                                      :-W :clippy::expect_used]}}}}))

(fn handlers []
  (doto {}
        (tset :beancount beancount-ls-handler)
        (tset :fennel_language_server fennel-ls-handler)
        (tset :lua_ls lua-ls-handler)
        (tset :rust_analyzer rust-analyzer-ls-handler)
        (tset 1 #(lsp.configure $1))))

(define-augroup rz_lsp_config
  (define-autocmd LspAttach _ lsp.on-attach))

[(plug williamboman/mason-lspconfig.nvim
       {:dependencies [(plug neovim/nvim-lspconfig
                             {:event (%w BufReadPost BufNewFile)})
                       (plug williamboman/mason.nvim {:cmd [:Mason]
                                                      :opts {}})]
        :event (%w BufReadPre BufNewFile)
        :opts {:handlers (handlers)}})

 (plug glepnir/lspsaga.nvim
       {:branch :main
        :cmd :Lspsaga
        :event :LspAttach
        :opts {:ui {:code_action :A}
               :symbol_in_winbar {:enable false}}})

 (plug ray-x/lsp_signature.nvim
       {:event :VeryLazy
        :opts {}
        :config #((. (require :lsp_signature) :setup))})

 (plug cshuaimin/ssr.nvim
       {:keys [(plug-key n|x
                         <localleader>SR
                         #(let [ssr (require :ssr)]
                            (ssr.open))
                         {:desc "structural search and replace"})]})

 (plug akinsho/flutter-tools.nvim
       {:cond #(not= 0 (vim.fn.filereadable :pubspec.yaml))
        :dependencies [:nvim-lua/plenary.nvim]
        :lazy false
        :opts #{:dev_log {:open_cmd :0tabnew}
                :lsp {:on_attach lsp.on-attach}}})

 (plug j-hui/fidget.nvim
       {:config true
        :event :LspAttach})]
