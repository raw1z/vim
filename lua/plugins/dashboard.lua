-- [nfnl] Compiled from lua/plugins/dashboard.fnl by https://github.com/Olical/nfnl, do not edit.
local dashboard_width = 80
local dashboard_mru_size = 9
local function padding(size)
  return {type = "padding", val = size}
end
local function header()
  return {type = "text", val = {" _____     _ _     ", "|  |  |___| | |___ ", "|     | -_| | | . |", "|__|__|___|_|_|___|"}, opts = {hl = "RZDashboardHeader", position = "center", shrink_margin = false}}
end
local function button(shortcut, label, command)
  local keybind = string.format("<cmd>%s <cr>", command)
  local formatted_label
  if (#label < (dashboard_width - 4)) then
    formatted_label = label
  else
    formatted_label = vim.fn.pathshorten(label)
  end
  local function _2_()
    return vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes((keybind .. "<Ignore>"), true, false, true), "t", false)
  end
  return {type = "button", val = string.format(" %s", formatted_label), on_press = _2_, opts = {position = "center", shortcut = shortcut, cursor = 4, width = dashboard_width, align_shortcut = "left", hl_shortcut = "RZDashboardButtonShortcut", keymap = {"n", shortcut, keybind, {noremap = true, silent = true, nowait = true}}}}
end
local function section_title(text)
  return {type = "group", val = {{type = "text", val = string.format("%s%s", text, vim.fn["repeat"](" ", (dashboard_width - #text))), opts = {position = "center", hl = "RZDashboardSectionTitleText"}}, {type = "text", val = vim.fn["repeat"]("\226\149\144", dashboard_width), opts = {position = "center", hl = "RZDashboardSectionTitleSep"}}}}
end
local function section_body(body)
  return {type = "group", val = body, opts = {shrink_margin = false}}
end
local function section(title, items)
  return {type = "group", val = {section_title(title), items, padding(2)}}
end
local function file_button(index, file)
  return button(tostring(index), vim.fn.fnamemodify(file, ":."), string.format("edit %s", file))
end
local function mru_files()
  local mru = require("rz.lib.mru")
  local mru_files0 = mru.get(vim.fn.getcwd(), dashboard_mru_size)
  local tbl_21_auto = {}
  local i_22_auto = 0
  for index, file in ipairs(mru_files0) do
    local val_23_auto = file_button(index, file)
    if (nil ~= val_23_auto) then
      i_22_auto = (i_22_auto + 1)
      tbl_21_auto[i_22_auto] = val_23_auto
    else
    end
  end
  return tbl_21_auto
end
local function mru()
  return section("Most Recently Used", section_body(mru_files))
end
local function buttons()
  local items = {button("e", "New File", "enew")}
  do
    local session = require("rz.lib.session")
    local ok_3f, exists_3f = pcall(session["file-exist?"])
    if (ok_3f and exists_3f) then
      table.insert(items, button("s", "Load Session", "LoadSession"))
    else
    end
  end
  table.insert(items, button("q", "Quit", "quit"))
  return section("Menu", section_body(items))
end
local function miscelanious_infos()
  local git = require("rz.lib.git")
  local is_repo_3f = git["is-git-repository?"](vim.fn.getcwd())
  local branch_name
  if is_repo_3f then
    branch_name = ("\239\144\152 " .. git["current-branch"]())
  else
    branch_name = ""
  end
  return {type = "group", val = {{type = "text", val = branch_name, opts = {position = "center", hl = "RZDashboardMiscelaniousInfos"}}}}
end
local function date()
  return {type = "group", val = {{type = "text", val = os.date("%A, %B %d, %Y"), opts = {position = "center", hl = "RZDashboardDate"}}}}
end
local function truth()
  local text = "Oui, Dieu a tant aim\195\169 le monde qu\226\128\153il a donn\195\169 son Fils, son unique,\n             pour que tous ceux qui placent leur confiance en lui \195\169chappent \195\160 la perdition\n             et qu\226\128\153ils aient la vie \195\169ternelle."
  local lines
  local function _6_(_241)
    return {type = "text", val = _241, opts = {position = "center", hl = "RZDashboardTruthText"}}
  end
  lines = vim.tbl_map(_6_, vim.tbl_map(vim.trim, vim.split(text, "\n")))
  local reference = {type = "text", val = "Jean 3.16", opts = {position = "center", hl = "RZDashboardTruthReference"}}
  return {type = "group", val = vim.fn.extendnew(lines, {reference})}
end
local function opts()
  return {layout = {padding(1), header(), date(), padding(1), truth(), padding(2), mru(), buttons(), padding(2), miscelanious_infos()}}
end
local plugins_1_auto = require("rz.lib.plugins")
return plugins_1_auto.declare("goolord/alpha-nvim", {dependencies = {"nvim-tree/nvim-web-devicons"}, opts = opts, lazy = false})
