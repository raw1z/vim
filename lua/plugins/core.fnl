(require-macros :macros.plugins)

[(plug Olical/nfnl
       {:lazy false
        :priority 10000})

 (plug nvim-lua/plenary.nvim)

 (plug folke/snacks.nvim
       {:keys [(plug-key n <space>N #(Snacks.notifier.show_history) {:desc "Notification History"})]
        :lazy false
        :opts {:input {:enabled true}
               :notifier {:enabled true}
               :statuscolumn {:enabled true}}
        :priority 1000})]
