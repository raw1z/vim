-- [nfnl] Compiled from lua/plugins/trouble.fnl by https://github.com/Olical/nfnl, do not edit.
local function close_trouble()
  local trouble = require("trouble")
  return trouble.close()
end
local plugins_1_auto = require("rz.lib.plugins")
local _1_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _2_()
    return vim.cmd("Trouble diagnostics toggle focus=true")
  end
  _1_ = plugins_2_auto.key({"n"}, "<space>tD", _2_, {desc = "show entire workspace's diagnostics"})
end
local _3_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _4_()
    return vim.cmd("Trouble diagnostics toggle filter.buf=0 focus=true")
  end
  _3_ = plugins_2_auto.key({"n"}, "<space>td", _4_, {desc = "show current buffer's diagnostics"})
end
local _5_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _6_()
    return vim.cmd("Trouble lsp_implementations toggle focus=true")
  end
  _5_ = plugins_2_auto.key({"n"}, "<space>ti", _6_, {desc = "show implementations from lsp"})
end
local _7_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _8_()
    return vim.cmd("Trouble loclist toggle focus=true")
  end
  _7_ = plugins_2_auto.key({"n"}, "<space>tl", _8_, {desc = "show location list with trouble"})
end
local _9_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _10_()
    return vim.cmd("Trouble symbols toggle pinned=true win.relative=win win.position=right focus=true")
  end
  _9_ = plugins_2_auto.key({"n"}, "<space>to", _10_, {desc = "show document symbols"})
end
local _11_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _12_()
    return vim.cmd("Trouble qflist toggle focus=true")
  end
  _11_ = plugins_2_auto.key({"n"}, "<space>tq", _12_, {desc = "show quickfix with trouble"})
end
local _13_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _14_()
    return vim.cmd("Trouble lsp_references toggle focus=true")
  end
  _13_ = plugins_2_auto.key({"n"}, "<space>tr", _14_, {desc = "show references from lsp"})
end
local _15_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _16_()
    return vim.cmd("Trouble lsp toggle focus=true")
  end
  _15_ = plugins_2_auto.key({"n"}, "<space>ts", _16_, {desc = "LSP definitions, references, implementations, type definitions, and declarations"})
end
local function _17_(...)
  local plugins_2_auto = require("rz.lib.plugins")
  return plugins_2_auto.key({"n"}, "<space>tt", close_trouble, {desc = "close trouble's buffer"})
end
return plugins_1_auto.declare("folke/trouble.nvim", {opts = {auto_close = true}, cmd = {"Trouble"}, dependencies = {"nvim-tree/nvim-web-devicons"}, keys = {_1_, _3_, _5_, _7_, _9_, _11_, _13_, _15_, _17_(...)}})
