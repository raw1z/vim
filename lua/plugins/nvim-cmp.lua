-- [nfnl] Compiled from lua/plugins/nvim-cmp.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local cmp = autoload("cmp")
local luasnip = autoload("luasnip")
local lspkind = autoload("lspkind")
local function sources()
  return cmp.config.sources({{name = "nvim_lsp"}, {name = "luasnip"}, {name = "path"}, {name = "buffer"}})
end
local function expand(_2_)
  local body = _2_["body"]
  return luasnip.lsp_expand(body)
end
local function scroll_back()
  return cmp.mapping(cmp.mapping.scroll_docs(-4), {"i", "c"})
end
local function scroll_forward()
  return cmp.mapping(cmp.mapping.scroll_docs(4), {"i", "c"})
end
local function i_complete()
  return cmp.mapping.confirm({behavior = cmp.ConfirmBehavior.Replace, select = false})
end
local function c_complete(fallback)
  if cmp.visible() then
    return cmp.confirm({behavior = cmp.ConfirmBehavior.Replace, select = false})
  else
    return fallback()
  end
end
local function complete()
  return cmp.mapping({i = i_complete(), c = c_complete})
end
local function close()
  return cmp.mapping({i = cmp.mapping.abort(), c = cmp.mapping.close()})
end
local function confirm()
  return cmp.mapping.confirm({behavior = cmp.ConfirmBehavior.Replace, select = true})
end
local function i_control_n(fallback)
  if cmp.visible() then
    return cmp.select_next_item({behavior = cmp.SelectBehavior.Replace})
  else
    return fallback()
  end
end
local function control_n()
  return cmp.mapping({i = i_control_n})
end
local function i_control_p(fallback)
  if cmp.visible() then
    return cmp.select_prev_item({behavior = cmp.SelectBehavior.Replace})
  else
    return fallback()
  end
end
local function control_p()
  return cmp.mapping({i = i_control_p})
end
local function mapping()
  return cmp.mapping.preset.insert({["<C-n>"] = control_n(), ["<C-p>"] = control_p(), ["<C-b>"] = scroll_back(), ["<C-f>"] = scroll_forward(), ["<C-Space>"] = complete(), ["<C-e>"] = close(), ["<CR>"] = confirm()})
end
local function format()
  return lspkind.cmp_format({mode = "symbol_text", menu = {buffer = "[Buffer]", nvim_lsp = "[LSP]", luasnip = "[Snippet]", path = "[Path]"}})
end
local function opts(...)
  return {snippet = {expand = expand}, mapping = mapping(), sources = sources(), formatting = {format = format()}, view = {entries = "custom"}, preselect = cmp.PreselectMode.Item, experimental = {ghost_text = true}}
end
local function config(_, opts0)
  cmp.setup(opts0)
  cmp.setup.cmdline("/", {mapping = cmp.mapping.preset.cmdline(), sources = {{name = "buffer"}}})
  cmp.setup.cmdline(":", {mapping = cmp.mapping.preset.cmdline(), sources = cmp.config.sources({{name = "path"}}, {{name = "cmdline", option = {ignore_cmds = {"Man", "!"}}}})})
  return cmp.setup.filetype("http", {sources = cmp.config.sources({{name = "kulala-cmp-graphql"}}, {{name = "buffer"}})})
end
local plugins_1_auto = require("rz.lib.plugins")
return plugins_1_auto.declare("hrsh7th/nvim-cmp", {dependencies = {"hrsh7th/cmp-buffer", "hrsh7th/cmp-cmdline", "hrsh7th/cmp-nvim-lsp", "hrsh7th/cmp-path", "onsails/lspkind-nvim", "saadparwaiz1/cmp_luasnip", "PaterJason/cmp-conjure", "mistweaverco/kulala-cmp-graphql.nvim"}, event = {"CmdlineEnter", "InsertEnter"}, opts = opts, config = config})
