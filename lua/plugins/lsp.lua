-- [nfnl] Compiled from lua/plugins/lsp.fnl by https://github.com/Olical/nfnl, do not edit.
local lsp = require("rz.lib.lsp")
local function lua_ls_handler()
  return lsp.configure("lua_ls", {settings = {Lua = {runtime = {version = "LuaJIT"}, format = {enable = false}, diagnostics = {globals = {"vim", "hs"}}, workspace = {library = vim.api.nvim_get_runtime_file("", true)}, telemetry = {enable = false}}}})
end
local function fennel_ls_handler()
  return lsp.configure("fennel_language_server", {settings = {fennel = {workspace = {library = vim.api.nvim_list_runtime_paths()}, diagnostics = {globals = {"vim"}}}}})
end
local function beancount_ls_handler()
  return lsp.configure("beancount", {init_options = {journal_file = vim.fn.expand("<afile>:p")}})
end
local function rust_analyzer_ls_handler()
  return lsp.configure("rust_analyzer", {settings = {["rust-analyzer"] = {checkOnSave = {command = "clippy", extraArgs = {"--", "-W", "clippy::pedantic", "-W", "clippy::nursery", "-W", "clippy::unwrap_used", "-W", "clippy::expect_used"}}}}})
end
local function handlers()
  local tmp_9_auto = {}
  tmp_9_auto["beancount"] = beancount_ls_handler
  tmp_9_auto["fennel_language_server"] = fennel_ls_handler
  tmp_9_auto["lua_ls"] = lua_ls_handler
  tmp_9_auto["rust_analyzer"] = rust_analyzer_ls_handler
  local function _1_(_241)
    return lsp.configure(_241)
  end
  tmp_9_auto[1] = _1_
  return tmp_9_auto
end
do
  local _2_ = vim.api.nvim_create_augroup("rz_lsp_config", {clear = true})
  do local _ = {vim.api.nvim_create_autocmd({"LspAttach"}, {callback = lsp["on-attach"], group = _2_})} end
end
local _3_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local _4_
  do
    local plugins_1_auto0 = require("rz.lib.plugins")
    _4_ = plugins_1_auto0.declare("neovim/nvim-lspconfig", {event = {"BufReadPost", "BufNewFile"}})
  end
  local function _5_(...)
    local plugins_1_auto0 = require("rz.lib.plugins")
    return plugins_1_auto0.declare("williamboman/mason.nvim", {cmd = {"Mason"}, opts = {}})
  end
  _3_ = plugins_1_auto.declare("williamboman/mason-lspconfig.nvim", {dependencies = {_4_, _5_(...)}, event = {"BufReadPre", "BufNewFile"}, opts = {handlers = handlers()}})
end
local _6_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _6_ = plugins_1_auto.declare("glepnir/lspsaga.nvim", {branch = "main", cmd = "Lspsaga", event = "LspAttach", opts = {ui = {code_action = "A"}, symbol_in_winbar = {enable = false}}})
end
local _7_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local function _8_()
    return require("lsp_signature").setup()
  end
  _7_ = plugins_1_auto.declare("ray-x/lsp_signature.nvim", {event = "VeryLazy", opts = {}, config = _8_})
end
local _9_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local function _10_(...)
    local plugins_2_auto = require("rz.lib.plugins")
    local function _11_()
      local ssr = require("ssr")
      return ssr.open()
    end
    return plugins_2_auto.key({"n", "x"}, "<localleader>SR", _11_, {desc = "structural search and replace"})
  end
  _9_ = plugins_1_auto.declare("cshuaimin/ssr.nvim", {keys = {_10_(...)}})
end
local _12_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local function _13_()
    return (0 ~= vim.fn.filereadable("pubspec.yaml"))
  end
  local function _14_()
    return {dev_log = {open_cmd = "0tabnew"}, lsp = {on_attach = lsp["on-attach"]}}
  end
  _12_ = plugins_1_auto.declare("akinsho/flutter-tools.nvim", {cond = _13_, dependencies = {"nvim-lua/plenary.nvim"}, opts = _14_, lazy = false})
end
local function _15_(...)
  local plugins_1_auto = require("rz.lib.plugins")
  return plugins_1_auto.declare("j-hui/fidget.nvim", {config = true, event = "LspAttach"})
end
return {_3_, _6_, _7_, _9_, _12_, _15_(...)}
