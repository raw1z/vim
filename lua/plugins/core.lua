-- [nfnl] Compiled from lua/plugins/core.fnl by https://github.com/Olical/nfnl, do not edit.
local _1_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _1_ = plugins_1_auto.declare("Olical/nfnl", {priority = 10000, lazy = false})
end
local _2_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _2_ = plugins_1_auto.declare("nvim-lua/plenary.nvim")
end
local function _3_(...)
  local plugins_1_auto = require("rz.lib.plugins")
  local function _4_(...)
    local plugins_2_auto = require("rz.lib.plugins")
    local function _5_()
      return Snacks.notifier.show_history()
    end
    return plugins_2_auto.key({"n"}, "<space>N", _5_, {desc = "Notification History"})
  end
  return plugins_1_auto.declare("folke/snacks.nvim", {keys = {_4_(...)}, opts = {input = {enabled = true}, notifier = {enabled = true}, statuscolumn = {enabled = true}}, priority = 1000, lazy = false})
end
return {_1_, _2_, _3_(...)}
