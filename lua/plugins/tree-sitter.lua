-- [nfnl] Compiled from lua/plugins/tree-sitter.fnl by https://github.com/Olical/nfnl, do not edit.
local function install_parsers()
  local parsers = require("nvim-treesitter.parsers")
  local parser_config = parsers.get_parser_configs()
  parser_config["lfe"] = {install_info = {url = "https://github.com/raw1z/tree-sitter-lfe.git", files = {"src/parser.c"}}}
  parser_config["shen"] = {install_info = {url = "https://github.com/raw1z/tree-sitter-shen.git", branch = "main", files = {"src/parser.c"}}}
  parser_config["grpcnvim"] = {install_info = {url = "https://github.com/antosha417/tree-sitter-grpc-nvim", branch = "master", files = {"src/parser.c", "src/scanner.cc"}, filetype = "grpcnvim"}}
  return nil
end
local function config(_, opts)
  install_parsers()
  local configs = require("nvim-treesitter.configs")
  return configs.setup(opts)
end
local function installed_languages()
  return {"bash", "c", "clojure", "css", "dart", "diff", "eex", "elixir", "elm", "fish", "fennel", "git_config", "git_rebase", "gitattributes", "gitcommit", "gitignore", "go", "graphql", "heex", "html", "http", "janet_simple", "javascript", "json", "lua", "make", "markdown", "nix", "nu", "proto", "python", "query", "racket", "ruby", "rust", "scheme", "sql", "starlark", "tsx", "typescript", "vim", "vue", "vimdoc", "xml", "yaml"}
end
local _1_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _1_ = plugins_1_auto.declare("nvim-treesitter/playground", {cmd = "TSPlaygroundToggle"})
end
local _2_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _2_ = plugins_1_auto.declare("nvim-treesitter/nvim-treesitter", {build = ":TSUpdate", cmd = {"TSUpdateSync"}, config = config, dependencies = {"JoosepAlviste/nvim-ts-context-commentstring", "nvim-treesitter/nvim-treesitter-textobjects"}, event = {"BufReadPost", "BufNewFile"}, opts = {ensure_installed = installed_languages(), highlight = {enable = true}, indent = {enable = true}, textobjects = {enable = true, select = {enable = true, lookahead = true, keymaps = {af = "@function.outer", ["if"] = "@function.inner", ac = "@class.outer", ic = "@context.inner", ad = "@context.outer"}}, move = {enable = true, set_jumps = true, goto_next_start = {["]f"] = {query = "@function.outer", desc = "Next function start"}, ["]c"] = {query = "@class.outer", desc = "Next class start"}, ["]d"] = {query = "@context.outer", desc = "Next context start"}}, goto_next_end = {["]F"] = {query = "@function.outer", desc = "Next function start"}, ["]C"] = {query = "@class.outer", desc = "Next class start"}, ["]D"] = {query = "@context.outer", desc = "Next context start"}}, goto_previous_start = {["[f"] = {query = "@function.outer", desc = "Previous function start"}, ["[c"] = {query = "@class.outer", desc = "Previous class start"}, ["[d"] = {query = "@context.outer", desc = "Previous context start"}}, goto_previous_end = {["[F"] = {query = "@function.outer", desc = "Previous function start"}, ["[C"] = {query = "@class.outer", desc = "Previous class start"}, ["[D"] = {query = "@context.outer", desc = "Previous context start"}}}, ls_interop = {enable = false}}}})
end
local function _3_(...)
  local plugins_1_auto = require("rz.lib.plugins")
  return plugins_1_auto.declare("JoosepAlviste/nvim-ts-context-commentstring", {config = true})
end
return {_1_, _2_, _3_(...)}
