-- [nfnl] Compiled from lua/plugins/test.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local text = autoload("rz.lib.text")
local function run_last()
  local neotest = require("neotest")
  vim.cmd("silent! wall")
  return neotest.run.run_last()
end
local function notify_event(event)
  local function _2_()
    return vim.api.nvim_exec_autocmds("User", {pattern = event})
  end
  return vim.schedule(_2_)
end
local function update_statusline(status)
  vim.g.rz_test_status = status
  return nil
end
local function update_accumulator(current, value)
  local acc = (current or {passed = 0, failed = 0, skipped = 0})
  if ("passed" == value.status) then
    acc["passed"] = (1 + acc.passed)
  elseif ("failed" == value.status) then
    acc["failed"] = (1 + acc.failed)
  elseif ("skipped" == value.status) then
    acc["skipped"] = (1 + acc.skipped)
  else
  end
  return acc
end
local function format_results(results)
  local pattern = "\\v(.+)\\:\\:(.+)"
  local acc = {}
  for key, value in pairs(results) do
    if text["matches?"](key, pattern) then
      local filename = text.replace(key, "\\v(.+)\\:\\:(.+)", "\\1")
      local updated = update_accumulator(acc[filename], value)
      acc[filename] = updated
      acc = acc
    else
      acc = acc
    end
  end
  return acc
end
local function update_winbar(results)
  if (nil == results) then
    vim.g.rz_test_results = nil
    return nil
  else
    vim.g.rz_test_results = format_results(results)
    return nil
  end
end
local function on_starting()
  update_statusline("initializing\226\128\166")
  return notify_event("NeotestStarting")
end
local function on_run()
  update_statusline("running\226\128\166")
  return notify_event("NeotestRunning")
end
local function on_results(_, results)
  update_winbar(results)
  update_statusline("done")
  notify_event("NeotestDone")
  local function _6_()
    update_statusline(nil)
    return notify_event("NeotestCleaned")
  end
  return vim.defer_fn(_6_, 1000)
end
local function statusline_consumer(client)
  client.listeners.starting = on_starting
  client.listeners.run = on_run
  client.listeners.results = on_results
  return nil
end
local plugins_1_auto = require("rz.lib.plugins")
local function _7_()
  local function _8_()
    local neotest_dart = require("neotest-dart")
    return neotest_dart({command = "flutter", use_lsp = true})
  end
  return {adapters = {require("neotest-rspec"), require("neotest-go"), _8_()}, consumers = {statusline = statusline_consumer}, icons = {passed = "\226\151\143", failed = "\226\151\143", running = "\226\151\143", skipped = "\226\151\143"}}
end
local _9_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _10_()
    local neotest_2_auto = require("neotest")
    vim.cmd("silent! wall")
    return neotest_2_auto.run.run()
  end
  _9_ = plugins_2_auto.key({"n"}, "<leader>tn", _10_, {desc = "run nearest test"})
end
local _11_
do
  local plugins_2_auto = require("rz.lib.plugins")
  _11_ = plugins_2_auto.key({"n"}, "<leader>tt", run_last, {desc = "re-run last test"})
end
local _12_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _13_()
    local neotest_2_auto = require("neotest")
    vim.cmd("silent! wall")
    return neotest_2_auto.run.run(vim.fn.expand("%"))
  end
  _12_ = plugins_2_auto.key({"n"}, "<leader>tf", _13_, {desc = "run all test file"})
end
local _14_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _15_()
    local neotest_2_auto = require("neotest")
    vim.cmd("silent! wall")
    return neotest_2_auto.run.run(vim.fn.getcwd())
  end
  _14_ = plugins_2_auto.key({"n"}, "<leader>ta", _15_, {desc = "run all test suite"})
end
local _16_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _17_()
    local neotest_2_auto = require("neotest")
    vim.cmd("silent! wall")
    return neotest_2_auto.run.run({strategy = "dap"})
  end
  _16_ = plugins_2_auto.key({"n"}, "<leader>td", _17_, {desc = "run test with dap"})
end
local _18_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _19_()
    return vim.cmd("Neotest output")
  end
  _18_ = plugins_2_auto.key({"n"}, "<leader>to", _19_, {desc = "show test ouptut"})
end
local _20_
do
  local plugins_2_auto = require("rz.lib.plugins")
  local function _21_()
    return vim.cmd("Neotest output-panel")
  end
  _20_ = plugins_2_auto.key({"n"}, "<leader>tp", _21_, {desc = "show test output panel"})
end
local function _22_(...)
  local plugins_2_auto = require("rz.lib.plugins")
  local function _23_()
    return vim.cmd("Neotest summary")
  end
  return plugins_2_auto.key({"n"}, "<leader>ts", _23_, {desc = "open test summary pane"})
end
return plugins_1_auto.declare("nvim-neotest/neotest", {dependencies = {"nvim-neotest/nvim-nio", "nvim-lua/plenary.nvim", "nvim-treesitter/nvim-treesitter", "antoinemadec/FixCursorHold.nvim", "olimorris/neotest-rspec", "nvim-neotest/neotest-go", "sidlatau/neotest-dart"}, opts = _7_, keys = {_9_, _11_, _12_, _14_, _16_, _18_, _20_, _22_(...)}})
