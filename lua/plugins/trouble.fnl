(require-macros :macros.plugins)

(macro defkey [key action desc]
  `(plug-key n
             ,(.. :<space>t (tostring key))
             #(vim.cmd ,(.. "Trouble " (tostring action) " focus=true"))
             {:desc ,desc}))

(fn close-trouble []
  (let [trouble (require :trouble)]
    (trouble.close)))

(plug folke/trouble.nvim
      {:opts {:auto_close true}
       :cmd [:Trouble]
       :dependencies [:nvim-tree/nvim-web-devicons]
       :keys [(defkey D "diagnostics toggle" "show entire workspace's diagnostics")
              (defkey d "diagnostics toggle filter.buf=0" "show current buffer's diagnostics")
              (defkey i "lsp_implementations toggle" "show implementations from lsp")
              (defkey l "loclist toggle" "show location list with trouble")
              (defkey o "symbols toggle pinned=true win.relative=win win.position=right" "show document symbols")
              (defkey q "qflist toggle" "show quickfix with trouble")
              (defkey r "lsp_references toggle" "show references from lsp")
              (defkey s "lsp toggle" "LSP definitions, references, implementations, type definitions, and declarations")
              (plug-key n <space>tt close-trouble {:desc "close trouble's buffer"})]})
