-- [nfnl] Compiled from lua/plugins/languages.fnl by https://github.com/Olical/nfnl, do not edit.
local _1_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _1_ = plugins_1_auto.declare("bakpakin/fennel.vim", {event = {"BufReadPost", "BufNewFile"}})
end
local _2_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _2_ = plugins_1_auto.declare("lifepillar/pgsql.vim", {ft = "pgsql"})
end
local _3_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _3_ = plugins_1_auto.declare("ron-rs/ron.vim", {ft = "ron"})
end
local _4_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _4_ = plugins_1_auto.declare("wstrinz/shen.vim", {ft = "shen"})
end
local _5_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _5_ = plugins_1_auto.declare("vim-jp/syntax-vim-ex", {ft = "vim"})
end
local _6_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _6_ = plugins_1_auto.declare("fladson/vim-kitty", {ft = "kitty"})
end
local _7_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _7_ = plugins_1_auto.declare("wstrinz/shen.vim", {ft = "shen"})
end
local _8_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _8_ = plugins_1_auto.declare("janet-lang/janet.vim", {ft = "janet"})
end
local _9_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local function _10_(plugin)
    vim.opt.rtp:append((plugin.dir .. "/editor-support/vim"))
    local loader = require("lazy.core.loader")
    return loader.ftdetect((plugin.dir .. "/editor-support/vim"))
  end
  local function _11_(plugin)
    local loader = require("lazy.core.loader")
    return loader.ftdetect((plugin.dir .. "/editor-support/vim"))
  end
  _9_ = plugins_1_auto.declare("unisonweb/unison", {branch = "trunk", config = _10_, ft = "unison", init = _11_, enabled = false})
end
local _12_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _12_ = plugins_1_auto.declare("imsnif/kdl.vim", {ft = "kdl"})
end
local _13_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _13_ = plugins_1_auto.declare("wsdjeg/vim-pony", {ft = "pony"})
end
local _14_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _14_ = plugins_1_auto.declare("hashivim/vim-terraform", {ft = "terraform"})
end
local _15_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _15_ = plugins_1_auto.declare("jparise/vim-graphql", {ft = "graphql"})
end
local _16_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _16_ = plugins_1_auto.declare("mxw/vim-prolog", {ft = "prolog"})
end
local _17_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _17_ = plugins_1_auto.declare("wlangstroth/vim-racket", {ft = "racket"})
end
local _18_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _18_ = plugins_1_auto.declare("slim-template/vim-slim", {ft = "slim"})
end
local function _19_(...)
  local plugins_1_auto = require("rz.lib.plugins")
  return plugins_1_auto.declare("darfink/vim-plist", {ft = "plist"})
end
return {_1_, _2_, _3_, _4_, _5_, _6_, _7_, _8_, _9_, _12_, _13_, _14_, _15_, _16_, _17_, _18_, _19_(...)}
