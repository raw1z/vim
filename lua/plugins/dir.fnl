(require-macros :macros.core)
(require-macros :macros.plugins)
(require-macros :macros.vim)

(macro defkey [key direction desc]
  `(plug-key n ,(.. :- (tostring key))
             #(let [dir# (require :rz.lib.dir)]
                (dir#.open-current-buffer-file-dirname ,(tostring direction)))
             {:desc ,desc}))

(macro defkey-root [key direction desc]
  `(plug-key n ,(.. :<leader>- (tostring key))
             #(let [dir# (require :rz.lib.dir)]
                (dir#.open-project-root-directory ,(tostring direction)))
             {:desc ,desc}))

(plug stevearc/oil.nvim
      {:dependencies [:nvim-tree/nvim-web-devicons]
       :cmd :Oil
       :event :CmdlineEnter
       :keys [(defkey - no "browse current file's folder in same buffer")
              (defkey s horizontal "browse current file's folder in horizontal split")
              (defkey v vertical "browse current file's folder in same vertical split")
              (defkey t tab "browse current file's folder in new tab")
              (defkey f float "browse current file's folder in a floating window")
              (defkey-root - no "browse current project's root folder in same buffer")
              (defkey-root s horizontal "browse current project's root folder in horizontal split")
              (defkey-root v vertical "browse current project's root folder in same vertical split")
              (defkey-root t tab "browse current project's root folder in new tab")
              (defkey-root f float "browse current project's root folder in a floating window")]
       :opts {:default_file_explorer true
              :keymaps {:<CR>  :actions.select
                        :<C-p> :actions.preview
                        :q     :actions.close
                        :<C-r> :actions.refresh
                        :<S-U> :actions.parent
                        :<C-o> :actions.open_external
                        :<C-y> :actions.copy_entry_path}
              :prompt_save_on_select_new_entry false
              :skip_confirm_for_simple_edit true
              :use_default_keymaps false
              :watch_for_changes true
              :view_options {:show_hidden true}}})
