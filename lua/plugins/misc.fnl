(require-macros :macros.core)
(require-macros :macros.plugins)
(local {: autoload} (require :nfnl.module))
(local avante-providers (autoload :avante.providers))

[(plug tpope/vim-jdaddy {:ft :json})

 (plug tpope/vim-projectionist {:cond #(not= 0 (vim.fn.filereadable :.projections.json))
                                :lazy false
                                :keys [(plug-key n
                                                 <localleader>a
                                                 (%f (%> e)
                                                     (%> A))
                                                 {:desc "Switch to alternate file"})]})
 (plug tpope/vim-rails)

 (plug tpope/vim-repeat
       {:lazy false})

 (plug tpope/vim-rsi
       {:lazy false})

 (plug tpope/vim-abolish
       {:event :CmdlineEnter
        :keys [(plug-key n crs nil {:desc "coerce to snake case"})
               (plug-key n crm nil {:desc "coerce to mixed case"})
               (plug-key n crc nil {:desc "coerce to camel case"})
               (plug-key n cru nil {:desc "coerce to upper case"})
               (plug-key n cr- nil {:desc "coerce to dash case"})
               (plug-key n cr. nil {:desc "coerce to dot case"})]})

 (plug tpope/vim-bundler)

 (plug tpope/vim-speeddating)

 (plug tpope/vim-rake)

 (plug tpope/vim-eunuch
       {:cmd (%w Cfind
                 Chmod
                 Clocate
                 Copy
                 Delete
                 Duplicate
                 Lfind
                 Llocate
                 Move
                 Rename)})

 (plug dbakker/vim-projectroot
       {:config #(set vim.g.rootmarkers (%w .projectroot
                                            Gemfile
                                            .git
                                            .hg
                                            .svn
                                            .bzr
                                            _darcs
                                            build.xml))
        :lazy false})

 (plug AndrewRadev/linediff.vim
       {:cmd [:Linediff]})

 (plug mbbill/undotree
       {:cmd [:UndotreeToggle]
        :init (%f (set vim.g.undotree_WindowLayout 2)
                  (set vim.g.undotree_SetFocusWhenToggle 1)
                  (set vim.g.undotree_DiffpanelHeight 0))
        :keys [(plug-key n <leader>u :<cmd>UndotreeToggle<cr> {:desc "toggle undo-tree panel"})]})

 (plug gbprod/yanky.nvim
       {:opts {}
        :lazy false
        :keys [(plug-key n|x p "<Plug>(YankyPutAfter)")
               (plug-key n|x P "<Plug>(YankyPutBefore)")
               (plug-key n|x gp "<Plug>(YankyGPutAfter)")
               (plug-key n|x gP "<Plug>(YankyGPutBefore)")
               (plug-key n <c-n> "<Plug>(YankyCycleForward)")
               (plug-key n <c-p> "<Plug>(YankyCycleBackward)")]})

 (plug MunifTanjim/nui.nvim
       {:lazy false})

 (plug yamatsum/nvim-nonicons
       {:dependencies [(plug nvim-tree/nvim-web-devicons
                             {:opts {:default true
                                     :override {:janet {:icon :
                                                        :color :#095f89
                                                        :cterm_color :25
                                                        :name :Janet}}}})]})

 (plug tyru/open-browser.vim
       {:config #(set vim.g.openbrowser_default_search :duckduckgo)
        :keys [(plug-key n gx
                         "<Plug>(openbrowser-smart-search)"
                         {:remap true
                          :silent true
                          :desc "search for the text under the cursor in the browser"})
               (plug-key v gx
                         "<Plug>(openbrowser-smart-search)"
                         {:remap true
                          :silent true
                          :desc "search for the selection in the browser"})]})

 (plug junegunn/vim-easy-align
       {:keys [(plug-key x ga
                         "<Plug>(EasyAlign)"
                         {:remap true
                          :desc "easy align"})
               (plug-key n ga
                         "<Plug>(EasyAlign)"
                         {:remap true
                          :desc "easy align"})]})

 (plug chentoast/marks.nvim
       {:event (%w BufReadPre BufNewFile)
        :opts {:force_write_shada true}})

 (plug jghauser/mkdir.nvim
       {:event :BufWritePre})

 (plug lambdalisue/suda.vim
       {:cmd (%w SudaRead SudaWrite)})

 (plug kylechui/nvim-surround
       {:event :VeryLazy
        :opts {}})

 (plug rgroli/other.nvim
       {:cmd (%w Other OtherTabNew OtherSplit OtherVSplit)
        :config #(let [other (require :other-nvim)]
                   (other.setup {:mappings (%w golang rails rust)
                                 :style {:seperator "▏"}}))})

 (plug michaeljsmith/vim-indent-object
       {:lazy false})

 (plug mattn/emmet-vim
       {:init (%f (set vim.g.user_emmet_leader_key "ç")
                  (set vim.g.user_emmet_mode "i"))
        :keys [(plug-key i "<leader>," nil {:desc "expand with emmet"})]})

 (plug dietsche/vim-lastplace
       {:lazy false})

 (plug numToStr/Comment.nvim
       {:config true
        :event (%w BufReadPre BufNewFile)})

 (plug NvChad/nvim-colorizer.lua
       {:event (%w BufReadPost
                   BufNewFile)
        :opts {:filetypes (%w *
                              !DiffviewFileHistory
                              !beancount
                              !lazy
                              !checkhealth
                              !TelescopeResults)}})

 (plug samoshkin/vim-mergetool
       {:cmd [:MergetoolToggle]})

 (plug simrat39/symbols-outline.nvim
       {:cmd :SymbolsOutline
        :config true})

 (plug folke/which-key.nvim
       {:event :VeryLazy
        :init (%f (set vim.o.timeout true)
                  (set vim.o.timeoutlen 500))
        :opts {:icons {:mappings false}
               :win {:border :none}}})

 (plug RRethy/vim-illuminate
       {:event (%w BufReadPost
                   BufNewFile)
        :config #(let [illuminate (require :illuminate)]
                   (illuminate.configure {:filetypes_denylist [:lfe]
                                          :filetypes_denylist (%w dirbuf fugitive)
                                          :providers (%w lsp treesitter regex)}))})

 (plug wsdjeg/vim-fetch
       {:lazy false})

 (plug hudclark/grpc-nvim
       {:cmd :Grpc
        :dependencies [:nvim-lua/plenary.nvim]})

 (plug Olical/conjure
       {:ft (%w janet)
        :init #(set vim.g.conjure#filetypes (%w clojure
                                                janet
                                                hy
                                                julia
                                                racket
                                                scheme
                                                lisp))})

 (plug Wansmer/treesj
       {:cmd (%w TSJJoin
                 TSJSplit
                 TSJToggle)
        :keys [(plug-key n <leader>jj #(%> TSJJoin) {:desc "join node under cursor"})
               (plug-key n <leader>js #(%> TSJSplit) {:desc "split node under cursor"})
               (plug-key n <leader>jt #(%> TSJToggle) {:desc "toggle node under cursor"})]
        :opts {:use_default_keymaps false}})

 (plug mcauley-penney/tidy.nvim
       {:config true
        :event (%w BufReadPost
                   BufNewFile)})

 (plug ThePrimeagen/refactoring.nvim
       {:cmd :Refactor
        :dependencies (%w nvim-lua/plenary.nvim
                          nvim-treesitter/nvim-treesitter)})

 (plug ariel-frischer/bmessages.nvim
       {:event :CmdlineEnter
        :opts {}})

 (plug kkharji/sqlite.lua
       {:lazy false})

 (plug mistweaverco/kulala.nvim {:opts {}})

 (plug bullets-vim/bullets.vim {:init (%f (set vim.g.bullets_delete_last_bullet_if_empty true)
                                          (set vim.g.bullets_set_mappings false))
                                :ft (%w gitcommit
                                        markdown
                                        scratch
                                        text)})

 (plug ggandor/leap.nvim {:keys [(plug-key n|x|o <leader>f "<Plug>(leap-forward)" {:desc "leap forward"})
                                 (plug-key n|x|o <leader>F "<Plug>(leap-backward)" {:desc "leap backward"})
                                 (plug-key n|x|o <leader><leader>f "<Plug>(leap-from-window)" {:desc "leap in other windows"})]
                          :lazy false})

 (plug leath-dub/snipe.nvim {:keys [(plug-key n <space>l #(let [snipe (require :snipe)] (snipe.open_buffer_menu)) {:desc "open snipe menu"})]
                             :opts {:hints {:dictionary "asfghl;wertyuiop"}
                                    :navigate {:cancel_snipe :q}
                                    :sort :default
                                    :ui {:position :center}}})]
