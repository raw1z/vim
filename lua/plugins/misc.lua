-- [nfnl] Compiled from lua/plugins/misc.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local avante_providers = autoload("avante.providers")
local _2_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _2_ = plugins_1_auto.declare("tpope/vim-jdaddy", {ft = "json"})
end
local _3_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local function _4_()
    return (0 ~= vim.fn.filereadable(".projections.json"))
  end
  local function _5_(...)
    local plugins_2_auto = require("rz.lib.plugins")
    local function _6_()
      vim.cmd("e")
      return vim.cmd("A")
    end
    return plugins_2_auto.key({"n"}, "<localleader>a", _6_, {desc = "Switch to alternate file"})
  end
  _3_ = plugins_1_auto.declare("tpope/vim-projectionist", {cond = _4_, keys = {_5_(...)}, lazy = false})
end
local _7_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _7_ = plugins_1_auto.declare("tpope/vim-rails")
end
local _8_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _8_ = plugins_1_auto.declare("tpope/vim-repeat", {lazy = false})
end
local _9_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _9_ = plugins_1_auto.declare("tpope/vim-rsi", {lazy = false})
end
local _10_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local _11_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _11_ = plugins_2_auto.key({"n"}, "crs", nil, {desc = "coerce to snake case"})
  end
  local _12_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _12_ = plugins_2_auto.key({"n"}, "crm", nil, {desc = "coerce to mixed case"})
  end
  local _13_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _13_ = plugins_2_auto.key({"n"}, "crc", nil, {desc = "coerce to camel case"})
  end
  local _14_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _14_ = plugins_2_auto.key({"n"}, "cru", nil, {desc = "coerce to upper case"})
  end
  local _15_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _15_ = plugins_2_auto.key({"n"}, "cr-", nil, {desc = "coerce to dash case"})
  end
  local function _16_(...)
    local plugins_2_auto = require("rz.lib.plugins")
    return plugins_2_auto.key({"n"}, "cr.", nil, {desc = "coerce to dot case"})
  end
  _10_ = plugins_1_auto.declare("tpope/vim-abolish", {event = "CmdlineEnter", keys = {_11_, _12_, _13_, _14_, _15_, _16_(...)}})
end
local _17_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _17_ = plugins_1_auto.declare("tpope/vim-bundler")
end
local _18_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _18_ = plugins_1_auto.declare("tpope/vim-speeddating")
end
local _19_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _19_ = plugins_1_auto.declare("tpope/vim-rake")
end
local _20_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _20_ = plugins_1_auto.declare("tpope/vim-eunuch", {cmd = {"Cfind", "Chmod", "Clocate", "Copy", "Delete", "Duplicate", "Lfind", "Llocate", "Move", "Rename"}})
end
local _21_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local function _22_()
    vim.g.rootmarkers = {".projectroot", "Gemfile", ".git", ".hg", ".svn", ".bzr", "_darcs", "build.xml"}
    return nil
  end
  _21_ = plugins_1_auto.declare("dbakker/vim-projectroot", {config = _22_, lazy = false})
end
local _23_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _23_ = plugins_1_auto.declare("AndrewRadev/linediff.vim", {cmd = {"Linediff"}})
end
local _24_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local function _25_()
    vim.g.undotree_WindowLayout = 2
    vim.g.undotree_SetFocusWhenToggle = 1
    vim.g.undotree_DiffpanelHeight = 0
    return nil
  end
  local function _26_(...)
    local plugins_2_auto = require("rz.lib.plugins")
    return plugins_2_auto.key({"n"}, "<leader>u", "<cmd>UndotreeToggle<cr>", {desc = "toggle undo-tree panel"})
  end
  _24_ = plugins_1_auto.declare("mbbill/undotree", {cmd = {"UndotreeToggle"}, init = _25_, keys = {_26_(...)}})
end
local _27_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local _28_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _28_ = plugins_2_auto.key({"n", "x"}, "p", "<Plug>(YankyPutAfter)")
  end
  local _29_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _29_ = plugins_2_auto.key({"n", "x"}, "P", "<Plug>(YankyPutBefore)")
  end
  local _30_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _30_ = plugins_2_auto.key({"n", "x"}, "gp", "<Plug>(YankyGPutAfter)")
  end
  local _31_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _31_ = plugins_2_auto.key({"n", "x"}, "gP", "<Plug>(YankyGPutBefore)")
  end
  local _32_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _32_ = plugins_2_auto.key({"n"}, "<c-n>", "<Plug>(YankyCycleForward)")
  end
  local function _33_(...)
    local plugins_2_auto = require("rz.lib.plugins")
    return plugins_2_auto.key({"n"}, "<c-p>", "<Plug>(YankyCycleBackward)")
  end
  _27_ = plugins_1_auto.declare("gbprod/yanky.nvim", {opts = {}, keys = {_28_, _29_, _30_, _31_, _32_, _33_(...)}, lazy = false})
end
local _34_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _34_ = plugins_1_auto.declare("MunifTanjim/nui.nvim", {lazy = false})
end
local _35_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local function _36_(...)
    local plugins_1_auto0 = require("rz.lib.plugins")
    return plugins_1_auto0.declare("nvim-tree/nvim-web-devicons", {opts = {default = true, override = {janet = {icon = "\239\144\141", color = "#095f89", cterm_color = "25", name = "Janet"}}}})
  end
  _35_ = plugins_1_auto.declare("yamatsum/nvim-nonicons", {dependencies = {_36_(...)}})
end
local _37_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local function _38_()
    vim.g.openbrowser_default_search = "duckduckgo"
    return nil
  end
  local _39_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _39_ = plugins_2_auto.key({"n"}, "gx", "<Plug>(openbrowser-smart-search)", {remap = true, silent = true, desc = "search for the text under the cursor in the browser"})
  end
  local function _40_(...)
    local plugins_2_auto = require("rz.lib.plugins")
    return plugins_2_auto.key({"v"}, "gx", "<Plug>(openbrowser-smart-search)", {remap = true, silent = true, desc = "search for the selection in the browser"})
  end
  _37_ = plugins_1_auto.declare("tyru/open-browser.vim", {config = _38_, keys = {_39_, _40_(...)}})
end
local _41_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local _42_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _42_ = plugins_2_auto.key({"x"}, "ga", "<Plug>(EasyAlign)", {remap = true, desc = "easy align"})
  end
  local function _43_(...)
    local plugins_2_auto = require("rz.lib.plugins")
    return plugins_2_auto.key({"n"}, "ga", "<Plug>(EasyAlign)", {remap = true, desc = "easy align"})
  end
  _41_ = plugins_1_auto.declare("junegunn/vim-easy-align", {keys = {_42_, _43_(...)}})
end
local _44_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _44_ = plugins_1_auto.declare("chentoast/marks.nvim", {event = {"BufReadPre", "BufNewFile"}, opts = {force_write_shada = true}})
end
local _45_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _45_ = plugins_1_auto.declare("jghauser/mkdir.nvim", {event = "BufWritePre"})
end
local _46_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _46_ = plugins_1_auto.declare("lambdalisue/suda.vim", {cmd = {"SudaRead", "SudaWrite"}})
end
local _47_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _47_ = plugins_1_auto.declare("kylechui/nvim-surround", {event = "VeryLazy", opts = {}})
end
local _48_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local function _49_()
    local other = require("other-nvim")
    return other.setup({mappings = {"golang", "rails", "rust"}, style = {seperator = "\226\150\143"}})
  end
  _48_ = plugins_1_auto.declare("rgroli/other.nvim", {cmd = {"Other", "OtherTabNew", "OtherSplit", "OtherVSplit"}, config = _49_})
end
local _50_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _50_ = plugins_1_auto.declare("michaeljsmith/vim-indent-object", {lazy = false})
end
local _51_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local function _52_()
    vim.g.user_emmet_leader_key = "\195\167"
    vim.g.user_emmet_mode = "i"
    return nil
  end
  local function _53_(...)
    local plugins_2_auto = require("rz.lib.plugins")
    return plugins_2_auto.key({"i"}, "<leader>,", nil, {desc = "expand with emmet"})
  end
  _51_ = plugins_1_auto.declare("mattn/emmet-vim", {init = _52_, keys = {_53_(...)}})
end
local _54_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _54_ = plugins_1_auto.declare("dietsche/vim-lastplace", {lazy = false})
end
local _55_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _55_ = plugins_1_auto.declare("numToStr/Comment.nvim", {config = true, event = {"BufReadPre", "BufNewFile"}})
end
local _56_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _56_ = plugins_1_auto.declare("NvChad/nvim-colorizer.lua", {event = {"BufReadPost", "BufNewFile"}, opts = {filetypes = {"*", "!DiffviewFileHistory", "!beancount", "!lazy", "!checkhealth", "!TelescopeResults"}}})
end
local _57_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _57_ = plugins_1_auto.declare("samoshkin/vim-mergetool", {cmd = {"MergetoolToggle"}})
end
local _58_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _58_ = plugins_1_auto.declare("simrat39/symbols-outline.nvim", {cmd = "SymbolsOutline", config = true})
end
local _59_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local function _60_()
    vim.o.timeout = true
    vim.o.timeoutlen = 500
    return nil
  end
  _59_ = plugins_1_auto.declare("folke/which-key.nvim", {event = "VeryLazy", init = _60_, opts = {icons = {mappings = false}, win = {border = "none"}}})
end
local _61_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local function _62_()
    local illuminate = require("illuminate")
    return illuminate.configure({filetypes_denylist = {"dirbuf", "fugitive"}, providers = {"lsp", "treesitter", "regex"}})
  end
  _61_ = plugins_1_auto.declare("RRethy/vim-illuminate", {event = {"BufReadPost", "BufNewFile"}, config = _62_})
end
local _63_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _63_ = plugins_1_auto.declare("wsdjeg/vim-fetch", {lazy = false})
end
local _64_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _64_ = plugins_1_auto.declare("hudclark/grpc-nvim", {cmd = "Grpc", dependencies = {"nvim-lua/plenary.nvim"}})
end
local _65_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local function _66_()
    vim.g["conjure#filetypes"] = {"clojure", "janet", "hy", "julia", "racket", "scheme", "lisp"}
    return nil
  end
  _65_ = plugins_1_auto.declare("Olical/conjure", {ft = {"janet"}, init = _66_})
end
local _67_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local _68_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _69_()
      return vim.cmd("TSJJoin")
    end
    _68_ = plugins_2_auto.key({"n"}, "<leader>jj", _69_, {desc = "join node under cursor"})
  end
  local _70_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    local function _71_()
      return vim.cmd("TSJSplit")
    end
    _70_ = plugins_2_auto.key({"n"}, "<leader>js", _71_, {desc = "split node under cursor"})
  end
  local function _72_(...)
    local plugins_2_auto = require("rz.lib.plugins")
    local function _73_()
      return vim.cmd("TSJToggle")
    end
    return plugins_2_auto.key({"n"}, "<leader>jt", _73_, {desc = "toggle node under cursor"})
  end
  _67_ = plugins_1_auto.declare("Wansmer/treesj", {cmd = {"TSJJoin", "TSJSplit", "TSJToggle"}, keys = {_68_, _70_, _72_(...)}, opts = {use_default_keymaps = false}})
end
local _74_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _74_ = plugins_1_auto.declare("mcauley-penney/tidy.nvim", {config = true, event = {"BufReadPost", "BufNewFile"}})
end
local _75_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _75_ = plugins_1_auto.declare("ThePrimeagen/refactoring.nvim", {cmd = "Refactor", dependencies = {"nvim-lua/plenary.nvim", "nvim-treesitter/nvim-treesitter"}})
end
local _76_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _76_ = plugins_1_auto.declare("ariel-frischer/bmessages.nvim", {event = "CmdlineEnter", opts = {}})
end
local _77_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _77_ = plugins_1_auto.declare("kkharji/sqlite.lua", {lazy = false})
end
local _78_
do
  local plugins_1_auto = require("rz.lib.plugins")
  _78_ = plugins_1_auto.declare("mistweaverco/kulala.nvim", {opts = {}})
end
local _79_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local function _80_()
    vim.g.bullets_delete_last_bullet_if_empty = true
    vim.g.bullets_set_mappings = false
    return nil
  end
  _79_ = plugins_1_auto.declare("bullets-vim/bullets.vim", {init = _80_, ft = {"gitcommit", "markdown", "scratch", "text"}})
end
local _81_
do
  local plugins_1_auto = require("rz.lib.plugins")
  local _82_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _82_ = plugins_2_auto.key({"n", "x", "o"}, "<leader>f", "<Plug>(leap-forward)", {desc = "leap forward"})
  end
  local _83_
  do
    local plugins_2_auto = require("rz.lib.plugins")
    _83_ = plugins_2_auto.key({"n", "x", "o"}, "<leader>F", "<Plug>(leap-backward)", {desc = "leap backward"})
  end
  local function _84_(...)
    local plugins_2_auto = require("rz.lib.plugins")
    return plugins_2_auto.key({"n", "x", "o"}, "<leader><leader>f", "<Plug>(leap-from-window)", {desc = "leap in other windows"})
  end
  _81_ = plugins_1_auto.declare("ggandor/leap.nvim", {keys = {_82_, _83_, _84_(...)}, lazy = false})
end
local function _85_(...)
  local plugins_1_auto = require("rz.lib.plugins")
  local function _86_(...)
    local plugins_2_auto = require("rz.lib.plugins")
    local function _87_()
      local snipe = require("snipe")
      return snipe.open_buffer_menu()
    end
    return plugins_2_auto.key({"n"}, "<space>l", _87_, {desc = "open snipe menu"})
  end
  return plugins_1_auto.declare("leath-dub/snipe.nvim", {keys = {_86_(...)}, opts = {hints = {dictionary = "asfghl;wertyuiop"}, navigate = {cancel_snipe = "q"}, sort = "default", ui = {position = "center"}}})
end
return {_2_, _3_, _7_, _8_, _9_, _10_, _17_, _18_, _19_, _20_, _21_, _23_, _24_, _27_, _34_, _35_, _37_, _41_, _44_, _45_, _46_, _47_, _48_, _50_, _51_, _54_, _55_, _56_, _57_, _58_, _59_, _61_, _63_, _64_, _65_, _67_, _74_, _75_, _76_, _77_, _78_, _79_, _81_, _85_(...)}
