(require-macros :macros.core)
(require-macros :macros.plugins)

(fn config []
  (let [lint (require :lint)]
    (set lint.linters_by_ft {:fish (%w fish)})))

[(plug mfussenegger/nvim-lint
       {: config
        :events (%w BufWritePost
                    BufReadPost
                    InsertLeave)})]
