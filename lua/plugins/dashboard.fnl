(require-macros :macros.core)
(require-macros :macros.plugins)

(local dashboard-width 80)
(local dashboard-mru-size 9)

(fn padding [size]
  {:type :padding :val size})

(fn header []
  {:type :text
   :val [" _____     _ _     "
         "|  |  |___| | |___ "
         "|     | -_| | | . |"
         "|__|__|___|_|_|___|"]
   :opts {:hl :RZDashboardHeader
          :position :center
          :shrink_margin false}})

(fn button [shortcut label command]
  (local keybind (string.format "<cmd>%s <cr>" command))
  (local formatted-label (if (< (length label) (- dashboard-width 4))
                           label
                           (vim.fn.pathshorten label)))
  {:type :button
   :val (string.format " %s" formatted-label)
   :on_press #(-> (vim.api.nvim_replace_termcodes (.. keybind :<Ignore>) true false true)
                  (vim.api.nvim_feedkeys :t false))
   :opts {:position :center
          : shortcut
          :cursor 4
          :width dashboard-width
          :align_shortcut :left
          :hl_shortcut :RZDashboardButtonShortcut
          :keymap [:n shortcut keybind {:noremap true
                                        :silent true
                                        :nowait true}]}})

(fn section-title [text]
  {:type :group
   :val [{:type :text
          :val (string.format "%s%s" text (vim.fn.repeat " " (- dashboard-width (length text))))
          :opts {:position :center
                 :hl :RZDashboardSectionTitleText}}
         {:type :text
          :val (vim.fn.repeat :═ dashboard-width)
          :opts {:position :center
                 :hl :RZDashboardSectionTitleSep}}]})

(fn section-body [body]
  {:type :group
   :val body
   :opts {:shrink_margin false}})

(fn section [title items]
  {:type :group
   :val [(section-title title)
         items
         (padding 2)]})

(fn file-button [index file]
  (button (tostring index)
          (vim.fn.fnamemodify file ":.")
          (string.format "edit %s" file)))

(fn mru-files []
  (let [mru (require :rz.lib.mru)
        mru-files (mru.get (vim.fn.getcwd) dashboard-mru-size)]
    (icollect [index file (ipairs mru-files)]
              (file-button index file))))

(fn mru []
  (section "Most Recently Used"
           (section-body mru-files)))

(fn buttons []
  (var items [(button :e "New File" :enew)])
  (let [session (require :rz.lib.session)
        (ok? exists?) (pcall session.file-exist?)]
    (when (and ok? exists?)
      (table.insert items (button :s "Load Session" :LoadSession))))
  (table.insert items (button :q "Quit" :quit))

  (section "Menu"
           (section-body items)))

(fn miscelanious-infos []
  (let [git (require :rz.lib.git)
        is-repo? (git.is-git-repository? (vim.fn.getcwd))
        branch-name (if is-repo?
                      (.. " " (git.current-branch))
                      "")]
    {:type :group
     :val [{:type :text
            :val branch-name
            :opts {:position :center
                   :hl :RZDashboardMiscelaniousInfos}}]}))

(fn date []
  {:type :group
   :val [{:type :text
          :val (os.date "%A, %B %d, %Y")
          :opts {:position :center
                 :hl :RZDashboardDate}}]})

(fn truth []
  (let [text "Oui, Dieu a tant aimé le monde qu’il a donné son Fils, son unique,
             pour que tous ceux qui placent leur confiance en lui échappent à la perdition
             et qu’ils aient la vie éternelle."
        lines (->> (vim.split text "\n")
                   (vim.tbl_map vim.trim)
                   (vim.tbl_map #{:type :text
                                  :val $1
                                  :opts {:position :center
                                         :hl :RZDashboardTruthText}}))
        reference {:type :text
                   :val "Jean 3.16"
                   :opts {:position :center
                          :hl :RZDashboardTruthReference}}]
    {:type :group
     :val (vim.fn.extendnew lines [reference])}))

(fn opts []
  {:layout [(padding 1)
            (header)
            (date)
            (padding 1)
            (truth)
            (padding 2)
            (mru)
            (buttons)
            (padding 2)
            (miscelanious-infos)]})

(plug goolord/alpha-nvim
      {:dependencies [:nvim-tree/nvim-web-devicons]
       :lazy false
       : opts})
