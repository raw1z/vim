-- [nfnl] Compiled from lua/plugins/linter.fnl by https://github.com/Olical/nfnl, do not edit.
local function config()
  local lint = require("lint")
  lint.linters_by_ft = {fish = {"fish"}}
  return nil
end
local function _1_(...)
  local plugins_1_auto = require("rz.lib.plugins")
  return plugins_1_auto.declare("mfussenegger/nvim-lint", {config = config, events = {"BufWritePost", "BufReadPost", "InsertLeave"}})
end
return {_1_(...)}
