(require-macros :macros.core)
(require-macros :macros.plugins)

(macro dap [...]
  `(let [dap# (require :dap)]
     ((. dap# ,(unpack (vim.tbl_map tostring [...]))))))

(macro dap-widgets [command]
  `(let [widgets# (require :dap.ui.widgets)]
    ((. widgets# ,(tostring command)))))

(macro dap-widgets-float [kind]
  `(let [widgets# (require :dap.ui.widgets)]
    (widgets#.centered_float (. widgets# ,(tostring kind)))))

(macro dap-go [command]
  `(let [dap-go# (require :dap-go)]
     ((. dap-go# ,(tostring command)))))

(plug mfussenegger/nvim-dap
      {:dependencies [(plug leoluz/nvim-dap-go
                            {:config true
                             :keys [(plug-key n <leader>dt #(dap-go debug_test) {:desc "debug go test"})
                                    (plug-key n <leader>dl #(dap-go debug_last_test {:desc "debug last go test"}))]})
                      (plug suketa/nvim-dap-ruby
                            {:config true})]
       :keys [(plug-key n <leader>dd #(dap continue) {:desc "start the debugger or continue if already up"})
              (plug-key n <leader>dx #(dap terminate) {:desc "stop the debugger"})
              (plug-key n|v <leader>dh #(dap-widgets hover) {:desc "show value for local under the cursor"})
              (plug-key n|v <leader>dp #(dap-widgets preview) {:desc "preview value for local under the cursor"})
              (plug-key n <leader>db #(dap toggle_breakpoint) {:desc "toggle breakpoint"})
              (plug-key n <leader>do #(dap step_over) {:desc "debugger step over"})
              (plug-key n <leader>di #(dap step_into) {:desc "debugger step into"})
              (plug-key n <leader>dr #(dap repl open) {:desc "open debugger repl"})
              (plug-key n <leader>df #(dap-widgets-float frames) {:desc "show debugger frames"})
              (plug-key n <leader>ds #(dap-widgets-float scopes) {:desc "show debugger scopes"})]})
