(require-macros :macros.vim)
(require-macros :macros.plugins)

(fn opts [...]
  (let [actions (require :telescope.actions)
        layout_actions (require :telescope.actions.layout)
        trouble (require :trouble.sources.telescope)]
    {:defaults {:entry_prefix    "    "
                :prompt_prefix   " 􀁵  "
                :selection_caret " 􀰓  "
                :show_line false
                :borderchars {:prompt  [:▀ :▐ :▄ :▌ :▛ :▜ :▟ :▙ ]
                              :results [:▀ :▐ :▄ :▌ :▛ :▜ :▟ :▙ ]
                              :preview [:▀ :▐ :▄ :▌ :▛ :▜ :▟ :▙ ]}
                :mappings {:i {:<esc> actions.close
                               :<c-t> trouble.open
                               :<c-u> false
                               :<C-n> actions.cycle_history_next
                               :<C-p> actions.cycle_history_prev
                               :<C-f> actions.preview_scrolling_down
                               :<C-b> actions.preview_scrolling_up}
                           :n {:s actions.select_horizontal
                               :v actions.select_vertical
                               :t actions.select_tab
                               :q actions.close
                               :<tab> actions.smart_send_to_qflist
                               :<c-t> trouble.open
                               :<space> actions.toggle_selection
                               :K actions.preview_scrolling_up
                               :J actions.preview_scrolling_down
                               :L actions.preview_scrolling_right
                               :H actions.preview_scrolling_left
                               :R layout_actions.cycle_layout_next
                               :<C-d> actions.results_scrolling_down
                               :<C-u> actions.results_scrolling_up}}
                :file_ignore_patterns: [:%.git]
                :vimgrep_arguments [:rg
                                    :--color=never
                                    :--no-heading
                                    :--with-filename
                                    :--line-number
                                    :--column
                                    :--smart-case
                                    :--hidden
                                    :-g
                                    :!.git]}
                :pickers {:find_files {:find_command [:rg :--files :--hidden :-g :!.git]}}}))

(fn load-extension [name]
  (let [telescope (require :telescope)]
    (telescope.load_extension name)))

(macro defkey [key call desc opts]
  `(plug-key :n
             ,(.. :<space> (tostring key))
             #(let [builtin# (require :telescope.builtin)
                    method# (. builtin# ,(tostring call))
                    themes# (require :telescope.themes)
                    opts# (themes#.get_ivy (or ,opts {}))]
                (method# opts#))
             {:desc ,desc}))

(macro defextkey [key call desc]
  `(plug-key :n
             ,(.. :<space> (tostring key))
             #(let [telescope# (require :telescope)]
                ((. telescope# :extensions ,(unpack (vim.split (tostring call) "%.")))))
             {:desc ,desc}))

(fn keys [...]
  [(defkey B current_buffer_fuzzy_find "fuzzy search in current buffer")
   (defkey F grep_string "search for the word under the cursor")
   (defkey b buffers "list buffers")
   (defkey f find_files "find a file in current folder")
   (defkey h help_tags "search vim help")
   (defkey k live_grep "live grep in current folder")
   (defkey o lsp_document_symbols "list document symbols" {:symbol_width 100})
   (defkey q quickfix "show quickfix entries")
   (plug-key n
             <leader>g/
             #(let [builtin (require :telescope.builtin)]
                (builtin.git_status))
             {:desc "search files in current git status"})])

(fn define-autocmds []
  (define-augroup rz_telescope_custom
    (define-autocmd User TelescopePreviewerLoaded
      (set-local-opt :wrap false))

    (define-autocmd WinLeave *
      (when (and (= :TelescopePrompt vim.bo.ft)
                 (= :i (vim.fn.mode)))
        (-> (vim.api.nvim_replace_termcodes :<Esc> true false true)
            (vim.api.nvim_feedkeys :i false))))))

(fn config [_ opts]
  (let [telescope (require :telescope)]
    (telescope.setup opts)
    (define-autocmds)))

(macro telescope-extension [name repo]
  `(plug ,repo {:config #(load-extension ,(tostring name))}))

(plug nvim-telescope/telescope.nvim
      {:cmd [:Telescope]
       : config
       :dependencies [:nvim-lua/plenary.nvim
                      :nvim-tree/nvim-web-devicons
                      :folke/trouble.nvim
                      (plug nvim-telescope/telescope-fzf-native.nvim
                            {:build "make"
                             :config #(load-extension :fzf)})
                      (telescope-extension changes linarcx/telescope-changes.nvim)
                      (telescope-extension glyph ghassan0/telescope-glyph.nvim)
                      (telescope-extension emoji xiyaowong/telescope-emoji.nvim)
                      (telescope-extension gitmoji olacin/telescope-gitmoji.nvim)]
       : keys
       : opts})
