(require-macros :macros.core)
(require-macros :macros.plugins)

[(plug bakpakin/fennel.vim
       {:event (%w BufReadPost BufNewFile)})

 (plug lifepillar/pgsql.vim
       {:ft :pgsql})

 (plug ron-rs/ron.vim
       {:ft :ron})

 (plug wstrinz/shen.vim
       {:ft :shen})

 (plug vim-jp/syntax-vim-ex
       {:ft :vim})

 (plug fladson/vim-kitty
       {:ft :kitty})

 (plug wstrinz/shen.vim
       {:ft :shen})

 (plug janet-lang/janet.vim
       {:ft :janet})

 (plug unisonweb/unison
       {:branch :trunk
        :enabled false
        :config (fn [plugin]
                  (vim.opt.rtp:append (.. plugin.dir :/editor-support/vim))
                  (let [loader (require :lazy.core.loader)]
                    (loader.ftdetect (.. plugin.dir :/editor-support/vim))))
        :ft :unison
        :init (fn [plugin]
                (let [loader (require :lazy.core.loader)]
                  (loader.ftdetect (.. plugin.dir :/editor-support/vim))))})

 (plug imsnif/kdl.vim
       {:ft :kdl})

 (plug wsdjeg/vim-pony
       {:ft :pony})

 (plug hashivim/vim-terraform
       {:ft :terraform})

 (plug jparise/vim-graphql
       {:ft :graphql})

 (plug mxw/vim-prolog
       {:ft :prolog})

 (plug wlangstroth/vim-racket
       {:ft :racket})

 (plug slim-template/vim-slim
       {:ft :slim})

 (plug darfink/vim-plist
       {:ft :plist})]
