(require-macros :macros.core)
(require-macros :macros.plugins)

(local opts {:default_format_opts {:timeout_ms 3000
                                   :async false
                                   :quiet false
                                   :lsp_format :fallback}
             :formatters {:erb_lint {:command :erb_lint
                                     :args [:-a :$FILENAME]
                                     :inherit false
                                     :stdin false}}
             :formatters_by_ft {:eruby [:erb_lint]
                                :fish [:fish_indent]
                                :javascript (%o :prettier {:stop_after_first true})
                                :lua (%w stylua)
                                :rust (%o :rustfmt {:lsp_format :fallback})}
             :format_on_save {:timeout_ms 5000
                              :lsp_format :fallback}})

[(plug stevearc/conform.nvim
       {:event (%w BufWritePre)
        :cmd (%w ConformInfo)
        : opts})]
