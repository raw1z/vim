(require-macros :macros.core)
(require-macros :macros.plugins)

(local {: autoload} (require :nfnl.module))
(local text (autoload :rz.lib.text))

(macro run [...]
  `(let [neotest# (require :neotest)]
     (%> silent! wall)
     (neotest#.run.run ,...)))

(fn run-last []
  (let [neotest (require :neotest)]
    (%> silent! wall)
     (neotest.run.run_last)))

(fn notify-event [event]
  (vim.schedule #(vim.api.nvim_exec_autocmds :User {:pattern event})))

(fn update-statusline [status]
  (set vim.g.rz_test_status status))

(fn update-accumulator [current value]
  (var acc (or current {:passed 0
                        :failed 0
                        :skipped 0}))
  (if
    (= :passed  (. value :status)) (tset acc :passed  (+ 1 (. acc :passed)))
    (= :failed  (. value :status)) (tset acc :failed  (+ 1 (. acc :failed)))
    (= :skipped (. value :status)) (tset acc :skipped (+ 1 (. acc :skipped))))
  acc)

(fn format-results [results]
  (local pattern "\\v(.+)\\:\\:(.+)")
  (accumulate [acc {}
               key value (pairs results)]
    (if (text.matches? key pattern)
      (let [filename (text.replace key "\\v(.+)\\:\\:(.+)" "\\1")
            updated (update-accumulator (. acc filename) value)]
        (tset acc filename updated)
        acc)
      acc)))

(fn update-winbar [results]
  (if (= nil results)
    (set vim.g.rz_test_results nil)
    (set vim.g.rz_test_results (format-results results))))

(fn on-starting []
  ; (update-winbar nil)
  (update-statusline :initializing…)
  (notify-event :NeotestStarting))

(fn on-run []
  ; (update-winbar nil)
  (update-statusline :running…)
  (notify-event :NeotestRunning))

(fn on-results [_ results]
  (update-winbar results)
  (update-statusline :done)
  (notify-event :NeotestDone)
  (vim.defer_fn (%f (update-statusline nil)
                    (notify-event :NeotestCleaned)) 1000))

(fn statusline-consumer [client]
  (set client.listeners.starting on-starting)
  (set client.listeners.run on-run)
  (set client.listeners.results on-results)
  nil)

(plug nvim-neotest/neotest
      {:dependencies [:nvim-neotest/nvim-nio
                      :nvim-lua/plenary.nvim
                      :nvim-treesitter/nvim-treesitter
                      :antoinemadec/FixCursorHold.nvim
                      :olimorris/neotest-rspec
                      :nvim-neotest/neotest-go
                      :sidlatau/neotest-dart]
       :opts #{:adapters [(require :neotest-rspec)
                          (require :neotest-go)
                          (let [neotest-dart (require :neotest-dart)]
                            (neotest-dart {:command :flutter
                                           :use_lsp true}))]
               :consumers {:statusline statusline-consumer}
               :icons {:passed  "●"
                       :failed  "●"
                       :running "●"
                       :skipped "●"}}
       :keys [(plug-key n <leader>tn #(run) {:desc "run nearest test"})
              (plug-key n <leader>tt run-last {:desc "re-run last test"})
              (plug-key n <leader>tf #(run (vim.fn.expand :%)) {:desc "run all test file"})
              (plug-key n <leader>ta #(run (vim.fn.getcwd)) {:desc "run all test suite"})
              (plug-key n <leader>td #(run {:strategy :dap}) {:desc "run test with dap"})
              (plug-key n <leader>to #(%> Neotest output) {:desc "show test ouptut"})
              (plug-key n <leader>tp #(%> Neotest output-panel) {:desc "show test output panel"})
              (plug-key n <leader>ts #(%> Neotest summary) {:desc "open test summary pane"})]})
