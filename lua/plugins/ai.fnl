(require-macros :macros.core)
(require-macros :macros.plugins)

(plug yetone/avante.nvim
      {:build :make
       :dependencies [:nvim-treesitter/nvim-treesitter
                      (plug stevearc/dressing.nvim
                            {:input {:enabled false}})
                      :nvim-lua/plenary.nvim
                      :MunifTanjim/nui.nvim
                      :nvim-tree/nvim-web-devicons

                      (plug HakonHarnes/img-clip.nvim
                            {:event :VeryLazy
                             :opts {:default {:embed_image_as_base64 false
                                              :prompt_for_file_name false
                                              :drag_and_drop {:insert_mode true}
                                              :use_absolute_path true}}})

                      (plug MeanderingProgrammer/render-markdown.nvim
                            {:dependencies (%w nvim-treesitter/nvim-treesitter
                                               nvim-tree/nvim-web-devicons)
                             :ft (%w Avante)
                             :opts {:file_types (%w Avante)
                                    :heading {:icons [" " "● " "* "]
                                              :signs []}
                                    :bullet {:icons ["▪ " "▫ "]}
                                    :checkbox {:unchecked {:icon "□ "}
                                               :checked {:icon "▣ "}}
                                    :latex {:enabled false}
                                    :link {:image " "
                                           :email "@ "
                                           :hyperlink " "
                                           :custom {:web {:icon " "}}}}})]
       :event :VeryLazy
       :lazy false
       :opts {:behaviour {:auto_suggestions true
                          :auto_set_keymaps true}
              :mappings {:suggestion {:accept :<localleader><space>
                                      :next :<localleader>j
                                      :previous :<localleader>k}}
              :suggestion {:debounce 300
                           :throttle 300}
              :windows {:width 50}}
       :version false})
