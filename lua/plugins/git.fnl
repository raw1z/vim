(require-macros :macros.core)
(require-macros :macros.plugins)
(require-macros :macros.vim)

(fn checkout-trunk []
  (let [git (require :rz.lib.git)
        trunk-branch (git.guess-trunk-branch)]
    (-> (string.format "Git checkout %s" trunk-branch)
        (vim.cmd))))

(fn show-branches []
  (local lazy (require :lazy))
  (lazy.load {:plugins [:telescope.nvim]})

  (let [telescope-builtin (require :telescope.builtin)
        telescope-themes (require :telescope.themes)]
    (-> (telescope-themes.get_dropdown {:previewer false})
        (telescope-builtin.git_branches))))

(fn show-folder-history []
  (let [git (require :rz.lib.git)
        current-branch (git.current-branch)
        trunk-branch (git.guess-trunk-branch)
        current-folder (vim.fn.getcwd)]
    (if (= current-branch trunk-branch)
      (-> (string.format "DiffviewFileHistory --no-merges %s" current-folder)
          (vim.cmd))
      (-> (string.format "DiffviewFileHistory --range=%s..HEAD --no-merges %s" trunk-branch current-folder)
          (vim.cmd)))))

(fn show-file-history []
  (%> DiffviewFileHistory --no-merges %))

(fn at-top [func]
  (func)
  (vim.schedule #(%> wincmd K)))

(fn amend []
  (%> Git commit --amend --quiet --no-edit))

(fn stage-all-and-amend []
  (%> Git add .)
  (amend))

(fn commit []
  (%> Git commit --quiet))

(fn stage-all-and-commit []
  (%> Git add .)
  (commit))

[(plug tpope/vim-fugitive
       {:cmd (%w G Git Gw Gread Gsplit Gvsplit Gvdiff)
        :keys [(plug-key n <leader>gg #(%> Git) {:desc "open git status"})
               (plug-key n <leader>gw #(%> Gw) {:desc "git stage current buffer's file"})
               (plug-key n <leader>gc #(at-top commit) {:desc "git commit"})
               (plug-key n <leader>gC #(at-top stage-all-and-commit) {:desc "stage all changes and git commit"})
               (plug-key n <leader>ge #(at-top amend) {:desc "git amend without edit"})
               (plug-key n <leader>gE #(at-top stage-all-and-amend) {:desc "stage all changes and git amend without edit"})
               (plug-key n <leader>ga #(%> Git commit --amend --quiet) {:desc "git amend"})
               (plug-key n <leader>gb #(Snacks.git.blame_line) {:desc "show git blame for current line"})
               (plug-key n <leader>gp #(%> Git! push) {:desc "git push"})
               (plug-key n <leader>gP #(%> Git! enforce) {:desc "git force push"})
               (plug-key n <leader>gu #(%> Git! pull) {:desc "git pull"})
               (plug-key n <leader>gf #(%> Git! fetch) {:desc "git fetch"})
               (plug-key n <leader>gd #(%> Gvdiff) {:desc "git diff current buffer"})
               (plug-key n <leader>gT checkout-trunk {:desc "checkout trunk"})
               (plug-key n <leader>gh show-folder-history {:desc "show git history for the current folder"})
               (plug-key n <leader>gz #(%> Lazygit) {:desc "open Lazygit"})
               (plug-key n <leader>gH show-file-history {:desc "show git history for the current file"})
               (plug-key n <leader>gB show-branches {:desc "show git branches"})
               (plug-key n <leader>gR (%f (%> Gread) (%> :update)) {:desc "git restore current file"})
               (plug-key n <leader>gS #(%> Git add .) {:desc "stage all changes"})
               (plug-key n <leader>gmm #(%> MergetoolToggle) {:desc "open git conflict resolution view"})
               (plug-key n <leader>gmtc #(%> G checkout main) {:desc "checkout trunk"})
               (plug-key n <leader>gmtr #(%> RebaseTrunk) {:desc "rebase trunk"})
               (plug-key n <leader>gmtb #(%> BranchTrunk) {:desc "create new branch from trunk"})
               (plug-key n <leader>gmtu #(%> UpdateTrunk) {:desc "update the trunk"})
               (plug-key n <leader>gmrc #(%> RebaseContinue) {:desc "git rebase continue"})
               (plug-key n <leader>gmra #(%> RebaseAbort) {:desc "git rebase abort"})
               (plug-key n <leader>gmss #(%> SaveChanges) {:desc "stash current changes"})
               (plug-key n <leader>gmsp #(%> RestoreChanges) {:desc "stash pop"})]})

 (plug sindrets/diffview.nvim
       {:cmd (%w DiffviewFileHistory
                 DiffviewOpen)
        :opts {:keymaps {:file_panel {:q "<Cmd>DiffviewClose<CR>"}
                         :file_history_panel {:q "<Cmd>DiffviewClose<CR>"}}}})

 (plug lewis6991/gitsigns.nvim
       {:event (%w BufReadPre BufNewFile)
        :opts {:on_attach (%f (def-key/buffer "go to next hunk" $1 n <leader>gj (%> Gitsigns next_hunk))
                              (def-key/buffer "go to previous hunk" $1 n <leader>gk (%> Gitsigns prev_hunk))
                              (def-key/buffer "stage hunk" $1 n <leader>gs (%> Gitsigns stage_hunk))
                              (def-key/buffer "reset hunk" $1 n <leader>gr (%> Gitsigns reset_hunk)))
               :signs {:add          {:text   :+}
                       :change       {:text   :▌}
                       :delete       {:text   :_}
                       :topdelete    {:text   :‾}
                       :changedelete {:text   "~"}
                       :untracked    {:text   :▌}}}})

 (plug ruifm/gitlinker.nvim
       {:dependencies [:nvim-lua/plenary.nvim]
        :keys [(plug-key n <leader>gy "<cmd>lua require'gitlinker'.get_buf_range_url('n')<cr>" {:desc "copy a permalink for current line"})
               (plug-key v <leader>gy "<cmd>lua require'gitlinker'.get_buf_range_url('v')<cr>" {:desc "copy a permalink for selected line range"})]
        :opts {:mapping nil
               :print_url true}})

(plug niuiic/git-log.nvim
      {:dependencies [:niuiic/core.nvim]
       :keys [(plug-key n|v <leader>gl "<cmd>lua require'git-log'.check_log()<cr>" {:desc "check git log of the selected code"})]
       :opts {}})]
