(require-macros :macros.core)
(require-macros :macros.plugins)
(require-macros :macros.vim)

(fn load-snippets []
  (let [path (require :rz.lib.path)
        from-vscode (require :luasnip.loaders.from_vscode)
        from-lua (require :luasnip.loaders.from_lua)]
    (from-vscode.lazy_load {:exclude (%w clojure
                                         fennel
                                         gitcommit
                                         go
                                         ruby
                                         yaml)})
    (from-lua.lazy_load {:paths (path.join (vim.fn.stdpath :config) :snippets)})))

(fn setup [opts]
  (let [ls (require :luasnip)]
    (ls.config.set_config opts)))

(define-command SnipEdit [filetype?] {:complete :filetype}
  (let [snippets (require :rz.lib.snippets)
        buffer (require :rz.lib.buffer)
        filetype (if (blank? filetype?)
                   (buffer.filetype (buffer.current))
                   filetype?)
        event (snippets.watch filetype)]
     (snippets.edit filetype)
     (vim.api.nvim_buf_set_option 0 :bufhidden :delete)
     (define-autocmd/this-buffer BufUnload (event:stop))))

(define-command SnipReload [filetype?] {:complete :filetype}
  (let [snippets (require :rz.lib.snippets)
        buffer (require :rz.lib.buffer)
        filetype (if (blank? filetype?)
                   (buffer.filetype (buffer.current))
                   filetype?)]
    (snippets.reload filetype)))

(fn config [_ opts]
  (setup opts)
  (load-snippets))

(fn jump-to-next []
  (let [ls (require :luasnip)]
    (when (ls.expand_or_jumpable)
      (ls.expand_or_jump))))

(fn jump-to-prev []
  (let [ls (require :luasnip)]
    (when (ls.jumpable -1)
      (ls.jump -1))))

(fn previous-choice []
  (let [ls (require :luasnip)]
    (when (ls.choice_active)
      (ls.change_choice -1))))

(fn next-choice []
  (let [ls (require :luasnip)]
    (when (ls.choice_active)
      (ls.change_choice 1))))

(fn keys [...]
  [(plug-key i|s <C-j> jump-to-next {:desc "expand snippet or jump to next slot"})
   (plug-key i|s <C-k> jump-to-prev {:desc "jump to previous slot in snippet"})
   (plug-key i|s <C-h> previous-choice {:desc "select previous slot choice in snippet"})
   (plug-key i|s <C-l> next-choice {:desc "select next slot choice in snippet"})])

(plug L3MON4D3/LuaSnip
      {: config
       :dependencies [:rafamadriz/friendly-snippets]
       : keys
       :opts {:history true
              :updateevents "TextChanged,TextChangedI"
              :enable_autosnippets false}
       :version "v1.*"})
