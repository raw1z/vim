-- [nfnl] Compiled from lua/rz/lib/terminal.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local core = autoload("nfnl.core")
local M = {}
if (nil == vim.g.rz_named_terminals) then
  vim.g.rz_named_terminals = {}
else
end
M["prevent-leaving"] = function(buffer_number)
  local function _3_()
    local function _4_()
    end
    return _4_
  end
  local function _5_()
    local function _6_()
    end
    return _6_
  end
  return {vim.keymap.set({"n"}, "<C-o>", _3_, {buffer = (buffer_number or 0), silent = true}), vim.keymap.set({"n"}, "<C-i>", _5_, {buffer = (buffer_number or 0), silent = true})}
end
M["setup-insert-mode"] = function(buffer_number)
  local session = require("rz.lib.session")
  local _7_ = vim.api.nvim_create_augroup("rz_terminal_insert_mode", {clear = true})
  local function _8_()
    if not session["loading?"]() then
      vim.cmd("startinsert")
      return vim.cmd("redraw")
    else
      return nil
    end
  end
  local function _10_()
    if not session["loading?"]() then
      vim.cmd("stopinsert")
      return vim.cmd("redraw")
    else
      return nil
    end
  end
  return {vim.api.nvim_create_autocmd({"BufEnter"}, {buffer = (buffer_number or 0), callback = _8_, group = _7_}), vim.api.nvim_create_autocmd({"BufLeave"}, {buffer = (buffer_number or 0), callback = _10_, group = _7_})}
end
M["setup-auto-close"] = function(buffer_number)
  local _12_ = vim.api.nvim_create_augroup("rz_terminal_autoclose", {clear = true})
  local function _13_()
    if (0 == vim.v.event.status) then
      return vim.api.nvim_win_close(vim.fn.bufwinid(buffer_number), true)
    else
      return nil
    end
  end
  return {vim.api.nvim_create_autocmd({"TermClose"}, {buffer = (buffer_number or 0), callback = _13_, group = _12_})}
end
local function create_terminal()
  local terminal = {}
  terminal["create-window"] = function(direction)
    local window = require("rz.lib.window")
    local buffer_number = vim.api.nvim_create_buf(false, true)
    vim.api.nvim_buf_set_option(buffer_number, "bufhidden", "delete")
    terminal["buffer-number"] = buffer_number
    window.open(buffer_number, direction)
    return M["prevent-leaving"](buffer_number)
  end
  terminal["build-term-command"] = function(shell_command)
    if core["nil?"](shell_command) then
      return ""
    elseif core["string?"](shell_command) then
      return shell_command
    else
      return vim.fn.join(shell_command, " ")
    end
  end
  terminal["open-term"] = function(shell_command, opts)
    local fish_path = vim.fn.exepath("fish")
    local command = terminal["build-term-command"](shell_command)
    local opts0 = (opts or {})
    local start_insert_3f = (opts0["start-insert?"] or false)
    local auto_close_3f = (opts0["auto-close?"] or false)
    local buffer_name
    if core["empty?"](command) then
      buffer_name = "Terminal"
    else
      buffer_name = command
    end
    local channel_id
    if core["empty?"](command) then
      channel_id = vim.fn.termopen(fish_path)
    else
      channel_id = vim.fn.termopen({fish_path, "-i", "-c", command})
    end
    M["setup-insert-mode"](terminal["buffer-number"])
    vim.api.nvim_buf_set_var(terminal["buffer-number"], "special_buffer_name", buffer_name)
    vim.api.nvim_buf_set_var(terminal["buffer-number"], "is_special_buffer", true)
    if start_insert_3f then
      vim.api.nvim_input("A")
    else
    end
    if auto_close_3f then
      M["setup-auto-close"](terminal["buffer-number"])
    else
    end
    return channel_id
  end
  terminal["buffer-exist?"] = function()
    return not core["nil?"](terminal["buffer-number"])
  end
  terminal["destroy-buffer"] = function()
    if (0 ~= vim.fn.bufexists(terminal["buffer-number"])) then
      return vim.cmd((terminal["buffer-number"] .. "bwipeout!"))
    else
      return nil
    end
  end
  terminal.start = function(direction, shell_command, opts)
    if terminal["buffer-exist?"]() then
      terminal["destroy-buffer"]()
    else
    end
    terminal["create-window"](direction)
    return terminal["open-term"](shell_command, opts)
  end
  return terminal
end
M.start = function(shell_command, opts)
  local opts0 = (opts or {})
  local direction = (opts0.direction or "horizontal")
  local terminal = (vim.g.rz_named_terminals[opts0.name] or create_terminal())
  if not core["nil?"](opts0.name) then
    vim.g.rz_named_terminals = vim.tbl_extend("force", vim.g.rz_named_terminals, {[opts0.name] = terminal})
  else
  end
  return terminal.start(direction, shell_command, opts0), terminal["buffer-number"]
end
M["direnv-exec"] = function(shell_command, opts)
  return M.start(("direnv exec $PWD " .. shell_command), opts)
end
return M
