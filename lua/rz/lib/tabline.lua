-- [nfnl] Compiled from lua/rz/lib/tabline.fnl by https://github.com/Olical/nfnl, do not edit.
local utils = require("heirline.utils")
local spacer = {provider = "%="}
local separator = {provider = " "}
local function filename(bufnr)
  local bufname = vim.fn.bufname(bufnr)
  local vimbar = require("rz.lib.vimbar")
  local special_name = vimbar["special-buffer-filename"](bufnr)
  if (nil ~= special_name) then
    return special_name
  elseif ("" ~= bufname) then
    return vim.fn.fnamemodify(bufname, ":t")
  else
    return "[No Name]"
  end
end
local function flags(bufnr)
  local bufmodified = vim.fn.getbufvar(bufnr, "&mod")
  if (1 == bufmodified) then
    return "[+]"
  else
    return ""
  end
end
local function bufnr(tab)
  local winnr = vim.fn.tabpagewinnr(tab)
  local buflist = vim.fn.tabpagebuflist(tab)
  return buflist[winnr]
end
local function is_previous_tab_3f(tab)
  return (tab == vim.fn.tabpagenr("#"))
end
local tab
local function _3_(_241)
  if _241.is_active then
    return "TabLineTabLeftBorderSel"
  else
    return "TabLineTabLeftBorder"
  end
end
local function _5_(_241)
  _241.is_previous = is_previous_tab_3f(_241.tabnr)
  return nil
end
local function _6_(_241)
  return string.format("%%%sT%s%%T", _241.tabpage, _241.tabnr)
end
local function _7_(_241)
  if _241.is_active then
    return "TabLineNumberSel"
  elseif _241.is_previous then
    return "TabLineNumberPreviousSel"
  else
    return "TabLineNumber"
  end
end
local function _9_(_241)
  if _241.is_active then
    return "TabLineNumberSeparatorSel"
  else
    return "TabLineNumberSeparator"
  end
end
local function _11_(_241)
  return string.format(" %s", filename(bufnr(_241.tabnr)))
end
local function _12_(_241)
  if _241.is_active then
    return "TabLineSel"
  else
    return "TabLine"
  end
end
local function _14_(_241)
  return flags(bufnr(_241.tabnr))
end
local function _15_(_241)
  if _241.is_active then
    return "TabLineSel"
  else
    return "TabLine"
  end
end
local function _17_(_241)
  if _241.is_active then
    return "TabLineTabRightBorderSel"
  else
    return "TabLineTabRightBorder"
  end
end
tab = {{provider = "\238\130\182\226\150\136", hl = _3_}, {init = _5_, provider = _6_, hl = _7_}, {provider = "\226\150\136\238\130\188", hl = _9_}, {provider = _11_, hl = _12_}, {provider = _14_, hl = _15_}, {provider = "\226\150\136\238\130\180", hl = _17_}, separator}
local function _19_()
  return (#vim.api.nvim_list_tabpages() > 1)
end
return utils.insert({condition = _19_}, utils.make_tablist(tab), spacer)
