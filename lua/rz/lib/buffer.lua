-- [nfnl] Compiled from lua/rz/lib/buffer.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local core = autoload("nfnl.core")
local window = autoload("rz.lib.window")
local digest = autoload("rz.lib.digest")
local M = {}
M.number = function(buffer)
  if core["string?"](buffer) then
    return vim.fn.bufnr(buffer)
  else
    return buffer
  end
end
M.current = function()
  return vim.fn.bufnr()
end
M["window-number"] = function(buffer)
  return vim.fn.bufwinnr(M.number(buffer))
end
M["buffer-exist?"] = function(buffer)
  return (0 ~= vim.fn.bufexists(M.number(buffer)))
end
M["switch-to"] = function(buffer)
  return vim.cmd((M["window-number"](buffer) .. "wincmd w"))
end
M["scroll-to-bottom"] = function(buffer)
  return vim.cmd((M["window-number"](buffer) .. "windo norm G0"))
end
M.delete = function(buffer)
  if M["buffer-exist?"](buffer) then
    return vim.api.nvim_buf_delete(M.number(buffer), {force = true})
  else
    return nil
  end
end
M["current-word"] = function()
  return vim.fn.trim(vim.fn.expand("<cword>"))
end
M["current-line"] = function()
  return vim.fn.trim(vim.fn.getline("."))
end
M["current-line-number"] = function()
  return vim.fn.line(".")
end
M["last-line-number"] = function()
  return vim.fn.line("$")
end
local function sanitize_range(line_start, column_start, line_end, column_end)
  if ((vim.fn.line2byte(line_start) + column_start) > (vim.fn.line2byte(line_end) + column_end)) then
    return line_end, column_end, line_start, column_start
  else
    return line_start, column_start, line_end, column_end
  end
end
M["current-visual-selection-range"] = function()
  local _, line_start, column_start, _0 = unpack(vim.fn.getpos("v"))
  local _1, line_end, column_end, _2 = unpack(vim.fn.getpos("."))
  return sanitize_range(line_start, column_start, line_end, column_end)
end
M["current-visual-selection"] = function()
  local mode = vim.fn.mode()
  local line_start, column_start, line_end, column_end = M["current-visual-selection-range"]()
  if (mode == "V") then
    return vim.api.nvim_buf_get_lines(0, (line_start - 1), line_end, true)
  else
    return vim.api.nvim_buf_get_text(0, (line_start - 1), (column_start - 1), (line_end - 1), column_end, {})
  end
end
M["select-last-pasted-text"] = function()
  return vim.api.nvim_input("`[v`]")
end
M.hashid = function(buffer)
  return digest.md5(vim.api.nvim_buf_get_name(buffer))
end
M["enable-autowrite"] = function(buffer)
  vim.cmd("write!")
  vim.b["autowrite-enabled"] = true
  local _6_ = vim.api.nvim_create_augroup(core.str("BufferAutowrite-", M.hashid(buffer)), {clear = true})
  local function _7_()
    if (vim.b["autowrite-enabled"] and vim.bo.modified) then
      vim.cmd("update")
      return false
    else
      return nil
    end
  end
  return {vim.api.nvim_create_autocmd({"CursorHold"}, {buffer = buffer, callback = _7_, group = _6_})}
end
M["disable-autowrite"] = function(buffer)
  if vim.b["autowrite-enabled"] then
    vim.b["autowrite-enabled"] = false
    return vim.api.nvim_del_augroup_by_name(core.str("BufferAutowrite-", M.hashid(buffer)))
  else
    return nil
  end
end
M["toggle-autowrite"] = function(buffer)
  if vim.b["autowrite-enabled"] then
    return M["disable-autowrite"](buffer)
  else
    return M["enable-autowrite"](buffer)
  end
end
M.new = function(direction, opts)
  local opts0 = (opts or {})
  local direction0 = (direction or "horizontal")
  local listed
  if core["nil?"](opts0.listed) then
    listed = false
  else
    listed = opts0.listed
  end
  local scratch
  if core["nil?"](opts0.scratch) then
    scratch = true
  else
    scratch = opts0.scratch
  end
  local bufnr = vim.api.nvim_create_buf(listed, scratch)
  window.open(bufnr, direction0)
  return bufnr
end
M.filetype = function(buffer)
  local args
  if ("number" == type(buffer)) then
    args = {buf = buffer}
  else
    args = {filename = buffer}
  end
  return vim.filetype.match(args)
end
M.name = function(bufnr)
  return vim.fn.bufname(bufnr)
end
M.text = function(buffer, start_row, start_col, end_row, end_col)
  if ((start_row ~= end_row) and (0 == start_col) and (0 == end_col)) then
    return vim.api.nvim_buf_get_lines(0, (start_row - 1), end_row, true)
  else
    return vim.api.nvim_buf_get_text(buffer, (start_row - 1), start_col, (end_row - 1), (end_col + 1), {})
  end
end
M["content-as-string"] = function(buffer)
  return vim.fn.join(vim.api.nvim_buf_get_lines(buffer, 0, -1, false), "\n")
end
M["floating?"] = function(buffer)
  return vim.tbl_contains(vim.tbl_map(vim.api.nvim_win_get_buf, vim.tbl_filter(window["floating?"], vim.api.nvim_list_wins())), buffer)
end
M.dirname = function(buffer)
  return vim.fn.fnamemodify(vim.fn.fnamemodify(M.filename(buffer, true), ":h"), ":p")
end
M.filename = function(buffer, full_3f)
  local full_path = vim.api.nvim_buf_get_name(buffer)
  if full_3f then
    return full_path
  else
    return vim.fn.fnamemodify(full_path, ":.")
  end
end
M["copy-path"] = function(buffer, include_line_3f, full_3f)
  local path = M.filename(buffer, full_3f)
  if include_line_3f then
    return string.format("%s:%s", path, vim.fn.line("."))
  else
    return path
  end
end
return M
