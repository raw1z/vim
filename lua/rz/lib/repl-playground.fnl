(require-macros :macros.core)
(require-macros :macros.vim)

(local {: autoload} (require :nfnl.module))
(local buffer (autoload :rz.lib.buffer))
(local draft (autoload :rz.lib.draft))
(local str (autoload :nfnl.string))
(local terminal (autoload :rz.lib.terminal))

(local M {})

(fn M.create [filetype command opts]
  (local opts (or opts {}))
  (let [playground {: opts
                    : filetype
                    : command}]
    (tset playground :opts :scratch-suffix (or opts.scratch-suffix ""))
    (tset playground :opts :activate-term-on-send? (if (not= nil opts.activate-term-on-send?)
                                                     opts.activate-term-on-send?
                                                     false))

    (fn playground.create-input-buffer []
      (let [input-buffer (draft.create :tab filetype {:scratch? false
                                                      :suffix playground.opts.scratch-suffix})]
        (tset playground :input-buffer input-buffer)
        (vim.api.nvim_buf_set_var input-buffer :is_special_buffer true)
        (vim.api.nvim_buf_set_var input-buffer :special_buffer_name (string.format "Language Shell - %s" playground.filetype))
        (playground.setup-input-keymaps)))

    (fn playground.attach-buffer [bufnr]
      (tset playground :input-buffer bufnr)
      (buffer.switch-to bufnr)
      (playground.setup-input-keymaps))

    (fn playground.create-term-buffer []
      (let [(channel-id buffer-number) (terminal.start command {:direction :horizontal
                                                                :start-insert? false})]
        (tset playground :channel-id channel-id)
        (tset playground :term-buffer buffer-number))
      (%> wincmd K)
      (buffer.scroll-to-bottom playground.term-buffer)
      (buffer.switch-to playground.input-buffer))

    (fn playground.setup-input-keymaps []
      (def-keys/this-buffer
        "close the playground" n <leader>x playground.close
        "restart the repl" n <leader>r playground.restart-term
        "send current line to the repl" n <Enter>   playground.send-current-line-to-repl
        "send current selection to repl" v <Enter>   playground.send-current-selection-to-repl)

      (define-operator/this-buffer n <localleader><space> playground.send-textobject-to-repl))

    (fn playground.close []
      (playground.close-buffer :term)
      (playground.close-buffer :input))

    (fn playground.close-buffer [kind]
      (let [buffer-number (. playground (.. kind :-buffer))]
        (buffer.delete buffer-number)))

    (fn playground.restart-term []
      (playground.close-buffer :term)
      (playground.create-term-buffer))

    (fn playground.send-current-line-to-repl []
      (-> [(vim.fn.getline :.)]
          (playground.send-to-repl)))

    (fn playground.send-current-selection-to-repl []
      (-> (buffer.current-visual-selection)
          (playground.send-to-repl)))

    (fn playground.send-textobject-to-repl [{: text}]
      (playground.send-to-repl text))

    (fn playground.send-to-repl [code]
      (let [code-lines (->> code
                            (vim.tbl_filter #(not (str.blank? $1)))
                            (vim.tbl_map str.trimr)
                            (vim.tbl_map #(.. $1 "\r")))]
        (each [_ input (ipairs code-lines)]
          (vim.fn.chansend playground.channel-id input))
        (buffer.scroll-to-bottom playground.term-buffer)
        (if playground.opts.activate-term-on-send?
          (do
            (buffer.switch-to playground.term-buffer)
            (%> startinsert))
          (do
            (buffer.switch-to playground.input-buffer)
            (%> stopinsert)))))

    (fn playground.start []
      (playground.create-input-buffer)
      (playground.create-term-buffer))

    (fn playground.attach-current-buffer []
      (playground.attach-buffer (buffer.current))
      (playground.create-term-buffer))

    playground))

M
