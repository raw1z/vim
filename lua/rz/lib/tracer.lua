-- [nfnl] Compiled from lua/rz/lib/tracer.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local text = autoload("rz.lib.text")
local M = {}
if (0 ~= vim.fn.empty(M)) then
  M["traces"] = {}
  M["timeout"] = 5000
else
end
local function max_width(lines)
  local acc = 0
  for _, line in ipairs(lines) do
    acc = math.max(acc, #line)
  end
  return acc
end
local function display(entry)
  if ("string" == type(entry)) then
    return entry
  elseif ("table" == type(entry)) then
    return vim.inspect(entry)
  else
    return tostring(entry)
  end
end
local function build_lines()
  local acc = {}
  for _, entry in ipairs(M.traces) do
    acc = vim.fn.extendnew(text.lines(display(entry)), acc)
  end
  return acc
end
local function create_traces_window(lines)
  local lazy = require("lazy")
  lazy.load({plugins = {"nui.nvim"}})
  local height = #lines
  local width = max_width(lines)
  local Popup = require("nui.popup")
  local popup = Popup({position = {row = (vim.o.lines - height - 1), col = (vim.o.columns - width)}, relative = "editor", size = {width = width, height = height}, win_options = {winhighlight = "Normal:Normal"}, focusable = false})
  M["popup-bufnr"] = popup.bufnr
  local function _4_()
    return popup:unmount()
  end
  popup:on("BufDelete", _4_, {once = true})
  vim.api.nvim_buf_set_lines(popup.bufnr, 0, 0, false, lines)
  return popup:mount()
end
local function destroy_traces_window()
  if ((nil ~= M["popup-bufnr"]) and vim.api.nvim_buf_is_valid(M["popup-bufnr"])) then
    return vim.api.nvim_buf_delete(M["popup-bufnr"], {force = true})
  else
    return nil
  end
end
local function render()
  destroy_traces_window()
  local lines = build_lines()
  if (#lines > 0) then
    return create_traces_window(lines)
  else
    return nil
  end
end
local function pop_traces()
  return table.remove(M.traces)
end
local function update()
  render()
  if (#M.traces > 0) then
    local function _7_()
      pop_traces()
      return update()
    end
    vim.defer_fn(_7_, M.timeout)
    return nil
  else
    return nil
  end
end
M.print = function(data)
  table.insert(M.traces, 1, data)
  return update()
end
return M
