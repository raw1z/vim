-- [nfnl] Compiled from lua/rz/lib/lsp.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
M["autoformat-code"] = function()
  return vim.lsp.buf.format()
end
local function refresh_code_lens()
  return vim.lsp.codelens.refresh()
end
local function highlight_document()
  return vim.lsp.buf.document_highlight()
end
local function clear_references()
  return vim.lsp.buf.clear_references()
end
local function show_code_actions()
  return vim.cmd("Lspsaga code_action")
end
local function show_code_actions_for_virtual_selection()
  vim.fn.feedkeys(vim.api.nvim_replace_termcodes("<C-U>", true, false, true))
  return vim.cmd("Lspsaga range_code_action")
end
local function show_line_diagnostics()
  return vim.cmd("Lspsaga show_line_diagnostics")
end
local function go_to_next_diagnostic()
  return vim.cmd("Lspsaga diagnostic_jump_next")
end
local function go_to_previous_diagnostic()
  return vim.cmd("Lspsaga diagnostic_jump_prev")
end
local function rename()
  return vim.cmd("Lspsaga rename")
end
local function hover_doc()
  return vim.cmd("Lspsaga hover_doc")
end
local function jump_to_definition()
  return vim.lsp.buf.definition({reuse_win = true})
end
local function finder()
  return vim.cmd("Lspsaga finder")
end
local function capabilities_blacklist()
  return {elmls = {"codeLensProvider"}}
end
local function capability_disabled_3f(client, capability)
  local list = require("rz.lib.list")
  local disabled_capabilities = capabilities_blacklist()[client.name]
  return ((nil ~= disabled_capabilities) and list["contains?"](disabled_capabilities, capability))
end
local function has_capability_3f(client, capability)
  local server_capabilities = vim.tbl_keys(client.server_capabilities)
  return (not capability_disabled_3f(client, capability) and (nil ~= server_capabilities[capability]))
end
local function setup_keymaps(client, bufnr)
  return {vim.keymap.set({"n"}, "K", hover_doc, {buffer = bufnr, desc = "show hover doc", silent = true}), vim.keymap.set({"n"}, "<localleader>ba", show_code_actions, {buffer = bufnr, desc = "show code actions", silent = true}), vim.keymap.set({"n"}, "<localleader>bf", M["autoformat-code"], {buffer = bufnr, desc = "autoformat the code", silent = true}), vim.keymap.set({"n"}, "<localleader>bf", finder, {buffer = bufnr, desc = "open the finder", silent = true}), vim.keymap.set({"n"}, "<localleader>br", rename, {buffer = bufnr, desc = "rename", silent = true}), vim.keymap.set({"n"}, "<localleader>dd", show_line_diagnostics, {buffer = bufnr, desc = "show diagnostics for current line", silent = true}), vim.keymap.set({"n"}, "<localleader>dj", go_to_next_diagnostic, {buffer = bufnr, desc = "go to the next diagnostic", silent = true}), vim.keymap.set({"n"}, "<localleader>dk", go_to_previous_diagnostic, {buffer = bufnr, desc = "go to the previous diagnostic", silent = true}), vim.keymap.set({"v"}, "<localleader>ba", show_code_actions_for_virtual_selection, {buffer = bufnr, desc = "show code actions for virtual selection", silent = true}), vim.keymap.set({"n"}, "gd", jump_to_definition, {buffer = bufnr, desc = "jump to definition", silent = true}), vim.keymap.set({"n"}, "gD", jump_to_definition, {buffer = bufnr, desc = "jump to definition", silent = true}), vim.keymap.set({"n"}, "<C-l>", jump_to_definition, {buffer = bufnr, desc = "jump to definition", silent = true})}
end
local function setup_codelens_display(client, bufnr)
  if has_capability_3f(client, "codeLensProvider") then
    local _1_ = vim.api.nvim_create_augroup(("LspCodeLens." .. client.name), {clear = true})
    return {vim.api.nvim_create_autocmd({"BufWritePost", "CursorHold"}, {buffer = bufnr, callback = refresh_code_lens, group = _1_})}
  else
    return nil
  end
end
local function setup_document_highlight(client, bufnr)
  if has_capability_3f(client, "documentHighlightProvider") then
    local _3_ = vim.api.nvim_create_augroup(("LspDocumentHighlight." .. client.name), {clear = true})
    return {vim.api.nvim_create_autocmd({"CursorHold"}, {buffer = bufnr, callback = highlight_document, group = _3_}), vim.api.nvim_create_autocmd({"CursorMoved"}, {buffer = bufnr, callback = clear_references, group = _3_})}
  else
    return nil
  end
end
M["on-attach"] = function(event)
  if event.data then
    local client = vim.lsp.get_client_by_id(event.data.client_id)
    if client then
      setup_keymaps(client, event.buf)
      setup_document_highlight(client, event.buf)
      return setup_codelens_display(client, event.buf)
    else
      return nil
    end
  else
    return nil
  end
end
M.capabilities = function()
  local cmp_nvim_lsp = require("cmp_nvim_lsp")
  return cmp_nvim_lsp.default_capabilities()
end
M.configure = function(server_name, opts)
  local lspconfig = require("lspconfig")
  local config = lspconfig[server_name]
  return config.setup(vim.tbl_extend("force", {capabilities = M.capabilities()}, (opts or {})))
end
return M
