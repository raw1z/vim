-- [nfnl] Compiled from lua/rz/lib/log.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
local function run(command, input)
  local job = require("rz.lib.job")
  job["async-run"](string.format("fish -c '%s \"%s\"'", command, vim.inspect(input)))
  return input
end
M.rlog = function(input)
  return run("rlog", input)
end
M.flog = function(input)
  return run("flog", input)
end
return M
