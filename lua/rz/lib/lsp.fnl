(require-macros :macros.core)
(require-macros :macros.vim)

(local M {})

(fn M.autoformat-code []
  (vim.lsp.buf.format))

(fn refresh-code-lens []
  (vim.lsp.codelens.refresh))

(fn highlight-document []
  (vim.lsp.buf.document_highlight))

(fn clear-references []
  (vim.lsp.buf.clear_references))

(fn show-code-actions []
  (%> Lspsaga code_action))

(fn show-code-actions-for-virtual-selection []
  (-> (vim.api.nvim_replace_termcodes :<C-U> true false true)
      (vim.fn.feedkeys))
  (%> Lspsaga range_code_action))

(fn show-line-diagnostics []
  (%> Lspsaga show_line_diagnostics))

(fn go-to-next-diagnostic []
  (%> Lspsaga diagnostic_jump_next))

(fn go-to-previous-diagnostic []
  (%> Lspsaga diagnostic_jump_prev))

(fn rename []
  (%> Lspsaga rename))

(fn hover-doc []
  (%> Lspsaga hover_doc))

(fn jump-to-definition []
  (vim.lsp.buf.definition {:reuse_win true}))

(fn finder []
  (%> Lspsaga finder))

(fn capabilities-blacklist []
  {:elmls [:codeLensProvider]})

(fn capability-disabled? [client capability]
  (let [list (require :rz.lib.list)
        disabled-capabilities (. (capabilities-blacklist) (. client :name))]
    (and (not= nil disabled-capabilities)
         (list.contains? disabled-capabilities capability))))

(fn has-capability? [client capability]
  (let [server-capabilities (vim.tbl_keys client.server_capabilities)]
    (and (not (capability-disabled? client capability))
         (not= nil (. server-capabilities capability)))))

(fn setup-keymaps [client bufnr]
  (def-keys/buffer bufnr
    "show hover doc" n K hover-doc
    "show code actions" n <localleader>ba show-code-actions
    "autoformat the code" n <localleader>bf M.autoformat-code
    "open the finder" n <localleader>bf finder
    "rename" n <localleader>br rename
    "show diagnostics for current line" n <localleader>dd show-line-diagnostics
    "go to the next diagnostic" n <localleader>dj go-to-next-diagnostic
    "go to the previous diagnostic" n <localleader>dk go-to-previous-diagnostic
    "show code actions for virtual selection" v <localleader>ba show-code-actions-for-virtual-selection
    "jump to definition" n gd jump-to-definition
    "jump to definition" n gD jump-to-definition
    "jump to definition" n <C-l> jump-to-definition))

(fn setup-codelens-display [client bufnr]
  (when (has-capability? client :codeLensProvider)
    (define-augroup (.. :LspCodeLens. client.name)
      (define-autocmd/buffer BufWritePost|CursorHold bufnr refresh-code-lens))))

(fn setup-document-highlight [client bufnr]
  (when (has-capability? client :documentHighlightProvider)
    (define-augroup (.. :LspDocumentHighlight. client.name)
      (define-autocmd/buffer CursorHold bufnr highlight-document)
      (define-autocmd/buffer CursorMoved bufnr clear-references))))

(fn M.on-attach [event]
  (when event.data
    (when-let [client (vim.lsp.get_client_by_id event.data.client_id)]
      (setup-keymaps client event.buf)
      (setup-document-highlight client event.buf)
      (setup-codelens-display client event.buf))))

(fn M.capabilities []
  (let [cmp-nvim-lsp (require :cmp_nvim_lsp)]
    (cmp-nvim-lsp.default_capabilities)))

(fn M.configure [server-name opts]
  (let [lspconfig (require :lspconfig)
        config (. lspconfig server-name)]
    (config.setup (vim.tbl_extend :force
                                  {:capabilities (M.capabilities)}
                                  (or opts {})))))

M
