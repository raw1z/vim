(local M {})

(fn format-message [...]
  (vim.fn.join [" " ... " "] " "))

(fn log [kind ...]
  (-> (format-message ...)
      ((. Snacks.notify kind))))

(fn M.error [...] (log :error ...))
(fn M.warn  [...] (log :warn  ...))
(fn M.info  [...] (log :info  ...))

(fn M.trace [...]
  (let [tracer (require :rz.lib.tracer)]
    (-> (format-message ...)
        (tracer.print))))

(fn M.dismiss []
  (Snacks.notifier.hide))

M
