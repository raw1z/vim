(local M {})

(fn M.watch-file [path on-change]
  "watch modification on file at the given path
  and call the given `on-change` callback passing it the file path"
  (let [event (vim.uv.new_fs_event)]
    (vim.uv.fs_event_start event
                           path
                           {:watch_entry true}
                           #(on-change path))
    event))

(fn M.watch-file-with-debounce [path timeout on-change]
  "watch modification on file at the given path
  and call the given `on-change` callback passing it the file path
  after `timeout` seconds"
  (let [debounce (require :rz.lib.debounce)
        debouncer (debounce.new)]
    (M.watch-file path
                  #(debouncer.schedule-after timeout
                                             #(on-change path)))))

M
