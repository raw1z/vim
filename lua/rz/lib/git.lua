-- [nfnl] Compiled from lua/rz/lib/git.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
local function git(...)
  return vim.fn.trim(vim.fn.system(("git " .. ...)))
end
local function git_lines(...)
  return vim.fn.split(vim.fn.trim(vim.fn.system(("git " .. ...))), "\n")
end
M["guess-trunk-branch"] = function()
  return vim.fn.substitute(git("branch --list main master"), "\\s*\\*\\s*", "", "")
end
M["current-branch"] = function()
  local text = require("rz.lib.text")
  local function _1_(_241)
    return not text["starts-with?"](_241, "fatal: not a git repository")
  end
  return vim.tbl_filter(_1_, vim.fn.systemlist({"git", "symbolic-ref", "--short", "HEAD"}))[1]
end
M["root-dir"] = function()
  return git("rev-parse --show-toplevel")
end
M["is-git-repository?"] = function(path)
  return ("true" == git(("-C " .. path .. " rev-parse --is-inside-work-tree")))
end
local function parse_status(status)
  if (status == "M") then
    return "modified"
  elseif (status == "A") then
    return "added"
  elseif (status == "??") then
    return "untracked"
  elseif (status == "D") then
    return "deleted"
  elseif (status == "R") then
    return "renamed"
  else
    local _ = status
    return status
  end
end
local function parse_status_change(_3_)
  local status = _3_[1]
  local file = _3_[2]
  return {file = (M["root-dir"]() .. "/" .. file), status = parse_status(status)}
end
M.status = function()
  local list = require("rz.lib.list")
  local function _4_(_241)
    return vim.fn.split(_241, "\\s")
  end
  return vim.tbl_map(parse_status_change, vim.tbl_map(list.compact, vim.tbl_map(_4_, vim.tbl_map(vim.fn.trim, git_lines("status --porcelain --untracked --ignore-submodules")))))
end
M["repo-dirty?"] = function()
  return not vim.tbl_isempty(M.status())
end
M["changed-files-between-branches"] = function(branch1, branch2)
  local lines = git_lines(("diff --name-status " .. branch1 .. ".." .. branch2))
  local function _5_(_241)
    return {path = _241[2], status = parse_status(_241[1])}
  end
  local function _6_(_241)
    return vim.fn.split(_241, "\\t")
  end
  return vim.tbl_map(_5_, vim.tbl_map(_6_, lines))
end
M["save-changes"] = function(message)
  local command = "G stash --all"
  if ("" ~= message) then
    command = string.format("%s -m '%s'", command, message)
  else
  end
  return vim.cmd(command)
end
M["save-staged-changes"] = function(message)
  local command = "G stash-staged"
  if ("" ~= message) then
    command = string.format("%s -m '%s'", command, message)
  else
  end
  return vim.cmd(command)
end
M["restore-changes"] = function()
  return vim.cmd("G stash pop")
end
M["continue-rebase"] = function()
  return vim.cmd("G rebase --continue")
end
M["abort-rebase"] = function()
  return vim.cmd("G rebase --abort")
end
return M
