-- [nfnl] Compiled from lua/rz/lib/list.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local core = autoload("nfnl.core")
local str = autoload("nfnl.string")
local M = {}
M.compact = function(list)
  local function _2_(_241)
    return not str["blank?"](_241)
  end
  return core.filter(_2_, list)
end
M["fuzzy-filter"] = function(list, search)
  if str["blank?"](search) then
    return list
  else
    return vim.fn.matchfuzzy(list, search)
  end
end
M["map-strip"] = function(list, removed_tokens)
  local function _4_(_241)
    local result = _241
    for _, removed_token in ipairs(removed_tokens) do
      result = vim.fn.substitute(result, removed_token, "", "")
    end
    return result
  end
  return core.map(_4_, list)
end
M.chunk = function(list, count)
  local function _5_()
    local acc,results = {}, {}
    for _, item in ipairs(list) do
      local function _6_()
        local new_acc = core.concat(acc, {item})
        if (#acc < (count - 1)) then
          return {new_acc, results}
        else
          return {{}, core.concat(results, {new_acc})}
        end
      end
      local _set_8_ = _6_()
      acc = _set_8_[1]
      results = _set_8_[2]
    end
    return {acc, results}
  end
  return core.last(_5_())
end
M["contains?"] = function(list, value)
  return vim.tbl_contains(list, value)
end
M["empty?"] = function(list)
  return vim.tbl_isempty(list)
end
M.index = function(list, value)
  local index = vim.fn.index(list, value)
  if (-1 == index) then
    return nil
  else
    return (1 + index)
  end
end
M.flatten = function(list)
  return vim.iter(list):flatten(10):totable()
end
M.join = function(list, sep)
  return vim.fn.join(list, (sep or ""))
end
M["flat-join"] = function(list, sep)
  return M.join(M.flatten(list), sep)
end
return M
