-- [nfnl] Compiled from lua/rz/lib/snippets.fnl by https://github.com/Olical/nfnl, do not edit.
local lazy = require("lazy")
local M = {}
M.file = function(filetype)
  local path = require("rz.lib.path")
  return path.expand(path.join(vim.fn.stdpath("config"), "snippets", (filetype .. ".fnl")))
end
M["lua-file"] = function(filetype)
  local path = require("rz.lib.path")
  return path["lua-alternate"](M.file(filetype))
end
M.edit = function(filetype)
  vim.cmd(("split " .. M.file(filetype)))
  return vim.cmd(("lcd " .. vim.fn.stdpath("config")))
end
local function reload_snippets_file(path)
  local loaders = require("luasnip.loaders")
  local notify = require("rz.lib.notify")
  loaders.reload_file(path)
  return notify.trace(("Reloaded snippets file: " .. path))
end
M.reload = function(filetype)
  return reload_snippets_file(M["lua-file"](filetype))
end
M.watch = function(filetype)
  lazy.load({plugins = {"LuaSnip"}})
  local watcher = require("rz.lib.watcher")
  return watcher["watch-file-with-debounce"](M["lua-file"](filetype), 500, reload_snippets_file)
end
return M
