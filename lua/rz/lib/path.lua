-- [nfnl] Compiled from lua/rz/lib/path.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local core = autoload("nfnl.core")
local scandir = autoload("plenary.scandir")
local list = autoload("rz.lib.list")
local text = autoload("rz.lib.text")
local M = {}
M.expand = function(path)
  return vim.fn.expand(path)
end
M["exist?"] = function(path)
  local full_path = M.expand(path)
  local f = io.open(full_path, "r")
  if (nil ~= f) then
    f:close()
  else
  end
  return (nil ~= f)
end
M.read = function(path)
  local full_path = M.expand(path)
  local f = io.open(full_path, "r")
  if (nil ~= f) then
    local content = f:read("*a")
    f:close()
    return content
  else
    return nil
  end
end
M["lua-alternate"] = function(path)
  return vim.fn.substitute(path, "\\.fnl", ".lua", "g")
end
M.join = function(...)
  return vim.fn.substitute(vim.fn.join({...}, "/"), "//", "/", "g")
end
M.dirname = function(path)
  if vim.fn.isdirectory(path) then
    return vim.fn.fnamemodify(path, ":h")
  else
    return vim.fn.fnamemodify(path, ":p:h")
  end
end
M.basename = function(path)
  if vim.fn.isdirectory(path) then
    return vim.fn.fnamemodify(path, ":t")
  else
    return vim.fn.fnamemodify(path, ":p:t")
  end
end
M["basename-without-extension"] = function(path)
  return vim.fn.fnamemodify(path, ":p:t:r")
end
M["current-directory"] = function()
  return vim.fn.getcwd()
end
M.scan = function(path, opts)
  local opts0 = (opts or {hidden = false})
  local strip_basename_3f
  if core["nil?"](opts0["strip-basename?"]) then
    strip_basename_3f = true
  else
    strip_basename_3f = opts0["strip-basename?"]
  end
  local scan_opts = vim.fn.extendnew({depth = 1}, opts0)
  local files = scandir.scan_dir(path, scan_opts)
  local filtered_files
  if opts0.filter then
    filtered_files = vim.tbl_filter(opts0.filter, files)
  else
    filtered_files = files
  end
  if strip_basename_3f then
    return list["map-strip"](filtered_files, {path, "/"})
  else
    return filtered_files
  end
end
M["same?"] = function(path1, path2)
  return (M.expand(path1) == M.expand(path2))
end
M["starts-with?"] = function(path, prefix_path)
  return text["starts-with?"](M.expand(path), M.expand(prefix_path))
end
M["relative-from"] = function(path, base)
  local relative = text.replace(path, base, "")
  if text["starts-with?"](relative, "/") then
    return vim.fn.slice(relative, 1)
  else
    return relative
  end
end
M.preview = function(path)
  local job = require("rz.lib.job")
  return job.run(string.format("qlmanage -p %s", path))
end
M.create = function(path, content)
  local fout = io.open(path, "w")
  local function close_handlers_12_auto(ok_13_auto, ...)
    fout:close()
    if ok_13_auto then
      return ...
    else
      return error(..., 0)
    end
  end
  local function _11_()
    local function _12_()
      if vim.tbl_islist(content) then
        return list["flat-join"](content, "\n")
      else
        return content
      end
    end
    return fout:write(_12_())
  end
  return close_handlers_12_auto(_G.xpcall(_11_, (package.loaded.fennel or _G.debug or {}).traceback))
end
M.delete = function(path)
  return vim.fn.delete(path)
end
return M
