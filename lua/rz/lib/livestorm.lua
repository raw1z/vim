-- [nfnl] Compiled from lua/rz/lib/livestorm.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local path = autoload("rz.lib.path")
local list = autoload("rz.lib.list")
local M = {}
M["root-path"] = function()
  return path.join(vim.env.PROJECTS_PATH, "entropy")
end
local function packages_folder()
  return path.join(M["root-path"](), "packages")
end
M["list-packages"] = function()
  local function _2_(_241)
    return vim.fn.fnamemodify(path["relative-from"](_241, packages_folder()), ":h")
  end
  local function _3_()
    local tbl_21_auto = {}
    local i_22_auto = 0
    for _, search_pattern in ipairs({".pkg$", "OWNERS"}) do
      local val_23_auto = path.scan(packages_folder(), {depth = 5, hidden = true, respect_gitignore = true, search_pattern = search_pattern, ["strip-basename?"] = false})
      if (nil ~= val_23_auto) then
        i_22_auto = (i_22_auto + 1)
        tbl_21_auto[i_22_auto] = val_23_auto
      else
      end
    end
    return tbl_21_auto
  end
  return vim.fn.uniq(vim.fn.sort(vim.tbl_map(_2_, list.flatten(_3_()))))
end
M["package-path"] = function(name)
  return path.join(packages_folder(), name)
end
return M
