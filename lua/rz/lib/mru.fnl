(local {: autoload} (require :nfnl.module))
(local sqlite (autoload :sqlite.db))
(local path (autoload :rz.lib.path))
(local buffer (autoload :rz.lib.buffer))
(local list (autoload :rz.lib.list))
(local text (autoload :rz.lib.text))

(fn type [kind opts]
  (tset opts 1 kind)
  opts)

(local M {:db (sqlite {:uri (-> (vim.fn.stdpath :state)
                                (path.join :mru.db))
                       :mru {:path (type :text {:unique true
                                                :primary true})
                             :added (type :timestamp {:default (sqlite.lib.strftime :%s :now)})}
                       :opts {:keep_open true}})})

(fn is-ignored-file? [path]
  (-> (vim.iter [:.git/COMMIT_EDITMSG])
      (: :any #(text.ends-with? path $1))))

(fn valid-path? [path]
  (and (> (length path) 0)
       (= 0 (vim.fn.isdirectory path))
       (not= 0 (vim.fn.filereadable path))
       (not (is-ignored-file? path))))

(fn insert-or-replace [path]
  (M.db:eval "insert or replace into mru (path) values (?)" path))

(fn save-current [path]
  (when (valid-path? path)
    (insert-or-replace path)))

(fn M.on-win-enter []
  (vim.schedule #(-> (vim.api.nvim_get_current_buf)
                     (buffer.name)
                     (vim.fn.fnamemodify ":p")
                     (save-current))))

(fn list-recent-files [root limit]
  (let [limit (or limit 5)]
    (let [result (->> (string.format "select path
                                        from mru
                                       where path like '%s%%'
                                    order by added desc
                                       limit %s" root limit)
                      (M.db:eval))]
      (if (= true result)
        []
        result))))

(fn M.get [root limit]
  (->> (list-recent-files root limit)
       (vim.tbl_map #(. $1 :path))))

M
