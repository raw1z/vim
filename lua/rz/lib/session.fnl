(require-macros :macros.core)
(require-macros :macros.vim)

(local {: autoload} (require :nfnl.module))
(local path (autoload :rz.lib.path))
(local git (autoload :rz.lib.git))
(local notify (autoload :rz.lib.notify))
(local buffer (autoload :rz.lib.buffer))
(local inflector (autoload :rz.lib.inflector))

(local M {})

(fn M.loading? []
  (= true vim.g.loading_session))

(fn M.folder []
  "return the session folder for the current state"
  (-> vim.env.PWD
      (vim.fn.fnamemodify ":.")
      (path.join "/.vim-sessions")))

(fn M.file []
  "return the session file name matching the current state"
  (let [branch (or (?. vim.b.gitsigns_status_dict :head)
                   (git.current-branch))
        folder-path (M.folder)
        branch-file-name (inflector.underscore branch)]
    (if (= 0 (length branch))
      (.. folder-path "/nogit")
      (.. folder-path "/" branch-file-name))))

(fn M.save []
  "create or override a session file matching current state"
  (vim.fn.mkdir (M.folder) :p)
  (let [session-file (M.file)]
    ; ensure that all opened quicfix windows are closed
    (vim.cmd "windo cclose")
    (->> (vim.fn.fnameescape session-file)
         (string.format "mksession! %s")
         (vim.cmd))
    session-file))

(fn M.load []
  "load the session file matching the current state"
  (let [file-path (M.file)]
    (set vim.g.loading_session true)
    (vim.cmd (.. "silent! source " file-path))
    (set vim.g.loaded_session file-path)
    (set vim.g.loaded_session_folder (M.folder))
    (set vim.g.loading_session false)))

(fn M.delete []
  "delete the loaded session file"
  (-> (M.file)
      (vim.uv.fs_unlink)))

(fn M.file-exist? []
  "return true if a session file matching current state exist
   else returns false"
  (path.exist? (M.file)))

(fn M.loaded? []
  "return true if the session file matching the current state is loaded
   else returns false"
  (path.same? (M.file) vim.g.loaded_session))

(fn has-modified-buffers? [buffers]
  (let [modified-buffers (vim.tbl_filter #(vim.api.nvim_buf_get_option $1 :modified)
                                         buffers)]
    (> 0 (length modified-buffers))))

(fn delete-buffers [buffers]
  (let [current-buffer (buffer.current)
        buffers-to-delete (->> buffers
                               (vim.tbl_filter vim.api.nvim_buf_is_valid)
                               (vim.tbl_filter #(not= $1 current-buffer)))]
    (each [_ buf (ipairs buffers-to-delete)]
      (vim.api.nvim_buf_delete buf {:force true}))
    (vim.api.nvim_buf_delete current-buffer {:force true})))

(fn safe-delete-existing-buffers [callback]
  (let [buffers (vim.api.nvim_list_bufs)]
    (if (has-modified-buffers? buffers)
      (notify.error "can't load session: the files in the current session have changed.")
      (vim.schedule (%f (M.stop-updates)
                        (delete-buffers buffers)
                        (callback))))))

(fn M.safe-load []
  "load the session file if nvim was started without arguments and
   we are not alreaded loading a session and
   the session file exists and
   the session file is not already loaded.
   Also delete existing buffers (if not modified) before loading the session"
  (when (and (= 0 (vim.fn.argc))
             (not (M.loading?))
             (M.file-exist?)
             (not (M.loaded?)))
    (safe-delete-existing-buffers M.load)))

(fn update []
  (if (path.same? vim.g.rz_updating_session (M.file))
    (M.save)
    (M.stop-updates)))

(fn M.keep-updated []
  (when (= nil vim.g.rz_updating_session)
    (define-augroup rz_session_updates
      (define-autocmd VimLeavePre * (update))
      (define-autocmd BufEnter * (update)))
    (set vim.g.rz_updating_session (M.file))))

(fn M.stop-updates []
  (when (not= nil vim.g.rz_updating_session)
    (vim.api.nvim_del_augroup_by_name :rz_session_updates)
    (set vim.g.rz_updating_session nil)))

M
