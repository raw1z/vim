(local {: autoload} (require :nfnl.module))
(local core (autoload :nfnl.core))
(local text (autoload :rz.lib.text))

(local M {})

(fn contain-separator? [str]
  (not= nil (string.find str "[%s%._%-/]")))

(fn M.common-separators [str]
  "return common separators for a given string"
  (if (contain-separator? str)
    "\\s\\|[._\\-/]"
    "[A-Z]\\?[a-z]*\\zs"))

(fn M.split [str opts]
  "split a given string using common separators (or optional separators) and returns the array of tokens"
  (let [{:sep separators} (or opts {})
        separators (or separators
                       (M.common-separators str))]
    (core.map
      #(vim.fn.tolower $1)
      (vim.fn.split str separators))))

(fn M.split-join [str joiner opts]
  "split a string and join the tokens with the given joiner"
  (-> (M.split str opts)
      (vim.fn.join joiner)))

(fn M.split-map-join [str transform joiner opts]
  "split a string, apply the given transform (any one arg function) to each token and join the results with the given joiner"
  (->> (M.split str opts)
       (core.map transform)
       (#(vim.fn.join $1 joiner))))

(fn M.constantize [str opts]
  (M.split-map-join str vim.fn.toupper "_" opts))

(fn M.capitalize [str]
  (-> (vim.fn.tolower str)
      (vim.fn.substitute "\\w" "\\=toupper(submatch(0))" "")))

(fn M.pascalize [str opts]
  (M.split-map-join str M.capitalize "" opts))

(fn M.camelize [str opts]
  (-> (M.pascalize str opts)
      (vim.fn.substitute "^\\w" "\\=tolower(submatch(0))" "")))

(fn M.titleize [str]
  (M.split-map-join str M.capitalize " "))

(fn M.underscore [str opts]
  (M.split-join str "_" opts))

(fn M.dasherize [str opts]
  (M.split-join str "-" opts))

(fn M.dotify [str opts]
  (M.split-join str "." opts))

(fn M.slashify [str opts]
  (M.split-join str "/" opts))

(fn M.classify [str opts]
  (M.split-map-join str M.capitalize "::" opts))

M
