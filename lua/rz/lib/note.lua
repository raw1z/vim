-- [nfnl] Compiled from lua/rz/lib/note.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local git = autoload("rz.lib.git")
local path = autoload("rz.lib.path")
local buffer = autoload("rz.lib.buffer")
local M = {folder = path.join(vim.fn.fnamemodify(vim.env.PWD, ":."), ".vim-notes")}
M.file = function()
  if git["is-git-repository?"](vim.env.PWD) then
    return M["branch-file"]()
  else
    return M["root-file"]()
  end
end
M["branch-file"] = function()
  local branch
  local _4_
  do
    local t_3_ = vim.b.gitsigns_status_dict
    if (nil ~= t_3_) then
      t_3_ = t_3_.head
    else
    end
    _4_ = t_3_
  end
  branch = (_4_ or git["current-branch"]())
  return path.join(M.folder, (branch .. ".md"))
end
M["root-file"] = function()
  return path.join(M.folder, "root.md")
end
local function configure_note_buffer()
  buffer["enable-autowrite"](0)
  vim.wo.foldmethod = "indent"
  return nil
end
M.edit = function()
  vim.cmd(string.format("tab drop %s", M.file()))
  return configure_note_buffer()
end
M["edit-root"] = function()
  vim.cmd(string.format("tab drop %s", M["root-file"]()))
  return configure_note_buffer()
end
return M
