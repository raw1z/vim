(require-macros :macros.core)
(local lazy (require :lazy))

(local M {})

(fn M.file [filetype]
  "returns the fennel file where snippets are defined for a given filetype"
  (let [path (require :rz.lib.path)]
    (-> (vim.fn.stdpath :config)
        (path.join :snippets (.. filetype :.fnl))
        (path.expand))))

(fn M.lua-file [filetype]
  "returns the lua file corresponding to the fennel file where snippets are defined for a given filetype"
  (let [path (require :rz.lib.path)]
    (-> (M.file filetype)
        (path.lua-alternate))))

(fn M.edit [filetype]
  (-> (.. "split " (M.file filetype))
      (vim.cmd))
  (-> (.. "lcd " (vim.fn.stdpath :config))
      (vim.cmd)))

(fn reload-snippets-file [path]
  (let [loaders (require :luasnip.loaders)
        notify (require :rz.lib.notify)]
    (loaders.reload_file path)
    (notify.trace (.. "Reloaded snippets file: " path))))

(fn M.reload [filetype]
  (-> (M.lua-file filetype)
      (reload-snippets-file)))

(fn M.watch [filetype]
  (lazy.load {:plugins [:LuaSnip]})
  (let [watcher (require :rz.lib.watcher)]
    (-> (M.lua-file filetype)
        (watcher.watch-file-with-debounce 500 reload-snippets-file))))

M
