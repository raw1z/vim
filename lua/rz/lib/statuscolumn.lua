-- [nfnl] Compiled from lua/rz/lib/statuscolumn.fnl by https://github.com/Olical/nfnl, do not edit.
local function real_line_number(number)
  if (0 == vim.v.virtnum) then
    return number
  else
    return nil
  end
end
local function show_relative_number_3f()
  local _2_
  do
    local opt_value_11_auto = vim.opt.relativenumber
    _2_ = opt_value_11_auto:get()
  end
  return (_2_ and (vim.v.relnum ~= 0))
end
local function line_number()
  if show_relative_number_3f() then
    return real_line_number(vim.v.relnum)
  else
    local _3_
    do
      local opt_value_11_auto = vim.opt.number
      _3_ = opt_value_11_auto:get()
    end
    if _3_ then
      return real_line_number(vim.v.lnum)
    else
      return nil
    end
  end
end
local spacer = {provider = "%="}
local separator = {provider = " "}
local line_number0 = {provider = line_number}
local sign = {provider = "%s"}
local fold = {provider = "%C "}
return {spacer, line_number0, separator, sign, fold, separator}
