-- [nfnl] Compiled from lua/rz/lib/config-loader.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
local function runtime_files()
  local text = require("rz.lib.text")
  local scan = require("plenary.scandir")
  local function _1_(_241)
    return text["ends-with?"](_241, ".fnl")
  end
  local _2_
  do
    local opt_value_11_auto = vim.opt.rtp
    _2_ = opt_value_11_auto:get()
  end
  return vim.tbl_filter(_1_, scan.scan_dir(vim.fn.globpath(vim.fn.join(_2_, ","), "lua/rz/config"), {depth = 1, hidden = false}))
end
local function load_config()
  return vim.tbl_map(require, vim.tbl_map(M["package-name"], runtime_files()))
end
M["load-all"] = function()
  require("rz.base")
  require("rz.keymaps")
  return load_config()
end
M["unload-package"] = function(package_name)
  local reload = require("plenary.reload")
  return reload.reload_module(package_name)
end
local function rz_modules()
  local text = require("rz.lib.text")
  local function _3_(_241)
    return text["starts-with?"](_241, "rz")
  end
  return vim.tbl_filter(_3_, vim.tbl_keys(package.loaded))
end
local function unload_all()
  for _, package_name in ipairs(rz_modules()) do
    M["unload-package"](package_name)
  end
  return nil
end
M["reload-all"] = function()
  unload_all()
  return M["load-all"]()
end
M["package-name"] = function(file_name)
  local text = require("rz.lib.text")
  local path = require("rz.lib.path")
  if path["starts-with?"](file_name, vim.fn.stdpath("config")) then
    return string.format("rz.config.%s", path["basename-without-extension"](file_name))
  else
    return nil
  end
end
M["reload-package"] = function(package_name)
  M["unload-package"](package_name)
  return require(package_name)
end
return M
