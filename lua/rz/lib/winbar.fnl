(require-macros :macros.core)

(local {: autoload} (require :nfnl.module))
(local vimbar (autoload :rz.lib.vimbar))
(local conditions (require :heirline.conditions))
(local utils (require :heirline.utils))
(local nvim-web-devicons (autoload :nvim-web-devicons))

(fn i [...] (utils.insert ...))

(fn is-active? []
  (= vim.g.actual_curwin (tostring (vim.fn.win_getid))))

(fn is-modified? []
  vim.bo.modified)

(fn is-readonly? []
  (and (not= :terminal vim.bo.buftype)
       (or (not vim.bo.modifiable)
           vim.bo.readonly)))

(fn left-delimiter-hl []
  (if (is-active?)
    :RZWinbarLeftDelimiter
    :RZWinbarLeftDelimiterInactive))

(fn right-delimiter-hl []
  (if (is-active?)
    :RZWinbarRightDelimiter
    :RZWinbarRightDelimiterInactive))

(fn text-hl []
  (if (is-active?)
    :RZWinbarText
    :RZWinbarTextInactive))

(fn flag-hl []
  (if (is-active?)
    :RZWinbarFlag
    :RZWinbarFlagInactive))

(fn spacer-hl []
  (if (is-active?)
    :RZWinbarSpacer
    :RZWinbarSpacerInactive))

(fn icon-hl []
  (if (is-active?)
    :RZWinbarIcon
    :RZWinbarIconInactive))

(fn icon-delimiter-hl []
  (if (is-active?)
    :RZWinbarIconDelimiter
    :RZWinbarIconDelimiterInactive))

; spacer {{{
(local spacer {:provider :%=
               :hl spacer-hl})
; }}}
; delimiter {{{
(local left-delimiter {:provider :█
                       :hl left-delimiter-hl})
(local right-delimiter {:provider :█
                        :hl right-delimiter-hl})
; }}}
; flags {{{
(fn flag [condition text]
  (i {: condition}

     {:provider " "
      :hl flag-hl}

     {:provider text
      :hl flag-hl}))

(local flags [(flag is-modified? :+)
              (flag is-readonly? :)])
; }}}
; filename {{{
(fn sanitized-filename [self]
  (let [filename (vim.fn.fnamemodify self.filename ":.")]
    (if (vimbar.is-special-buffer? 0)
          (vimbar.special-buffer-filename 0)
        (= "" filename)
          "UNNAMED"
        (= :help vim.bo.filetype)
          (vim.fn.fnamemodify self.filename ":t")
        (vimbar.sanitize-filename filename))))

(fn file-icon [self]
  (let [(icon _) (nvim-web-devicons.get_icon_color self.filename)]
    icon))

(local filename (i {:init #(set $1.filename (vim.api.nvim_buf_get_name 0))}

                  {:provider " "
                   :hl text-hl}

                   {:provider #(string.upper (sanitized-filename $1))
                    :hl text-hl}))
; }}}
; diagnostics {{{
(fn get-count [kind]
  (->> {:severity (. vim.diagnostic.severity (string.upper kind))}
      (vim.diagnostic.get 0)
      (length)))

(fn get-icon [kind]
  (let [inflector (require :rz.lib.inflector)
        name (string.format "DiagnosticSign%s"
                            (inflector.capitalize kind))]
    (?. (vim.fn.sign_getdefined name) 1 :text)))

(local diagnostics (i {:condition conditions.has_diagnostics
                       :init (%f (set $1.errors   (get-count :error))
                                 (set $1.warnings (get-count :warn))
                                 (set $1.infos    (get-count :info))
                                 (set $1.hints    (get-count :hint))
                                 (set $1.icons    {:error (get-icon :error)
                                                   :warn  (get-icon :warn)
                                                   :info  (get-icon :info)
                                                   :hint  (get-icon :hint)}))
                       :update (%w DiagnosticChanged BufEnter)}

                      {:condition #(> $1.errors 0)
                       :provider #(string.format " %s%s" $1.icons.error $1.errors)
                       :hl :DiagnosticError}

                      {:condition #(> $1.warnings 0)
                       :provider #(string.format " %s%s" $1.icons.warn $1.warnings)
                       :hl :DiagnosticWarn}

                      {:condition #(> $1.infos 0)
                       :provider #(string.format " %s%s" $1.icons.info $1.infos)
                       :hl :Diagnosticinfo}

                      {:condition #(> $1.hints 0)
                       :provider #(string.format " %s%s" $1.icons.hint $1.hints)
                       :hl :DiagnosticHint}

                      {:provider " ──"
                       :hl spacer-hl}))
; }}}
; test {{{
(fn file-results []
  (let [filename (vim.api.nvim_buf_get_name 0)]
    (or (?. vim.g.rz_test_results filename)
        {:passed 0
         :failed 0
         :skipped 0})))

(fn show-test-results? [self]
  (or
    (> (. (file-results) :passed) 0)
    (> (. (file-results) :failed) 0)))

(fn formatted-results []
  (let [results (file-results)]
    (if
      (> (. results :failed) 0) :✖
      (> (. results :passed) 0) :✔)))

(fn formatted-results-hl []
  (let [results (file-results)]
    (if
      (> (. results :failed) 0) :RZWinbarTestFailed
      (> (. results :passed) 0) :RZWinbarTestPassed)))

(fn formatted-results-delimiter-hl []
  (let [results (file-results)]
    (if
      (> (. results :failed) 0) :RZWinbarTestFailedDelimiter
      (> (. results :passed) 0) :RZWinbarTestPassedDelimiter)))

(local test (i {:update (vimbar.redraw-on-event :User :NeotestDone)}

               (i {:condition show-test-results?}
                  {:provider :╌╌╌╌
                   :hl spacer-hl}

                  {:provider :
                   :hl formatted-results-delimiter-hl}

                  {:provider formatted-results
                   :hl formatted-results-hl}

                  {:provider :
                   :hl formatted-results-delimiter-hl})))
; }}}

[test
 spacer
 diagnostics
 filename
 flags]
