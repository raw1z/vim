(local {: autoload} (require :nfnl.module))
(local path (autoload :rz.lib.path))
(local list (autoload :rz.lib.list))

(local M {})

(fn M.root-path []
  (path.join vim.env.PROJECTS_PATH :entropy))

(fn packages-folder []
  (path.join (M.root-path) :packages))

(fn M.list-packages []
  "return the list of package inside entropy folder"
  (->> (icollect [_ search_pattern (ipairs [:.pkg$ :OWNERS])]
          (path.scan (packages-folder) {:depth 5
                                        :hidden true
                                        :respect_gitignore true
                                        : search_pattern
                                        :strip-basename? false}))
       (list.flatten)
       (vim.tbl_map #(-> (path.relative-from $1 (packages-folder))
                         (vim.fn.fnamemodify ":h")))
       (vim.fn.sort)
       (vim.fn.uniq)))

(fn M.package-path [name]
  (path.join (packages-folder) name))


M
