(require-macros :macros.core)

(local {: autoload} (require :nfnl.module))
(local core (autoload :nfnl.core))
(local str (autoload :nfnl.string))
(local Popup (autoload :nui.popup))
(local Tree (autoload :nui.tree))
(local browser (autoload :rz.lib.browser))
(local buffer (autoload :rz.lib.buffer))
(local digest (autoload :rz.lib.digest))
(local job (autoload :rz.lib.job))
(local line-builder (autoload :rz.lib.github-status.line-builder))
(local node-builder (autoload :rz.lib.github-status.node-builder))
(local notify (autoload :rz.lib.notify))
(local status (autoload :rz.lib.github-status.status))

(local M {})
(local mine-id :MINE)
(local theirs-id :THEIRS)
(local namespace-id (vim.api.nvim_create_namespace :rz-github-status))
(local mark-id 1)
(local node-state {})

(fn build-popup []
  (Popup {:enter true
          :relative :editor
          :focusable true
          :position :50%
          :buf_options {:modifiable false
                        :readonly true}
          :border {:style :single
                   :padding [1 1]}
          :win_options {:winhighlight "Normal:RZGithubStatusNormal,FloatBorder:RZGithusStatusBorder"}
          :size {:width :90%
                 :height :70%}}))

(fn update-tree-if-no-pull-requests [tree parent-id]
  (-> [(node-builder.message :<none>)]
      (tree:set_nodes parent-id))
  (tree:render))

(fn apply-node-state [pull-request]
  (let [matching-state (. node-state pull-request.id)]
    (if (not= nil matching-state)
      (vim.tbl_extend :force pull-request matching-state)
      pull-request)))

(fn apply-state [pull-requests]
  (core.map apply-node-state pull-requests))

(fn update-tree-if-any-pull-requests [tree parent-id pull-requests]
  (-> (core.map node-builder.pull-request (apply-state pull-requests))
      (tree:set_nodes parent-id))
  (tree:render))

(fn update-tree [tree parent-id pull-requests]
  (if (= 0 (length pull-requests))
    (update-tree-if-no-pull-requests tree parent-id)
    (update-tree-if-any-pull-requests tree parent-id pull-requests)))

(fn collapse-node [node]
  (tset node-state node.id {:expanded? false})
  (node:collapse))

(fn expand-node [node]
  (tset node-state node.id {:expanded? true})
  (node:expand))

(fn toggle-current-node [tree]
  (let [node (tree:get_node)]
    (when (= :pr node.kind)
      (if (node:is_expanded)
        (collapse-node node)
        (expand-node node))
      (tree:render))))

(fn create-update-mark [popup]
  (let [opts {:virt_text_pos :right_align
              :id mark-id
              :virt_text [["•••"  :RZGithubStatusExtMarkInv] ]}]
    (vim.api.nvim_buf_set_extmark popup.bufnr
                          namespace-id
                          0 0 opts)))

(fn delete-update-mark [popup]
  (vim.api.nvim_buf_del_extmark popup.bufnr
                        namespace-id
                        mark-id))

(fn refresh-tree [popup tree on-done]
  (create-update-mark popup)
  (var updating-mine true)
  (var updating-theirs true)
  (var updating-renovate true)
  (fn clear-extmark []
    (when (and (= false updating-mine)
               (= false updating-theirs)
               (= false updating-renovate))
      (delete-update-mark popup)
      (when (not= nil on-done)
        (on-done))))
  (status.fetch-mine #(do
                        (update-tree tree mine-id $1)
                        (set updating-mine false)
                        (clear-extmark)))
  (status.fetch-theirs #(do
                          (update-tree tree theirs-id $1)
                          (set updating-theirs false)
                          (clear-extmark))))

(fn copy-url [tree]
  (let [node (tree:get_node)]
    (vim.fn.setreg :+ node.url)
    (notify.info "copied url to clipboard")))

(fn browse [tree]
  (let [node (tree:get_node)]
    (match node.kind
      :check (browser.open node.url)
      :pr    (do
               (if (= mine-id (node:get_parent_id))
                 (browser.open node.url)
                 (browser.open (.. node.url :/files)))))))

(fn jump-to-next-pull-request [tree]
  (fn do-jump [tree line]
    (when (<= line (buffer.last-line-number))
      (local node (tree:get_node line))
      (if (= :pr node.kind)
        (vim.cmd (.. "exe " line))
        (do-jump tree (+ 1 line)))))
  (do-jump tree (+ 1 (buffer.current-line-number))))

(fn jump-to-previous-pull-request [tree]
  (fn do-jump [tree line]
    (when (> line 0)
      (local node (tree:get_node line))
      (if (= :pr node.kind)
        (vim.cmd (.. "exe " line))
        (do-jump tree (- line 1)))))
  (do-jump tree (- (buffer.current-line-number) 1)))

(fn format-message [node template ...]
  (core.str (string.format "[#%s] " node.id)
            (string.format template ...)))

(fn on-job-failed [popup _ message]
  (delete-update-mark popup)
  (notify.error message))

(fn on-job-succeeded [popup tree message]
  (refresh-tree popup tree #(notify.info message)))

(fn run [popup tree node command success failure]
  (create-update-mark popup)
  (job.run-with-timeout
    command
    10_000
    {:on-error   #(if (= :function (type failure))
                    (failure popup tree node)
                    (on-job-failed popup node failure))
     :on-success #(if (= :function (type success))
                    (success popup tree node)
                    (on-job-succeeded popup tree success))}))

(fn checkout-pull-request-branch [popup tree]
  (let [node (tree:get_node)]
    (when (and (= :pr node.kind)
               (not node.matchesCurrentBranch?))
      (let [command (string.format "gh pr checkout %s" node.id)
            failure (format-message node "Checkout failed %s" node.headRefName)
            success (format-message node "Checked out branch %s" node.headRefName)]
        (run popup tree node command success failure)))))

(fn toggle-draft [popup tree]
  (let [node (tree:get_node)]
    (when (= :pr node.kind)
      (var command (string.format "gh pr ready %s" node.id))
      (when (not node.isDraft?)
        (set command (.. command " --undo")))
      (let [failure (format-message node "Toggle draft failed for %s" node.id)
            success (format-message node "Toggled draft for %s" node.id)]
        (run popup tree node command success failure)))))

(fn define-keymaps [popup tree]
  (popup:map :n :q #(popup:unmount))
  (popup:map :n :za #(toggle-current-node tree))
  (popup:map :n :t #(toggle-current-node tree))
  (popup:map :n :r #(refresh-tree popup tree))
  (popup:map :n :<C-l> #(browse tree))
  (popup:map :n :<enter> #(browse tree))
  (popup:map :n :zj #(jump-to-next-pull-request tree))
  (popup:map :n :zk #(jump-to-previous-pull-request tree))
  (popup:map :n :s #(checkout-pull-request-branch popup tree))
  (popup:map :n :o #(toggle-draft popup tree))
  (popup:map :n :y #(copy-url tree)))

(fn get-node-id [node]
  (if (str.blank? (core.str node.id))
    (digest.uuid)
    node.id))

(fn build-tree [popup]
  (let [tree (Tree {:bufnr popup.bufnr
                    :prepare_node line-builder.build
                    :get_node_id get-node-id})
        title-node (node-builder.title "Pull Requests Status")
        mine-node (node-builder.group mine-id "Mine")
        theirs-node (node-builder.group theirs-id "Theirs")]
    (define-keymaps popup tree)

    (tree:add_node title-node)
    (tree:add_node (node-builder.divider))

    (tree:add_node mine-node)
    (mine-node:expand)
    (tree:add_node (node-builder.divider))

    (tree:add_node theirs-node)
    (theirs-node:expand)
    (tree:add_node (node-builder.divider))

    (tree:render)
    tree))

(fn init-tree [popup]
  (let [tree (build-tree popup)]
    (popup:mount)
    (refresh-tree popup tree)))

(fn M.open []
  (-> (build-popup)
      (init-tree)))

M
