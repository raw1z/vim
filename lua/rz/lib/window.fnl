(require-macros :macros.core)

(local M {})

(fn open-horizontal [buffer]
  (-> (.. "sbuffer " buffer)
      (vim.cmd)))

(fn open-vertical [buffer]
  (-> (.. "vert sbuffer " buffer)
      (vim.cmd)))

(fn open-tab [buffer]
  (-> (.. "tab sbuffer "  buffer)
      (vim.cmd)))

(fn open-float [buffer]
  (let [margin-x 10
        margin-y 5
        width (- vim.o.columns (* 2 margin-x))
        height (- vim.o.lines (* 3 margin-y))
        win (vim.api.nvim_open_win buffer true {: width
                                        : height
                                        :col margin-x
                                        :row margin-y
                                        :relative :editor
                                        :style :minimal
                                        :border :rounded})]
    (vim.api.nvim_win_set_option win :winblend 0)))

(fn open-top [buffer]
  (open-horizontal buffer)
  (%> wincmd K))

(fn open-bottom [buffer]
  (open-horizontal buffer)
  (%> wincmd J))

(fn open-left [buffer]
  (open-vertical buffer)
  (%> wincmd H))

(fn open-right [buffer]
  (open-vertical buffer)
  (%> wincmd L))

(fn M.open [buffer direction]
  "open a window showing the given buffer in the given direction"
  (match direction
    :horizontal (open-horizontal buffer)
    :vertical   (open-vertical buffer)
    :tab        (open-tab buffer)
    :float      (open-float buffer)
    :top        (open-top buffer)
    :bottom     (open-bottom buffer)
    :left       (open-left buffer)
    :right      (open-right buffer)))

(fn M.floating? [window]
  "return true if the window is a floating one otherwise return 0"
  (not= "" (. (vim.api.nvim_win_get_config window) :relative)))

M
