-- [nfnl] Compiled from lua/rz/lib/github.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local browser = autoload("rz.lib.browser")
local buffer = autoload("rz.lib.buffer")
local terminal = autoload("rz.lib.terminal")
local M = {}
local pr_pattern = "^#(%d+).*"
local url_pattern = "^.*(https://.*)$"
M["silent-run"] = function(command, ...)
  local pr_id = ...
  local command0 = string.format(command, pr_id)
  return vim.cmd(("silent !gh " .. command0))
end
local function term_run(command, opts)
  local opts0 = (opts or {})
  local start_insert_3f
  if (nil == opts0["start-insert?"]) then
    start_insert_3f = false
  else
    start_insert_3f = opts0["start-insert?"]
  end
  return terminal["direnv-exec"](("gh " .. command), {name = "github", ["start-insert?"] = start_insert_3f, direction = "float"})
end
local function match_pr_3f(line)
  return string.match(line, pr_pattern)
end
local function match_url_3f(line)
  return string.match(line, url_pattern)
end
local function parse_pr_id(line)
  return string.gsub(line, pr_pattern, "%1")
end
local function parse_url(line)
  return string.gsub(line, url_pattern, "%1")
end
local function navigate_to_pr(line)
  return M["silent-run"]("pr view --web %s", parse_pr_id(line))
end
local function navigate_to_url(line)
  return browser.open(parse_url(line))
end
local function navigate()
  local line = buffer["current-line"]()
  if match_pr_3f(line) then
    return navigate_to_pr(line)
  elseif match_url_3f(line) then
    return navigate_to_url(line)
  else
    return nil
  end
end
local function checkout_pr()
  local line = buffer["current-line"]()
  if match_pr_3f(line) then
    local command = string.format("pr checkout %s", parse_pr_id(line))
    return term_run(command, {["start-insert?"] = true})
  else
    return nil
  end
end
local function setup_keymaps()
  local function _5_()
    return vim.cmd("quit")
  end
  vim.keymap.set({"n"}, "q", _5_, {buffer = true, silent = true})
  vim.keymap.set({"n"}, "s", checkout_pr, {buffer = true, silent = true})
  return vim.keymap.set({"n"}, "<Enter>", navigate, {buffer = true, silent = true})
end
local function run(command, ...)
  local command0 = string.format(command, ...)
  term_run(command0)
  return setup_keymaps()
end
local function run_insert(command, ...)
  local command0 = string.format(command, ...)
  term_run(command0, {["start-insert?"] = true})
  return setup_keymaps()
end
M.issues = function()
  return run("pr list --author '@me'")
end
M.checks = function(pr_id)
  return run("pr checks --watch %s", pr_id)
end
local function view_checks()
  local line = buffer["current-line"]()
  if match_pr_3f(line) then
    return M.checks(parse_pr_id(line))
  else
    return nil
  end
end
M.status = function()
  run("pr status")
  return vim.keymap.set({"n"}, "<S-k>", view_checks, {buffer = true, silent = true})
end
M.view = function(pr_id)
  return run("pr view --comments %s", pr_id)
end
M.web = function(pr_id)
  return M["silent-run"]("pr view --web %s", pr_id)
end
M["create-pr"] = function(label)
  return run_insert("pr create --draft --title '%s' --base main --fill", label)
end
return M
