(require-macros :macros.vim)

(local M {})

(fn runtime-files []
  (let [text (require :rz.lib.text)
        scan (require :plenary.scandir)]
    (vim.tbl_filter #(text.ends-with? $1 :.fnl)
                    (-> (vim.fn.join (get-opt rtp) ",")
                        (vim.fn.globpath :lua/rz/config)
                        (scan.scan_dir {:depth 1
                                        :hidden false})))))

(fn load-config []
  (->> (runtime-files)
       (vim.tbl_map M.package-name)
       (vim.tbl_map require)))

(fn M.load-all []
  (require :rz.base)
  (require :rz.keymaps)
  (load-config))

(fn M.unload-package [package-name]
  (let [reload (require :plenary.reload)]
    (reload.reload_module package-name)))

(fn rz-modules []
  "returns the list of rz modules"
  (let [text (require :rz.lib.text)]
    (->> (vim.tbl_keys package.loaded)
         (vim.tbl_filter #(text.starts-with? $1 :rz)))))

(fn unload-all []
  (each [_ package-name (ipairs (rz-modules))]
    (M.unload-package package-name)))

(fn M.reload-all []
  "reload all rz modules"
  (unload-all)
  (M.load-all))

(fn M.package-name [file-name]
  "returns the package name matching the given filename"
  (let [text (require :rz.lib.text)
        path (require :rz.lib.path)]
    (when (path.starts-with? file-name (vim.fn.stdpath :config))
      (->> (path.basename-without-extension file-name)
           (string.format "rz.config.%s")))))

(fn M.reload-package [package-name]
  "reload a module"
  (M.unload-package package-name)
  (require package-name))

M
