-- [nfnl] Compiled from lua/rz/lib/watcher.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
M["watch-file"] = function(path, on_change)
  local event = vim.uv.new_fs_event()
  local function _1_()
    return on_change(path)
  end
  vim.uv.fs_event_start(event, path, {watch_entry = true}, _1_)
  return event
end
M["watch-file-with-debounce"] = function(path, timeout, on_change)
  local debounce = require("rz.lib.debounce")
  local debouncer = debounce.new()
  local function _2_()
    local function _3_()
      return on_change(path)
    end
    return debouncer["schedule-after"](timeout, _3_)
  end
  return M["watch-file"](path, _2_)
end
return M
