-- [nfnl] Compiled from lua/rz/lib/ui.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local core = autoload("nfnl.core")
local list = autoload("rz.lib.list")
local function attach_mappings(prompt_bufnr, _, on_select)
  do
    local actions = require("telescope.actions")
    local actions_state = require("telescope.actions.state")
    local function _2_()
      actions.close(prompt_bufnr)
      return on_select(actions_state.get_selected_entry().value)
    end
    actions.select_default:replace(_2_)
  end
  return true
end
local function make_entry(item, format_display)
  local display = format_display(item)
  return {value = item, ordinal = display, display = display}
end
local function build_picker(items, opts)
  local pickers = require("telescope.pickers")
  local finders = require("telescope.finders")
  local themes = require("telescope.themes")
  local prompt = (opts.prompt or "Select an item")
  local on_select
  local or_3_ = opts["on-select"]
  if not or_3_ then
    local function _4_()
    end
    or_3_ = _4_
  end
  on_select = or_3_
  local selected_item = (opts["selected-item"] or core.first(items))
  local format_display
  local or_5_ = opts["format-display"]
  if not or_5_ then
    local function _6_(x)
      return x
    end
    or_5_ = _6_
  end
  format_display = or_5_
  local conf = require("telescope.config").values
  local opts0 = themes.get_dropdown()
  local function _7_(_241)
    return make_entry(_241, format_display)
  end
  local function _8_(_241, _242)
    return attach_mappings(_241, _242, on_select)
  end
  return pickers.new(opts0, {prompt_title = prompt, finder = finders.new_table({results = items, entry_maker = _7_}), sorter = conf.generic_sorter(opts0), attach_mappings = _8_, default_selection_index = list.index(items, selected_item)})
end
M.select = function(items, opts)
  local picker = build_picker(items, opts)
  return picker:find()
end
M.input = function(...)
  local _9_, _10_, _11_ = ...
  if ((nil ~= _9_) and (nil ~= _10_) and (nil ~= _11_)) then
    local prompt = _9_
    local default = _10_
    local callback = _11_
    local function _12_(_241)
      if _241 then
        return callback(_241)
      else
        return nil
      end
    end
    return vim.ui.input({prompt = string.format("%s: ", prompt), default = default}, _12_)
  elseif ((nil ~= _9_) and (nil ~= _10_)) then
    local prompt = _9_
    local callback = _10_
    local function _14_(_241)
      if _241 then
        return callback(_241)
      else
        return nil
      end
    end
    return vim.ui.input({prompt = string.format("%s: ", prompt)}, _14_)
  else
    return nil
  end
end
return M
