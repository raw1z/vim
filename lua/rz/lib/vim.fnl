(local M {})

(fn extract-buffer-text [[start_row start_col] [end_row end_col]]
  (let [buffer (require :rz.lib.buffer)]
    (buffer.text
      (buffer.current)
      start_row
      start_col
      end_row
      end_col)))

(fn add-operator-func [on-triggered]
  #(let [start (vim.api.nvim_buf_get_mark 0 "[")
         end   (vim.api.nvim_buf_get_mark 0 "]")
         text  (extract-buffer-text start end)]
     (on-triggered {:context $1
                    : start
                    : end
                    : text})
     (tset _G :rz_operatorfunc nil)))

(fn add-operator-keymap-callback [on-triggered]
  (fn []
    (tset _G :rz_operatorfunc (add-operator-func on-triggered))
    (set vim.go.operatorfunc "v:lua.rz_operatorfunc")
    (vim.api.nvim_feedkeys "g@" :n false)))

(fn M.add-operator [mode key custom-opts on-triggered]
  "add an operator for the given modes matching the given key"
  (vim.keymap.set
    mode
    key
    (add-operator-keymap-callback on-triggered)
    custom-opts))

M
