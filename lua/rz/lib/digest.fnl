(local {: autoload} (require :nfnl.module))
(local job (autoload :rz.lib.job))

(local M {})

(fn M.md5 [str]
  (job.capture [:md5 :-q :-s str]))

(fn M.uuid []
  (job.capture [:uuidgen]))

M
