(local utils (require :heirline.utils))

; spacer {{{
(local spacer {:provider :%=})
; }}}
; separator {{{
(local separator {:provider " "})
; }}}
; tab {{{
(fn filename [bufnr]
  (let [bufname (vim.fn.bufname bufnr)
        vimbar (require :rz.lib.vimbar)
        special-name (vimbar.special-buffer-filename bufnr)]
    (if (not= nil special-name)
          special-name
        (not= "" bufname)
          (vim.fn.fnamemodify bufname ":t")
        "[No Name]")))

(fn flags [bufnr]
  (let [bufmodified (vim.fn.getbufvar bufnr :&mod)]
    (if (= 1 bufmodified)
      "[+]"
      "")))

(fn bufnr [tab]
  (let [winnr (vim.fn.tabpagewinnr tab)
        buflist (vim.fn.tabpagebuflist tab)]
    (. buflist winnr)))

(fn is-previous-tab? [tab]
  (= tab (vim.fn.tabpagenr :#)))

(local tab [{:provider :█
             :hl #(if $1.is_active
                    :TabLineTabLeftBorderSel
                    :TabLineTabLeftBorder)}

            {:init #(set $1.is_previous (is-previous-tab? $1.tabnr))
             :provider #(string.format "%%%sT%s%%T" $1.tabpage $1.tabnr)
             :hl #(if $1.is_active :TabLineNumberSel
                      $1.is_previous :TabLineNumberPreviousSel
                      :TabLineNumber)}

            {:provider :█
             :hl #(if $1.is_active
                    :TabLineNumberSeparatorSel
                    :TabLineNumberSeparator)}

            {:provider #(string.format " %s" (filename (bufnr $1.tabnr)))
             :hl #(if $1.is_active :TabLineSel :TabLine)}

            {:provider #(flags (bufnr $1.tabnr))
             :hl #(if $1.is_active :TabLineSel :TabLine)}

            {:provider :█
             :hl #(if $1.is_active
                    :TabLineTabRightBorderSel
                    :TabLineTabRightBorder)}

            separator])
; }}}

(utils.insert {:condition #(> (length (vim.api.nvim_list_tabpages)) 1)}
              (utils.make_tablist tab)
              spacer)
