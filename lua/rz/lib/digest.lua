-- [nfnl] Compiled from lua/rz/lib/digest.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local job = autoload("rz.lib.job")
local M = {}
M.md5 = function(str)
  return job.capture({"md5", "-q", "-s", str})
end
M.uuid = function()
  return job.capture({"uuidgen"})
end
return M
