(require-macros :macros.core)
(require-macros :macros.vim)

(local {: autoload} (require :nfnl.module))
(local core (autoload :nfnl.core))
(local window (autoload :rz.lib.window))
(local digest (autoload :rz.lib.digest))

(local M {})

(fn M.number [buffer]
  "returns the number matching the given buffer (name or number)"
  (if (core.string? buffer)
    (vim.fn.bufnr buffer)
    buffer))

(fn M.current []
  "returns the number of the current buffer"
  (vim.fn.bufnr))

(fn M.window-number [buffer]
  "returns the number of the window show the given buffer (by name or number)"
  (-> buffer
      (M.number)
      (vim.fn.bufwinnr)))

(fn M.buffer-exist? [buffer]
  "returns true if the given number (by name or number) exists, otherwise returns false"
  (not= 0 (vim.fn.bufexists (M.number buffer))))

(fn M.switch-to [buffer]
  "go to the window showing the given buffer (by name or number)"
  (-> (M.window-number buffer)
      (.. "wincmd w")
      (vim.cmd)))

(fn M.scroll-to-bottom [buffer]
  "scroll to the bottom of the window showing the given buffer (by name or number)"
  (-> (M.window-number buffer)
      (.. "windo norm G0")
      (vim.cmd)))

(fn M.delete [buffer]
  "delete the given buffer (by name or number)"
  (when (M.buffer-exist? buffer)
    (vim.api.nvim_buf_delete (M.number buffer) {:force true})))

(fn M.current-word []
  "returns the word under the cursor"
  (-> (vim.fn.expand :<cword>)
      (vim.fn.trim)))

(fn M.current-line []
  "returns the line where the cursor is located"
  (-> (vim.fn.getline :.)
      (vim.fn.trim)))

(fn M.current-line-number []
  "returns the line's number where the cursor is located"
  (vim.fn.line :.))

(fn M.last-line-number []
  "returns the last line's number"
  (vim.fn.line :$))

(fn sanitize-range [line-start column-start line-end column-end]
  (if (> (+ (vim.fn.line2byte line-start) column-start)
         (+ (vim.fn.line2byte line-end) column-end))
    (values line-end column-end line-start column-start)
    (values line-start column-start line-end column-end)))

(fn M.current-visual-selection-range []
  "returns the range (start row, start column, end row, end column) of the currently selected text"
  (let [(_ line-start column-start _) (unpack (vim.fn.getpos :v))
        (_ line-end column-end _) (unpack (vim.fn.getpos :.)) ]
    (sanitize-range line-start column-start line-end column-end)))

(fn M.current-visual-selection []
  "returns the currently selected text"
  (let [mode (vim.fn.mode)
        (line-start column-start line-end column-end) (M.current-visual-selection-range)]
    (if (= mode :V)
      (vim.api.nvim_buf_get_lines 0 (- line-start 1) line-end true)
      (vim.api.nvim_buf_get_text 0 (- line-start 1) (- column-start 1) (- line-end 1) column-end {}))))

(fn M.select-last-pasted-text []
  "visually selects the last pasted text"
  (vim.api.nvim_input "`[v`]"))

(fn M.hashid [buffer]
  "returns an id built from the buffer name"
  (digest.md5 (vim.api.nvim_buf_get_name buffer)))

(fn M.enable-autowrite [buffer]
  "enable auto saving changes for the given buffer"
  (%> write!)
  (set vim.b.autowrite-enabled true)
  (define-augroup (core.str "BufferAutowrite-" (M.hashid buffer))
    (define-autocmd/buffer CursorHold buffer
      (when (and vim.b.autowrite-enabled
                 vim.bo.modified)
        (%> update)
        ; we must return false in order to prevent neovim
        ; from removing the autocommand automatically
        false))))

(fn M.disable-autowrite [buffer]
  "disable auto saving changes for the given buffer"
  (when vim.b.autowrite-enabled
    (set vim.b.autowrite-enabled false)
    (vim.api.nvim_del_augroup_by_name (core.str "BufferAutowrite-" (M.hashid buffer)))))

(fn M.toggle-autowrite [buffer]
  "toggle auto saving changes for the given buffer"
  (if vim.b.autowrite-enabled
    (M.disable-autowrite buffer)
    (M.enable-autowrite buffer)))

(fn M.new [direction opts]
  "create a buffer and open it in the given direction"
  (let [opts (or opts {})
        direction (or direction :horizontal)
        listed (if (core.nil? opts.listed) false opts.listed)
        scratch (if (core.nil? opts.scratch) true opts.scratch)
        bufnr (vim.api.nvim_create_buf listed scratch)]
    (window.open bufnr direction)
    bufnr))

(fn M.filetype [buffer]
  "returns the filetype for the buffer (by name or number)"
  (let [args (if (= :number (type buffer)) {:buf buffer} {:filename buffer})]
    (vim.filetype.match args)))

(fn M.name [bufnr]
  "returns the name for the buffer matching given buffer number"
  (vim.fn.bufname bufnr))

(fn M.text [buffer start-row start-col end-row end-col]
  "returns the text in the given bounds formatted as array of lines"
  (if (and (not= start-row end-row) (= 0 start-col) (= 0 end-col))
    (vim.api.nvim_buf_get_lines 0 (- start-row 1) end-row true)
    (vim.api.nvim_buf_get_text buffer (- start-row 1) start-col (- end-row 1) (+ end-col 1) {})))

(fn M.content-as-string [buffer]
  "returns a string corresponding to the entire content of the buffer"
  (-> (vim.api.nvim_buf_get_lines buffer 0 -1 false)
      (vim.fn.join "\n")))

(fn M.floating? [buffer]
  "return true if the buffer is displayed in a floating window otherwise return false"
  (vim.tbl_contains (->> (vim.api.nvim_list_wins)
                         (vim.tbl_filter window.floating?)
                         (vim.tbl_map vim.api.nvim_win_get_buf))
                    buffer))

(fn M.dirname [buffer]
  "return the folder which contains the given buffer's file"
  (-> (M.filename buffer true)
      (vim.fn.fnamemodify ":h")
      (vim.fn.fnamemodify ":p")))

(fn M.filename [buffer full?]
  "return the filename for the given buffer"
  (let [full-path (vim.api.nvim_buf_get_name buffer)]
    (if full?
      full-path
      (vim.fn.fnamemodify full-path ":."))))

(fn M.copy-path [buffer include-line? full?]
  "copy the path for the given buffer"
  (let [path (M.filename buffer full?)]
    (if include-line?
      (string.format "%s:%s"
                     path
                     (vim.fn.line :.))
      path)))

M
