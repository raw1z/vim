-- [nfnl] Compiled from lua/rz/lib/github-status.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local core = autoload("nfnl.core")
local str = autoload("nfnl.string")
local Popup = autoload("nui.popup")
local Tree = autoload("nui.tree")
local browser = autoload("rz.lib.browser")
local buffer = autoload("rz.lib.buffer")
local digest = autoload("rz.lib.digest")
local job = autoload("rz.lib.job")
local line_builder = autoload("rz.lib.github-status.line-builder")
local node_builder = autoload("rz.lib.github-status.node-builder")
local notify = autoload("rz.lib.notify")
local status = autoload("rz.lib.github-status.status")
local M = {}
local mine_id = "MINE"
local theirs_id = "THEIRS"
local namespace_id = vim.api.nvim_create_namespace("rz-github-status")
local mark_id = 1
local node_state = {}
local function build_popup()
  return Popup({enter = true, relative = "editor", focusable = true, position = "50%", buf_options = {readonly = true, modifiable = false}, border = {style = "single", padding = {1, 1}}, win_options = {winhighlight = "Normal:RZGithubStatusNormal,FloatBorder:RZGithusStatusBorder"}, size = {width = "90%", height = "70%"}})
end
local function update_tree_if_no_pull_requests(tree, parent_id)
  tree:set_nodes({node_builder.message("<none>")}, parent_id)
  return tree:render()
end
local function apply_node_state(pull_request)
  local matching_state = node_state[pull_request.id]
  if (nil ~= matching_state) then
    return vim.tbl_extend("force", pull_request, matching_state)
  else
    return pull_request
  end
end
local function apply_state(pull_requests)
  return core.map(apply_node_state, pull_requests)
end
local function update_tree_if_any_pull_requests(tree, parent_id, pull_requests)
  tree:set_nodes(core.map(node_builder["pull-request"], apply_state(pull_requests)), parent_id)
  return tree:render()
end
local function update_tree(tree, parent_id, pull_requests)
  if (0 == #pull_requests) then
    return update_tree_if_no_pull_requests(tree, parent_id)
  else
    return update_tree_if_any_pull_requests(tree, parent_id, pull_requests)
  end
end
local function collapse_node(node)
  node_state[node.id] = {["expanded?"] = false}
  return node:collapse()
end
local function expand_node(node)
  node_state[node.id] = {["expanded?"] = true}
  return node:expand()
end
local function toggle_current_node(tree)
  local node = tree:get_node()
  if ("pr" == node.kind) then
    if node:is_expanded() then
      collapse_node(node)
    else
      expand_node(node)
    end
    return tree:render()
  else
    return nil
  end
end
local function create_update_mark(popup)
  local opts = {virt_text_pos = "right_align", id = mark_id, virt_text = {{"\226\128\162\226\128\162\226\128\162", "RZGithubStatusExtMarkInv"}}}
  return vim.api.nvim_buf_set_extmark(popup.bufnr, namespace_id, 0, 0, opts)
end
local function delete_update_mark(popup)
  return vim.api.nvim_buf_del_extmark(popup.bufnr, namespace_id, mark_id)
end
local function refresh_tree(popup, tree, on_done)
  create_update_mark(popup)
  local updating_mine = true
  local updating_theirs = true
  local updating_renovate = true
  local function clear_extmark()
    if ((false == updating_mine) and (false == updating_theirs) and (false == updating_renovate)) then
      delete_update_mark(popup)
      if (nil ~= on_done) then
        return on_done()
      else
        return nil
      end
    else
      return nil
    end
  end
  local function _8_(_241)
    update_tree(tree, mine_id, _241)
    updating_mine = false
    return clear_extmark()
  end
  status["fetch-mine"](_8_)
  local function _9_(_241)
    update_tree(tree, theirs_id, _241)
    updating_theirs = false
    return clear_extmark()
  end
  return status["fetch-theirs"](_9_)
end
local function copy_url(tree)
  local node = tree:get_node()
  vim.fn.setreg("+", node.url)
  return notify.info("copied url to clipboard")
end
local function browse(tree)
  local node = tree:get_node()
  local _10_ = node.kind
  if (_10_ == "check") then
    return browser.open(node.url)
  elseif (_10_ == "pr") then
    if (mine_id == node:get_parent_id()) then
      return browser.open(node.url)
    else
      return browser.open((node.url .. "/files"))
    end
  else
    return nil
  end
end
local function jump_to_next_pull_request(tree)
  local function do_jump(tree0, line)
    if (line <= buffer["last-line-number"]()) then
      local node = tree0:get_node(line)
      if ("pr" == node.kind) then
        return vim.cmd(("exe " .. line))
      else
        return do_jump(tree0, (1 + line))
      end
    else
      return nil
    end
  end
  return do_jump(tree, (1 + buffer["current-line-number"]()))
end
local function jump_to_previous_pull_request(tree)
  local function do_jump(tree0, line)
    if (line > 0) then
      local node = tree0:get_node(line)
      if ("pr" == node.kind) then
        return vim.cmd(("exe " .. line))
      else
        return do_jump(tree0, (line - 1))
      end
    else
      return nil
    end
  end
  return do_jump(tree, (buffer["current-line-number"]() - 1))
end
local function format_message(node, template, ...)
  return core.str(string.format("[#%s] ", node.id), string.format(template, ...))
end
local function on_job_failed(popup, _, message)
  delete_update_mark(popup)
  return notify.error(message)
end
local function on_job_succeeded(popup, tree, message)
  local function _17_()
    return notify.info(message)
  end
  return refresh_tree(popup, tree, _17_)
end
local function run(popup, tree, node, command, success, failure)
  create_update_mark(popup)
  local function _18_()
    if ("function" == type(failure)) then
      return failure(popup, tree, node)
    else
      return on_job_failed(popup, node, failure)
    end
  end
  local function _20_()
    if ("function" == type(success)) then
      return success(popup, tree, node)
    else
      return on_job_succeeded(popup, tree, success)
    end
  end
  return job["run-with-timeout"](command, 10000, {["on-error"] = _18_, ["on-success"] = _20_})
end
local function checkout_pull_request_branch(popup, tree)
  local node = tree:get_node()
  if (("pr" == node.kind) and not node["matchesCurrentBranch?"]) then
    local command = string.format("gh pr checkout %s", node.id)
    local failure = format_message(node, "Checkout failed %s", node.headRefName)
    local success = format_message(node, "Checked out branch %s", node.headRefName)
    return run(popup, tree, node, command, success, failure)
  else
    return nil
  end
end
local function toggle_draft(popup, tree)
  local node = tree:get_node()
  if ("pr" == node.kind) then
    local command = string.format("gh pr ready %s", node.id)
    if not node["isDraft?"] then
      command = (command .. " --undo")
    else
    end
    local failure = format_message(node, "Toggle draft failed for %s", node.id)
    local success = format_message(node, "Toggled draft for %s", node.id)
    return run(popup, tree, node, command, success, failure)
  else
    return nil
  end
end
local function define_keymaps(popup, tree)
  local function _25_()
    return popup:unmount()
  end
  popup:map("n", "q", _25_)
  local function _26_()
    return toggle_current_node(tree)
  end
  popup:map("n", "za", _26_)
  local function _27_()
    return toggle_current_node(tree)
  end
  popup:map("n", "t", _27_)
  local function _28_()
    return refresh_tree(popup, tree)
  end
  popup:map("n", "r", _28_)
  local function _29_()
    return browse(tree)
  end
  popup:map("n", "<C-l>", _29_)
  local function _30_()
    return browse(tree)
  end
  popup:map("n", "<enter>", _30_)
  local function _31_()
    return jump_to_next_pull_request(tree)
  end
  popup:map("n", "zj", _31_)
  local function _32_()
    return jump_to_previous_pull_request(tree)
  end
  popup:map("n", "zk", _32_)
  local function _33_()
    return checkout_pull_request_branch(popup, tree)
  end
  popup:map("n", "s", _33_)
  local function _34_()
    return toggle_draft(popup, tree)
  end
  popup:map("n", "o", _34_)
  local function _35_()
    return copy_url(tree)
  end
  return popup:map("n", "y", _35_)
end
local function get_node_id(node)
  if str["blank?"](core.str(node.id)) then
    return digest.uuid()
  else
    return node.id
  end
end
local function build_tree(popup)
  local tree = Tree({bufnr = popup.bufnr, prepare_node = line_builder.build, get_node_id = get_node_id})
  local title_node = node_builder.title("Pull Requests Status")
  local mine_node = node_builder.group(mine_id, "Mine")
  local theirs_node = node_builder.group(theirs_id, "Theirs")
  define_keymaps(popup, tree)
  tree:add_node(title_node)
  tree:add_node(node_builder.divider())
  tree:add_node(mine_node)
  mine_node:expand()
  tree:add_node(node_builder.divider())
  tree:add_node(theirs_node)
  theirs_node:expand()
  tree:add_node(node_builder.divider())
  tree:render()
  return tree
end
local function init_tree(popup)
  local tree = build_tree(popup)
  popup:mount()
  return refresh_tree(popup, tree)
end
M.open = function()
  return init_tree(build_popup())
end
return M
