-- [nfnl] Compiled from lua/rz/lib/inflector.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local core = autoload("nfnl.core")
local text = autoload("rz.lib.text")
local M = {}
local function contain_separator_3f(str)
  return (nil ~= string.find(str, "[%s%._%-/]"))
end
M["common-separators"] = function(str)
  if contain_separator_3f(str) then
    return "\\s\\|[._\\-/]"
  else
    return "[A-Z]\\?[a-z]*\\zs"
  end
end
M.split = function(str, opts)
  local _let_3_ = (opts or {})
  local separators = _let_3_["sep"]
  local separators0 = (separators or M["common-separators"](str))
  local function _4_(_241)
    return vim.fn.tolower(_241)
  end
  return core.map(_4_, vim.fn.split(str, separators0))
end
M["split-join"] = function(str, joiner, opts)
  return vim.fn.join(M.split(str, opts), joiner)
end
M["split-map-join"] = function(str, transform, joiner, opts)
  local function _5_(_241)
    return vim.fn.join(_241, joiner)
  end
  return _5_(core.map(transform, M.split(str, opts)))
end
M.constantize = function(str, opts)
  return M["split-map-join"](str, vim.fn.toupper, "_", opts)
end
M.capitalize = function(str)
  return vim.fn.substitute(vim.fn.tolower(str), "\\w", "\\=toupper(submatch(0))", "")
end
M.pascalize = function(str, opts)
  return M["split-map-join"](str, M.capitalize, "", opts)
end
M.camelize = function(str, opts)
  return vim.fn.substitute(M.pascalize(str, opts), "^\\w", "\\=tolower(submatch(0))", "")
end
M.titleize = function(str)
  return M["split-map-join"](str, M.capitalize, " ")
end
M.underscore = function(str, opts)
  return M["split-join"](str, "_", opts)
end
M.dasherize = function(str, opts)
  return M["split-join"](str, "-", opts)
end
M.dotify = function(str, opts)
  return M["split-join"](str, ".", opts)
end
M.slashify = function(str, opts)
  return M["split-join"](str, "/", opts)
end
M.classify = function(str, opts)
  return M["split-map-join"](str, M.capitalize, "::", opts)
end
return M
