(require-macros :macros.core)
(require-macros :macros.vim)

(local M {})

(fn create-scratch-buffer [direction]
  (let [buffer (require :rz.lib.buffer)]
    (doto (buffer.new direction)
          (vim.api.nvim_buf_set_var :special_buffer_name "Draft")
          (vim.api.nvim_buf_set_var :is_special_buffer true))))

(fn M.folder []
  "returns the location of the junk folder"
  (let [path (require :rz.lib.path)]
    (path.join (vim.fn.stdpath :data) :junk)))

(fn junk-file-path [opts]
  (let [path (require :rz.lib.path)]
    (->> opts.suffix
         (.. (os.date "%Y/%m/%d/%H_%M_%S"))
         (path.join (M.folder)))))

(fn M.junk-file [opts]
  "creates and returns the path to a new junk file"
  (let [path (require :rz.lib.path)
        file-path (junk-file-path opts)]
    (vim.fn.mkdir (path.dirname file-path) :p)
    (vim.fn.writefile [] file-path)
    file-path))

(fn setup-autowrite [bufnr]
  (let [buffer (require :rz.lib.buffer)]
    (buffer.enable-autowrite bufnr))
  bufnr)

(fn create-junk-buffer [direction opts]
  (let [name (M.junk-file opts)
        buffer (require :rz.lib.buffer)
        bufnr (buffer.new direction {:listed true
                                     :scratch false})]
    (vim.api.nvim_buf_set_name bufnr name)
    (setup-autowrite bufnr)))

(fn create-buffer [direction opts]
  (if opts.scratch?
    (create-scratch-buffer direction)
    (create-junk-buffer direction opts)))

(fn configure-buffer [bufnr filetype]
  (vim.api.nvim_set_option_value :filetype
                                 filetype
                                 {:buf bufnr})
  bufnr)

(fn setup-keymaps [bufnr opts]
  (when opts.scratch?
    (define-keys/buffer bufnr
      n <C-o>   (%> echo noop)
      n <C-i>   (%> echo :noop)))
  bufnr)

(fn M.create [direction filetype opts]
  "creates a given filetype's draft buffer opened in the given direction and return its number"
  (let [opts (or opts {:scratch? true
                       :suffix ""})]
    (-> (create-buffer direction opts)
        (configure-buffer filetype)
        (setup-keymaps opts))))

M
