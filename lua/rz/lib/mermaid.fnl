(local {: autoload} (require :nfnl.module))
(local job (autoload :rz.lib.job))
(local notify (autoload :rz.lib.notify))

(local M {:config {:width  1200
                   :height 800
                   :scale  2
                   :file "~/.config/nvim/.mermaid-config.json"}})

(fn update-command [src dest]
  (string.format "mmdc -c %s -i %s -o %s --width %s --height %s --scale %s"
                 M.config.file
                 src
                 dest
                 M.config.width
                 M.config.height
                 M.config.scale))

(fn M.compile [src dest callback]
  (-> (update-command src dest)
      (job.async-run {:on-success callback}))
  (notify.trace "compiling diagram..."))

M
