(local {: autoload} (require :nfnl.module))
(local core (autoload :nfnl.core))
(local str (autoload :nfnl.string))
(local terminal (autoload :rz.lib.terminal))
(local git (autoload :rz.lib.git))

(local M {})

(fn M.run [location]
  "run a test in a terminal"
  (terminal.direnv-exec
    (.. "rspec --format progress --no-profile " location " 2>/dev/null")
    {:name :rspec}))

(fn working-files []
  (->> (git.status)
       (core.filter #(not= $1.status :deleted))
       (core.map #(. $1 :file))))

(fn branch-changed-files []
  (let [trunk-branch (git.guess-trunk-branch)
        current-branch (git.current-branch)]
    (if (or (str.blank? trunk-branch)
            (str.blank? current-branch))
      []
      (->> (git.changed-files-between-branches trunk-branch current-branch)
           (core.map #$1.path)))))

(fn matching-test-file [path]
  (if (not= -1 (vim.fn.stridx path "_spec.rb"))
    path
    (-> path
        (vim.fn.substitute "/app/" "/spec/" "")
        (vim.fn.substitute ".rb" "_spec.rb" ""))))

(fn M.test-files-for-current-branch []
  "returns test files in the working space and by comparing with the trunk"
  (->> (core.concat (working-files) (branch-changed-files))
       (core.filter #(string.match $1 ".rb$"))
       (core.map matching-test-file)
       (core.filter #(not= -1 (vim.fn.stridx $1 (vim.fn.getcwd))))
       (core.filter #(not= 0 (vim.fn.filereadable $1)))
       (core.map #(vim.fn.fnamemodify $1 ":~:."))
       (vim.fn.sort)
       (vim.fn.uniq)))

M
