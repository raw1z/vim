(local M {})

(fn M.parse-plugins [...]
  (accumulate [[plugins opts] [[] {}]
               _ item (ipairs [...])]
    (if (= :table (type item))
      [plugins (tset opts (. plugins (length plugins)) item)]
      [(vim.fn.extend plugins [(tostring item)]) opts])))

(fn M.build-specs [plugins opts]
  (icollect [_ plugin (ipairs plugins)]
    (let [plugin-opts (or (. opts plugin) {})]
      (tset plugin-opts 1 plugin))))

(fn M.declare [url opts]
  (vim.tbl_extend :force (or opts {}) {1 url}))

(fn M.key [mode lhs rhs opts]
  (vim.tbl_extend :force
                  (or opts {})
                  {1 lhs
                   2 rhs
                   :silent true
                   : mode}))

M
