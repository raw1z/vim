(local M {})

(fn run [command input]
  (let [job (require :rz.lib.job)]
    (->> (vim.inspect input)
         (string.format "fish -c '%s \"%s\"'" command)
         (job.async-run))
    input))

(fn M.rlog [input]
  (run :rlog input))

(fn M.flog [input]
  (run :flog input))

M
