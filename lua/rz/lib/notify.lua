-- [nfnl] Compiled from lua/rz/lib/notify.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
local function format_message(...)
  return vim.fn.join({" ", ..., " "}, " ")
end
local function log(kind, ...)
  return Snacks.notify[kind](format_message(...))
end
M.error = function(...)
  return log("error", ...)
end
M.warn = function(...)
  return log("warn", ...)
end
M.info = function(...)
  return log("info", ...)
end
M.trace = function(...)
  local tracer = require("rz.lib.tracer")
  return tracer.print(format_message(...))
end
M.dismiss = function()
  return Snacks.notifier.hide()
end
return M
