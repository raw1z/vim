(local {: autoload} (require :nfnl.module))
(local core (autoload :nfnl.core))
(local str (autoload :nfnl.string))

(local M {})

(fn M.compact [list]
  "returns a new list with all blank elements removed"
  (core.filter #(not (str.blank? $1)) list))

(fn M.fuzzy-filter [list search]
  "returns a list filtered using fuzzy matching the given search"
  (if (str.blank? search)
    list
    (vim.fn.matchfuzzy list search)))

(fn M.map-strip [list removed-tokens]
  "returns a new list where all the first occurrence of each removed token has been stripped from each list item"
  (->> list
       (core.map #(accumulate [result $1
                               _ removed-token (ipairs removed-tokens)]
                    (vim.fn.substitute result removed-token "" "")))))

(fn M.chunk [list count]
  "return a list of lists containing count elements each"
  (core.last
    (accumulate [[acc results] [[] []]
                 _ item (ipairs list)]
      (let [new-acc (core.concat acc [item])]
        (if (< (length acc) (- count 1))
          [new-acc results]
          [[] (core.concat results [new-acc])])))))

(fn M.contains? [list value]
  "check if the given list contains the given value"
  (vim.tbl_contains list value))

(fn M.empty? [list]
  "returns true if the given list is empty otherwise returns false"
  (vim.tbl_isempty list))

(fn M.index [list value]
  "returns the lowest index in the given list where the item has the given value.
  If the given value is not found then returns nil"
  (let [index (vim.fn.index list value)]
    (if (= -1 index) nil (+ 1 index))))

(fn M.flatten [list]
  "returns a flatten list from the input"
  (-> (vim.iter list)
      (: :flatten 10)
      (: :totable)))

(fn M.join [list sep]
  "returns a string built by joining the list items with the separator"
  (vim.fn.join list (or sep "")))

(fn M.flat-join [list sep]
  "returns a string built by flattening the given list and joining its items with the separator"
  (M.join (M.flatten list) sep))

M
