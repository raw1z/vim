-- [nfnl] Compiled from lua/rz/lib/window.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
local function open_horizontal(buffer)
  return vim.cmd(("sbuffer " .. buffer))
end
local function open_vertical(buffer)
  return vim.cmd(("vert sbuffer " .. buffer))
end
local function open_tab(buffer)
  return vim.cmd(("tab sbuffer " .. buffer))
end
local function open_float(buffer)
  local margin_x = 10
  local margin_y = 5
  local width = (vim.o.columns - (2 * margin_x))
  local height = (vim.o.lines - (3 * margin_y))
  local win = vim.api.nvim_open_win(buffer, true, {width = width, height = height, col = margin_x, row = margin_y, relative = "editor", style = "minimal", border = "rounded"})
  return vim.api.nvim_win_set_option(win, "winblend", 0)
end
local function open_top(buffer)
  open_horizontal(buffer)
  return vim.cmd("wincmd K")
end
local function open_bottom(buffer)
  open_horizontal(buffer)
  return vim.cmd("wincmd J")
end
local function open_left(buffer)
  open_vertical(buffer)
  return vim.cmd("wincmd H")
end
local function open_right(buffer)
  open_vertical(buffer)
  return vim.cmd("wincmd L")
end
M.open = function(buffer, direction)
  if (direction == "horizontal") then
    return open_horizontal(buffer)
  elseif (direction == "vertical") then
    return open_vertical(buffer)
  elseif (direction == "tab") then
    return open_tab(buffer)
  elseif (direction == "float") then
    return open_float(buffer)
  elseif (direction == "top") then
    return open_top(buffer)
  elseif (direction == "bottom") then
    return open_bottom(buffer)
  elseif (direction == "left") then
    return open_left(buffer)
  elseif (direction == "right") then
    return open_right(buffer)
  else
    return nil
  end
end
M["floating?"] = function(window)
  return ("" ~= vim.api.nvim_win_get_config(window).relative)
end
return M
