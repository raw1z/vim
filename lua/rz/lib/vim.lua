-- [nfnl] Compiled from lua/rz/lib/vim.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
local function extract_buffer_text(_1_, _2_)
  local start_row = _1_[1]
  local start_col = _1_[2]
  local end_row = _2_[1]
  local end_col = _2_[2]
  local buffer = require("rz.lib.buffer")
  return buffer.text(buffer.current(), start_row, start_col, end_row, end_col)
end
local function add_operator_func(on_triggered)
  local function _3_(_241)
    local start = vim.api.nvim_buf_get_mark(0, "[")
    local _end = vim.api.nvim_buf_get_mark(0, "]")
    local text = extract_buffer_text(start, _end)
    on_triggered({context = _241, start = start, ["end"] = _end, text = text})
    _G["rz_operatorfunc"] = nil
    return nil
  end
  return _3_
end
local function add_operator_keymap_callback(on_triggered)
  local function _4_()
    _G["rz_operatorfunc"] = add_operator_func(on_triggered)
    vim.go.operatorfunc = "v:lua.rz_operatorfunc"
    return vim.api.nvim_feedkeys("g@", "n", false)
  end
  return _4_
end
M["add-operator"] = function(mode, key, custom_opts, on_triggered)
  return vim.keymap.set(mode, key, add_operator_keymap_callback(on_triggered), custom_opts)
end
return M
