(require-macros :macros.core)

(local M {})

(fn M.open [path direction]
  "open a buffer following given direction to browse the content of the folder at the given path"
  (let [buffer (require :rz.lib.buffer)
        oil (require :oil)]
    (buffer.new direction)
    (oil.open path)))

(fn M.open-current-buffer-file-dirname [direction]
  "open a buffer following given direction to browse the content of the current folder"
  (-> (vim.fn.expand "%:p:h")
      (M.open direction)))

(fn M.open-project-root-directory [direction]
  "open a buffer following given direction to browse the content of the current project's root folder"
  (-> (vim.fn.projectroot#guess)
      (M.open direction)))

(fn M.go-to-folder-in-new-tab [path]
  (M.open path :tab)
  (vim.cmd (.. "lcd " path)))

(fn M.contains? [file dir-path]
  "return true if the directory at the given path contains the given file.
  if the directory's path is not given, consider working directory"
  (let [path (require :rz.lib.path)]
    (-> (or dir-path (vim.fn.getcwd))
        (path.join file)
        (path.exist?))))

M
