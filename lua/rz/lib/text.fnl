(local {: autoload} (require :nfnl.module))
(local core (autoload :nfnl.core))

(local M {})

(fn M.last-part [str sep]
  "returns the last part after given string has been splitted using given separator"
  (-> (vim.split str sep {:plain true})
      (core.last)))

(fn M.first-part [str sep]
  "returns the first part after given string has been splitted using given separator"
  (-> (vim.split str sep {:plain true})
      (core.first)))

(fn M.find-index [str pattern]
  "returns the index where the given string matches the given pattern if found otherwise returns -1"
  (let [(start _) (string.find str pattern 1 true)]
    (or start -1)))

(fn M.contains? [str pattern]
  "returns true if the given string contains the given pattern"
  (not= -1 (M.find-index str pattern)))

(fn M.matches? [str pattern]
  "return true if the given string matches the given pattern otherwise returns false"
  (not= -1 (M.find-index str pattern)))

(fn M.ends-with? [str suffix]
  "return true if the given string ends with the given suffix otherwise returns false"
  (vim.endswith str suffix))

(fn M.starts-with? [str prefix]
  "return true if the given string starts with the given prefix otherwise returns false"
  (vim.startswith str prefix))

(fn M.append [str suffix]
  "return a new string with the given suffix added at the end of the given string"
  (.. str suffix))

(fn M.prepend [str prefix]
  "return a new string with the given prefix added at the beginning of the given string"
  (.. prefix str))

(fn M.replace [str pattern replacement]
  "return a string in which the first occurrence of the given pattern was replaced by the given replacement"
  (if (not (core.table? pattern))
    (string.gsub str pattern replacement 1)
    (accumulate [acc str
                 _ item (ipairs pattern)]
      (M.replace acc item replacement))))

(fn M.replace-all [str pattern replacement]
  "return a string in which all occurrences of the given pattern was replaced by the given replacement"
  (if (not (core.table? pattern))
    (string.gsub str pattern replacement 1)
    (accumulate [acc str
                 _ item (ipairs pattern)]
      (M.replace-all acc item replacement))))

(fn M.clear [str pattern]
  "return a string in which all occurrences of the given pattern was removed"
  (M.replace-all str pattern ""))

(fn M.split [str pattern count]
  "splits a string following the given pattern and returns the tokens.
  If a count is not given then all the tokens are returned.
  If a count is given only count tokens are returned"
  (let [tokens (vim.split str pattern)]
    (if (or (= nil count) (<= count 0))
      tokens
      (let [tokens-count (length tokens)
            real-count (if (> count tokens-count) tokens-count count)
            head (vim.list_slice tokens 1 (- real-count 1))
            rest (-> (vim.list_slice tokens real-count)
                     (table.concat " "))]
        (core.concat head [rest])))))

(fn M.lines [str]
  "splits the given string by lines"
  (M.split str "\\n"))

M
