(require-macros :macros.core)

(local M {})

(fn parse-xresources-file []
  (->> "~/.Xresources"
       (vim.fn.expand)
       (vim.fn.resolve)
       (vim.fn.readfile)
       (vim.tbl_filter #(= 0 (vim.fn.stridx $1 :*.)))
       (vim.tbl_map #(vim.fn.substitute $1 "\\v\\*\\." "" :g))
       (vim.tbl_map #(vim.fn.substitute $1 "\\v\\s+" "" :g))
       (vim.tbl_map #(vim.fn.split $1 ":"))))

(fn xresources-colors []
  (let [(ok? colors-data) (pcall parse-xresources-file)]
    (if (not ok?)
      {}
      (accumulate [colors {}
                   _ [key value] (ipairs colors-data)]
                  (doto colors (tset key value))))))

(fn M.set []
  (set vim.g.rz_colors (xresources-colors)))

(fn M.reload []
  (set vim.g.rz_colors nil)
  (%> colorscheme rz))

(fn M.colorscheme-dark? []
  (= (. vim.g.rz_colors :colorscheme)
     :dark))

(fn M.get-color [color-name]
  (let [(dark light) (color-name:match "(.+)|(.+)")]
    (if (and dark light)
      (if (M.colorscheme-dark?)
        (. vim.g.rz_colors dark)
        (. vim.g.rz_colors light))
      (. vim.g.rz_colors color-name))))

M
