(require-macros :macros.core)
(require-macros :macros.vim)

(local {: autoload} (require :nfnl.module))
(local buffer (autoload :rz.lib.buffer))
(local browser (autoload :rz.lib.browser))
(local ts_utils (autoload :nvim-treesitter.ts_utils))
(local list (autoload :rz.lib.list))

(local M {})
(local query-string "
  (call arguments: (argument_list .
                                  (string (string_content) @gem-name) .
                                  (pair key: (hash_key_symbol) @symbol
                                        value: (string (string_content) @repo))?))")

(fn node-at-cursor []
  (var node (ts_utils.get_node_at_cursor))
  (var parent (node:parent))
  (let [start-row (node:start)]
    (while (and (not= nil parent) (= start-row (parent:start)))
      (set node parent)
      (set parent (node:parent)))
    node))

(fn captures-for-node [node]
  (let [bufnr (vim.api.nvim_get_current_buf)
        query (vim.treesitter.query.parse :ruby query-string)
        captures (icollect [_ capture (query:iter_captures node bufnr)]
                   (vim.treesitter.get_node_text capture bufnr))]
    (list.flatten captures)))

(fn uri-for-captures [captures]
  (match captures
    [_ :git repo]
      repo
    [gem-name]
      (.. "https://rubygems.org/gems/" gem-name)))

(fn browse-gem []
  (%> "norm _")
  (when (= :gem (buffer.current-word))
    (-> (node-at-cursor)
        (captures-for-node)
        (uri-for-captures)
        (browser.open))))

(fn M.setup []
  (when (= :Gemfile (vim.fn.expand :<afile>))
    (define-key/this-buffer n <Enter> browse-gem)))


M
