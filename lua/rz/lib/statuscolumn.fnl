(require-macros :macros.vim)

(fn real-line-number [number]
  (when (= 0 vim.v.virtnum)
    number))

(fn show-relative-number? []
  (and (get-opt relativenumber)
       (not= vim.v.relnum 0)))

(fn line-number []
  (if (show-relative-number?)
    (real-line-number vim.v.relnum)
    (when (get-opt number)
      (real-line-number vim.v.lnum))))

; spacer {{{
(local spacer {:provider :%=})
; }}}
; separator {{{
(local separator {:provider " "})
; }}}
; line-number {{{
(local line-number {:provider line-number})
; }}}
; sign {{{
(local sign {:provider :%s})
; }}}
; fold {{{
(local fold {:provider "%C "})
; }}}

[spacer
 line-number
 separator
 sign
 fold
 separator]
