(local M {})
(local {: autoload} (require :nfnl.module))
(local core (autoload :nfnl.core))
(local list (autoload :rz.lib.list))

(fn attach-mappings [prompt-bufnr _ on-select]
  (let [actions (require :telescope.actions)
        actions-state (require :telescope.actions.state)]
    (actions.select_default:replace
      #(do
         (actions.close prompt-bufnr)
         (-> (actions-state.get_selected_entry)
             (. :value)
             (on-select)))))
  true)

(fn make-entry [item format-display]
  (let [display (format-display item)]
    {:value item
     :ordinal display
     : display}))

(fn build-picker [items opts]
  (let [pickers (require :telescope.pickers)
        finders (require :telescope.finders)
        themes (require :telescope.themes)
        prompt (or opts.prompt "Select an item")
        on-select (or opts.on-select (fn []))
        selected-item (or opts.selected-item (core.first items))
        format-display (or opts.format-display (fn [x] x))
        conf (. (require :telescope.config) :values)
        opts (themes.get_dropdown)]
    (pickers.new opts {:prompt_title prompt
                       :finder (finders.new_table {:results items
                                                   :entry_maker #(make-entry $1 format-display)})
                       :sorter (conf.generic_sorter opts)
                       :attach_mappings #(attach-mappings $1 $2 on-select)
                       :default_selection_index (list.index items selected-item)})))

(fn M.select [items opts]
  "Select between a list of string items"
  (let [picker (build-picker items opts)]
    (picker:find)))

(fn M.input [...]
  "ask for an input from the user"
  (match [...]
    [prompt default callback]
      (vim.ui.input {:prompt (string.format "%s: " prompt)
                     : default}
                    #(when $1 (callback $1)))
    [prompt callback]
      (vim.ui.input {:prompt (string.format "%s: " prompt)}
                    #(when $1 (callback $1)))))

M
