-- [nfnl] Compiled from lua/rz/lib/colors.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
local function parse_xresources_file()
  local function _1_(_241)
    return vim.fn.split(_241, ":")
  end
  local function _2_(_241)
    return vim.fn.substitute(_241, "\\v\\s+", "", "g")
  end
  local function _3_(_241)
    return vim.fn.substitute(_241, "\\v\\*\\.", "", "g")
  end
  local function _4_(_241)
    return (0 == vim.fn.stridx(_241, "*."))
  end
  return vim.tbl_map(_1_, vim.tbl_map(_2_, vim.tbl_map(_3_, vim.tbl_filter(_4_, vim.fn.readfile(vim.fn.resolve(vim.fn.expand("~/.Xresources")))))))
end
local function xresources_colors()
  local ok_3f, colors_data = pcall(parse_xresources_file)
  if not ok_3f then
    return {}
  else
    local colors = {}
    for _, _5_ in ipairs(colors_data) do
      local key = _5_[1]
      local value = _5_[2]
      colors[key] = value
      colors = colors
    end
    return colors
  end
end
M.set = function()
  vim.g.rz_colors = xresources_colors()
  return nil
end
M.reload = function()
  vim.g.rz_colors = nil
  return vim.cmd("colorscheme rz")
end
M["colorscheme-dark?"] = function()
  return (vim.g.rz_colors.colorscheme == "dark")
end
M["get-color"] = function(color_name)
  local dark, light = color_name:match("(.+)|(.+)")
  if (dark and light) then
    if M["colorscheme-dark?"]() then
      return vim.g.rz_colors[dark]
    else
      return vim.g.rz_colors[light]
    end
  else
    return vim.g.rz_colors[color_name]
  end
end
return M
