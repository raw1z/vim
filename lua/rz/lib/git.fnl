(local M {})

(fn git [...]
  "run a git command and returns the output"
  (-> (vim.fn.system (.. "git " ...))
      (vim.fn.trim)))

(fn git-lines [...]
  "run a git command and returns the output splitted in lines"
  (-> (vim.fn.system (.. "git " ...))
      (vim.fn.trim)
      (vim.fn.split "\n")))

(fn M.guess-trunk-branch []
  "if a branch named main or master exitsts, then returns it else return an empty string"
  (-> (git "branch --list main master")
      (vim.fn.substitute "\\s*\\*\\s*" "" "")))

(fn M.current-branch []
  "returns the name of the branch currently checked out"
  (let [text (require :rz.lib.text)]
    (-> [:git :symbolic-ref :--short :HEAD]
        (vim.fn.systemlist)
        (->> (vim.tbl_filter #(not (text.starts-with? $1 "fatal: not a git repository"))))
        (. 1))))

(fn M.root-dir []
  "returns the root dir of the current repository"
  (git "rev-parse --show-toplevel"))

(fn M.is-git-repository? [path]
  "returns true if the given directory path belongs to a git repository"
  (= :true (git (.. "-C " path " rev-parse --is-inside-work-tree"))))

(fn parse-status [status]
  (match status
    :M  :modified
    :A  :added
    :?? :untracked
    :D  :deleted
    :R  :renamed
    _ status))

(fn parse-status-change [[status file]]
  {:file (.. (M.root-dir) :/ file)
   :status (parse-status status)})

(fn M.status []
  "returns a list of changes"
  (let [list (require :rz.lib.list)]
    (->> (git-lines "status --porcelain --untracked --ignore-submodules")
         (vim.tbl_map vim.fn.trim)
         (vim.tbl_map #(vim.fn.split $1 "\\s"))
         (vim.tbl_map list.compact)
         (vim.tbl_map parse-status-change))))

(fn M.repo-dirty? []
  "returns true if there is any uncommitted changes, otherwise returns false"
  (not (vim.tbl_isempty (M.status))))

(fn M.changed-files-between-branches [branch1 branch2]
  "returns a list of files changed in a branch compared to another"
  (let [lines (-> (.. "diff --name-status " branch1 ".." branch2)
                  (git-lines))]
    (->> lines
         (vim.tbl_map #(vim.fn.split $1 :\t))
         (vim.tbl_map #{:path (. $1 2)
                        :status (parse-status (. $1 1))}))))

(fn M.save-changes [message]
  (var command "G stash --all")
  (when (not= "" message)
    (set command (string.format "%s -m '%s'" command message)))
  (vim.cmd command))

(fn M.save-staged-changes [message]
  (var command "G stash-staged")
  (when (not= "" message)
    (set command (string.format "%s -m '%s'" command message)))
  (vim.cmd command))

(fn M.restore-changes []
  (vim.cmd "G stash pop"))

(fn M.continue-rebase []
  (vim.cmd "G rebase --continue"))

(fn M.abort-rebase []
  (vim.cmd "G rebase --abort"))

M
