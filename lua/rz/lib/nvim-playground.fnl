(require-macros :macros.core)
(require-macros :macros.vim)

(local {: autoload} (require :nfnl.module))
(local core (require :nfnl.core))
(local buffer (autoload :rz.lib.buffer))
(local config (autoload :nfnl.config))
(local draft (autoload :rz.lib.draft))
(local fennel (autoload :nfnl.fennel))

(local M {})

(fn compile [cfg path source]
  (set fennel.path (cfg [:fennel-path]))
  (set fennel.macro-path (cfg [:fennel-macro-path]))
  (pcall fennel.compileString
         source
         (core.merge
           {:filename path}
           {:compiler-env _G})))

(fn eval-buffer []
  (let [destination-path (.. (vim.fn.tempname) ".lua")
        root-dir (vim.fn.stdpath :config)
        path (vim.fn.expand "%:p")
        cfg (config.cfg-fn {} {: root-dir})
        source (buffer.content-as-string 0)
        (ok? output) (compile cfg path source)]
    (if (not ok?)
      (vim.print output)
      (do
        (core.spit destination-path output)
        (vim.cmd (.. "source " destination-path))))))

(fn M.setup [bufnr]
  "setup the given buffer as nvim playground"
  (vim.api.nvim_buf_set_var bufnr :is_special_buffer true)
  (vim.api.nvim_buf_set_var bufnr :special_buffer_name "Language Shell - nvim")
  (buffer.enable-autowrite bufnr)
  (def-keys/this-buffer
    "eval buffer" n <localleader>eb eval-buffer))

(fn M.open [direction]
  (let [lazy (require :lazy)]
    (lazy.load {:plugins [:bmessages.nvim]}))

  (let [bmessages (require :bmessages)
        bufnr (draft.create direction
                            :fennel
                            {:scratch? false
                             :suffix :-nvim-playground.fnl})]
    (M.setup bufnr)
    (def-key/buffer "close the playground"
      bufnr n <leader>x (do
                          (bmessages.toggle)
                          (buffer.delete bufnr)))
    (bmessages.toggle {:split_type :vsplit})
    (%> wincmd p)))

M
