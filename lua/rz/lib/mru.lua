-- [nfnl] Compiled from lua/rz/lib/mru.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local sqlite = autoload("sqlite.db")
local path = autoload("rz.lib.path")
local buffer = autoload("rz.lib.buffer")
local list = autoload("rz.lib.list")
local text = autoload("rz.lib.text")
local function type(kind, opts)
  opts[1] = kind
  return opts
end
local M = {db = sqlite({uri = path.join(vim.fn.stdpath("state"), "mru.db"), mru = {path = type("text", {unique = true, primary = true}), added = type("timestamp", {default = sqlite.lib.strftime("%s", "now")})}, opts = {keep_open = true}})}
local function is_ignored_file_3f(path0)
  local function _2_(_241)
    return text["ends-with?"](path0, _241)
  end
  return vim.iter({".git/COMMIT_EDITMSG"}):any(_2_)
end
local function valid_path_3f(path0)
  return ((#path0 > 0) and (0 == vim.fn.isdirectory(path0)) and (0 ~= vim.fn.filereadable(path0)) and not is_ignored_file_3f(path0))
end
local function insert_or_replace(path0)
  return M.db:eval("insert or replace into mru (path) values (?)", path0)
end
local function save_current(path0)
  if valid_path_3f(path0) then
    return insert_or_replace(path0)
  else
    return nil
  end
end
M["on-win-enter"] = function()
  local function _4_()
    return save_current(vim.fn.fnamemodify(buffer.name(vim.api.nvim_get_current_buf()), ":p"))
  end
  return vim.schedule(_4_)
end
local function list_recent_files(root, limit)
  local limit0 = (limit or 5)
  local result = M.db:eval(string.format("select path\n                                        from mru\n                                       where path like '%s%%'\n                                    order by added desc\n                                       limit %s", root, limit0))
  if (true == result) then
    return {}
  else
    return result
  end
end
M.get = function(root, limit)
  local function _6_(_241)
    return _241.path
  end
  return vim.tbl_map(_6_, list_recent_files(root, limit))
end
return M
