-- [nfnl] Compiled from lua/rz/lib/text.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local core = autoload("nfnl.core")
local M = {}
M["last-part"] = function(str, sep)
  return core.last(vim.split(str, sep, {plain = true}))
end
M["first-part"] = function(str, sep)
  return core.first(vim.split(str, sep, {plain = true}))
end
M["find-index"] = function(str, pattern)
  local start, _ = string.find(str, pattern, 1, true)
  return (start or -1)
end
M["contains?"] = function(str, pattern)
  return (-1 ~= M["find-index"](str, pattern))
end
M["matches?"] = function(str, pattern)
  return (-1 ~= M["find-index"](str, pattern))
end
M["ends-with?"] = function(str, suffix)
  return vim.endswith(str, suffix)
end
M["starts-with?"] = function(str, prefix)
  return vim.startswith(str, prefix)
end
M.append = function(str, suffix)
  return (str .. suffix)
end
M.prepend = function(str, prefix)
  return (prefix .. str)
end
M.replace = function(str, pattern, replacement)
  if not core["table?"](pattern) then
    return string.gsub(str, pattern, replacement, 1)
  else
    local acc = str
    for _, item in ipairs(pattern) do
      acc = M.replace(acc, item, replacement)
    end
    return acc
  end
end
M["replace-all"] = function(str, pattern, replacement)
  if not core["table?"](pattern) then
    return string.gsub(str, pattern, replacement, 1)
  else
    local acc = str
    for _, item in ipairs(pattern) do
      acc = M["replace-all"](acc, item, replacement)
    end
    return acc
  end
end
M.clear = function(str, pattern)
  return M["replace-all"](str, pattern, "")
end
M.split = function(str, pattern, count)
  local tokens = vim.split(str, pattern)
  if ((nil == count) or (count <= 0)) then
    return tokens
  else
    local tokens_count = #tokens
    local real_count
    if (count > tokens_count) then
      real_count = tokens_count
    else
      real_count = count
    end
    local head = vim.list_slice(tokens, 1, (real_count - 1))
    local rest = table.concat(vim.list_slice(tokens, real_count), " ")
    return core.concat(head, {rest})
  end
end
M.lines = function(str)
  return M.split(str, "\\n")
end
return M
