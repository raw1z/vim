(require-macros :macros.core)
(require-macros :macros.vim)
(local {: autoload} (require :nfnl.module))
(local text (autoload :rz.lib.text))
(local list (autoload :rz.lib.list))

(fn dart-defines []
  (let [pattern :DART_DEFINE_
        env-lines (-> (vim.fn.system :env)
                      (vim.fn.split :\n))]
    (->> env-lines
         (vim.tbl_filter #(text.starts-with? $1 pattern))
         (vim.tbl_map #(text.clear $1 pattern))
         (vim.tbl_map #(text.prepend $1 :--dart-define=)))))

(fn cli-extensions [platform]
  (-> [(dart-defines)
       :--dart-define=APP_ENV=development
       :-d
       platform]
      (list.flatten)
      (vim.fn.join " ")))

(fn start [platform]
  (->> (cli-extensions platform)
       (.. "FlutterRun ")
       (vim.cmd)))

(fn show-logs []
  (vim.api.nvim_input :1gt))

(def-keys
  "run the app on all supported devices" n <space>mrr (start :all)
  "run the app on iphone" n <space>mri (start :iPhone)
  "run the app on macos" n <space>mrm (start :macos)
  "show logs" n <space>ml (show-logs)
  "restart the app" n <space>mh (%> FlutterRestart)
  "quit the app" n <space>mq (%> FlutterQuit)
  "update dependencies" n <space>mg (%> FlutterPubGet)
  "show code outline" n <space>mo (%> FlutterOutline)
  "detach app" n <space>md (%> FlutterDetach)
  "open devtools" n <space>mt (%> FlutterOpenDevTools))

(fn setup-keymaps []
  (define-key/this-buffer n q (%> quit))
  (define-key/this-buffer n <C-l> (%> FlutterLogClear))
  (define-autocmd/this-buffer BufEnter (%> normal G)))

(fn close-logs []
  (%> bdelete __FLUTTER_DEV_LOG__))

(define-augroup FlutterProject
  (define-autocmd BufEnter :__FLUTTER_DEV_LOG__
    (setup-keymaps))
  (define-autocmd VimLeavePre *
    (close-logs)))

{}
