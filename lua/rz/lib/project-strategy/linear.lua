-- [nfnl] Compiled from lua/rz/lib/project-strategy/linear.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local linear = autoload("rz.lib.linear")
local M = {}
local function _2_()
  return linear.pick()
end
vim.api.nvim_create_user_command("LinearIssues", _2_, {force = true})
local function _3_()
  return vim.cmd("LinearIssues")
end
vim.keymap.set({"n"}, "<leader>gl", _3_, {buffer = false, desc = "search linear issues", silent = true})
return {}
