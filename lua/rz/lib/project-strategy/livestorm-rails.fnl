(require-macros :macros.core)
(require-macros :macros.vim)
(local {: autoload} (require :nfnl.module))
(local terminal (autoload :rz.lib.terminal))

(define-command BundleInstall []
  (terminal.direnv-exec
     "bundle check || bundle install && bundle check || bundle install"
     {:name :ruby
      :start-insert? true
      :auto-close? true
      :direction :bottom}))

{}
