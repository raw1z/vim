(require-macros :macros.core)
(require-macros :macros.vim)
(local {: autoload} (require :nfnl.module))
(local repl-playground (autoload :rz.lib.repl-playground))
(local terminal (autoload :rz.lib.terminal))
(local ruby (autoload :rz.lib.ruby))

(local pry-console-command "direnv exec $PWD bundle exec pry")
(local irb-console-command "direnv exec $PWD bundle exec irb")

(define-command PryConsole []
  (let [playground (repl-playground.create :ruby pry-console-command {:scratch-suffix :-pry-console.rb})]
     (playground.start)))

(define-command PryConsoleThis []
  (let [playground (repl-playground.create :ruby pry-console-command)]
     (playground.attach-current-buffer)))

(define-command IrbConsole []
  (let [playground (repl-playground.create :ruby irb-console-command {:scratch-suffix :-irb-console.rb})]
     (playground.start)))

(define-command IrbConsoleThis []
  (let [playground (repl-playground.create :ruby irb-console-command)]
     (playground.attach-current-buffer)))

(define-command BundleInstall []
  (terminal.direnv-exec
     "bundle check || bundle install"
     {:name :ruby
      :start-insert? true
      :auto-close? true
      :direction :bottom}))

(define-command Bi []
  (%> BundleInstall))

(define-command BundleUpdate []
  (terminal.direnv-exec
     "bundle update"
     {:name :ruby
      :start-insert? true
      :direction :bottom}))

(fn fold-all-functions []
  (%> "g/\\v(self.)?def\\s/norm zfaf")
  (%> "nohlsearch"))
(def-key "fold all functions" n zFf (fold-all-functions))

{}
