(require-macros :macros.core)
(require-macros :macros.vim)
(local {: autoload} (require :nfnl.module))
(local str (autoload :nfnl.string))
(local git (autoload :rz.lib.git))
(local list (autoload :rz.lib.list))
(local notify (autoload :rz.lib.notify))
(local terminal (autoload :rz.lib.terminal))
(local ui (autoload :rz.lib.ui))

(fn with-trunk [callback]
  (let [trunk-branch (git.guess-trunk-branch)]
    (if (str.blank? trunk-branch)
      (notify.error "You must specify a trunk branch")
      (callback trunk-branch))))

(fn exec [commands]
  (terminal.direnv-exec
    (-> (list.flatten [commands])
        (list.join " && "))
    {:name :trunk-based
     :start-insert? true
     :direction :bottom}))

(fn update-trunk-command [trunk-branch]
  (string.format "git fetch origin %s:%s" trunk-branch trunk-branch))

(fn rebase-trunk-command [trunk-branch]
  (string.format "git rebase %s" trunk-branch))

(fn rebase-trunk [trunk-branch]
  (when (not= (git.current-branch) trunk-branch)
    (exec ["git diff --cached --exit-code"
           "git diff --exit-code"
           (update-trunk-command trunk-branch)
           (rebase-trunk-command trunk-branch)])))

(define-command RebaseTrunk []
  (with-trunk rebase-trunk))

(fn update-trunk [trunk-branch]
  (exec (update-trunk-command trunk-branch)))

(define-command UpdateTrunk []
  (with-trunk update-trunk))

(fn branch-from-trunk-command [trunk-branch branch-name]
  (string.format "git checkout -b %s %s" branch-name trunk-branch))

(fn branch-from-trunk [trunk-branch branch-name]
  (exec [(update-trunk-command trunk-branch)
         (branch-from-trunk-command trunk-branch branch-name)]))

(define-command BranchTrunk [branch-name?]
  (do
    (var branch-name branch-name?)
    (when (blank? branch-name)
      (ui.input "branch name" #(set branch-name $1)))
    (when (not (blank? branch-name))
      (with-trunk #(branch-from-trunk $1 branch-name)))))

{}
