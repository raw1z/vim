(require-macros :macros.vim)
(local {: autoload} (require :nfnl.module))
(local terminal (autoload :rz.lib.terminal))

(define-key n <leader>r
  (terminal.direnv-exec
    "tree-sitter generate && tree-sitter test"
    {:name :tree-sitter
     :direction :bottom}))

(define-key n <leader>b
  (terminal.direnv-exec
    "tree-sitter generate"
    {:name :tree-sitter
     :direction :bottom}))

{}
