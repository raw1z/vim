-- [nfnl] Compiled from lua/rz/lib/project-strategy/livestorm-go.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local terminal = autoload("rz.lib.terminal")
local repl_playground = autoload("rz.lib.repl-playground")
local ui = autoload("rz.lib.ui")
local function _2_()
  vim.cmd("wa")
  return terminal.start("bazel test ...", {name = "livestorm", ["start-insert?"] = true, ["auto-close?"] = true, direction = "bottom"})
end
vim.api.nvim_create_user_command("BazelTest", _2_, {force = true})
local function _3_()
  return terminal.start("bazel run :prj.apply", {name = "livestorm", ["start-insert?"] = true, ["auto-close?"] = true, direction = "bottom"})
end
vim.api.nvim_create_user_command("BazelApply", _3_, {force = true})
local function _4_(_241)
  local function _5_(name)
    return terminal.start(string.format("cd migrations && bazel run //tools/migrate -- %s", name), {name = "livestorm", ["start-insert?"] = true, ["auto-close?"] = true, direction = "bottom"})
  end
  return _5_(_241.args)
end
vim.api.nvim_create_user_command("BazelAddMigration", _4_, {force = true, nargs = 1})
local function _6_(_241)
  local function _7_(port_3f)
    local filetype = "pgsql"
    local port
    if ("" == port_3f) then
      port = 5433
    else
      port = port_3f
    end
    local command = string.format("pgcli -h localhost -p %s -U livestorm", port)
    local scratch_suffix = "-automated-actions.pgsql"
    local playground = repl_playground.create(filetype, command, {["scratch-suffix"] = scratch_suffix, ["activate-term-on-send?"] = true})
    return playground.start()
  end
  return _7_(_241.args)
end
vim.api.nvim_create_user_command("LSDbPlayground", _6_, {force = true, nargs = "?"})
local function add_migration()
  local function _9_(_241)
    return vim.cmd(("BazelAddMigration " .. _241))
  end
  return ui.input("migration name", "new_migration", _9_)
end
local function _10_()
  return vim.cmd("BazelApply")
end
local function _11_()
  return vim.cmd("BazelTest")
end
local function _12_()
  return vim.cmd("LSDbPlayground")
end
local function _13_()
  return add_migration()
end
do local _ = {vim.keymap.set({"n"}, "<space>ma", _10_, {buffer = false, desc = "deploy in the local cluster", silent = true}), vim.keymap.set({"n"}, "<space>mt", _11_, {buffer = false, desc = "run project tests", silent = true}), vim.keymap.set({"n"}, "<space>mdp", _12_, {buffer = false, desc = "open db playground", silent = true}), vim.keymap.set({"n"}, "<space>mdm", _13_, {buffer = false, desc = "create db migration", silent = true})} end
return {}
