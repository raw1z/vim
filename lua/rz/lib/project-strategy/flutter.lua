-- [nfnl] Compiled from lua/rz/lib/project-strategy/flutter.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local text = autoload("rz.lib.text")
local list = autoload("rz.lib.list")
local function dart_defines()
  local pattern = "DART_DEFINE_"
  local env_lines = vim.fn.split(vim.fn.system("env"), "\\n")
  local function _2_(_241)
    return text.prepend(_241, "--dart-define=")
  end
  local function _3_(_241)
    return text.clear(_241, pattern)
  end
  local function _4_(_241)
    return text["starts-with?"](_241, pattern)
  end
  return vim.tbl_map(_2_, vim.tbl_map(_3_, vim.tbl_filter(_4_, env_lines)))
end
local function cli_extensions(platform)
  return vim.fn.join(list.flatten({dart_defines(), "--dart-define=APP_ENV=development", "-d", platform}), " ")
end
local function start(platform)
  return vim.cmd(("FlutterRun " .. cli_extensions(platform)))
end
local function show_logs()
  return vim.api.nvim_input("1gt")
end
local function _5_()
  return start("all")
end
local function _6_()
  return start("iPhone")
end
local function _7_()
  return start("macos")
end
local function _8_()
  return show_logs()
end
local function _9_()
  return vim.cmd("FlutterRestart")
end
local function _10_()
  return vim.cmd("FlutterQuit")
end
local function _11_()
  return vim.cmd("FlutterPubGet")
end
local function _12_()
  return vim.cmd("FlutterOutline")
end
local function _13_()
  return vim.cmd("FlutterDetach")
end
local function _14_()
  return vim.cmd("FlutterOpenDevTools")
end
do local _ = {vim.keymap.set({"n"}, "<space>mrr", _5_, {buffer = false, desc = "run the app on all supported devices", silent = true}), vim.keymap.set({"n"}, "<space>mri", _6_, {buffer = false, desc = "run the app on iphone", silent = true}), vim.keymap.set({"n"}, "<space>mrm", _7_, {buffer = false, desc = "run the app on macos", silent = true}), vim.keymap.set({"n"}, "<space>ml", _8_, {buffer = false, desc = "show logs", silent = true}), vim.keymap.set({"n"}, "<space>mh", _9_, {buffer = false, desc = "restart the app", silent = true}), vim.keymap.set({"n"}, "<space>mq", _10_, {buffer = false, desc = "quit the app", silent = true}), vim.keymap.set({"n"}, "<space>mg", _11_, {buffer = false, desc = "update dependencies", silent = true}), vim.keymap.set({"n"}, "<space>mo", _12_, {buffer = false, desc = "show code outline", silent = true}), vim.keymap.set({"n"}, "<space>md", _13_, {buffer = false, desc = "detach app", silent = true}), vim.keymap.set({"n"}, "<space>mt", _14_, {buffer = false, desc = "open devtools", silent = true})} end
local function setup_keymaps()
  local function _15_()
    return vim.cmd("quit")
  end
  vim.keymap.set({"n"}, "q", _15_, {buffer = true, silent = true})
  local function _16_()
    return vim.cmd("FlutterLogClear")
  end
  vim.keymap.set({"n"}, "<C-l>", _16_, {buffer = true, silent = true})
  local function _17_()
    return vim.cmd("normal G")
  end
  return vim.api.nvim_create_autocmd({"BufEnter"}, {buffer = 0, callback = _17_})
end
local function close_logs()
  return vim.cmd("bdelete __FLUTTER_DEV_LOG__")
end
do
  local _18_ = vim.api.nvim_create_augroup("FlutterProject", {clear = true})
  local function _19_()
    return setup_keymaps()
  end
  local function _20_()
    return close_logs()
  end
  do local _ = {vim.api.nvim_create_autocmd({"BufEnter"}, {callback = _19_, group = _18_, pattern = {"__FLUTTER_DEV_LOG__"}}), vim.api.nvim_create_autocmd({"VimLeavePre"}, {callback = _20_, group = _18_, pattern = {"*"}})} end
end
return {}
