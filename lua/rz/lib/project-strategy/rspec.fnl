(require-macros :macros.core)
(require-macros :macros.vim)
(local {: autoload} (require :nfnl.module))
(local core (autoload :nfnl.core))
(local str (autoload :nfnl.string))
(local rspec (autoload :rz.lib.rspec))

(define-command RSpec [command]
  {:complete :file}
  (rspec.run command))

(define-command RspecChanges []
  (let [test-files (rspec.test-files-for-current-branch)]
    (when (not (core.empty? test-files))
      (-> (str.join " " test-files)
          (rspec.run)))))

(fn fold-all-contexts []
  (%> "g/\\vcontext.+do/norm zfad")
  (%> "nohlsearch"))
(def-key "fold all contexts" n zFc (fold-all-contexts))

{}
