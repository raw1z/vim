-- [nfnl] Compiled from lua/rz/lib/project-strategy/github.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local github = autoload("rz.lib.github")
local github_status = autoload("rz.lib.github-status")
local function _2_()
  return github_status.open()
end
vim.api.nvim_create_user_command("PRStatus", _2_, {force = true})
local function _3_(_241)
  local function _4_(pr_id_3f)
    return github.web(pr_id_3f)
  end
  return _4_(_241.args)
end
vim.api.nvim_create_user_command("PRWeb", _3_, {force = true, nargs = "?"})
local function _5_(_241)
  local function _6_(label)
    return github["create-pr"](label)
  end
  return _6_(_241.args)
end
vim.api.nvim_create_user_command("PRCreate", _5_, {force = true, nargs = 1})
local function _7_()
  return vim.cmd("PRStatus")
end
vim.keymap.set({"n"}, "<leader>hs", _7_, {buffer = false, desc = "show github status", silent = true})
local function _8_()
  return vim.cmd("PRWeb")
end
vim.keymap.set({"n"}, "<leader>hv", _8_, {buffer = false, desc = "open PR in the browser", silent = true})
return {}
