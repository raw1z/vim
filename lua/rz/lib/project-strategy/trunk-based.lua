-- [nfnl] Compiled from lua/rz/lib/project-strategy/trunk-based.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local str = autoload("nfnl.string")
local git = autoload("rz.lib.git")
local list = autoload("rz.lib.list")
local notify = autoload("rz.lib.notify")
local terminal = autoload("rz.lib.terminal")
local ui = autoload("rz.lib.ui")
local function with_trunk(callback)
  local trunk_branch = git["guess-trunk-branch"]()
  if str["blank?"](trunk_branch) then
    return notify.error("You must specify a trunk branch")
  else
    return callback(trunk_branch)
  end
end
local function exec(commands)
  return terminal["direnv-exec"](list.join(list.flatten({commands}), " && "), {name = "trunk-based", ["start-insert?"] = true, direction = "bottom"})
end
local function update_trunk_command(trunk_branch)
  return string.format("git fetch origin %s:%s", trunk_branch, trunk_branch)
end
local function rebase_trunk_command(trunk_branch)
  return string.format("git rebase %s", trunk_branch)
end
local function rebase_trunk(trunk_branch)
  if (git["current-branch"]() ~= trunk_branch) then
    return exec({"git diff --cached --exit-code", "git diff --exit-code", update_trunk_command(trunk_branch), rebase_trunk_command(trunk_branch)})
  else
    return nil
  end
end
local function _4_()
  return with_trunk(rebase_trunk)
end
vim.api.nvim_create_user_command("RebaseTrunk", _4_, {force = true})
local function update_trunk(trunk_branch)
  return exec(update_trunk_command(trunk_branch))
end
local function _5_()
  return with_trunk(update_trunk)
end
vim.api.nvim_create_user_command("UpdateTrunk", _5_, {force = true})
local function branch_from_trunk_command(trunk_branch, branch_name)
  return string.format("git checkout -b %s %s", branch_name, trunk_branch)
end
local function branch_from_trunk(trunk_branch, branch_name)
  return exec({update_trunk_command(trunk_branch), branch_from_trunk_command(trunk_branch, branch_name)})
end
local function _6_(_241)
  local function _7_(branch_name_3f)
    local branch_name = branch_name_3f
    if (1 == vim.fn.empty(branch_name)) then
      local function _8_(_2410)
        branch_name = _2410
        return nil
      end
      ui.input("branch name", _8_)
    else
    end
    if not (1 == vim.fn.empty(branch_name)) then
      local function _10_(_2410)
        return branch_from_trunk(_2410, branch_name)
      end
      return with_trunk(_10_)
    else
      return nil
    end
  end
  return _7_(_241.args)
end
vim.api.nvim_create_user_command("BranchTrunk", _6_, {force = true, nargs = "?"})
return {}
