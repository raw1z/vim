-- [nfnl] Compiled from lua/rz/lib/project-strategy/livestorm-rails.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local terminal = autoload("rz.lib.terminal")
local function _2_()
  return terminal["direnv-exec"]("bundle check || bundle install && bundle check || bundle install", {name = "ruby", ["start-insert?"] = true, ["auto-close?"] = true, direction = "bottom"})
end
vim.api.nvim_create_user_command("BundleInstall", _2_, {force = true})
return {}
