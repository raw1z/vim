(require-macros :macros.vim)

(define-augroup rz_eslint_autoformat
  (define-autocmd BufWritePre *.js|*.ts :EslintFixAll))

{}
