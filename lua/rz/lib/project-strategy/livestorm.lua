-- [nfnl] Compiled from lua/rz/lib/project-strategy/livestorm.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local repl_playground = autoload("rz.lib.repl-playground")
local github = autoload("rz.lib.github")
local linear = autoload("rz.lib.linear")
local terminal = autoload("rz.lib.terminal")
local dir = autoload("rz.lib.dir")
local livestorm = autoload("rz.lib.livestorm")
local draft = autoload("rz.lib.draft")
local git = autoload("rz.lib.git")
local browser = autoload("rz.lib.browser")
local function _2_(_241)
  local function _3_(pr_id)
    local playground = repl_playground.create("ruby", ("lsctl preview console " .. pr_id), {["scratch-suffix"] = ("-ls-preview-" .. pr_id .. "-console.rb")})
    return playground.start()
  end
  return _3_(_241.args)
end
vim.api.nvim_create_user_command("LSPreviewConsole", _2_, {force = true, nargs = 1})
local function _4_(_241)
  local function _5_(pr_id)
    local playground = repl_playground.create("ruby", ("lsctl preview console " .. pr_id))
    return playground["attach-current-buffer"]()
  end
  return _5_(_241.args)
end
vim.api.nvim_create_user_command("LSPreviewConsoleThis", _4_, {force = true, nargs = 1})
local function _6_(_241)
  local function _7_(pr_id)
    local playground = repl_playground.create("pgsql", ("lsctl preview db " .. pr_id), {["scratch-suffix"] = ("-ls-preview-" .. pr_id .. "-db.pgsql")})
    return playground.start()
  end
  return _7_(_241.args)
end
vim.api.nvim_create_user_command("LSPreviewDb", _6_, {force = true, nargs = 1})
local function _8_(_241)
  local function _9_(pr_id)
    local playground = repl_playground.create("pgsql", ("lsctl preview db " .. pr_id))
    return playground["attach-current-buffer"]()
  end
  return _9_(_241.args)
end
vim.api.nvim_create_user_command("LSPreviewDbThis", _8_, {force = true, nargs = 1})
local function _10_()
  local playground = repl_playground.create("ruby", "sx exec rails --main", {["scratch-suffix"] = "-ls-staging-console.rb"})
  return playground.start()
end
vim.api.nvim_create_user_command("LSStagingConsole", _10_, {force = true})
local function _11_()
  local playground = repl_playground.create("ruby", "sx exec rails --main")
  return playground["attach-current-buffer"]()
end
vim.api.nvim_create_user_command("LSStagingConsoleThis", _11_, {force = true})
local function _12_()
  local playground = repl_playground.create("pgsql", "sx exec rails-db --main", {["scratch-suffix"] = "-ls-staging-db.pgsql"})
  return playground.start()
end
vim.api.nvim_create_user_command("LSStagingDb", _12_, {force = true})
local function _13_()
  local playground = repl_playground.create("pgsql", "sx exec rails-db --main")
  return playground["attach-current-buffer"]()
end
vim.api.nvim_create_user_command("LSStagingDbThis", _13_, {force = true})
local function _14_()
  local playground = repl_playground.create("ruby", "sx exec rails --prod", {["scratch-suffix"] = "-ls-prod-console.rb"})
  return playground.start()
end
vim.api.nvim_create_user_command("LSProdConsole", _14_, {force = true})
local function _15_()
  local playground = repl_playground.create("ruby", "sx exec rails --prod")
  return playground["attach-current-buffer"]()
end
vim.api.nvim_create_user_command("LSProdConsoleThis", _15_, {force = true})
local function _16_()
  local playground = repl_playground.create("ruby", "sx exec rails", {["scratch-suffix"] = "-ls-sandbox-back-console.rb"})
  return playground.start()
end
vim.api.nvim_create_user_command("LSSandboxBackConsole", _16_, {force = true})
local function _17_()
  local playground = repl_playground.create("ruby", "sx exec rails")
  return playground["attach-current-buffer"]()
end
vim.api.nvim_create_user_command("LSSandboxBackConsoleThis", _17_, {force = true})
local function _18_()
  local playground = repl_playground.create("pgsql", "sx exec rails-db", {["scratch-suffix"] = "-ls-sandbox-back-db.pgsql"})
  return playground.start()
end
vim.api.nvim_create_user_command("LSSandboxBackDb", _18_, {force = true})
local function _19_()
  local playground = repl_playground.create("ruby", "sx exec rails-db")
  return playground["attach-current-buffer"]()
end
vim.api.nvim_create_user_command("LSSandboxBackDbThis", _19_, {force = true})
local function _20_(_241)
  local function _21_(label)
    return github["create-pr"](string.format("%s, %s", label, linear.id()))
  end
  return _21_(_241.args)
end
vim.api.nvim_create_user_command("LSPrCreate", _20_, {force = true, nargs = 1})
local function _22_(_241)
  local function _23_(name)
    return dir["go-to-folder-in-new-tab"](livestorm["package-path"](name))
  end
  return _23_(_241.args)
end
vim.api.nvim_create_user_command("LSGo", _22_, {complete = livestorm["list-packages"], force = true, nargs = 1})
local function _24_()
  return terminal["direnv-exec"]("bazel run //packages/services/iam/authentication:prj.apply", {name = "livestorm", ["start-insert?"] = true, ["auto-close?"] = true, direction = "bottom"})
end
vim.api.nvim_create_user_command("LSDeployAuth", _24_, {force = true})
local function _25_()
  return terminal.start("kubectl apply -f ~/Documents/backend-rpc.yaml", {name = "livestorm", ["start-insert?"] = true, ["auto-close?"] = true, direction = "bottom"})
end
vim.api.nvim_create_user_command("LSDeployBackendRpc", _25_, {force = true})
local function _26_()
  return terminal.start("kubectl apply -f ~/Documents/backend-db.yaml", {name = "livestorm", ["start-insert?"] = true, ["auto-close?"] = true, direction = "bottom"})
end
vim.api.nvim_create_user_command("LSDeployBackendDb", _26_, {force = true})
local function _27_()
  local lines
  local function _28_(_2410)
    return vim.fn.substitute(_2410, "\\v\\[(.+)\\]\\((.+)\\)", "<\\2|\\1> ", "")
  end
  lines = vim.tbl_map(_28_, vim.fn.getreg("+", 1, 1))
  return vim.api.nvim_buf_set_lines(draft.create("tab", "markdown"), 0, -1, false, lines)
end
vim.api.nvim_create_user_command("LSStandup", _27_, {force = true})
local function _29_()
  return terminal.start("bazel run //tools/local -- create --with-components=kafka", {name = "livestorm", ["start-insert?"] = true, ["auto-close?"] = true, direction = "bottom"})
end
vim.api.nvim_create_user_command("LSClusterCreate", _29_, {force = true})
local function _30_()
  return terminal.start("bazel run //tools/local -- delete", {name = "livestorm", ["start-insert?"] = true, ["auto-close?"] = true, direction = "bottom"})
end
vim.api.nvim_create_user_command("LSClusterDelete", _30_, {force = true})
local function _31_()
  return terminal.start("k9s", {name = "livestorm", ["start-insert?"] = true, ["auto-close?"] = true, direction = "tab"})
end
vim.api.nvim_create_user_command("LSClusterInspect", _31_, {force = true})
local function _32_()
  return terminal.start(string.format("cd %s && make tidy", livestorm["root-path"]()), {name = "livestorm", ["start-insert?"] = true, ["auto-close?"] = true, direction = "bottom"})
end
vim.api.nvim_create_user_command("LSMakeTidy", _32_, {force = true})
local function _33_()
  return terminal.start(string.format("cd %s && make mock", livestorm["root-path"]()), {name = "livestorm", ["start-insert?"] = true, ["auto-close?"] = true, direction = "bottom"})
end
vim.api.nvim_create_user_command("LSMakeMock", _33_, {force = true})
local function open_matching_linear_ticket()
  return browser.open(string.format("https://linear.app/livestorm/issue/%s", linear.id()))
end
local function _34_()
  return vim.cmd("LSMakeTidy")
end
local function _35_()
  return vim.cmd("LSMakeMock")
end
local function _36_()
  return vim.cmd("LSClusterCreate")
end
local function _37_()
  return vim.cmd("LSClusterDelete")
end
local function _38_()
  return vim.cmd("LSClusterInspect")
end
local function _39_()
  return open_matching_linear_ticket()
end
do local _ = {vim.keymap.set({"n"}, "<space>mm", _34_, {buffer = false, desc = "run make tidy", silent = true}), vim.keymap.set({"n"}, "<space>mo", _35_, {buffer = false, desc = "run make mock", silent = true}), vim.keymap.set({"n"}, "<space>mcc", _36_, {buffer = false, desc = "create cluster", silent = true}), vim.keymap.set({"n"}, "<space>mcd", _37_, {buffer = false, desc = "delete cluster", silent = true}), vim.keymap.set({"n"}, "<space>mci", _38_, {buffer = false, desc = "inspect cluster", silent = true}), vim.keymap.set({"n"}, "<leader>hl", _39_, {buffer = false, desc = "open matching linear ticket", silent = true})} end
return {}
