-- [nfnl] Compiled from lua/rz/lib/project-strategy/ruby.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local repl_playground = autoload("rz.lib.repl-playground")
local terminal = autoload("rz.lib.terminal")
local ruby = autoload("rz.lib.ruby")
local pry_console_command = "direnv exec $PWD bundle exec pry"
local irb_console_command = "direnv exec $PWD bundle exec irb"
local function _2_()
  local playground = repl_playground.create("ruby", pry_console_command, {["scratch-suffix"] = "-pry-console.rb"})
  return playground.start()
end
vim.api.nvim_create_user_command("PryConsole", _2_, {force = true})
local function _3_()
  local playground = repl_playground.create("ruby", pry_console_command)
  return playground["attach-current-buffer"]()
end
vim.api.nvim_create_user_command("PryConsoleThis", _3_, {force = true})
local function _4_()
  local playground = repl_playground.create("ruby", irb_console_command, {["scratch-suffix"] = "-irb-console.rb"})
  return playground.start()
end
vim.api.nvim_create_user_command("IrbConsole", _4_, {force = true})
local function _5_()
  local playground = repl_playground.create("ruby", irb_console_command)
  return playground["attach-current-buffer"]()
end
vim.api.nvim_create_user_command("IrbConsoleThis", _5_, {force = true})
local function _6_()
  return terminal["direnv-exec"]("bundle check || bundle install", {name = "ruby", ["start-insert?"] = true, ["auto-close?"] = true, direction = "bottom"})
end
vim.api.nvim_create_user_command("BundleInstall", _6_, {force = true})
local function _7_()
  return vim.cmd("BundleInstall")
end
vim.api.nvim_create_user_command("Bi", _7_, {force = true})
local function _8_()
  return terminal["direnv-exec"]("bundle update", {name = "ruby", ["start-insert?"] = true, direction = "bottom"})
end
vim.api.nvim_create_user_command("BundleUpdate", _8_, {force = true})
local function fold_all_functions()
  vim.cmd("g/\\v(self.)?def\\s/norm zfaf")
  return vim.cmd("nohlsearch")
end
local function _9_()
  return fold_all_functions()
end
vim.keymap.set({"n"}, "zFf", _9_, {buffer = false, desc = "fold all functions", silent = true})
return {}
