(require-macros :macros.core)
(require-macros :macros.vim)

(local {: autoload} (require :nfnl.module))
(local linear (autoload :rz.lib.linear))

(local M {})

(define-command LinearIssues []
  (linear.pick))

(def-key "search linear issues"
  n :<leader>gl (%> LinearIssues))

{}
