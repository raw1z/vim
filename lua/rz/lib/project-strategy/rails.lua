-- [nfnl] Compiled from lua/rz/lib/project-strategy/rails.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local terminal = autoload("rz.lib.terminal")
local repl_playground = autoload("rz.lib.repl-playground")
local rails_console_command = "direnv exec $PWD bundle exec rails console"
local rails_sandbox_command = "direnv exec $PWD bundle exec rails console --sandbox"
local rails_db_command = "direnv exec $PWD bundle exec rails db -p"
local function _2_()
  local playground = repl_playground.create("ruby", rails_sandbox_command, {["scratch-suffix"] = "-rails-sandbox.rb"})
  return playground.start()
end
vim.api.nvim_create_user_command("RailsSandbox", _2_, {force = true})
local function _3_()
  local playground = repl_playground.create("ruby", rails_sandbox_command)
  return playground["attach-current-buffer"]()
end
vim.api.nvim_create_user_command("RailsSandboxThis", _3_, {force = true})
local function _4_()
  local playground = repl_playground.create("ruby", rails_console_command, {["scratch-suffix"] = "-rails-console.rb"})
  return playground.start()
end
vim.api.nvim_create_user_command("RailsConsole", _4_, {force = true})
local function _5_()
  local playground = repl_playground.create("ruby", rails_console_command)
  return playground["attach-current-buffer"]()
end
vim.api.nvim_create_user_command("RailsConsoleThis", _5_, {force = true})
local function _6_()
  local playground = repl_playground.create("ruby", rails_db_command, {["scratch-suffix"] = "-rails-db-console.pgsql", ["activate-term-on-send?"] = true})
  return playground.start()
end
vim.api.nvim_create_user_command("RailsDb", _6_, {force = true})
local function _7_()
  local playground = repl_playground.create("ruby", rails_db_command, {["activate-term-on-send?"] = true})
  return playground["attach-current-buffer"]()
end
vim.api.nvim_create_user_command("RailsDbThis", _7_, {force = true})
local function _8_()
  return terminal["direnv-exec"]("bundle exec rails db:migrate", {name = "rails", ["start-insert?"] = true, ["auto-close?"] = true, direction = "bottom"})
end
vim.api.nvim_create_user_command("RailsDbMigrate", _8_, {force = true})
local function _9_()
  return terminal["direnv-exec"]("bundle exec rails db:migrate:status", {name = "rails", ["start-insert?"] = true, direction = "bottom"})
end
vim.api.nvim_create_user_command("RailsDbMigrateStatus", _9_, {force = true})
local function _10_()
  return terminal["direnv-exec"]("bundle exec rails db:rollback", {name = "rails", ["start-insert?"] = true, ["auto-close?"] = true, direction = "bottom"})
end
vim.api.nvim_create_user_command("RailsDbRollback", _10_, {force = true})
local function _11_()
  return terminal["direnv-exec"]("bundle exec rails db:reset && bundle exec rails restart", {name = "rails", ["start-insert?"] = true, ["auto-close?"] = true, direction = "bottom"})
end
vim.api.nvim_create_user_command("RailsDbReset", _11_, {force = true})
local function _12_()
  return terminal["direnv-exec"]("env RAILS_ENV=test bundle exec rails db:test:prepare", {name = "rails", ["start-insert?"] = true, ["auto-close?"] = true, direction = "bottom"})
end
vim.api.nvim_create_user_command("RailsDbTestPrepare", _12_, {force = true})
local function _13_()
  return terminal["direnv-exec"]("bundle exec rails restart", {name = "rails", ["auto-close?"] = true, direction = "bottom", ["start-insert?"] = false})
end
vim.api.nvim_create_user_command("RailsRestart", _13_, {force = true})
local function _14_()
  return vim.cmd("RailsSandbox")
end
local function _15_()
  return vim.cmd("RailsConsole")
end
local function _16_()
  return vim.cmd("RailsDb")
end
local function _17_()
  return vim.cmd("RailsDbMigrate")
end
local function _18_()
  return vim.cmd("RailsDbRollback")
end
local function _19_()
  return vim.cmd("RailsDbTestPrepare")
end
local function _20_()
  return vim.cmd("RailsDbReset")
end
local function _21_()
  return vim.cmd("RailsRestart")
end
do local _ = {vim.keymap.set({"n"}, "<space>mrs", _14_, {buffer = false, desc = "open rails sandbox playground", silent = true}), vim.keymap.set({"n"}, "<space>mrc", _15_, {buffer = false, desc = "open rails console playground", silent = true}), vim.keymap.set({"n"}, "<space>mrd", _16_, {buffer = false, desc = "open rails db console playground", silent = true}), vim.keymap.set({"n"}, "<space>mdf", _17_, {buffer = false, desc = "run db:migrate", silent = true}), vim.keymap.set({"n"}, "<space>mdb", _18_, {buffer = false, desc = "run db:rollback", silent = true}), vim.keymap.set({"n"}, "<space>mdt", _19_, {buffer = false, desc = "run db:test:prepare", silent = true}), vim.keymap.set({"n"}, "<space>mdr", _20_, {buffer = false, desc = "run db:reset", silent = true}), vim.keymap.set({"n"}, "<space>mrr", _21_, {buffer = false, desc = "restart rails", silent = true})} end
return {}
