-- [nfnl] Compiled from lua/rz/lib/project-strategy/eslint.fnl by https://github.com/Olical/nfnl, do not edit.
do
  local _1_ = vim.api.nvim_create_augroup("rz_eslint_autoformat", {clear = true})
  do local _ = {vim.api.nvim_create_autocmd({"BufWritePre"}, {command = "EslintFixAll", group = _1_, pattern = {"*.js", "*.ts"}})} end
end
return {}
