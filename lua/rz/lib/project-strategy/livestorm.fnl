(require-macros :macros.core)
(require-macros :macros.vim)
(local {: autoload} (require :nfnl.module))
(local repl-playground (autoload :rz.lib.repl-playground))
(local github (autoload :rz.lib.github))
(local linear (autoload :rz.lib.linear))
(local terminal (autoload :rz.lib.terminal))
(local dir (autoload :rz.lib.dir))
(local livestorm (autoload :rz.lib.livestorm))
(local draft (autoload :rz.lib.draft))
(local git (autoload :rz.lib.git))
(local browser (autoload :rz.lib.browser))

(define-command LSPreviewConsole [pr-id]
  (let [playground (repl-playground.create
                     :ruby
                     (.. "lsctl preview console " pr-id)
                     {:scratch-suffix (.. :-ls-preview- pr-id :-console.rb)})]
    (playground.start)))

(define-command LSPreviewConsoleThis [pr-id]
  (let [playground (repl-playground.create :ruby (.. "lsctl preview console " pr-id))]
    (playground.attach-current-buffer)))

(define-command LSPreviewDb [pr-id]
  (let [playground (repl-playground.create
                     :pgsql
                     (.. "lsctl preview db " pr-id)
                     {:scratch-suffix (.. :-ls-preview- pr-id :-db.pgsql)})]
    (playground.start)))

(define-command LSPreviewDbThis [pr-id]
  (let [playground (repl-playground.create :pgsql (.. "lsctl preview db " pr-id))]
    (playground.attach-current-buffer)))

(define-command LSStagingConsole []
  (let [playground (repl-playground.create
                     :ruby
                     "sx exec rails --main"
                     {:scratch-suffix :-ls-staging-console.rb})]
    (playground.start)))

(define-command LSStagingConsoleThis []
  (let [playground (repl-playground.create :ruby "sx exec rails --main")]
    (playground.attach-current-buffer)))

(define-command LSStagingDb []
  (let [playground (repl-playground.create
                     :pgsql
                     "sx exec rails-db --main"
                     {:scratch-suffix :-ls-staging-db.pgsql})]
    (playground.start)))

(define-command LSStagingDbThis []
  (let [playground (repl-playground.create :pgsql "sx exec rails-db --main")]
    (playground.attach-current-buffer)))

(define-command LSProdConsole []
  (let [playground (repl-playground.create
                     :ruby
                     "sx exec rails --prod"
                     {:scratch-suffix :-ls-prod-console.rb})]
    (playground.start)))

(define-command LSProdConsoleThis []
  (let [playground (repl-playground.create :ruby "sx exec rails --prod")]
    (playground.attach-current-buffer)))

(define-command LSSandboxBackConsole []
  (let [playground (repl-playground.create
                     :ruby
                     "sx exec rails"
                     {:scratch-suffix :-ls-sandbox-back-console.rb})]
    (playground.start)))

(define-command LSSandboxBackConsoleThis []
  (let [playground (repl-playground.create :ruby "sx exec rails")]
    (playground.attach-current-buffer)))

(define-command LSSandboxBackDb []
  (let [playground (repl-playground.create
                     :pgsql
                     "sx exec rails-db"
                     {:scratch-suffix :-ls-sandbox-back-db.pgsql})]
    (playground.start)))

(define-command LSSandboxBackDbThis []
  (let [playground (repl-playground.create :ruby "sx exec rails-db")]
    (playground.attach-current-buffer)))


(define-command LSPrCreate [label]
  (-> (string.format "%s, %s" label (linear.id))
      (github.create-pr)))

(define-command LSGo [name] {:complete livestorm.list-packages}
  (-> (livestorm.package-path name)
      (dir.go-to-folder-in-new-tab)))

(define-command LSDeployAuth []
  (terminal.direnv-exec
    "bazel run //packages/services/iam/authentication:prj.apply"
    {:name :livestorm
     :start-insert? true
     :auto-close? true
     :direction :bottom}))

(define-command LSDeployBackendRpc []
  (terminal.start "kubectl apply -f ~/Documents/backend-rpc.yaml"
                  {:name :livestorm
                   :start-insert? true
                   :auto-close? true
                   :direction :bottom}))

(define-command LSDeployBackendDb []
  (terminal.start "kubectl apply -f ~/Documents/backend-db.yaml"
                  {:name :livestorm
                   :start-insert? true
                   :auto-close? true
                   :direction :bottom}))

(define-command LSStandup []
  (let [lines (->> (vim.fn.getreg :+ 1 1)
                   (vim.tbl_map #(vim.fn.substitute $1 "\\v\\[(.+)\\]\\((.+)\\)" "<\\2|\\1> " "")))]
    (-> (draft.create :tab :markdown)
        (vim.api.nvim_buf_set_lines 0 -1 false lines))))

(define-command LSClusterCreate []
  (terminal.start "bazel run //tools/local -- create --with-components=kafka"
                  {:name :livestorm
                   :start-insert? true
                   :auto-close? true
                   :direction :bottom}))

(define-command LSClusterDelete []
  (terminal.start "bazel run //tools/local -- delete"
                  {:name :livestorm
                   :start-insert? true
                   :auto-close? true
                   :direction :bottom}))

(define-command LSClusterInspect []
  (terminal.start "k9s"
                  {:name :livestorm
                   :start-insert? true
                   :auto-close? true
                   :direction :tab}))

(define-command LSMakeTidy []
  (terminal.start (string.format "cd %s && make tidy" (livestorm.root-path))
                  {:name :livestorm
                   :start-insert? true
                   :auto-close? true
                   :direction :bottom}))

(define-command LSMakeMock []
  (terminal.start (string.format "cd %s && make mock" (livestorm.root-path))
                  {:name :livestorm
                   :start-insert? true
                   :auto-close? true
                   :direction :bottom}))

(fn open-matching-linear-ticket []
  (-> (string.format "https://linear.app/livestorm/issue/%s" (linear.id))
      (browser.open)))

(def-keys
  "run make tidy" n <space>mm (%> LSMakeTidy)
  "run make mock" n <space>mo (%> LSMakeMock)
  "create cluster"  n <space>mcc (%> LSClusterCreate)
  "delete cluster"  n <space>mcd (%> LSClusterDelete)
  "inspect cluster" n <space>mci (%> LSClusterInspect)
  "open matching linear ticket" n <leader>hl (open-matching-linear-ticket))

{}
