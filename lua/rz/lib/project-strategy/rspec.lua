-- [nfnl] Compiled from lua/rz/lib/project-strategy/rspec.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local core = autoload("nfnl.core")
local str = autoload("nfnl.string")
local rspec = autoload("rz.lib.rspec")
local function _2_(_241)
  local function _3_(command)
    return rspec.run(command)
  end
  return _3_(_241.args)
end
vim.api.nvim_create_user_command("RSpec", _2_, {complete = "file", force = true, nargs = 1})
local function _4_()
  local test_files = rspec["test-files-for-current-branch"]()
  if not core["empty?"](test_files) then
    return rspec.run(str.join(" ", test_files))
  else
    return nil
  end
end
vim.api.nvim_create_user_command("RspecChanges", _4_, {force = true})
local function fold_all_contexts()
  vim.cmd("g/\\vcontext.+do/norm zfad")
  return vim.cmd("nohlsearch")
end
local function _6_()
  return fold_all_contexts()
end
vim.keymap.set({"n"}, "zFc", _6_, {buffer = false, desc = "fold all contexts", silent = true})
return {}
