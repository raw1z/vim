(require-macros :macros.core)
(require-macros :macros.vim)

(local {: autoload} (require :nfnl.module))
(local terminal (autoload :rz.lib.terminal))
(local repl-playground (autoload :rz.lib.repl-playground))

(local rails-console-command "direnv exec $PWD bundle exec rails console")
(local rails-sandbox-command "direnv exec $PWD bundle exec rails console --sandbox")
(local rails-db-command "direnv exec $PWD bundle exec rails db -p")

(define-command RailsSandbox []
  (let [playground (repl-playground.create :ruby
                                           rails-sandbox-command
                                           {:scratch-suffix :-rails-sandbox.rb})]
     (playground.start)))

(define-command RailsSandboxThis []
  (let [playground (repl-playground.create :ruby
                                           rails-sandbox-command)]
     (playground.attach-current-buffer)))

(define-command RailsConsole []
  (let [playground (repl-playground.create :ruby
                                           rails-console-command
                                           {:scratch-suffix :-rails-console.rb})]
     (playground.start)))

(define-command RailsConsoleThis []
  (let [playground (repl-playground.create :ruby
                                           rails-console-command)]
     (playground.attach-current-buffer)))

(define-command RailsDb []
  (let [playground (repl-playground.create :ruby
                                           rails-db-command
                                           {:scratch-suffix :-rails-db-console.pgsql
                                            :activate-term-on-send? true})]
     (playground.start)))

(define-command RailsDbThis []
  (let [playground (repl-playground.create :ruby
                                           rails-db-command
                                           {:activate-term-on-send? true})]
     (playground.attach-current-buffer)))

(define-command RailsDbMigrate []
  (terminal.direnv-exec
     "bundle exec rails db:migrate"
     {:name :rails
      :start-insert? true
      :auto-close? true
      :direction :bottom}))

(define-command RailsDbMigrateStatus []
  (terminal.direnv-exec
     "bundle exec rails db:migrate:status"
     {:name :rails
      :start-insert? true
      :direction :bottom}))

(define-command RailsDbRollback []
  (terminal.direnv-exec
     "bundle exec rails db:rollback"
     {:name :rails
      :start-insert? true
      :auto-close? true
      :direction :bottom}))

(define-command RailsDbReset []
  (terminal.direnv-exec
    "bundle exec rails db:reset && bundle exec rails restart"
    {:name :rails
     :start-insert? true
     :auto-close? true
     :direction :bottom}))

(define-command RailsDbTestPrepare []
  (terminal.direnv-exec
    "env RAILS_ENV=test bundle exec rails db:test:prepare"
    {:name :rails
     :start-insert? true
     :auto-close? true
     :direction :bottom}))

(define-command RailsRestart []
  (terminal.direnv-exec
    "bundle exec rails restart"
    {:name :rails
     :start-insert? false
     :auto-close? true
     :direction :bottom
     :auto-close? true}))

(def-keys
  "open rails sandbox playground" n <space>mrs (%> RailsSandbox)
  "open rails console playground" n <space>mrc (%> RailsConsole)
  "open rails db console playground" n <space>mrd (%> RailsDb)
  "run db:migrate" n <space>mdf (%> RailsDbMigrate)
  "run db:rollback" n <space>mdb (%> RailsDbRollback)
  "run db:test:prepare" n <space>mdt (%> RailsDbTestPrepare)
  "run db:reset" n <space>mdr (%> RailsDbReset)
  "restart rails" n <space>mrr (%> RailsRestart))

{}
