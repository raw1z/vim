-- [nfnl] Compiled from lua/rz/lib/project-strategy/golang.fnl by https://github.com/Olical/nfnl, do not edit.
local function fold_all_functions()
  vim.cmd("g/func /norm zfaf")
  return vim.cmd("nohlsearch")
end
local function _1_()
  return fold_all_functions()
end
do local _ = {vim.keymap.set({"n"}, "zFf", _1_, {buffer = false, desc = "fold all functions", silent = true})} end
return {}
