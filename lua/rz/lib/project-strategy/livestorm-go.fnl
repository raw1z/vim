(require-macros :macros.core)
(require-macros :macros.vim)
(local {: autoload} (require :nfnl.module))
(local terminal (autoload :rz.lib.terminal))
(local repl-playground (autoload :rz.lib.repl-playground))
(local ui (autoload :rz.lib.ui))

(define-command BazelTest []
  (do
    (%> wa)
    (terminal.start "bazel test ..."
                    {:name :livestorm
                     :start-insert? true
                     :auto-close? true
                     :direction :bottom})))

(define-command BazelApply []
  (terminal.start "bazel run :prj.apply"
                  {:name :livestorm
                   :start-insert? true
                   :auto-close? true
                   :direction :bottom}))

(define-command BazelAddMigration [name]
  (-> (string.format "cd migrations && bazel run //tools/migrate -- %s" name)
      (terminal.start {:name :livestorm
                       :start-insert? true
                       :auto-close? true
                       :direction :bottom})))

(define-command LSDbPlayground [port?]
  (let [filetype :pgsql
        port (if (= "" port?) 5433 port?)
        command (string.format "pgcli -h localhost -p %s -U livestorm" port)
        scratch-suffix :-automated-actions.pgsql
        playground (repl-playground.create filetype command {: scratch-suffix
                                                             :activate-term-on-send? true})]
    (playground.start)))

(fn add-migration []
  (ui.input "migration name"
            "new_migration"
            #(vim.cmd (.. "BazelAddMigration " $1))))

(def-keys
  "deploy in the local cluster" n <space>ma (%> BazelApply)
  "run project tests" n <space>mt (%> BazelTest)
  "open db playground"  n <space>mdp (%> LSDbPlayground)
  "create db migration" n <space>mdm (add-migration))

{}
