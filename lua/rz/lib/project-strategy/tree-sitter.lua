-- [nfnl] Compiled from lua/rz/lib/project-strategy/tree-sitter.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local terminal = autoload("rz.lib.terminal")
local function _2_()
  return terminal["direnv-exec"]("tree-sitter generate && tree-sitter test", {name = "tree-sitter", direction = "bottom"})
end
vim.keymap.set({"n"}, "<leader>r", _2_, {buffer = false, silent = true})
local function _3_()
  return terminal["direnv-exec"]("tree-sitter generate", {name = "tree-sitter", direction = "bottom"})
end
vim.keymap.set({"n"}, "<leader>b", _3_, {buffer = false, silent = true})
return {}
