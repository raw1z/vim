(require-macros :macros.core)
(require-macros :macros.vim)
(local {: autoload} (require :nfnl.module))
(local github (autoload :rz.lib.github))
(local github-status (autoload :rz.lib.github-status))

(define-command PRStatus []
  (github-status.open))

(define-command PRWeb [pr-id?]
  (github.web pr-id?))

(define-command PRCreate [label]
  (github.create-pr label))

(def-key "show github status" n <leader>hs (%> PRStatus))
(def-key "open PR in the browser" n <leader>hv (%> PRWeb))

{}
