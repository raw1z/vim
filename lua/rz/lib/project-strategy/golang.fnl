(require-macros :macros.core)
(require-macros :macros.vim)

(fn fold-all-functions []
  (%> "g/func /norm zfaf")
  (%> "nohlsearch"))

(def-keys
  "fold all functions" n zFf (fold-all-functions))

{}
