(local {: autoload} (require :nfnl.module))
(local git (autoload :rz.lib.git))
(local path (autoload :rz.lib.path))
(local buffer (autoload :rz.lib.buffer))

(local M {:folder (-> vim.env.PWD
                      (vim.fn.fnamemodify ":.")
                      (path.join ".vim-notes"))})

(fn M.file []
  "return the note file name matching the current branch in the current folder.
  If the folder is not a git repo then the filename is 'nogit'"
  (if (git.is-git-repository? vim.env.PWD)
    (M.branch-file)
    (M.root-file)))

(fn M.branch-file []
  "return the note file name matching the current branch"
  (let [branch (or (?. vim.b.gitsigns_status_dict :head)
                   (git.current-branch))]
    (path.join M.folder (.. branch :.md))))

(fn M.root-file []
  "return the note file name matching the current folder"
  (path.join M.folder "root.md"))

(fn configure-note-buffer []
  (buffer.enable-autowrite 0)
  (set vim.wo.foldmethod :indent))

(fn M.edit []
  "open note file"
  (-> (string.format "tab drop %s" (M.file))
      (vim.cmd))
  (configure-note-buffer))

(fn M.edit-root []
  "open root note"
  (-> (string.format "tab drop %s" (M.root-file))
      (vim.cmd))
  (configure-note-buffer))

M
