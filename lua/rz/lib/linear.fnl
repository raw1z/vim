(local {: autoload} (require :nfnl.module))
(local core (autoload :nfnl.core))
(local str (autoload :nfnl.string))
(local browser (autoload :rz.lib.browser))

(local M {})
(local fetch-issues-query
  {:query "query{viewer{teams{nodes{activeCycle{name issues(filter:{assignee:{isMe:{eq:true}}}){nodes{id number identifier url title description branchName sortOrder state{name type position color}}}}}}}}"})

(fn M.id []
  "returns the linear id for the current git branch"
  (let [git (require :rz.lib.git)
        [first second & _] (-> (git.current-branch)
                               (vim.fn.split :-))]
    (vim.fn.join [first second] "-")))

(fn on-exit [_ exit-code _]
  (vim.cmd "echon ''")
  (when (not= 0 exit-code)
    (vim.cmd "echoerr 'Failed fetching linear issues'")))

(fn process-stdout [data callback]
  (when (not (str.blank? (core.first data)))
    (let [team       (-> (vim.fn.json_decode data)
                         (core.get-in [:data :viewer :teams :nodes])
                         (core.first))
          cycle-name (core.get-in team [:activeCycle :name])
          issues     (core.get-in team [:activeCycle :issues :nodes])]
      (callback {: cycle-name
                 : issues}))))

(fn M.fetch-issues [callback]
  "fetch issues for me (identified by env var LINEAR_TOKEN)"
  (vim.fn.jobstart
    [:curl
     :--noproxy
     :-X :POST "https://api.linear.app/graphql"
     :-H "Content-Type: application/json"
     :-H (.. "Authorization: " vim.env.LINEAR_TOKEN)
     :--data (vim.fn.json_encode fetch-issues-query)]
    {:stdout_buffered true
     :on_stdout #(process-stdout $2 callback)
     :on_exit on-exit})
  (let [notify (require :rz.lib.notify)]
    (notify.trace "Fetching linear issues...")))

(fn checkout-issue-branch [entry]
  (let [lazy (require :lazy)]
    (lazy.load {:plugins [:vim-fugitive]}))
  (let [branch-name entry.value.branchName
        {: exit_status} (vim.fn.FugitiveExecute [:checkout :-b branch-name :main])]
    (when (not= 0 exit_status)
      (vim.fn.FugitiveExecute [:checkout branch-name]))))

(fn browse-issue [entry]
  (browser.open entry.value.url))

(fn attach-mappings [prompt-buffer _]
  (let [actions      (require :telescope.actions)
        action_state (require :telescope.actions.state)]
    (actions.select_default:replace
      (fn []
        (actions.close prompt-buffer)
        (checkout-issue-branch (action_state.get_selected_entry))))
    (actions.select_tab:replace
      (fn []
        (actions.close prompt-buffer)
        (browse-issue (action_state.get_selected_entry))))))

(fn preview-issue [self entry _]
  (when (core.string? entry.value.description)
    (let [lines (-> entry.value.description
                    (str.split "\n"))]
      (vim.api.nvim_buf_set_lines self.state.bufnr 0 -1 false lines)
      (vim.api.nvim_buf_set_option self.state.bufnr :ft :markdown)
      (vim.api.nvim_win_set_option self.state.winid :wrap true))))

(fn state-display [state]
  (let [inflector (require :rz.lib.inflector)]
    [state
     (.. :LinearState (inflector.pascalize state))]))

(fn make-display [issue]
  (let [entry_display (require :telescope.pickers.entry_display)
        displayer (entry_display.create {:separator " "
                                         :items [{:width 10}
                                                 {:width 18}
                                                 {:remaining true}]})]
    (displayer [[issue.value.identifier :LinearIdentifier]
                (state-display issue.value.state.name)
                issue.value.title])))

(fn issue-ordinal [issue]
  (let [identifier issue.identifier
        title issue.title
        state (.. "[" issue.state.type "]")]
    (.. identifier " " state " " title)))

(fn make-entry [issue]
  {:value issue
   :display make-display
   :ordinal (issue-ordinal issue)
   :identifier issue.identifier
   :state issue.state.type})

(fn finder [issues]
  (let [finders (require :telescope.finders)]
    (finders.new_table {:results issues
                        :entry_maker make-entry})))

(fn previewer []
  (let [previewers (require :telescope.previewers)]
    (previewers.new_buffer_previewer {:define_preview preview-issue})))

(fn sorter []
  (let [conf (. (require :telescope.config) :values)]
    (conf.generic_sorter {})))

(fn picker [issues]
  (let [pickers (require :telescope.pickers)]
    (pickers.new {} {:finder (finder issues)
                     :previewer (previewer)
                     :sorter (sorter)
                     :attach_mappings attach-mappings})))

(fn show-issues-picker [{: issues}]
  (: (picker issues) :find))

(fn M.pick []
  (M.fetch-issues show-issues-picker))

M
