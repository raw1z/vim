-- [nfnl] Compiled from lua/rz/lib/github-status/status.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local job = autoload("rz.lib.job")
local text = autoload("rz.lib.text")
local node_model = autoload("rz.lib.github-status.node-model")
local M = {}
local json_config = " --json 'id,number,author,baseRefName,headRefName,title,state,statusCheckRollup,url,isDraft,mergeable,reviewDecision'"
local function fetch(command, callback)
  local function _2_(_241)
    return callback(node_model["from-json"](vim.fn.json_decode(_241[1])))
  end
  return job["run-with-timeout"](text.append(command, json_config), 10000, {["on-stdout"] = _2_})
end
M["fetch-mine"] = function(callback)
  return fetch("gh pr list --author '@me'", callback)
end
M["fetch-theirs"] = function(callback)
  return fetch("gh pr list --search 'review-requested:@me' --jq='map(select(.headRefName | contains(\"renovate-\") | not))'", callback)
end
return M
