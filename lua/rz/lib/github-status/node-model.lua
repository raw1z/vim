-- [nfnl] Compiled from lua/rz/lib/github-status/node-model.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
local success_status = "SUCCESS"
local function check_node_data(pr, check)
  local digest = require("rz.lib.digest")
  local url = (check.targetUrl or check.detailsUrl)
  return {id = (pr.id .. "/" .. digest.md5(url)), url = url, text = (check.context or check.workflowName), kind = "check", typename = check.__typename, status = (check.conclusion or check.state)}
end
local function pull_request_status(checks)
  if (0 == #checks) then
    return success_status
  else
    local function _1_(_241)
      return _241.status
    end
    return vim.fn.uniq(vim.fn.sort(vim.tbl_map(_1_, checks)))[1]
  end
end
local function pull_request_node_data(pr)
  local id = pr.number
  local checks
  local function _3_(_241)
    return (_241.status ~= success_status)
  end
  local function _4_(_241)
    return check_node_data(pr, _241)
  end
  checks = vim.tbl_filter(_3_, vim.tbl_map(_4_, pr.statusCheckRollup))
  local status = pull_request_status(checks)
  local git = require("rz.lib.git")
  local _6_
  do
    local t_5_ = pr
    if (nil ~= t_5_) then
      t_5_ = t_5_.author
    else
    end
    if (nil ~= t_5_) then
      t_5_ = t_5_.login
    else
    end
    _6_ = t_5_
  end
  return {id = id, text = pr.title, url = pr.url, kind = "pr", number = ("#" .. id), ["isDraft?"] = pr.isDraft, ["mergeable?"] = pr.mergeable, headRefName = pr.headRefName, baseRefName = pr.baseRefName, ["matchesCurrentBranch?"] = (git["current-branch"]() == pr.headRefName), author = _6_, reviewDecision = pr.reviewDecision, checks = checks, status = status}
end
M["from-json"] = function(json)
  return vim.tbl_map(pull_request_node_data, json)
end
return M
