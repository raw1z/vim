(local {: autoload} (require :nfnl.module))
(local Text (autoload :nui.text))

(local M {})
(local success-status  :SUCCESS)
(local failure-status  :FAILURE)
(local pending-icon    :•)
(local success-icon    :)
(local failure-icon    :)

(macro << [...]
  (if (= 2 (length [...]))
    `(line:append (Text ,...))
    `(line:append ,...)))

(fn offset [node line]
  (let [offset-size (- (node:get_depth) 1)
        offset (string.rep "  " offset-size)]
    (<< offset)))

(fn collapse-indicator [node line]
  (if (node:has_children)
    (if (node:is_expanded)
      (<< :- :RZGithubStatusFoldOpened)
      (<< :+ :RZGithubStatusFoldClosed))
    (<< " "))
  (<< " "))

(fn parse-pull-request-title [text]
  (let [rx "\\v^(build|chore|ci|docs|feat|fix|perf|refactor|revert|style|test)(\\(.+\\))?:\\s?(.+)$"]
    (match (vim.fn.matchlist text rx)
      [_ kind context rest] (-> (vim.iter [kind context]
                                          (vim.tbl_map vim.trim
                                                       (vim.split rest ",")))
                                (: :flatten)
                                (: :totable))
      _ nil)))

(fn status-icon [node]
  (match node.status
    success-status (Text success-icon :RZGithubStatusSuccess)
    failure-status (Text failure-icon :RZGithubStatusFailure)
    _              (Text pending-icon :RZGithubStatusPending)))

(fn mergeable-icon [node]
  (if node.mergeable?
    (Text "" :RZGithubStatusSuccess)
    (Text "" :RZGithubStatusFailure)))

(fn is-current-branch-icon []
  (Text "" :RZGithubStatusCurrentBranch))

(fn author [node]
  (Text node.author :RZGithubStatusAuthor))

(fn review-decision [node]
  (local highlight (match node.reviewDecision
                     :APPROVED :RZGithubStatusSuccess
                     :CHANGES_REQUESTED :RZGithubStatusPending
                     _ :TSComment))
  (local icon (match node.reviewDecision
                :APPROVED ""
                _ "?"))
  (Text icon highlight))

(fn append-metadata [icon line]
  (<< "  ")
  (<< icon))

(fn conventional-format-pull-request [node line kind context message tag]
  (<< kind    :RZGithubStatusCommitKind)
  (<< context :RZGithubStatusCommitContext)
  (<< ": "    :RZGithubStatusDelimiter)
  (<< message (if node.matchesCurrentBranch?
                :RZGithubStatusCurrentBranch
                :RZGithubStatusCommitMessage))

  (when (and (not= nil tag) (> (length tag) 0))
    (<< ", " :RZGithubStatusDelimiter)
    (<< tag :RZGithubStatusCommitTag)))

(fn fallback-format-pull-request [node line]
  (<< node.text (if node.matchesCurrentBranch?
                  :RZGithubStatusCurrentBranch
                  :RZGithubStatusCommitMessage)))

(fn pull-request [node line]
  (collapse-indicator node line)
  (<< node.number (if node.isDraft? :RZGithubStatusPrNumberDraft :RZGithubStatusPrNumber))
  (<< " ")

  (match (parse-pull-request-title node.text)
    [kind context message tag] (conventional-format-pull-request node line kind context message tag)
    nil (fallback-format-pull-request node line))

  (append-metadata (author node) line)

  (when node.matchesCurrentBranch?
    (append-metadata (is-current-branch-icon) line))

  (append-metadata (mergeable-icon node) line)
  (append-metadata (status-icon node) line)
  (append-metadata (review-decision node) line))

(fn check [node line]
  (<< (status-icon node))
  (<< "  ")

  (match node.status
    success-status (<< node.text :RZGithubStatusSuccess)
    failure-status (<< node.text :RZGithubStatusFailure)
    _              (<< node.text :RZGithubStatusPending)))

(fn group [node line]
  (if (= 0 (length node.text))
    (<< node.text :Normal)
    (do
      (<< "== " :RZGithubStatusDelimiter)
      (<< node.text :RZGithubStatusGroup))))

(fn message [node line]
  (<< node.text :RZGithubStatusMessage))

(fn title [node line]
  (<< (.. " " node.text " ") :RZGithubStatusTitle))

(fn M.build [node]
  (local Line (require :nui.line))
  (var line (Line))

  (offset node line)

  (match node.kind
    :group   (group node line)
    :pr      (pull-request node line)
    :message (message node line)
    :title   (title node line)
    _        (check node line))

  line)

M
