(local {: autoload} (require :nfnl.module))
(local job (autoload :rz.lib.job))
(local text (autoload :rz.lib.text))
(local node-model (autoload :rz.lib.github-status.node-model))

(local M {})

(local json-config " --json 'id,number,author,baseRefName,headRefName,title,state,statusCheckRollup,url,isDraft,mergeable,reviewDecision'")

(fn fetch [command callback]
  (job.run-with-timeout
    (text.append command json-config)
    10_000
    {:on-stdout #(callback (->> (. $1 1)
                                (vim.fn.json_decode)
                                (node-model.from-json)))}))

(fn M.fetch-mine [callback]
  (fetch
    "gh pr list --author '@me'"
    callback))

(fn M.fetch-theirs [callback]
  (fetch
    "gh pr list --search 'review-requested:@me' --jq='map(select(.headRefName | contains(\"renovate-\") | not))'"
    callback))

M
