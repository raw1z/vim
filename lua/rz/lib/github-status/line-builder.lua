-- [nfnl] Compiled from lua/rz/lib/github-status/line-builder.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local Text = autoload("nui.text")
local M = {}
local success_status = "SUCCESS"
local failure_status = "FAILURE"
local pending_icon = "\226\128\162"
local success_icon = "\239\129\152"
local failure_icon = "\239\129\151"
local function offset(node, line)
  local offset_size = (node:get_depth() - 1)
  local offset0 = string.rep("  ", offset_size)
  return line:append(offset0)
end
local function collapse_indicator(node, line)
  if node:has_children() then
    if node:is_expanded() then
      line:append(Text("-", "RZGithubStatusFoldOpened"))
    else
      line:append(Text("+", "RZGithubStatusFoldClosed"))
    end
  else
    line:append(" ")
  end
  return line:append(" ")
end
local function parse_pull_request_title(text)
  local rx = "\\v^(build|chore|ci|docs|feat|fix|perf|refactor|revert|style|test)(\\(.+\\))?:\\s?(.+)$"
  local _4_ = vim.fn.matchlist(text, rx)
  if ((_G.type(_4_) == "table") and true and (nil ~= _4_[2]) and (nil ~= _4_[3]) and (nil ~= _4_[4])) then
    local _ = _4_[1]
    local kind = _4_[2]
    local context = _4_[3]
    local rest = _4_[4]
    return vim.iter({kind, context}, vim.tbl_map(vim.trim, vim.split(rest, ","))):flatten():totable()
  else
    local _ = _4_
    return nil
  end
end
local function status_icon(node)
  local _6_ = node.status
  if (_6_ == success_status) then
    return Text(success_icon, "RZGithubStatusSuccess")
  elseif (_6_ == failure_status) then
    return Text(failure_icon, "RZGithubStatusFailure")
  else
    local _ = _6_
    return Text(pending_icon, "RZGithubStatusPending")
  end
end
local function mergeable_icon(node)
  if node["mergeable?"] then
    return Text("\239\144\135", "RZGithubStatusSuccess")
  else
    return Text("\239\144\135", "RZGithubStatusFailure")
  end
end
local function is_current_branch_icon()
  return Text("\239\144\181", "RZGithubStatusCurrentBranch")
end
local function author(node)
  return Text(node.author, "RZGithubStatusAuthor")
end
local function review_decision(node)
  local highlight
  do
    local _9_ = node.reviewDecision
    if (_9_ == "APPROVED") then
      highlight = "RZGithubStatusSuccess"
    elseif (_9_ == "CHANGES_REQUESTED") then
      highlight = "RZGithubStatusPending"
    else
      local _ = _9_
      highlight = "TSComment"
    end
  end
  local icon
  do
    local _11_ = node.reviewDecision
    if (_11_ == "APPROVED") then
      icon = "\239\146\150"
    else
      local _ = _11_
      icon = "?"
    end
  end
  return Text(icon, highlight)
end
local function append_metadata(icon, line)
  line:append("  ")
  return line:append(icon)
end
local function conventional_format_pull_request(node, line, kind, context, message, tag)
  line:append(Text(kind, "RZGithubStatusCommitKind"))
  line:append(Text(context, "RZGithubStatusCommitContext"))
  line:append(Text(": ", "RZGithubStatusDelimiter"))
  local function _13_()
    if node["matchesCurrentBranch?"] then
      return "RZGithubStatusCurrentBranch"
    else
      return "RZGithubStatusCommitMessage"
    end
  end
  line:append(Text(message, _13_()))
  if ((nil ~= tag) and (#tag > 0)) then
    line:append(Text(", ", "RZGithubStatusDelimiter"))
    return line:append(Text(tag, "RZGithubStatusCommitTag"))
  else
    return nil
  end
end
local function fallback_format_pull_request(node, line)
  local function _15_()
    if node["matchesCurrentBranch?"] then
      return "RZGithubStatusCurrentBranch"
    else
      return "RZGithubStatusCommitMessage"
    end
  end
  return line:append(Text(node.text, _15_()))
end
local function pull_request(node, line)
  collapse_indicator(node, line)
  local function _16_()
    if node["isDraft?"] then
      return "RZGithubStatusPrNumberDraft"
    else
      return "RZGithubStatusPrNumber"
    end
  end
  line:append(Text(node.number, _16_()))
  line:append(" ")
  do
    local _17_ = parse_pull_request_title(node.text)
    if ((_G.type(_17_) == "table") and (nil ~= _17_[1]) and (nil ~= _17_[2]) and (nil ~= _17_[3]) and (nil ~= _17_[4])) then
      local kind = _17_[1]
      local context = _17_[2]
      local message = _17_[3]
      local tag = _17_[4]
      conventional_format_pull_request(node, line, kind, context, message, tag)
    elseif (_17_ == nil) then
      fallback_format_pull_request(node, line)
    else
    end
  end
  append_metadata(author(node), line)
  if node["matchesCurrentBranch?"] then
    append_metadata(is_current_branch_icon(), line)
  else
  end
  append_metadata(mergeable_icon(node), line)
  append_metadata(status_icon(node), line)
  return append_metadata(review_decision(node), line)
end
local function check(node, line)
  line:append(status_icon(node))
  line:append("  ")
  local _20_ = node.status
  if (_20_ == success_status) then
    return line:append(Text(node.text, "RZGithubStatusSuccess"))
  elseif (_20_ == failure_status) then
    return line:append(Text(node.text, "RZGithubStatusFailure"))
  else
    local _ = _20_
    return line:append(Text(node.text, "RZGithubStatusPending"))
  end
end
local function group(node, line)
  if (0 == #node.text) then
    return line:append(Text(node.text, "Normal"))
  else
    line:append(Text("== ", "RZGithubStatusDelimiter"))
    return line:append(Text(node.text, "RZGithubStatusGroup"))
  end
end
local function message(node, line)
  return line:append(Text(node.text, "RZGithubStatusMessage"))
end
local function title(node, line)
  return line:append(Text((" " .. node.text .. " "), "RZGithubStatusTitle"))
end
M.build = function(node)
  local Line = require("nui.line")
  local line = Line()
  offset(node, line)
  do
    local _23_ = node.kind
    if (_23_ == "group") then
      group(node, line)
    elseif (_23_ == "pr") then
      pull_request(node, line)
    elseif (_23_ == "message") then
      message(node, line)
    elseif (_23_ == "title") then
      title(node, line)
    else
      local _ = _23_
      check(node, line)
    end
  end
  return line
end
return M
