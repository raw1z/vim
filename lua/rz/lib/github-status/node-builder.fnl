(local {: autoload} (require :nfnl.module))
(local Tree (autoload :nui.tree))

(local M {})

(fn M.title [text]
  (Tree.Node {:kind :title
              : text}))

(fn M.message [text]
  (Tree.Node {:kind :message
              : text}))

(fn M.group [id text]
  (Tree.Node {: id
              :kind :group
              : text}
             [(M.message "...")]))

(fn M.divider []
  (Tree.Node {:kind :group :text ""}))

(fn M.check [check]
  (Tree.Node check))

(fn M.pull-request [pull-request]
  (var sub-nodes (vim.tbl_map M.check pull-request.checks))
  (when (> (length sub-nodes) 0)
    (table.insert sub-nodes (M.divider)))
  (var node (Tree.Node pull-request sub-nodes))
  (if node.expanded?
    (node:expand)
    (node:collapse))
  node)

M
