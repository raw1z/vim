-- [nfnl] Compiled from lua/rz/lib/github-status/node-builder.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local Tree = autoload("nui.tree")
local M = {}
M.title = function(text)
  return Tree.Node({kind = "title", text = text})
end
M.message = function(text)
  return Tree.Node({kind = "message", text = text})
end
M.group = function(id, text)
  return Tree.Node({id = id, kind = "group", text = text}, {M.message("...")})
end
M.divider = function()
  return Tree.Node({kind = "group", text = ""})
end
M.check = function(check)
  return Tree.Node(check)
end
M["pull-request"] = function(pull_request)
  local sub_nodes = vim.tbl_map(M.check, pull_request.checks)
  if (#sub_nodes > 0) then
    table.insert(sub_nodes, M.divider())
  else
  end
  local node = Tree.Node(pull_request, sub_nodes)
  if node["expanded?"] then
    node:expand()
  else
    node:collapse()
  end
  return node
end
return M
