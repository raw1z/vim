(local M {})
(local success-status  :SUCCESS)

(fn check-node-data [pr check]
  (let [digest (require :rz.lib.digest)
        url (or (. check :targetUrl) (. check :detailsUrl))]
    {:id (.. pr.id :/ (digest.md5 url))
     : url
     :text (or (. check :context) (. check :workflowName))
     :kind :check
     :typename (. check :__typename)
     :status (or (. check :conclusion) (. check :state))}))

(fn pull-request-status [checks]
  (if (= 0 (length checks))
    success-status
    (-> (vim.tbl_map #(. $1 :status) checks)
        (vim.fn.sort)
        (vim.fn.uniq)
        (. 1))))

(fn pull-request-node-data [pr]
  (let [id (. pr :number)
        checks (->> (. pr :statusCheckRollup)
                    (vim.tbl_map #(check-node-data pr $1))
                    (vim.tbl_filter #(not= $1.status success-status)))
        status (pull-request-status checks)
        git (require :rz.lib.git)]
    {: id
     :text (. pr :title)
     :url (. pr :url)
     :kind :pr
     :number (.. :# id)
     :isDraft? (. pr :isDraft)
     :mergeable? (. pr :mergeable)
     :headRefName (. pr :headRefName)
     :baseRefName (. pr :baseRefName)
     :matchesCurrentBranch? (= (git.current-branch) (. pr :headRefName))
     :author (?. pr :author :login)
     :reviewDecision (. pr :reviewDecision)
     : checks
     : status}))

(fn M.from-json [json]
  (vim.tbl_map pull-request-node-data json))

M
