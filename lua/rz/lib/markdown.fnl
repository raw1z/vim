(require-macros :macros.core)
(require-macros :macros.vim)

(local M {})
(local {: autoload} (require :nfnl.module))
(local path (autoload :rz.lib.path))
(local terminal (autoload :rz.lib.terminal))

(fn M.preview-current []
  (let [full-path (path.expand "%:p")
        (_ bufnr) (terminal.start
                    (string.format "glow %s" full-path)
                    {:start-insert? false
                     :direction :tab})]
    (define-key/buffer bufnr n q (%> q))))

M
