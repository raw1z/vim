-- [nfnl] Compiled from lua/rz/lib/nvim-playground.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local core = require("nfnl.core")
local buffer = autoload("rz.lib.buffer")
local config = autoload("nfnl.config")
local draft = autoload("rz.lib.draft")
local fennel = autoload("nfnl.fennel")
local M = {}
local function compile(cfg, path, source)
  fennel.path = cfg({"fennel-path"})
  fennel["macro-path"] = cfg({"fennel-macro-path"})
  return pcall(fennel.compileString, source, core.merge({filename = path}, {["compiler-env"] = _G}))
end
local function eval_buffer()
  local destination_path = (vim.fn.tempname() .. ".lua")
  local root_dir = vim.fn.stdpath("config")
  local path = vim.fn.expand("%:p")
  local cfg = config["cfg-fn"]({}, {["root-dir"] = root_dir})
  local source = buffer["content-as-string"](0)
  local ok_3f, output = compile(cfg, path, source)
  if not ok_3f then
    return vim.print(output)
  else
    core.spit(destination_path, output)
    return vim.cmd(("source " .. destination_path))
  end
end
M.setup = function(bufnr)
  vim.api.nvim_buf_set_var(bufnr, "is_special_buffer", true)
  vim.api.nvim_buf_set_var(bufnr, "special_buffer_name", "Language Shell - nvim")
  buffer["enable-autowrite"](bufnr)
  return {vim.keymap.set({"n"}, "<localleader>eb", eval_buffer, {buffer = true, desc = "eval buffer", silent = true})}
end
M.open = function(direction)
  do
    local lazy = require("lazy")
    lazy.load({plugins = {"bmessages.nvim"}})
  end
  local bmessages = require("bmessages")
  local bufnr = draft.create(direction, "fennel", {suffix = "-nvim-playground.fnl", ["scratch?"] = false})
  M.setup(bufnr)
  local function _3_()
    bmessages.toggle()
    return buffer.delete(bufnr)
  end
  vim.keymap.set({"n"}, "<leader>x", _3_, {buffer = bufnr, desc = "close the playground", silent = true})
  bmessages.toggle({split_type = "vsplit"})
  return vim.cmd("wincmd p")
end
return M
