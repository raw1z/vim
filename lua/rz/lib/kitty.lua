-- [nfnl] Compiled from lua/rz/lib/kitty.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
M["reload-conf"] = function()
  local job = require("rz.lib.job")
  return job["async-run"]({"fish", "-c", "reload_kitty_conf"})
end
return M
