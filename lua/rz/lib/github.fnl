(require-macros :macros.core)
(require-macros :macros.vim)
(local {: autoload} (require :nfnl.module))
(local browser (autoload :rz.lib.browser))
(local buffer (autoload :rz.lib.buffer))
(local terminal (autoload :rz.lib.terminal))

(local M {})
(local pr-pattern "^#(%d+).*")
(local url-pattern "^.*(https://.*)$")

(fn M.silent-run [command ...]
  (let [[pr-id] [...]
        command (string.format command pr-id)]
    (vim.cmd (.. "silent !gh " command))))

(fn term-run [command opts]
  (let [opts (or opts {})
        start-insert? (if (= nil opts.start-insert?)
                       false
                       opts.start-insert?)]
    (terminal.direnv-exec
      (.. "gh " command)
      {:name :github
       :start-insert? start-insert?
       :direction :float})))

(fn match-pr? [line]
  (string.match line pr-pattern))

(fn match-url? [line]
  (string.match line url-pattern))

(fn parse-pr-id [line]
  (string.gsub line pr-pattern "%1"))

(fn parse-url [line]
  (string.gsub line url-pattern "%1"))

(fn navigate-to-pr [line]
  (->> (parse-pr-id line)
       (M.silent-run "pr view --web %s")))

(fn navigate-to-url [line]
  (-> (parse-url line)
      (browser.open)))

(fn navigate []
  (let [line (buffer.current-line)]
    (if
      (match-pr? line) (navigate-to-pr line)
      (match-url? line) (navigate-to-url line))))

(fn checkout-pr []
  (let [line (buffer.current-line)]
    (when (match-pr? line)
      (let [command (->> (parse-pr-id line)
                         (string.format "pr checkout %s"))]
        (term-run command {:start-insert? true})))))

(fn setup-keymaps []
  (define-key/this-buffer n :q (%> quit))
  (define-key/this-buffer n :s checkout-pr)
  (define-key/this-buffer n :<Enter> navigate))

(fn run [command ...]
  (let [command (string.format command ...)]
    (term-run command)
    (setup-keymaps)))

(fn run-insert [command ...]
  (let [command (string.format command ...)]
    (term-run command {:start-insert? true})
    (setup-keymaps)))

(fn M.issues []
  (run "pr list --author '@me'"))

(fn M.checks [pr-id]
  (run "pr checks --watch %s" pr-id))

(fn view-checks []
  (let [line (buffer.current-line)]
    (when (match-pr? line)
      (->> (parse-pr-id line)
           (M.checks)))))

(fn M.status []
  (run "pr status")
  (define-key/this-buffer n :<S-k> view-checks))

(fn M.view [pr-id]
  (run "pr view --comments %s" pr-id))

(fn M.web [pr-id]
  (M.silent-run "pr view --web %s" pr-id))

(fn M.create-pr [label]
  (run-insert "pr create --draft --title '%s' --base main --fill" label))

M
