-- [nfnl] Compiled from lua/rz/lib/folds.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local buffer = autoload("rz.lib.buffer")
local text = autoload("rz.lib.text")
local ft = autoload("Comment.ft")
local U = autoload("Comment.utils")
local M = {}
M.foldtext = function()
  local indent = vim.fn["repeat"](" ", vim.fn.indent(vim.v.foldstart))
  local commentchars = vim.fn.trim(text.clear(vim.bo.commentstring, "%s"))
  local text0 = vim.fn.trim(vim.fn.trim(vim.fn.trim(text.clear(vim.api.nvim_buf_get_lines(0, (vim.v.foldstart - 1), vim.v.foldend, false)[1], vim.split(vim.wo.foldmarker, ","))), commentchars))
  return string.format("%s%s", indent, text0)
end
return M
