(local {: autoload} (require :nfnl.module))
(local core (autoload :nfnl.core))
(local scandir (autoload :plenary.scandir))
(local list (autoload :rz.lib.list))
(local text (autoload :rz.lib.text))

(local M {})

(fn M.expand [path]
  "returns the absolute path matching the given one"
  (vim.fn.expand path))

(fn M.exist? [path]
  "returns true if a file exists at the given false, otherwise returns false"
  (let [full-path (M.expand path)
        f (io.open full-path :r)]
    (when (not= nil f) (: f :close))
    (not= nil f)))

(fn M.read [path]
  "returns the content of the file at the given path"
  (let [full-path (M.expand path)
        f (io.open full-path :r)]
    (when (not= nil f)
      (local content (: f :read :*a))
      (: f :close)
      content)))

(fn M.lua-alternate [path]
  "returns the lua alternate file matching the given fennel file"
  (vim.fn.substitute path "\\.fnl" :.lua :g))

(fn M.join [...]
  "join the given parts with the path separator"
  (-> (vim.fn.join [...] :/)
      (vim.fn.substitute :// :/ :g)))

(fn M.dirname [path]
  "returns the dirname for the given path"
  (if (vim.fn.isdirectory path)
    (vim.fn.fnamemodify path ":h")
    (vim.fn.fnamemodify path ":p:h")))

(fn M.basename [path]
  "returns the basename for the given path"
  (if (vim.fn.isdirectory path)
    (vim.fn.fnamemodify path ":t")
    (vim.fn.fnamemodify path ":p:t")))

(fn M.basename-without-extension [path]
  "returns the basename for the given path without its extension"
  (vim.fn.fnamemodify path ":p:t:r"))

(fn M.current-directory []
  "returns the current directory's path"
  (vim.fn.getcwd))

(fn M.scan [path opts]
  "returns the list of files for the given directory path"
  (let [opts (or opts {:hidden false})
        strip-basename? (if (core.nil? opts.strip-basename?) true opts.strip-basename?)
        scan-opts (vim.fn.extendnew {:depth 1}
                                    opts)
        files (scandir.scan_dir path scan-opts)
        filtered-files (if opts.filter
                         (vim.tbl_filter opts.filter files)
                         files)]
    (if strip-basename?
      (list.map-strip filtered-files [path :/])
      filtered-files)))

(fn M.same? [path1 path2]
  "returns true if two paths are the same, after expanding them to their absolute form"
  (= (M.expand path1)
     (M.expand path2)))

(fn M.starts-with? [path prefix-path]
  "return true if the expanded path starts with the expanded prefix path otherwise return false"
  (text.starts-with? (M.expand path)
                     (M.expand prefix-path)))

(fn M.relative-from [path base]
  "return the relative path from the given base directory to the given path"
  (let [relative (text.replace path base "")]
    (if (text.starts-with? relative :/)
      (vim.fn.slice relative 1)
      relative)))

(fn M.preview [path]
  "preview the file at the given path"
  (let [job (require :rz.lib.job)]
    (-> (string.format "qlmanage -p %s" path)
        (job.run))))

(fn M.create [path content]
  "create a file at the given path with the given content"
  (with-open [fout (io.open path :w)]
    (fout:write (if (vim.tbl_islist content)
                  (list.flat-join content "\n")
                  content))))

(fn M.delete [path]
  "delete the file at the given path"
  (vim.fn.delete path))

M
