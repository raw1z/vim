-- [nfnl] Compiled from lua/rz/lib/job.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
M["run-with-timeout"] = function(command, timeout, _1_)
  local on_start = _1_["on-start"]
  local on_error = _1_["on-error"]
  local on_success = _1_["on-success"]
  local on_timeout = _1_["on-timeout"]
  local on_stdout = _1_["on-stdout"]
  local on_stderr = _1_["on-stderr"]
  local cwd = _1_["cwd"]
  local done_3f = false
  local notify = require("rz.lib.notify")
  local on_start0
  local or_2_ = on_start
  if not or_2_ then
    local function _3_()
    end
    or_2_ = _3_
  end
  on_start0 = or_2_
  local on_error0
  local or_4_ = on_error
  if not or_4_ then
    local function _5_(_241)
      return notify.error(string.format("Failed running '%s': %s", vim.inspect(command), vim.inspect(_241)))
    end
    or_4_ = _5_
  end
  on_error0 = or_4_
  local on_success0
  local or_6_ = on_success
  if not or_6_ then
    local function _7_()
    end
    or_6_ = _7_
  end
  on_success0 = or_6_
  local on_timeout0
  local or_8_ = on_timeout
  if not or_8_ then
    local function _9_()
      return notify.warn(string.format("Timeout error while running '%s'", vim.inspect(command)))
    end
    or_8_ = _9_
  end
  on_timeout0 = or_8_
  local on_stdout0
  local or_10_ = on_stdout
  if not or_10_ then
    local function _11_(output)
      return output
    end
    or_10_ = _11_
  end
  on_stdout0 = or_10_
  local on_stderr0
  local or_12_ = on_stderr
  if not or_12_ then
    local function _13_(output)
      return output
    end
    or_12_ = _13_
  end
  on_stderr0 = or_12_
  local cwd0 = (cwd or vim.uv.cwd())
  local timer = vim.uv.new_timer()
  local jobid
  local function _14_(_241, _242)
    return on_stdout0(_242)
  end
  local function _15_(_241, _242)
    return on_stderr0(_242)
  end
  local function _16_(_241, _242)
    done_3f = true
    if (0 ~= _242) then
      return on_error0(_242)
    else
      return on_success0()
    end
  end
  jobid = vim.fn.jobstart(command, {cwd = cwd0, stderr_buffered = true, stdout_buffered = true, on_stdout = _14_, on_stderr = _15_, on_exit = _16_})
  on_start0()
  local function _18_()
    if not done_3f then
      done_3f = true
      vim.fn.jobstop(jobid)
      timer:stop()
      timer:close()
      return on_timeout0()
    else
      return nil
    end
  end
  return timer:start(timeout, 0, vim.schedule_wrap(_18_))
end
M["async-run"] = function(command, opts)
  return M["run-with-timeout"](command, 10000, (opts or {}))
end
M.run = function(command)
  return vim.fn.system(command)
end
M.capture = function(command)
  return vim.fn.trim(vim.fn.system(command))
end
return M
