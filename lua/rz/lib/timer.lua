-- [nfnl] Compiled from lua/rz/lib/timer.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
M.new = function()
  local timer = vim.uv.new_timer()
  local M0 = {}
  M0["schedule-once"] = function(timeout, f)
    return vim.uv.timer_start(timer, timeout, 0, vim.schedule_wrap(f))
  end
  M0["repeat-every"] = function(...)
    local _1_, _2_, _3_ = ...
    if ((nil ~= _1_) and (nil ~= _2_) and (nil ~= _3_)) then
      local timeout = _1_
      local interval = _2_
      local f = _3_
      return vim.uv.timer_start(timer, timeout, interval, vim.schedule_wrap(f))
    elseif ((nil ~= _1_) and (nil ~= _2_)) then
      local interval = _1_
      local f = _2_
      return vim.uv.timer_start(timer, interval, interval, vim.schedule_wrap(f))
    else
      return nil
    end
  end
  M0.stop = function()
    return vim.uv.timer_stop(timer)
  end
  M0.destroy = function()
    M0.stop()
    return timer:close()
  end
  return M0
end
return M
