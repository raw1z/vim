-- [nfnl] Compiled from lua/rz/lib/draft.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
local function create_scratch_buffer(direction)
  local buffer = require("rz.lib.buffer")
  local tmp_9_auto = buffer.new(direction)
  vim.api.nvim_buf_set_var(tmp_9_auto, "special_buffer_name", "Draft")
  vim.api.nvim_buf_set_var(tmp_9_auto, "is_special_buffer", true)
  return tmp_9_auto
end
M.folder = function()
  local path = require("rz.lib.path")
  return path.join(vim.fn.stdpath("data"), "junk")
end
local function junk_file_path(opts)
  local path = require("rz.lib.path")
  return path.join(M.folder(), (os.date("%Y/%m/%d/%H_%M_%S") .. opts.suffix))
end
M["junk-file"] = function(opts)
  local path = require("rz.lib.path")
  local file_path = junk_file_path(opts)
  vim.fn.mkdir(path.dirname(file_path), "p")
  vim.fn.writefile({}, file_path)
  return file_path
end
local function setup_autowrite(bufnr)
  do
    local buffer = require("rz.lib.buffer")
    buffer["enable-autowrite"](bufnr)
  end
  return bufnr
end
local function create_junk_buffer(direction, opts)
  local name = M["junk-file"](opts)
  local buffer = require("rz.lib.buffer")
  local bufnr = buffer.new(direction, {listed = true, scratch = false})
  vim.api.nvim_buf_set_name(bufnr, name)
  return setup_autowrite(bufnr)
end
local function create_buffer(direction, opts)
  if opts["scratch?"] then
    return create_scratch_buffer(direction)
  else
    return create_junk_buffer(direction, opts)
  end
end
local function configure_buffer(bufnr, filetype)
  vim.api.nvim_set_option_value("filetype", filetype, {buf = bufnr})
  return bufnr
end
local function setup_keymaps(bufnr, opts)
  if opts["scratch?"] then
    local function _2_()
      return vim.cmd("echo noop")
    end
    local function _3_()
      return vim.cmd("echo noop")
    end
    do local _ = {vim.keymap.set({"n"}, "<C-o>", _2_, {buffer = bufnr, silent = true}), vim.keymap.set({"n"}, "<C-i>", _3_, {buffer = bufnr, silent = true})} end
  else
  end
  return bufnr
end
M.create = function(direction, filetype, opts)
  local opts0 = (opts or {["scratch?"] = true, suffix = ""})
  return setup_keymaps(configure_buffer(create_buffer(direction, opts0), filetype), opts0)
end
return M
