(local M {})

(fn M.reload-conf []
  (let [job (require :rz.lib.job)]
    (job.async-run [:fish :-c :reload_kitty_conf])))

M
