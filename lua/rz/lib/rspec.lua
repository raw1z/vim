-- [nfnl] Compiled from lua/rz/lib/rspec.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local core = autoload("nfnl.core")
local str = autoload("nfnl.string")
local terminal = autoload("rz.lib.terminal")
local git = autoload("rz.lib.git")
local M = {}
M.run = function(location)
  return terminal["direnv-exec"](("rspec --format progress --no-profile " .. location .. " 2>/dev/null"), {name = "rspec"})
end
local function working_files()
  local function _2_(_241)
    return _241.file
  end
  local function _3_(_241)
    return (_241.status ~= "deleted")
  end
  return core.map(_2_, core.filter(_3_, git.status()))
end
local function branch_changed_files()
  local trunk_branch = git["guess-trunk-branch"]()
  local current_branch = git["current-branch"]()
  if (str["blank?"](trunk_branch) or str["blank?"](current_branch)) then
    return {}
  else
    local function _4_(_241)
      return _241.path
    end
    return core.map(_4_, git["changed-files-between-branches"](trunk_branch, current_branch))
  end
end
local function matching_test_file(path)
  if (-1 ~= vim.fn.stridx(path, "_spec.rb")) then
    return path
  else
    return vim.fn.substitute(vim.fn.substitute(path, "/app/", "/spec/", ""), ".rb", "_spec.rb", "")
  end
end
M["test-files-for-current-branch"] = function()
  local function _7_(_241)
    return vim.fn.fnamemodify(_241, ":~:.")
  end
  local function _8_(_241)
    return (0 ~= vim.fn.filereadable(_241))
  end
  local function _9_(_241)
    return (-1 ~= vim.fn.stridx(_241, vim.fn.getcwd()))
  end
  local function _10_(_241)
    return string.match(_241, ".rb$")
  end
  return vim.fn.uniq(vim.fn.sort(core.map(_7_, core.filter(_8_, core.filter(_9_, core.map(matching_test_file, core.filter(_10_, core.concat(working_files(), branch_changed_files()))))))))
end
return M
