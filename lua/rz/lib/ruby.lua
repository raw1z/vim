-- [nfnl] Compiled from lua/rz/lib/ruby.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local str = autoload("nfnl.string")
local notify = autoload("rz.lib.notify")
local job = autoload("rz.lib.job")
local browser = autoload("rz.lib.browser")
local dir = autoload("rz.lib.dir")
local M = {}
local function parse_gem(line)
  local basename = vim.fn.fnamemodify(line, ":t")
  local pattern = "\\v(.+)-((-)@!)"
  local version = vim.fn.substitute(basename, pattern, "\\2", "")
  local name = vim.fn.substitute(basename, ("-" .. version), "", "")
  return {path = line, name = name, version = version}
end
local function parse_gems(lines)
  local function _2_(_241)
    return not str["blank?"](_241)
  end
  return vim.tbl_map(parse_gem, vim.tbl_filter(_2_, lines))
end
local function make_display(item, column_length)
  local entry_display = require("telescope.pickers.entry_display")
  local displayer = entry_display.create({separator = " ", items = {{width = (2 + column_length)}, {remaining = true}}})
  return displayer({item.value.name, {item.value.version, "RZDraftFindDate"}})
end
local function make_entry(gem, column_length)
  local function _3_(_241)
    return make_display(_241, column_length)
  end
  return {value = gem, ordinal = gem.name, display = _3_, path = gem.path}
end
local function name_column_length(gems)
  local function _4_(_241)
    return #_241.name
  end
  return vim.fn.max(vim.tbl_map(_4_, gems))
end
local function finder(lines)
  local finders = require("telescope.finders")
  local gems = parse_gems(lines)
  local function _5_(_241)
    return make_entry(_241, name_column_length(gems))
  end
  return finders.new_table({results = gems, entry_maker = _5_})
end
local function browse(item)
  return browser.open(string.format("https://rubygems.org/gems/%s", item.value.name))
end
local function edit(item, direction)
  local path = item.value.path
  dir.open(path, direction)
  return vim.cmd(("lcd " .. path))
end
local function attach_mappings(prompt_bufnr)
  do
    local actions = require("telescope.actions")
    local actions_state = require("telescope.actions.state")
    local function _6_()
      actions.close(prompt_bufnr)
      return browse(actions_state.get_selected_entry())
    end
    actions.select_default:replace(_6_)
    local function _7_()
      actions.close(prompt_bufnr)
      return edit(actions_state.get_selected_entry(), "tab")
    end
    actions.select_tab:replace(_7_)
    local function _8_()
      actions.close(prompt_bufnr)
      return edit(actions_state.get_selected_entry(), "horizontal")
    end
    actions.select_horizontal:replace(_8_)
    local function _9_()
      actions.close(prompt_bufnr)
      return edit(actions_state.get_selected_entry(), "vertical")
    end
    actions.select_vertical:replace(_9_)
  end
  return true
end
local function build_picker(lines)
  local telescope_themes = require("telescope.themes")
  local opts = telescope_themes.get_dropdown({previewer = false})
  local conf = require("telescope.config").values
  local pickers = require("telescope.pickers")
  return pickers.new(opts, {finder = finder(lines), sorter = conf.file_sorter(opts), attach_mappings = attach_mappings})
end
local function pick_gem(lines)
  local picker = build_picker(lines)
  return picker:find()
end
M["list-gems"] = function()
  notify.trace("Listing gems...")
  return job["async-run"]("bundle list --paths", {["on-stdout"] = pick_gem})
end
return M
