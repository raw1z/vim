-- [nfnl] Compiled from lua/rz/lib/plugins.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
M["parse-plugins"] = function(...)
  local plugins,opts = {}, {}
  for _, item in ipairs({...}) do
    local function _1_(...)
      if ("table" == type(item)) then
        opts[plugins[#plugins]] = item
        return {plugins, nil}
      else
        return {vim.fn.extend(plugins, {tostring(item)}), opts}
      end
    end
    local _set_2_ = _1_(...)
    plugins = _set_2_[1]
    opts = _set_2_[2]
  end
  return {plugins, opts}
end
M["build-specs"] = function(plugins, opts)
  local tbl_21_auto = {}
  local i_22_auto = 0
  for _, plugin in ipairs(plugins) do
    local val_23_auto
    do
      local plugin_opts = (opts[plugin] or {})
      plugin_opts[1] = plugin
      val_23_auto = nil
    end
    if (nil ~= val_23_auto) then
      i_22_auto = (i_22_auto + 1)
      tbl_21_auto[i_22_auto] = val_23_auto
    else
    end
  end
  return tbl_21_auto
end
M.declare = function(url, opts)
  return vim.tbl_extend("force", (opts or {}), {url})
end
M.key = function(mode, lhs, rhs, opts)
  return vim.tbl_extend("force", (opts or {}), {lhs, rhs, silent = true, mode = mode})
end
return M
