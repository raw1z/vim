-- [nfnl] Compiled from lua/rz/lib/statusline.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local vimbar = autoload("rz.lib.vimbar")
local utils = require("heirline.utils")
local conditions = require("heirline.conditions")
local nvim_web_devicons = require("nvim-web-devicons")
local strings = require("plenary.strings")
local left_border = "\238\130\182\226\150\136"
local right_border = "\226\150\136\238\130\180"
local pill_separator = "\226\150\136\238\130\188"
local function i(...)
  return utils.insert(...)
end
local function package_loaded_3f(pkg)
  return (nil ~= package.loaded[pkg])
end
local function pill(left_text, right_text, hl_opts)
  local hl_opts0 = (hl_opts or {})
  local left_hl = (hl_opts0.left or "StatusLinePillLeft")
  local right_hl = (hl_opts0.right or "StatusLinePillRight")
  local left_border_hl = (hl_opts0["left-border"] or "StatusLinePillLeftBorder")
  local right_border_hl = (hl_opts0["right-border"] or "StatusLinePillRightBorder")
  local sep_hl = (hl_opts0.sep or "StatusLinePillSeparator")
  local _2_
  if ("table" == type(right_text)) then
    _2_ = right_text
  else
    _2_ = {provider = right_text, hl = right_hl}
  end
  return i({provider = left_border, hl = left_border_hl}, {provider = left_text, hl = left_hl}, {provider = pill_separator, hl = sep_hl}, _2_, {provider = right_border, hl = right_border_hl})
end
local separator = {provider = " "}
local function vi_mode_highlight(scope, mode)
  local suffix
  if ((mode == "i") or (mode == "ic")) then
    suffix = "Insert"
  elseif ((mode == "v") or (mode == "V") or (mode == "\22")) then
    suffix = "Visual"
  elseif ((mode == "c") or (mode == "cv") or (mode == "ce") or (mode == "!")) then
    suffix = "Command"
  elseif ((mode == "s") or (mode == "S") or (mode == "\19")) then
    suffix = "Select"
  elseif ((mode == "r") or (mode == "R") or (mode == "Rv") or (mode == "rm") or (mode == "r?")) then
    suffix = "Replace"
  elseif (mode == "t") then
    suffix = "Terminal"
  else
    local _ = mode
    suffix = "Normal"
  end
  return ("StatusLine" .. scope .. suffix)
end
local function vi_mode_label(mode)
  if ((mode == "n") or (mode == "no")) then
    return "normal"
  elseif ((mode == "i") or (mode == "ic")) then
    return "insert"
  elseif ((mode == "v") or (mode == "\22")) then
    return "visual"
  elseif (mode == "V") then
    return "visual-line"
  elseif ((mode == "c") or (mode == "cv") or (mode == "ce") or (mode == "!")) then
    return "command"
  elseif ((mode == "s") or (mode == "\19")) then
    return "select"
  elseif (mode == "S") then
    return "select-line"
  elseif ((mode == "r") or (mode == "R") or (mode == "Rv") or (mode == "rm") or (mode == "r?")) then
    return "replace"
  elseif (mode == "t") then
    return "terminal"
  else
    local _ = mode
    return mode
  end
end
local vi_mode
local function _6_(_241)
  _241.mode = vim.fn.mode()
  return nil
end
local function _7_(_241)
  return string.format(" %s", vi_mode_label(_241.mode))
end
local function _8_(_241)
  return vi_mode_highlight("ViModeLeft", _241.mode)
end
local function _9_(_241)
  return vi_mode_highlight("ViModeMiddle", _241.mode)
end
local function _10_(_241)
  return vi_mode_highlight("ViModeRight", _241.mode)
end
vi_mode = i({init = _6_, update = {"ModeChanged", "ColorScheme"}}, pill("\238\159\133", _7_, {["left-border"] = _8_, left = _9_, sep = _10_}), separator)
local function has_command_3f()
  local buftype = vim.bo.buftype
  local vimbar0 = require("rz.lib.vimbar")
  if ("terminal" ~= buftype) then
    local _11_ = vimbar0.eval("%S")
    if ((_11_ == "") or (_11_ == "gj") or (_11_ == "gk")) then
      return false
    else
      local _ = _11_
      return true
    end
  else
    return nil
  end
end
local command = i({condition = has_command_3f}, pill("cmd", " %S"), separator)
local recording
local function _14_()
  return ("" ~= vim.fn.reg_recording())
end
local function _15_()
  return string.format(" %s", vim.fn.reg_recording())
end
recording = i({condition = _14_, update = {"RecordingEnter", "RecordingLeave"}}, pill("macro", _15_, {["left-border"] = "StatusLineRecordingLeft", left = "StatusLineRecording", sep = "StatusLineRecordingRight"}), separator)
local autowrite
local function _16_()
  return vim.b["autowrite-enabled"]
end
autowrite = i({condition = _16_}, separator, pill("autowrite", " on"))
local session
local function _17_()
  return (nil ~= vim.g.rz_updating_session)
end
session = i({condition = _17_}, separator, pill("session", " on"))
local function dap_session_active_3f()
  if package_loaded_3f("dap") then
    local dap = require("dap")
    return (nil ~= dap.session())
  else
    return nil
  end
end
local dap
local function _19_()
  local dap0 = require("dap")
  return string.format(" %s", dap0.status())
end
dap = i({condition = dap_session_active_3f}, pill("debug", _19_, {["left-border"] = "StatusLineDapLeft", left = "StatusLineDap", sep = "StatusLineDapRight"}))
local test
local function _20_()
  return (nil ~= vim.g.rz_test_status)
end
local function _21_()
  return string.format(" %s", vim.g.rz_test_status)
end
test = i({condition = _20_, update = vimbar["redraw-on-event"]("User", {"NeotestCleaned", "NeotestDone", "NeotestRunning", "NeotestStarting"})}, separator, pill("test", _21_, {["left-border"] = "StatusLineTestLeft", left = "StatusLineTest", sep = "StatusLineTestRight"}))
local spacer = {provider = "%="}
local function branch_name()
  return vim.b.gitsigns_status_dict.head
end
local function branch_clean_3f()
  return ((0 == (vim.b.gitsigns_status_dict.added or 0)) and (0 == (vim.b.gitsigns_status_dict.removed or 0)) and (0 == (vim.b.gitsigns_status_dict.changed or 0)))
end
local function branch_change(name, prefix, hl)
  local function _22_()
    local count = (vim.b.gitsigns_status_dict[name] or 0)
    return ((count > 0) and (prefix .. count))
  end
  local function _23_()
    return {fg = utils.get_highlight(hl).fg, bg = utils.get_highlight("StatusLinePillRight").bg}
  end
  return {provider = _22_, hl = _23_}
end
local branch_changes = i({provider = " ", hl = "StatusLinePillRight"}, i({condition = branch_clean_3f}, {provider = "\226\128\162", hl = "StatusLinePillRight"}), branch_change("added", "+", "GitSignsAdd"), branch_change("removed", "-", "GitSignsDelete"), branch_change("changed", "~", "GitSignsChange"))
local branch = i({condition = conditions.is_git_repo}, separator, pill(branch_name, branch_changes))
local function has_filetype_3f()
  local filetype = vim.bo.filetype
  local ignored_filetypes = {"dirbuf", "DiffviewFileHistory", "fugitive", "lazy", "TelescopePrompt"}
  if vim.tbl_contains(ignored_filetypes, filetype) then
    return false
  elseif (0 == #filetype) then
    return false
  else
    return true
  end
end
local function lsp_list()
  local client_names
  local function _25_(_241)
    return _241.name
  end
  client_names = vim.fn.uniq(vim.fn.sort(vim.tbl_map(_25_, vim.lsp.get_clients({bufnr = 0}))))
  if (1 == vim.fn.empty(client_names)) then
    return " \226\128\162"
  else
    return string.format(" %s ", vim.fn.join(client_names, " \226\128\162 "))
  end
end
local filetype
local function _27_(_241)
  _241.filetype = vim.bo.filetype
  _241.filename = vim.api.nvim_buf_get_name(0)
  _241.extension = vim.fn.fnamemodify(_241.filename, ":e")
  return nil
end
local function _28_(_241)
  return _241.filetype
end
filetype = i({condition = has_filetype_3f, init = _27_}, separator, pill(_28_, lsp_list))
local line_info = i(separator, pill("%l:%c", " %P"))
return {vi_mode, command, recording, spacer, dap, test, branch, autowrite, session, filetype, line_info}
