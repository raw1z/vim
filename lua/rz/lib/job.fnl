(local M {})

(fn M.run-with-timeout [command timeout {: on-start
                                         : on-error
                                         : on-success
                                         : on-timeout
                                         : on-stdout
                                         : on-stderr
                                         : cwd}]
  (var done? false)
  (let [notify (require :rz.lib.notify)
        on-start   (or on-start   (fn []))
        on-error   (or on-error   #(notify.error (string.format "Failed running '%s': %s" (vim.inspect command) (vim.inspect $1))))
        on-success (or on-success (fn []))
        on-timeout (or on-timeout #(notify.warn (string.format "Timeout error while running '%s'" (vim.inspect command))))
        on-stdout  (or on-stdout  (fn [output] output))
        on-stderr  (or on-stderr  (fn [output] output))
        cwd (or cwd (vim.uv.cwd))
        timer (vim.uv.new_timer)
        jobid (vim.fn.jobstart command
                               {: cwd
                                :stderr_buffered true
                                :stdout_buffered true
                                :on_stdout #(on-stdout $2)
                                :on_stderr #(on-stderr $2)
                                :on_exit #(do
                                            (set done? true)
                                            (if (not= 0 $2)
                                              (on-error $2)
                                              (on-success)))})]
    (on-start)
    (: timer :start timeout 0 (vim.schedule_wrap
                                #(when (not done?)
                                   (set done? true)
                                   (vim.fn.jobstop jobid)
                                   (: timer :stop)
                                   (: timer :close)
                                   (on-timeout))))))

(fn M.async-run [command opts]
  (M.run-with-timeout command 10_000 (or opts {})))

(fn M.run [command]
  (vim.fn.system command))

(fn M.capture [command]
  (-> (vim.fn.system command)
      (vim.fn.trim)))

M
