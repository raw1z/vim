-- [nfnl] Compiled from lua/rz/lib/browser.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
M.open = function(url_or_string)
  do
    local lazy = require("lazy")
    lazy.load({plugins = {"open-browser.vim"}})
  end
  return vim.fn["openbrowser#open"](url_or_string)
end
return M
