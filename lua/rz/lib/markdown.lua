-- [nfnl] Compiled from lua/rz/lib/markdown.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local path = autoload("rz.lib.path")
local terminal = autoload("rz.lib.terminal")
M["preview-current"] = function()
  local full_path = path.expand("%:p")
  local _, bufnr = terminal.start(string.format("glow %s", full_path), {direction = "tab", ["start-insert?"] = false})
  local function _2_()
    return vim.cmd("q")
  end
  return vim.keymap.set({"n"}, "q", _2_, {buffer = bufnr, silent = true})
end
return M
