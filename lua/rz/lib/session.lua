-- [nfnl] Compiled from lua/rz/lib/session.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local path = autoload("rz.lib.path")
local git = autoload("rz.lib.git")
local notify = autoload("rz.lib.notify")
local buffer = autoload("rz.lib.buffer")
local inflector = autoload("rz.lib.inflector")
local M = {}
M["loading?"] = function()
  return (true == vim.g.loading_session)
end
M.folder = function()
  return path.join(vim.fn.fnamemodify(vim.env.PWD, ":."), "/.vim-sessions")
end
M.file = function()
  local branch
  local _3_
  do
    local t_2_ = vim.b.gitsigns_status_dict
    if (nil ~= t_2_) then
      t_2_ = t_2_.head
    else
    end
    _3_ = t_2_
  end
  branch = (_3_ or git["current-branch"]())
  local folder_path = M.folder()
  local branch_file_name = inflector.underscore(branch)
  if (0 == #branch) then
    return (folder_path .. "/nogit")
  else
    return (folder_path .. "/" .. branch_file_name)
  end
end
M.save = function()
  vim.fn.mkdir(M.folder(), "p")
  local session_file = M.file()
  vim.cmd("windo cclose")
  vim.cmd(string.format("mksession! %s", vim.fn.fnameescape(session_file)))
  return session_file
end
M.load = function()
  local file_path = M.file()
  vim.g.loading_session = true
  vim.cmd(("silent! source " .. file_path))
  vim.g.loaded_session = file_path
  vim.g.loaded_session_folder = M.folder()
  vim.g.loading_session = false
  return nil
end
M.delete = function()
  return vim.uv.fs_unlink(M.file())
end
M["file-exist?"] = function()
  return path["exist?"](M.file())
end
M["loaded?"] = function()
  return path["same?"](M.file(), vim.g.loaded_session)
end
local function has_modified_buffers_3f(buffers)
  local modified_buffers
  local function _6_(_241)
    return vim.api.nvim_buf_get_option(_241, "modified")
  end
  modified_buffers = vim.tbl_filter(_6_, buffers)
  return (0 > #modified_buffers)
end
local function delete_buffers(buffers)
  local current_buffer = buffer.current()
  local buffers_to_delete
  local function _7_(_241)
    return (_241 ~= current_buffer)
  end
  buffers_to_delete = vim.tbl_filter(_7_, vim.tbl_filter(vim.api.nvim_buf_is_valid, buffers))
  for _, buf in ipairs(buffers_to_delete) do
    vim.api.nvim_buf_delete(buf, {force = true})
  end
  return vim.api.nvim_buf_delete(current_buffer, {force = true})
end
local function safe_delete_existing_buffers(callback)
  local buffers = vim.api.nvim_list_bufs()
  if has_modified_buffers_3f(buffers) then
    return notify.error("can't load session: the files in the current session have changed.")
  else
    local function _8_()
      M["stop-updates"]()
      delete_buffers(buffers)
      return callback()
    end
    return vim.schedule(_8_)
  end
end
M["safe-load"] = function()
  if ((0 == vim.fn.argc()) and not M["loading?"]() and M["file-exist?"]() and not M["loaded?"]()) then
    return safe_delete_existing_buffers(M.load)
  else
    return nil
  end
end
local function update()
  if path["same?"](vim.g.rz_updating_session, M.file()) then
    return M.save()
  else
    return M["stop-updates"]()
  end
end
M["keep-updated"] = function()
  if (nil == vim.g.rz_updating_session) then
    do
      local _12_ = vim.api.nvim_create_augroup("rz_session_updates", {clear = true})
      local function _13_()
        return update()
      end
      local function _14_()
        return update()
      end
      do local _ = {vim.api.nvim_create_autocmd({"VimLeavePre"}, {callback = _13_, group = _12_, pattern = {"*"}}), vim.api.nvim_create_autocmd({"BufEnter"}, {callback = _14_, group = _12_, pattern = {"*"}})} end
    end
    vim.g.rz_updating_session = M.file()
    return nil
  else
    return nil
  end
end
M["stop-updates"] = function()
  if (nil ~= vim.g.rz_updating_session) then
    vim.api.nvim_del_augroup_by_name("rz_session_updates")
    vim.g.rz_updating_session = nil
    return nil
  else
    return nil
  end
end
return M
