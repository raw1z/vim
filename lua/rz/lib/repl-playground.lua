-- [nfnl] Compiled from lua/rz/lib/repl-playground.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local buffer = autoload("rz.lib.buffer")
local draft = autoload("rz.lib.draft")
local str = autoload("nfnl.string")
local terminal = autoload("rz.lib.terminal")
local M = {}
M.create = function(filetype, command, opts)
  local opts0 = (opts or {})
  local playground = {opts = opts0, filetype = filetype, command = command}
  playground["opts"]["scratch-suffix"] = (opts0["scratch-suffix"] or "")
  local _2_
  if (nil ~= opts0["activate-term-on-send?"]) then
    _2_ = opts0["activate-term-on-send?"]
  else
    _2_ = false
  end
  playground["opts"]["activate-term-on-send?"] = _2_
  playground["create-input-buffer"] = function()
    local input_buffer = draft.create("tab", filetype, {suffix = playground.opts["scratch-suffix"], ["scratch?"] = false})
    playground["input-buffer"] = input_buffer
    vim.api.nvim_buf_set_var(input_buffer, "is_special_buffer", true)
    vim.api.nvim_buf_set_var(input_buffer, "special_buffer_name", string.format("Language Shell - %s", playground.filetype))
    return playground["setup-input-keymaps"]()
  end
  playground["attach-buffer"] = function(bufnr)
    playground["input-buffer"] = bufnr
    buffer["switch-to"](bufnr)
    return playground["setup-input-keymaps"]()
  end
  playground["create-term-buffer"] = function()
    do
      local channel_id, buffer_number = terminal.start(command, {direction = "horizontal", ["start-insert?"] = false})
      playground["channel-id"] = channel_id
      playground["term-buffer"] = buffer_number
    end
    vim.cmd("wincmd K")
    buffer["scroll-to-bottom"](playground["term-buffer"])
    return buffer["switch-to"](playground["input-buffer"])
  end
  playground["setup-input-keymaps"] = function()
    do local _ = {vim.keymap.set({"n"}, "<leader>x", playground.close, {buffer = true, desc = "close the playground", silent = true}), vim.keymap.set({"n"}, "<leader>r", playground["restart-term"], {buffer = true, desc = "restart the repl", silent = true}), vim.keymap.set({"n"}, "<Enter>", playground["send-current-line-to-repl"], {buffer = true, desc = "send current line to the repl", silent = true}), vim.keymap.set({"v"}, "<Enter>", playground["send-current-selection-to-repl"], {buffer = true, desc = "send current selection to repl", silent = true})} end
    local vim_36_auto = require("rz.lib.vim")
    return vim_36_auto["add-operator"]({"n"}, "<localleader><space>", {buffer = true, silent = true}, playground["send-textobject-to-repl"])
  end
  playground.close = function()
    playground["close-buffer"]("term")
    return playground["close-buffer"]("input")
  end
  playground["close-buffer"] = function(kind)
    local buffer_number = playground[(kind .. "-buffer")]
    return buffer.delete(buffer_number)
  end
  playground["restart-term"] = function()
    playground["close-buffer"]("term")
    return playground["create-term-buffer"]()
  end
  playground["send-current-line-to-repl"] = function()
    return playground["send-to-repl"]({vim.fn.getline(".")})
  end
  playground["send-current-selection-to-repl"] = function()
    return playground["send-to-repl"](buffer["current-visual-selection"]())
  end
  playground["send-textobject-to-repl"] = function(_4_)
    local text = _4_["text"]
    return playground["send-to-repl"](text)
  end
  playground["send-to-repl"] = function(code)
    local code_lines
    local function _5_(_241)
      return (_241 .. "\13")
    end
    local function _6_(_241)
      return not str["blank?"](_241)
    end
    code_lines = vim.tbl_map(_5_, vim.tbl_map(str.trimr, vim.tbl_filter(_6_, code)))
    for _, input in ipairs(code_lines) do
      vim.fn.chansend(playground["channel-id"], input)
    end
    buffer["scroll-to-bottom"](playground["term-buffer"])
    if playground.opts["activate-term-on-send?"] then
      buffer["switch-to"](playground["term-buffer"])
      return vim.cmd("startinsert")
    else
      buffer["switch-to"](playground["input-buffer"])
      return vim.cmd("stopinsert")
    end
  end
  playground.start = function()
    playground["create-input-buffer"]()
    return playground["create-term-buffer"]()
  end
  playground["attach-current-buffer"] = function()
    playground["attach-buffer"](buffer.current())
    return playground["create-term-buffer"]()
  end
  return playground
end
return M
