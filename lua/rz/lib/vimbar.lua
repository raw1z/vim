-- [nfnl] Compiled from lua/rz/lib/vimbar.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
M.eval = function(str, opts)
  local result = vim.api.nvim_eval_statusline(str, (opts or {}))
  return result.str
end
M["is-special-buffer?"] = function(bufnr)
  return pcall(vim.api.nvim_buf_get_var, bufnr, "is_special_buffer")
end
M["special-buffer-filename"] = function(bufnr)
  local ok_3f, name = pcall(vim.api.nvim_buf_get_var, bufnr, "special_buffer_name")
  if ok_3f then
    return string.upper(name)
  else
    return nil
  end
end
M["sanitize-filename"] = function(filename)
  local conditions = require("heirline.conditions")
  local short_3f = conditions.width_percent_below(#filename, 0.25)
  if short_3f then
    return filename
  else
    return vim.fn.pathshorten(filename)
  end
end
M["redraw-on-event"] = function(event, pattern)
  local pattern0 = (pattern or "*:*")
  local tmp_9_auto
  local function _3_()
    return vim.cmd("redrawstatus")
  end
  tmp_9_auto = {pattern = pattern0, callback = vim.schedule_wrap(_3_)}
  tmp_9_auto[1] = event
  return tmp_9_auto
end
return M
