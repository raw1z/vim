(require-macros :macros.core)

(local {: autoload} (require :nfnl.module))
(local text (autoload :rz.lib.text))

(local M {})

(when (not= 0 (vim.fn.empty M))
  (tset M :traces [])
  (tset M :timeout 5000))

(fn max-width [lines]
  (accumulate [acc 0
               _ line (ipairs lines)]
    (math.max acc (length line))))

(fn display [entry]
  (if
    (= :string (type entry))
      entry
    (= :table (type entry))
      (vim.inspect entry)
    (tostring entry)))

(fn build-lines []
  (accumulate [acc []
               _ entry (ipairs M.traces)]
              (vim.fn.extendnew (text.lines (display entry))
                                acc)))

(fn create-traces-window [lines]
  (local lazy (require :lazy))
  (lazy.load {:plugins [:nui.nvim]})

  (let [height (length lines)
        width (max-width lines)
        Popup (require :nui.popup)
        popup (Popup {:focusable false
                      :position {:row (- vim.o.lines height 1)
                                 :col (- vim.o.columns width)}
                      :relative :editor
                      :size {: width
                             : height}
                      :win_options {:winhighlight "Normal:Normal"}})]
    (tset M :popup-bufnr popup.bufnr)
    (popup:on :BufDelete
              #(popup:unmount)
              {:once true})
    (vim.api.nvim_buf_set_lines popup.bufnr 0 0 false lines)
    (popup:mount)))

(fn destroy-traces-window []
  (when (and (not= nil M.popup-bufnr)
             (vim.api.nvim_buf_is_valid M.popup-bufnr))
    (vim.api.nvim_buf_delete M.popup-bufnr {:force true})))

(fn render []
  (destroy-traces-window)
  (let [lines (build-lines)]
    (when (> (length lines) 0)
      (create-traces-window lines))))

(fn pop-traces []
  (table.remove M.traces))

(fn update []
  (render)
  (when (> (length M.traces) 0)
    (-> #(do
           (pop-traces)
           (update)) 
        (vim.defer_fn M.timeout))
    nil))

(fn M.print [data]
  (table.insert M.traces 1 data)
  (update))

M
