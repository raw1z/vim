(require-macros :macros.core)
(require-macros :macros.vim)

(local {: autoload} (require :nfnl.module))
(local core (autoload :nfnl.core))

(local M {})

(%g vim.g.rz_named_terminals {})

(fn M.prevent-leaving [buffer-number]
  (define-keys/buffer (or buffer-number 0)
    n <C-o> (fn [])
    n <C-i> (fn [])))

(fn M.setup-insert-mode [buffer-number]
  (let [session (require :rz.lib.session)]
    (define-augroup rz_terminal_insert_mode
      (define-autocmd/buffer BufEnter (or buffer-number 0)
        (when (not (session.loading?))
          (%> startinsert)
          (%> redraw)))

      (define-autocmd/buffer BufLeave (or buffer-number 0)
        (when (not (session.loading?))
          (%> stopinsert)
          (%> redraw))))))

(fn M.setup-auto-close [buffer-number]
  (define-augroup rz_terminal_autoclose
    (define-autocmd/buffer TermClose (or buffer-number 0)
      (when (= 0 vim.v.event.status)
        (-> (vim.fn.bufwinid buffer-number)
            (vim.api.nvim_win_close true))))))

(fn create-terminal []
  (let [terminal {}]

    (fn terminal.create-window [direction]
      (let [window (require :rz.lib.window)
            buffer-number (vim.api.nvim_create_buf false true)]
        (vim.api.nvim_buf_set_option buffer-number :bufhidden :delete)
        (tset terminal :buffer-number buffer-number)
        (window.open buffer-number direction)
        (M.prevent-leaving buffer-number)))

    (fn terminal.build-term-command [shell-command]
      (if
        (core.nil? shell-command)
          ""
        (core.string? shell-command)
          shell-command
        (vim.fn.join shell-command " ")))

    (fn terminal.open-term [shell-command opts]
      (let [fish-path (vim.fn.exepath :fish)
            command   (terminal.build-term-command shell-command)
            opts (or opts {})
            start-insert? (or (. opts :start-insert?) false)
            auto-close? (or (. opts :auto-close?) false)
            buffer-name (if (core.empty? command)
                          "Terminal"
                          command)
            channel-id (if (core.empty? command)
                            (vim.fn.termopen fish-path)
                            (vim.fn.termopen [fish-path :-i :-c command]))]
        (M.setup-insert-mode terminal.buffer-number)
        (vim.api.nvim_buf_set_var terminal.buffer-number :special_buffer_name buffer-name)
        (vim.api.nvim_buf_set_var terminal.buffer-number :is_special_buffer true)
        (when start-insert?
          (vim.api.nvim_input :A))
        (when auto-close?
          (M.setup-auto-close terminal.buffer-number))
        channel-id))

    (fn terminal.buffer-exist? []
      (not (core.nil? terminal.buffer-number)))

    (fn terminal.destroy-buffer []
      (when (not= 0 (vim.fn.bufexists terminal.buffer-number))
        (vim.cmd (.. terminal.buffer-number "bwipeout!"))))

    (fn terminal.start [direction shell-command opts]
      (when (terminal.buffer-exist?)
        (terminal.destroy-buffer))
      (terminal.create-window direction)
      (terminal.open-term shell-command opts))

    terminal))

(fn M.start [shell-command opts]
  (let [opts (or opts {})
        direction (or opts.direction :horizontal)
        terminal (or (. vim.g.rz_named_terminals opts.name)
                     (create-terminal))]
    (when (not (core.nil? opts.name))
      (set vim.g.rz_named_terminals
           (vim.tbl_extend :force vim.g.rz_named_terminals {opts.name terminal})))
    (values (terminal.start direction shell-command opts)
            terminal.buffer-number)))

(fn M.direnv-exec [shell-command opts]
  (M.start
    (.. "direnv exec $PWD " shell-command)
    opts))

M
