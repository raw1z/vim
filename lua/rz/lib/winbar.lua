-- [nfnl] Compiled from lua/rz/lib/winbar.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local vimbar = autoload("rz.lib.vimbar")
local conditions = require("heirline.conditions")
local utils = require("heirline.utils")
local nvim_web_devicons = autoload("nvim-web-devicons")
local function i(...)
  return utils.insert(...)
end
local function is_active_3f()
  return (vim.g.actual_curwin == tostring(vim.fn.win_getid()))
end
local function is_modified_3f()
  return vim.bo.modified
end
local function is_readonly_3f()
  return (("terminal" ~= vim.bo.buftype) and (not vim.bo.modifiable or vim.bo.readonly))
end
local function left_delimiter_hl()
  if is_active_3f() then
    return "RZWinbarLeftDelimiter"
  else
    return "RZWinbarLeftDelimiterInactive"
  end
end
local function right_delimiter_hl()
  if is_active_3f() then
    return "RZWinbarRightDelimiter"
  else
    return "RZWinbarRightDelimiterInactive"
  end
end
local function text_hl()
  if is_active_3f() then
    return "RZWinbarText"
  else
    return "RZWinbarTextInactive"
  end
end
local function flag_hl()
  if is_active_3f() then
    return "RZWinbarFlag"
  else
    return "RZWinbarFlagInactive"
  end
end
local function spacer_hl()
  if is_active_3f() then
    return "RZWinbarSpacer"
  else
    return "RZWinbarSpacerInactive"
  end
end
local function icon_hl()
  if is_active_3f() then
    return "RZWinbarIcon"
  else
    return "RZWinbarIconInactive"
  end
end
local function icon_delimiter_hl()
  if is_active_3f() then
    return "RZWinbarIconDelimiter"
  else
    return "RZWinbarIconDelimiterInactive"
  end
end
local spacer = {provider = "%=", hl = spacer_hl}
local left_delimiter = {provider = "\238\130\182\226\150\136", hl = left_delimiter_hl}
local right_delimiter = {provider = "\226\150\136\238\130\180", hl = right_delimiter_hl}
local function flag(condition, text)
  return i({condition = condition}, {provider = " ", hl = flag_hl}, {provider = text, hl = flag_hl})
end
local flags = {flag(is_modified_3f, "+"), flag(is_readonly_3f, "\239\145\150")}
local function sanitized_filename(self)
  local filename = vim.fn.fnamemodify(self.filename, ":.")
  if vimbar["is-special-buffer?"](0) then
    return vimbar["special-buffer-filename"](0)
  elseif ("" == filename) then
    return "UNNAMED"
  elseif ("help" == vim.bo.filetype) then
    return vim.fn.fnamemodify(self.filename, ":t")
  else
    return vimbar["sanitize-filename"](filename)
  end
end
local function file_icon(self)
  local icon, _ = nvim_web_devicons.get_icon_color(self.filename)
  return icon
end
local filename
local function _10_(_241)
  _241.filename = vim.api.nvim_buf_get_name(0)
  return nil
end
local function _11_(_241)
  return string.upper(sanitized_filename(_241))
end
filename = i({init = _10_}, {provider = " ", hl = text_hl}, {provider = _11_, hl = text_hl})
local function get_count(kind)
  return #vim.diagnostic.get(0, {severity = vim.diagnostic.severity[string.upper(kind)]})
end
local function get_icon(kind)
  local inflector = require("rz.lib.inflector")
  local name = string.format("DiagnosticSign%s", inflector.capitalize(kind))
  local t_12_ = vim.fn.sign_getdefined(name)
  if (nil ~= t_12_) then
    t_12_ = t_12_[1]
  else
  end
  if (nil ~= t_12_) then
    t_12_ = t_12_.text
  else
  end
  return t_12_
end
local diagnostics
local function _15_(_241)
  _241.errors = get_count("error")
  _241.warnings = get_count("warn")
  _241.infos = get_count("info")
  _241.hints = get_count("hint")
  _241.icons = {error = get_icon("error"), warn = get_icon("warn"), info = get_icon("info"), hint = get_icon("hint")}
  return nil
end
local function _16_(_241)
  return (_241.errors > 0)
end
local function _17_(_241)
  return string.format(" %s%s", _241.icons.error, _241.errors)
end
local function _18_(_241)
  return (_241.warnings > 0)
end
local function _19_(_241)
  return string.format(" %s%s", _241.icons.warn, _241.warnings)
end
local function _20_(_241)
  return (_241.infos > 0)
end
local function _21_(_241)
  return string.format(" %s%s", _241.icons.info, _241.infos)
end
local function _22_(_241)
  return (_241.hints > 0)
end
local function _23_(_241)
  return string.format(" %s%s", _241.icons.hint, _241.hints)
end
diagnostics = i({condition = conditions.has_diagnostics, init = _15_, update = {"DiagnosticChanged", "BufEnter"}}, {condition = _16_, provider = _17_, hl = "DiagnosticError"}, {condition = _18_, provider = _19_, hl = "DiagnosticWarn"}, {condition = _20_, provider = _21_, hl = "Diagnosticinfo"}, {condition = _22_, provider = _23_, hl = "DiagnosticHint"}, {provider = " \226\148\128\226\148\128", hl = spacer_hl})
local function file_results()
  local filename0 = vim.api.nvim_buf_get_name(0)
  local _25_
  do
    local t_24_ = vim.g.rz_test_results
    if (nil ~= t_24_) then
      t_24_ = t_24_[filename0]
    else
    end
    _25_ = t_24_
  end
  return (_25_ or {passed = 0, failed = 0, skipped = 0})
end
local function show_test_results_3f(self)
  return ((file_results().passed > 0) or (file_results().failed > 0))
end
local function formatted_results()
  local results = file_results()
  if (results.failed > 0) then
    return "\226\156\150"
  elseif (results.passed > 0) then
    return "\226\156\148"
  else
    return nil
  end
end
local function formatted_results_hl()
  local results = file_results()
  if (results.failed > 0) then
    return "RZWinbarTestFailed"
  elseif (results.passed > 0) then
    return "RZWinbarTestPassed"
  else
    return nil
  end
end
local function formatted_results_delimiter_hl()
  local results = file_results()
  if (results.failed > 0) then
    return "RZWinbarTestFailedDelimiter"
  elseif (results.passed > 0) then
    return "RZWinbarTestPassedDelimiter"
  else
    return nil
  end
end
local test = i({update = vimbar["redraw-on-event"]("User", "NeotestDone")}, i({condition = show_test_results_3f}, {provider = "\226\149\140\226\149\140\226\149\140\226\149\140", hl = spacer_hl}, {provider = "\238\130\182", hl = formatted_results_delimiter_hl}, {provider = formatted_results, hl = formatted_results_hl}, {provider = "\238\130\180", hl = formatted_results_delimiter_hl}))
return {test, spacer, diagnostics, filename, flags}
