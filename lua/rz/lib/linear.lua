-- [nfnl] Compiled from lua/rz/lib/linear.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local core = autoload("nfnl.core")
local str = autoload("nfnl.string")
local browser = autoload("rz.lib.browser")
local M = {}
local fetch_issues_query = {query = "query{viewer{teams{nodes{activeCycle{name issues(filter:{assignee:{isMe:{eq:true}}}){nodes{id number identifier url title description branchName sortOrder state{name type position color}}}}}}}}"}
M.id = function()
  local git = require("rz.lib.git")
  local _let_2_ = vim.fn.split(git["current-branch"](), "-")
  local first = _let_2_[1]
  local second = _let_2_[2]
  local _ = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_2_, 3)
  return vim.fn.join({first, second}, "-")
end
local function on_exit(_, exit_code, _0)
  vim.cmd("echon ''")
  if (0 ~= exit_code) then
    return vim.cmd("echoerr 'Failed fetching linear issues'")
  else
    return nil
  end
end
local function process_stdout(data, callback)
  if not str["blank?"](core.first(data)) then
    local team = core.first(core["get-in"](vim.fn.json_decode(data), {"data", "viewer", "teams", "nodes"}))
    local cycle_name = core["get-in"](team, {"activeCycle", "name"})
    local issues = core["get-in"](team, {"activeCycle", "issues", "nodes"})
    return callback({["cycle-name"] = cycle_name, issues = issues})
  else
    return nil
  end
end
M["fetch-issues"] = function(callback)
  local function _5_(_241, _242)
    return process_stdout(_242, callback)
  end
  vim.fn.jobstart({"curl", "--noproxy", "-X", "POST", "https://api.linear.app/graphql", "-H", "Content-Type: application/json", "-H", ("Authorization: " .. vim.env.LINEAR_TOKEN), "--data", vim.fn.json_encode(fetch_issues_query)}, {stdout_buffered = true, on_stdout = _5_, on_exit = on_exit})
  local notify = require("rz.lib.notify")
  return notify.trace("Fetching linear issues...")
end
local function checkout_issue_branch(entry)
  do
    local lazy = require("lazy")
    lazy.load({plugins = {"vim-fugitive"}})
  end
  local branch_name = entry.value.branchName
  local _let_6_ = vim.fn.FugitiveExecute({"checkout", "-b", branch_name, "main"})
  local exit_status = _let_6_["exit_status"]
  if (0 ~= exit_status) then
    return vim.fn.FugitiveExecute({"checkout", branch_name})
  else
    return nil
  end
end
local function browse_issue(entry)
  return browser.open(entry.value.url)
end
local function attach_mappings(prompt_buffer, _)
  local actions = require("telescope.actions")
  local action_state = require("telescope.actions.state")
  local function _8_()
    actions.close(prompt_buffer)
    return checkout_issue_branch(action_state.get_selected_entry())
  end
  actions.select_default:replace(_8_)
  local function _9_()
    actions.close(prompt_buffer)
    return browse_issue(action_state.get_selected_entry())
  end
  return actions.select_tab:replace(_9_)
end
local function preview_issue(self, entry, _)
  if core["string?"](entry.value.description) then
    local lines = str.split(entry.value.description, "\n")
    vim.api.nvim_buf_set_lines(self.state.bufnr, 0, -1, false, lines)
    vim.api.nvim_buf_set_option(self.state.bufnr, "ft", "markdown")
    return vim.api.nvim_win_set_option(self.state.winid, "wrap", true)
  else
    return nil
  end
end
local function state_display(state)
  local inflector = require("rz.lib.inflector")
  return {state, ("LinearState" .. inflector.pascalize(state))}
end
local function make_display(issue)
  local entry_display = require("telescope.pickers.entry_display")
  local displayer = entry_display.create({separator = " ", items = {{width = 10}, {width = 18}, {remaining = true}}})
  return displayer({{issue.value.identifier, "LinearIdentifier"}, state_display(issue.value.state.name), issue.value.title})
end
local function issue_ordinal(issue)
  local identifier = issue.identifier
  local title = issue.title
  local state = ("[" .. issue.state.type .. "]")
  return (identifier .. " " .. state .. " " .. title)
end
local function make_entry(issue)
  return {value = issue, display = make_display, ordinal = issue_ordinal(issue), identifier = issue.identifier, state = issue.state.type}
end
local function finder(issues)
  local finders = require("telescope.finders")
  return finders.new_table({results = issues, entry_maker = make_entry})
end
local function previewer()
  local previewers = require("telescope.previewers")
  return previewers.new_buffer_previewer({define_preview = preview_issue})
end
local function sorter()
  local conf = require("telescope.config").values
  return conf.generic_sorter({})
end
local function picker(issues)
  local pickers = require("telescope.pickers")
  return pickers.new({}, {finder = finder(issues), previewer = previewer(), sorter = sorter(), attach_mappings = attach_mappings})
end
local function show_issues_picker(_11_)
  local issues = _11_["issues"]
  return picker(issues):find()
end
M.pick = function()
  return M["fetch-issues"](show_issues_picker)
end
return M
