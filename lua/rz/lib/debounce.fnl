(local M {})

(fn M.new []
  (let [timer (require :rz.lib.timer)
        debounce {:running false
                  :timer (timer.new)}]

    (fn debounce.schedule-after [timeout f]
      (when (not debounce.running)
        (debounce.timer.stop)
        (debounce.timer.schedule-once timeout
                                      #(do
                                         (tset debounce :running true)
                                         (f)
                                         (tset debounce :running false)))))

    debounce))

M
