(local M {})

(fn M.open [url-or-string]
  "open an url or trigger a search in the browser"
  (let [lazy (require :lazy)]
    (lazy.load {:plugins [:open-browser.vim]}))
  (vim.fn.openbrowser#open url-or-string))

M
