-- [nfnl] Compiled from lua/rz/lib/dir.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
M.open = function(path, direction)
  local buffer = require("rz.lib.buffer")
  local oil = require("oil")
  buffer.new(direction)
  return oil.open(path)
end
M["open-current-buffer-file-dirname"] = function(direction)
  return M.open(vim.fn.expand("%:p:h"), direction)
end
M["open-project-root-directory"] = function(direction)
  return M.open(vim.fn["projectroot#guess"](), direction)
end
M["go-to-folder-in-new-tab"] = function(path)
  M.open(path, "tab")
  return vim.cmd(("lcd " .. path))
end
M["contains?"] = function(file, dir_path)
  local path = require("rz.lib.path")
  return path["exist?"](path.join((dir_path or vim.fn.getcwd()), file))
end
return M
