-- [nfnl] Compiled from lua/rz/lib/gemfile.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local buffer = autoload("rz.lib.buffer")
local browser = autoload("rz.lib.browser")
local ts_utils = autoload("nvim-treesitter.ts_utils")
local list = autoload("rz.lib.list")
local M = {}
local query_string = "\n  (call arguments: (argument_list .\n                                  (string (string_content) @gem-name) .\n                                  (pair key: (hash_key_symbol) @symbol\n                                        value: (string (string_content) @repo))?))"
local function node_at_cursor()
  local node = ts_utils.get_node_at_cursor()
  local parent = node:parent()
  local start_row = node:start()
  while ((nil ~= parent) and (start_row == parent:start())) do
    node = parent
    parent = node:parent()
  end
  return node
end
local function captures_for_node(node)
  local bufnr = vim.api.nvim_get_current_buf()
  local query = vim.treesitter.query.parse("ruby", query_string)
  local captures
  do
    local tbl_21_auto = {}
    local i_22_auto = 0
    for _, capture in query:iter_captures(node, bufnr) do
      local val_23_auto = vim.treesitter.get_node_text(capture, bufnr)
      if (nil ~= val_23_auto) then
        i_22_auto = (i_22_auto + 1)
        tbl_21_auto[i_22_auto] = val_23_auto
      else
      end
    end
    captures = tbl_21_auto
  end
  return list.flatten(captures)
end
local function uri_for_captures(captures)
  if ((_G.type(captures) == "table") and true and (captures[2] == "git") and (nil ~= captures[3])) then
    local _ = captures[1]
    local repo = captures[3]
    return repo
  elseif ((_G.type(captures) == "table") and (nil ~= captures[1])) then
    local gem_name = captures[1]
    return ("https://rubygems.org/gems/" .. gem_name)
  else
    return nil
  end
end
local function browse_gem()
  vim.cmd("norm _")
  if ("gem" == buffer["current-word"]()) then
    return browser.open(uri_for_captures(captures_for_node(node_at_cursor())))
  else
    return nil
  end
end
M.setup = function()
  if ("Gemfile" == vim.fn.expand("<afile>")) then
    return vim.keymap.set({"n"}, "<Enter>", browse_gem, {buffer = true, silent = true})
  else
    return nil
  end
end
return M
