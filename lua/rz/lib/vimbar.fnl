(require-macros :macros.core)

(local M {})

(fn M.eval [str opts]
  "evaluate statusline string, accepts same options as nvim_eval_statusline"
  (let [result (vim.api.nvim_eval_statusline str (or opts {}))]
    result.str))

(fn M.is-special-buffer? [bufnr]
  (pcall vim.api.nvim_buf_get_var bufnr :is_special_buffer))

(fn M.special-buffer-filename [bufnr]
  (let [(ok? name) (pcall vim.api.nvim_buf_get_var bufnr :special_buffer_name)]
    (if ok?
      (string.upper name)
      nil)))

(fn M.sanitize-filename [filename]
  (let [conditions (require :heirline.conditions)
        short? (conditions.width_percent_below (length filename) 0.25)]
    (if short?
      filename
      (vim.fn.pathshorten filename))))

(fn M.redraw-on-event [event pattern]
  (let [pattern (or pattern "*:*")]
    (doto {: pattern
           :callback (vim.schedule_wrap #(%> redrawstatus))}
          (tset 1 event))))

M
