(local {: autoload} (require :nfnl.module))
(local buffer (autoload :rz.lib.buffer))
(local text (autoload :rz.lib.text))
(local ft (autoload :Comment.ft))
(local U (autoload :Comment.utils))

(local M {})

(fn M.foldtext []
  (let [indent (->> (vim.fn.indent vim.v.foldstart)
                    (vim.fn.repeat " "))
        commentchars (-> (text.clear vim.bo.commentstring :%s)
                         (vim.fn.trim))
        text (-> (vim.api.nvim_buf_get_lines 0 (- vim.v.foldstart 1) vim.v.foldend false)
                 (. 1)
                 (text.clear (vim.split vim.wo.foldmarker ","))
                 (vim.fn.trim)
                 (vim.fn.trim commentchars)
                 (vim.fn.trim))]
    (string.format "%s%s"
                   indent
                   text)))

M
