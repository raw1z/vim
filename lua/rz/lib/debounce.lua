-- [nfnl] Compiled from lua/rz/lib/debounce.fnl by https://github.com/Olical/nfnl, do not edit.
local M = {}
M.new = function()
  local timer = require("rz.lib.timer")
  local debounce = {timer = timer.new(), running = false}
  debounce["schedule-after"] = function(timeout, f)
    if not debounce.running then
      debounce.timer.stop()
      local function _1_()
        debounce["running"] = true
        f()
        debounce["running"] = false
        return nil
      end
      return debounce.timer["schedule-once"](timeout, _1_)
    else
      return nil
    end
  end
  return debounce
end
return M
