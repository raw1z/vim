(local M {})

(fn M.new []
  (let [timer (vim.uv.new_timer)
        M {}]

    (fn M.schedule-once [timeout f]
      (vim.uv.timer_start timer
                      timeout
                      0
                      (vim.schedule_wrap f)))

    (fn M.repeat-every [...]
      (match [...]
        [timeout interval f]
          (vim.uv.timer_start timer
                          timeout
                          interval
                          (vim.schedule_wrap f))
        [interval f]
          (vim.uv.timer_start timer
                          interval
                          interval (vim.schedule_wrap f))))

    (fn M.stop []
      (vim.uv.timer_stop timer))

    (fn M.destroy []
      (M.stop)
      (: timer :close))

    M))

M
