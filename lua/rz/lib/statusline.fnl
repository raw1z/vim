(require-macros :macros.core)

(local {: autoload} (require :nfnl.module))
(local vimbar (autoload :rz.lib.vimbar))
(local utils (require :heirline.utils))
(local conditions (require :heirline.conditions))
(local nvim-web-devicons (require :nvim-web-devicons))
(local strings (require :plenary.strings))

(local left-border :█)
(local right-border :█)
(local pill-separator :█)

(fn i [...] (utils.insert ...))

(macro hi [group attr]
  `(. (utils.get_highlight ,group) ,(tostring attr)))

(fn package-loaded? [pkg]
  (not= nil (. package :loaded pkg)))

(fn pill [left-text right-text hl-opts]
  (let [hl-opts (or hl-opts {})
        left-hl (or hl-opts.left :StatusLinePillLeft)
        right-hl (or hl-opts.right :StatusLinePillRight)
        left-border-hl (or hl-opts.left-border :StatusLinePillLeftBorder)
        right-border-hl (or hl-opts.right-border :StatusLinePillRightBorder)
        sep-hl (or hl-opts.sep :StatusLinePillSeparator)]
    (i {:provider left-border
        :hl left-border-hl}

       {:provider left-text
        :hl left-hl}

       {:provider pill-separator
        :hl sep-hl}

       (if (= :table (type right-text))
         right-text
         {:provider right-text
          :hl right-hl})

       {:provider right-border
        :hl right-border-hl})))

; separator {{{
(local separator {:provider " "})
; }}}
; vi mode {{{
(fn vi-mode-highlight [scope mode]
  (let [suffix (match mode
                 (where (or :i :ic))              :Insert
                 (where (or :v :V ""))          :Visual
                 (where (or :c :cv :ce :!))       :Command
                 (where (or :s :S ""))          :Select
                 (where (or :r :R :Rv :rm :r?))   :Replace
                 :t                               :Terminal
                 _                                :Normal)]
    (.. :StatusLine scope suffix)))

(fn vi-mode-label [mode]
  (match mode
    (where (or :n :no))              :normal
    (where (or :i :ic))              :insert
    (where (or :v ""))             :visual
    :V                               :visual-line
    (where (or :c :cv :ce :!))       :command
    (where (or :s ""))             :select
    :S                               :select-line
    (where (or :r :R :Rv :rm :r?))   :replace
    :t                               :terminal
    _                                mode))

(local vi-mode (i {:init #(set $1.mode (vim.fn.mode))
                   :update [:ModeChanged :ColorScheme]}

                  (pill :
                        #(string.format " %s" (vi-mode-label $1.mode))
                        {:left-border #(vi-mode-highlight :ViModeLeft $1.mode)
                         :left #(vi-mode-highlight :ViModeMiddle $1.mode)
                         :sep #(vi-mode-highlight :ViModeRight $1.mode)})

                  separator))
; }}}
; command {{{
(fn has-command? []
  (let [buftype vim.bo.buftype
        vimbar (require :rz.lib.vimbar)]
    (when (not= :terminal buftype)
        (match (vimbar.eval :%S)
                 (where (or "" :gj :gk)) false
                 _ true))))

(local command (i {:condition has-command?}
                  (pill :cmd " %S")
                  separator))

; }}}
; recording {{{
(local recording (i {:condition #(not= "" (vim.fn.reg_recording))
                     :update (%w RecordingEnter RecordingLeave)}

                    (pill :macro
                          #(string.format " %s" (vim.fn.reg_recording))
                          {:left-border :StatusLineRecordingLeft
                           :left :StatusLineRecording
                           :sep :StatusLineRecordingRight})

                    separator
                    ))
; }}}
; autowrite {{{
(local autowrite (i {:condition #vim.b.autowrite-enabled}
                    separator
                    (pill :autowrite " on")))
; }}}
; session {{{
(local session (i {:condition #(not= nil vim.g.rz_updating_session)}
                  separator
                  (pill :session " on")))
; }}}
; dap {{{
(fn dap-session-active? []
  (when (package-loaded? :dap)
    (let [dap (require :dap)]
      (not= nil (dap.session)))))

(local dap (i {:condition dap-session-active?}
              (pill :debug
                    #(let [dap (require :dap)]
                       (string.format " %s" (dap.status)))
                    {:left-border :StatusLineDapLeft
                     :left :StatusLineDap
                     :sep :StatusLineDapRight})))
; }}}
; test {{{
  (local test (i {:condition #(not= nil vim.g.rz_test_status)
                  :update (vimbar.redraw-on-event :User (%w NeotestCleaned
                                                            NeotestDone
                                                            NeotestRunning
                                                            NeotestStarting))}
                  separator
                  (pill :test
                        #(string.format " %s" vim.g.rz_test_status)
                        {:left-border :StatusLineTestLeft
                        :left :StatusLineTest
                        :sep :StatusLineTestRight})))
; }}}
; spacer {{{
(local spacer {:provider :%=})
; }}}
; branch {{{
(fn branch-name []
  vim.b.gitsigns_status_dict.head)

(fn branch-clean? []
  (and (= 0 (or vim.b.gitsigns_status_dict.added 0))
       (= 0 (or vim.b.gitsigns_status_dict.removed 0))
       (= 0 (or vim.b.gitsigns_status_dict.changed 0))))

(fn branch-change [name prefix hl]
  {:provider #(let [count (or (. vim.b.gitsigns_status_dict name) 0)]
                (and (> count 0) (.. prefix count)))
   :hl #{:fg (hi hl fg)
         :bg (hi :StatusLinePillRight bg)}})

(local branch-changes (i {:provider " "
                          :hl :StatusLinePillRight}

                         (i {:condition branch-clean?}
                            {:provider :•
                             :hl :StatusLinePillRight})

                         (branch-change :added   "+" :GitSignsAdd)
                         (branch-change :removed "-" :GitSignsDelete)
                         (branch-change :changed "~" :GitSignsChange)))

(local branch (i {:condition conditions.is_git_repo}
                 separator
                 (pill branch-name
                       branch-changes)))
; }}})
; filetype and lsp {{{
(fn has-filetype? []
  (let [filetype vim.bo.filetype
        ignored-filetypes [:dirbuf
                           :DiffviewFileHistory
                           :fugitive
                           :lazy
                           :TelescopePrompt]]
    (if (vim.tbl_contains ignored-filetypes filetype)
          false
        (= 0 (length filetype))
          false
        true)))

(fn lsp-list []
  (let [client-names (->> (vim.lsp.get_clients {:bufnr 0})
                          (vim.tbl_map #$1.name)
                          (vim.fn.sort)
                          (vim.fn.uniq))]
    (if (= 1 (vim.fn.empty client-names))
        " •"
        (string.format " %s " (vim.fn.join client-names " • ")))))

(local filetype (i {:condition has-filetype?
                    :init (%f (set $1.filetype vim.bo.filetype)
                              (set $1.filename (vim.api.nvim_buf_get_name 0))
                              (set $1.extension (vim.fn.fnamemodify $1.filename ":e")))}
                   separator
                   (pill #$1.filetype lsp-list)))
; }}})
; line info {{{
(local line-info (i separator
                    (pill "%l:%c" " %P")))
; }}})

[vi-mode
 command
 recording
 spacer
 dap
 test
 branch
 autowrite
 session
 filetype
 line-info]
