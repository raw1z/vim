-- [nfnl] Compiled from lua/rz/lib/mermaid.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local job = autoload("rz.lib.job")
local notify = autoload("rz.lib.notify")
local M = {config = {width = 1200, height = 800, scale = 2, file = "~/.config/nvim/.mermaid-config.json"}}
local function update_command(src, dest)
  return string.format("mmdc -c %s -i %s -o %s --width %s --height %s --scale %s", M.config.file, src, dest, M.config.width, M.config.height, M.config.scale)
end
M.compile = function(src, dest, callback)
  job["async-run"](update_command(src, dest), {["on-success"] = callback})
  return notify.trace("compiling diagram...")
end
return M
