(require-macros :macros.core)
(require-macros :macros.vim)

; sensitive default {{{
(set-opt autoindent true)
(set-opt autoread true)
(set-opt backspace [:indent :eol :start])
(set-opt backupcopy :yes)
(set-opt cmdheight 0)
(set-opt complete [:. :w :b :u :t])
(set-opt completeopt [:menu :menuone :noselect])
(set-opt cursorline true)
(set-opt diffopt "vertical,filler,algorithm:histogram,indent-heuristic")
(prepend-opt display :truncate)
(set-opt expandtab true)
(set-opt exrc true)
(set-opt fillchars "vert:│,fold: ,foldopen:-,foldclose:+,foldsep:│,diff:╌,eob: ,wbr:─")
(set-opt foldcolumn "auto:1")
(prepend-opt formatoptions :j)
(set-opt grepprg "rg --vimgrep --hidden --no-heading --smart-case")
(set-opt history 10000)
(set-opt ignorecase true)
(set-opt incsearch true)
(set-opt laststatus 3)
(set-opt more true)
(set-opt nrformats [:bin :hex])
(set-opt number true)
(set-opt relativenumber true)
(set-opt ruler false)
(set-opt scrolloff 1)
(set-opt shada "!,'1000,<50,s10,h")
(set-opt shiftround true)
(set-opt shiftwidth 2)
(set-opt shortmess :acoOtWICF)
(set-opt showcmdloc :statusline)
(set-opt showmode false)
(set-opt sidescroll 1)
(set-opt sidescrolloff 2)
(set-opt signcolumn :yes)
(set-opt smartcase true)
(set-opt smarttab true)
(set-opt splitright true)
(set-opt swapfile false)
(set-opt tabstop 2)
(set-opt termguicolors true)
(set-opt title true)
(set-opt undofile true)
(set-opt updatetime 300)
(set-opt viewoptions [:cursor :folds])
(append-opt wildignore ["*.o" "*.obj" ".git" "*.rbc" "*.class" ".svn" "tmp/*" "*.so" "*.swp" "*.zip"
                        "*.png" "*.jpg" "*.gif" "*.jpeg"
                        "*/vendor/gems/*" "*/vendor/cache/*" "Gemfile.lock" "*/log/*" ".sass-cache" "*/coverage/*"
                        "*.pyc" "*.pyo"
                        "*/node_modules/*" "yarn.lock" "package-lock.json"
                        "*/elm-stuff/*"])
(set-opt wildignorecase true)
(set-opt wildmenu true)
(set-opt wrap false)
; }}}
; colorscheme {{{
(when (= nil vim.g.rz_base_init)
  (%> colorscheme rz)
  (set vim.g.rz_base_init true))
; }}}
; configure cursor {{{
(set-opt guicursor ["n-v-c-r:block-Cursor"
                    "i-ci-ve:ver25-ICursor"
                    "r-cr-o:block-RCursor"
                    "a:blinkwait700-blinkoff400-blinkon250"])
; }}}
; configure mksession {{{
(set-opt sessionoptions [:blank
                         :curdir
                         :folds
                         :help
                         :globals
                         :tabpages
                         :winsize
                         :slash
                         :unix])
; }}}
; disable editorconfig {{{
(set vim.g.editorconfig false)
; }}}
; configure terminal {{{
(define-augroup :NoNumbersForTerminal
  ; hide numbers for terminal buffers
  ; disable cursorline
  (define-autocmd TermOpen * ":setlocal nonumber norelativenumber nocursorline"))
; }}}
; custom filetypes {{{
(vim.filetype.add {:extension {:grpc :grpcnvim
                               :http :http
                               :pl :prolog
                               :plt :prolog
                               :ua :uiua
                               :lfe :lfe
                               :nu :nu}})
; }}}
; custom fold {{{
(set vim.wo.foldtext "v:lua.require('rz.lib.folds').foldtext()")
; }}}
