(require-macros :macros.core)
(require-macros :macros.vim)

; escape {{{
(define-key i|c kj :<esc>)
; }}}
; up and down in command mode{{{
(define-key c <C-p> :<Up>)
(define-key c <C-n> :<Down>)
; }}}
; tag navigation {{{
(remap-key n <C-l> "<C-]>")
; }}}
; define shortcuts for inserting backslash, tilde, brackets and square brackets, lt and gt{{{
(macro defkey [key mapped-key]
  `(remap-key n|i|v|c|t|s ,(.. "<leader>" key) ,mapped-key))

(defkey ")"          "]")
(defkey "("          "[")
(defkey "<leader>)"  "}")
(defkey "<leader>("  "{")
(defkey "n"          "~")
(defkey "/"           :\)
(defkey "!"           :|)
(defkey "'"          "`")
(defkey :n           "~")
(defkey "q"           :<)
(defkey "<leader>q"   :>)

(remap-key i <leader>- :^)

(remap-key i|c|s <localleader>f  "(")
(remap-key i|c|s <localleader>d  "[")
(remap-key i|c|s <localleader>s  "{")
(remap-key i|c|s <localleader>r  ")")
(remap-key i|c|s <localleader>e  "]")
(remap-key i|c|s <localleader>z  "}")
(remap-key i|c|s <localleader>!  "!")
; }}}
; wrap mode {{{
(def-key "toggle wrap"
  n <leader>W (%> set wrap! linebreak nolist))

(define-key n j :gj)
(define-key n k :gk)
; }}}
; resize windows {{{
(define-key n <Up>      :<C-w>+)
(define-key n <Down>    :<C-w>-)
(define-key n <Left>    :<C-w><)
(define-key n <Right>   :<C-w>>)
(define-key n <leader>= :<C-w>=)
; }}}
; new tab{{{
(def-key "create a new tab" n <C-w><C-t> (%> tabnew))
; }}}
; close tabs {{{
(def-keys
  "close current tab" n <C-w>C (%> tabclose)
  "close all other tabs" n <C-w>O (%> tabonly)
  "close everything except current window" n <C-w><C-o> (vim.cmd "only|tabonly"))
; }}}
; save and quit {{{
(def-key "write the whole buffer to the current file if modified"
  n <leader>s (%> update))

(def-key "write current file, if modified, and close the current window"
  n <leader>x (%> exit))

(def-key "close current tab"
  n <leader>X (%> tabclose))
; }}}
; select last pasted text {{{
(def-key "select last pasted text"
  n gp (let [buffer (require :rz.lib.buffer)]
        (buffer.select-last-pasted-text)))
; }}}
; system clipboard {{{
(define-key n Y "\"+yy")
(define-key v Y "\"+y")
(define-key n|v <leader>p "\"+p")
(define-key n|v <leader>P "\"+P")
(define-key n|v <localleader>p "\"0p")
(define-key n|v <localleader>P "\"0P")
; }}}
; insert new lines without leaving normal mode {{{
(def-keys
  "insert a new line below in normal mode" n <localleader>o :o<esc>
  "insert a new line above in normal mode" n <localleader>O :O<esc>)
; }}}
; keep cursor centered when navigating search results {{{
(define-key n n :nzzzv)
(define-key n N :Nzzzv)
; }}}
; Undo break points {{{
(define-key i ","  ",<c-g>u")
(define-key i "."  ".<c-g>u")
(define-key i "["  "[<c-g>u")
(define-key i "?"  "?<c-g>u")
; }}}
; reload config {{{
(fn reload-nvim-config []
  (let [config-loader (require :rz.lib.config-loader)
        notify (require :rz.lib.notify)]
    (config-loader.reload-all)
    (%> ReloadColors)
    (notify.trace "Neovim configuration reloaded")))

(def-key "reload nvim config"
  n <leader><leader>R reload-nvim-config)
; }}}
; indent {{{
(define-key v <TAB>   :>gv)
(define-key v <S-TAB> :<gv)
; }}}
; define terminal keymaps {{{
(define-key t <C-o> :<C-\><C-n>)
; }}}
; make window fullscreen {{{
(def-key "make current window fullscreen"
  n <c-w>z :<c-w>|<c-w>_)
;}}}
; aliase CTRL-W {{{
(define-key n <leader>w :<C-w>)
(define-key t <leader>w :<C-\><C-n><C-w>)
(remap-key  t <C-w> :<C-\><C-n><C-w>)
; }}}
; dismiss notifications {{{
(def-key "dismiss notifications"
  n <space>j (Snacks.notifier.hide))
; }}}
; jump to next nearest closed fold {{{
(fn go-to-closed-fold [direction]
  (let [cmd (.. "norm!z" direction)
        view (vim.fn.winsaveview)]
    (var l0 0)
    (var l view.lnum)
    (var open 1)
    (while (and (not= l0 l) open)
      (vim.api.nvim_command cmd)
      (set l0 l)
      (set l (vim.fn.line :.))
      (set open (< (vim.fn.foldclosed l) 0)))
    (when open
      (vim.fn.winrestview view))))

(def-key "go to next closed fold"
  n <leader>zj (go-to-closed-fold :j))

(def-key "go to previous closed fold"
  n <leader>zk (go-to-closed-fold :k))
; }}}
; disable search highlight {{{
(def-key "disable search highlight"
  n <leader>/ (%> nohlsearch))
; }}}
; toggle relative numbers {{{
(fn toggle-relative-numbers []
  (let [current (get-opt relativenumber)]
    (set-opt relativenumber (not current))))
(def-key "toggle relative numbers"
  n <space>n toggle-relative-numbers)
; }}}
; update/sync plugins {{{
(def-keys
  "update plugins" n <space>pu (%> Lazy update)
  "sync plugins with lockfile" n <space>ps (%> Lazy restore)
  "install plugins" n <space>pi (%> Lazy install)
  "restore plugins" n <space>pi (%> Lazy install)
  "open lazy" n <space>pl (%> Lazy))
; }}}
