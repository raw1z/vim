-- [nfnl] Compiled from lua/rz/config/draft.fnl by https://github.com/Olical/nfnl, do not edit.
local function draft_filetype(custom_filetype)
  local str = require("nfnl.string")
  local buffer = require("rz.lib.buffer")
  if str["blank?"](custom_filetype) then
    return buffer.filetype(buffer.current())
  else
    return custom_filetype
  end
end
local function create_draft_buffer(direction, custom_filetype)
  local draft = require("rz.lib.draft")
  return draft.create(direction, draft_filetype(custom_filetype))
end
local function _2_(_241)
  local function _3_(filetype_3f)
    return create_draft_buffer("horizontal", filetype_3f)
  end
  return _3_(_241.args)
end
vim.api.nvim_create_user_command("SDraft", _2_, {complete = "filetype", force = true, nargs = "?"})
local function _4_(_241)
  local function _5_(filetype_3f)
    return create_draft_buffer("vertical", filetype_3f)
  end
  return _5_(_241.args)
end
vim.api.nvim_create_user_command("VDraft", _4_, {complete = "filetype", force = true, nargs = "?"})
local function _6_(_241)
  local function _7_(filetype_3f)
    return create_draft_buffer("tab", filetype_3f)
  end
  return _7_(_241.args)
end
vim.api.nvim_create_user_command("TDraft", _6_, {complete = "filetype", force = true, nargs = "?"})
local function _8_(_241)
  local function _9_(filetype_3f)
    return create_draft_buffer("float", filetype_3f)
  end
  return _9_(_241.args)
end
vim.api.nvim_create_user_command("FDraft", _8_, {complete = "filetype", force = true, nargs = "?"})
local function select_filetype_and_open_draft(direction)
  local ui = require("rz.lib.ui")
  local filetypes = vim.fn.getcompletion("", "filetype")
  local function _10_(_241)
    return create_draft_buffer(direction, _241)
  end
  return ui.select(filetypes, {prompt = "Select a filetype", ["selected-item"] = vim.bo.filetype, ["on-select"] = _10_})
end
local function _11_()
  return select_filetype_and_open_draft("float")
end
local function _12_()
  return select_filetype_and_open_draft("tab")
end
local function _13_()
  return select_filetype_and_open_draft("vertical")
end
local function _14_()
  return select_filetype_and_open_draft("horizontal")
end
local function _15_()
  return create_draft_buffer("tab", "markdown")
end
return {vim.keymap.set({"n"}, "<space>df", _11_, {buffer = false, desc = "open a draft in a floating window", silent = true}), vim.keymap.set({"n"}, "<space>dt", _12_, {buffer = false, desc = "open a draft in a new tab", silent = true}), vim.keymap.set({"n"}, "<space>dv", _13_, {buffer = false, desc = "open a vertical draft", silent = true}), vim.keymap.set({"n"}, "<space>ds", _14_, {buffer = false, desc = "open an horizontal draft", silent = true}), vim.keymap.set({"n"}, "<space>dm", _15_, {buffer = false, desc = "open a markdown draft", silent = true})}
