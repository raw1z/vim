(require-macros :macros.core)
(require-macros :macros.vim)

(local {: autoload} (require :nfnl.module))
(local session (autoload :rz.lib.session))
(local notify (autoload :rz.lib.notify))

(define-command SaveSession []
  (do
    (session.save)               
    (vim.schedule (%f (session.keep-updated)
                      (notify.trace "saved session")))))

(define-command LoadSession []
  (do
    (session.safe-load)
    (vim.schedule session.keep-updated)))

(define-command DeleteSession []
  (do
    (session.delete)
    (vim.schedule (%f (session.stop-updates)
                      (notify.trace "delete session")))))

(def-key "Save session"
  n <space>ss (%> SaveSession))

(def-key "Load session"
  n <space>sl (%> LoadSession))

(def-key "Delete session"
  n <space>sd (%> DeleteSession))
