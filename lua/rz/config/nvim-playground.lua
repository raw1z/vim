-- [nfnl] Compiled from lua/rz/config/nvim-playground.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local nvim_playground = autoload("rz.lib.nvim-playground")
local buffer = autoload("rz.lib.buffer")
local function _2_()
  return nvim_playground.open("top")
end
vim.api.nvim_create_user_command("SPlay", _2_, {force = true})
local function _3_()
  return nvim_playground.open("right")
end
vim.api.nvim_create_user_command("VPlay", _3_, {force = true})
local function _4_()
  return nvim_playground.open("tab")
end
vim.api.nvim_create_user_command("TPlay", _4_, {force = true})
local function _5_()
  return nvim_playground.open("float")
end
vim.api.nvim_create_user_command("FPlay", _5_, {force = true})
local function _6_()
  return nvim_playground.setup(buffer.current())
end
vim.api.nvim_create_user_command("PlayThis", _6_, {force = true})
local function _7_()
  return vim.cmd("SPlay")
end
vim.keymap.set({"n"}, "<space>dp", _7_, {buffer = false, desc = "open an nvim playground on top", silent = true})
local function _8_()
  return vim.cmd("TPlay")
end
return vim.keymap.set({"n"}, "<space>dP", _8_, {buffer = false, desc = "open an nvim playground in new tab", silent = true})
