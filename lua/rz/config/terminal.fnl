(require-macros :macros.vim)

(fn open-term [command direction]
  (let [terminal (require :rz.lib.terminal)]
    (terminal.start command {: direction
                             :start-insert? true
                             :auto-close? false})))

(define-command Sterm [command?]
  (open-term command? :horizontal))

(define-command Vterm [command?]
  (open-term command? :vertical))

(define-command Tterm [command?]
  (open-term command? :tab))

(define-command Fterm [command?]
  (open-term command? :float))
