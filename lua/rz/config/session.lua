-- [nfnl] Compiled from lua/rz/config/session.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local session = autoload("rz.lib.session")
local notify = autoload("rz.lib.notify")
local function _2_()
  session.save()
  local function _3_()
    session["keep-updated"]()
    return notify.trace("saved session")
  end
  return vim.schedule(_3_)
end
vim.api.nvim_create_user_command("SaveSession", _2_, {force = true})
local function _4_()
  session["safe-load"]()
  return vim.schedule(session["keep-updated"])
end
vim.api.nvim_create_user_command("LoadSession", _4_, {force = true})
local function _5_()
  session.delete()
  local function _6_()
    session["stop-updates"]()
    return notify.trace("delete session")
  end
  return vim.schedule(_6_)
end
vim.api.nvim_create_user_command("DeleteSession", _5_, {force = true})
local function _7_()
  return vim.cmd("SaveSession")
end
vim.keymap.set({"n"}, "<space>ss", _7_, {buffer = false, desc = "Save session", silent = true})
local function _8_()
  return vim.cmd("LoadSession")
end
vim.keymap.set({"n"}, "<space>sl", _8_, {buffer = false, desc = "Load session", silent = true})
local function _9_()
  return vim.cmd("DeleteSession")
end
return vim.keymap.set({"n"}, "<space>sd", _9_, {buffer = false, desc = "Delete session", silent = true})
