-- [nfnl] Compiled from lua/rz/config/layout.fnl by https://github.com/Olical/nfnl, do not edit.
local _1_ = vim.api.nvim_create_augroup("rz_layout_autofit", {clear = true})
local function _2_()
  return vim.cmd("wincmd =")
end
local function _3_()
  return vim.cmd("wincmd =")
end
return {vim.api.nvim_create_autocmd({"VimResized"}, {callback = _2_, group = _1_, pattern = {"*"}}), vim.api.nvim_create_autocmd({"TabEnter"}, {callback = _3_, group = _1_, pattern = {"*"}})}
