-- [nfnl] Compiled from lua/rz/config/note.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local note = autoload("rz.lib.note")
local function _2_(_241)
  local function _3_(arg_3f)
    if (arg_3f == "--root") then
      return note["edit-root"]()
    else
      return note.edit()
    end
  end
  return _3_(_241.args)
end
vim.api.nvim_create_user_command("NoteEdit", _2_, {force = true, nargs = "?"})
local function _5_()
  return vim.cmd("NoteEdit")
end
vim.keymap.set({"n"}, "<space>ii", _5_, {buffer = false, desc = "edit note", silent = true})
local function _6_()
  return vim.cmd("NoteEdit --root")
end
return vim.keymap.set({"n"}, "<space>ir", _6_, {buffer = false, desc = "edit root note", silent = true})
