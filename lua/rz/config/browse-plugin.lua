-- [nfnl] Compiled from lua/rz/config/browse-plugin.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local core = autoload("nfnl.core")
local dir = autoload("rz.lib.dir")
local path = autoload("rz.lib.path")
local text = autoload("rz.lib.text")
local job = autoload("rz.lib.job")
local plugin_folder = path.join(vim.fn.stdpath("data"), "lazy")
local function plugins()
  local scan = require("plenary.scandir")
  return scan.scan_dir(plugin_folder, {depth = 1, only_dirs = true, hidden = false})
end
local function make_entry(plugin_path)
  local plugin_name = path.basename(plugin_path)
  return {value = plugin_path, ordinal = plugin_name, display = plugin_name}
end
local function finder()
  local finders = require("telescope.finders")
  return finders.new_table({results = plugins(), entry_maker = make_entry})
end
local function browse(plugin)
  local path0 = plugin.value
  return dir["go-to-folder-in-new-tab"](path0)
end
local function parse_url(output)
  local browser = require("rz.lib.browser")
  local matching_line
  local function _2_(_241)
    return text["contains?"](_241, "fetch")
  end
  matching_line = core.first(core.filter(_2_, output))
  return browser.open(vim.fn.split(matching_line, "\\s")[2])
end
local function browse_url(plugin)
  local cwd = plugin.value
  return job["run-with-timeout"]({"git", "remote", "-v"}, 1000, {cwd = cwd, ["on-stdout"] = parse_url})
end
local function attach_mappings(prompt_bufnr)
  do
    local actions = require("telescope.actions")
    local actions_state = require("telescope.actions.state")
    local function _3_()
      actions.close(prompt_bufnr)
      return browse_url(actions_state.get_selected_entry())
    end
    actions.select_default:replace(_3_)
    local function _4_()
      actions.close(prompt_bufnr)
      return browse(actions_state.get_selected_entry())
    end
    actions.select_tab:replace(_4_)
  end
  return true
end
local function build_picker()
  local themes = require("telescope.themes")
  local opts = themes.get_dropdown()
  local conf = require("telescope.config").values
  local pickers = require("telescope.pickers")
  return pickers.new(opts, {finder = finder(), sorter = conf.generic_sorter(opts), attach_mappings = attach_mappings})
end
local function show_plugins()
  return build_picker():find()
end
return vim.keymap.set({"n"}, "<space>pp", show_plugins, {buffer = false, desc = "show installed plugins", silent = true})
