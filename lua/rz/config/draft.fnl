(require-macros :macros.core)
(require-macros :macros.vim)

(fn draft-filetype [custom-filetype]
  (let [str (require :nfnl.string)
        buffer (require :rz.lib.buffer)]
    (if (str.blank? custom-filetype)
      (buffer.filetype (buffer.current))
      custom-filetype)))

(fn create-draft-buffer [direction custom-filetype]
  (let [draft (require :rz.lib.draft)]
    (draft.create direction
                  (draft-filetype custom-filetype))))

(define-command :SDraft [filetype?] {:complete :filetype}
  (create-draft-buffer :horizontal filetype?))

(define-command :VDraft [filetype?] {:complete :filetype}
  (create-draft-buffer :vertical filetype?))

(define-command :TDraft [filetype?] {:complete :filetype}
  (create-draft-buffer :tab filetype?))

(define-command :FDraft [filetype?] {:complete :filetype}
  (create-draft-buffer :float filetype?))

(fn select-filetype-and-open-draft [direction]
  (let [ui (require :rz.lib.ui)
        filetypes (vim.fn.getcompletion "" :filetype)]
    (ui.select filetypes {:prompt "Select a filetype"
                          :selected-item vim.bo.filetype
                          :on-select #(create-draft-buffer direction $1)})))

(def-keys
  "open a draft in a floating window" n <space>df (select-filetype-and-open-draft :float)
  "open a draft in a new tab" n <space>dt (select-filetype-and-open-draft :tab)
  "open a vertical draft" n <space>dv (select-filetype-and-open-draft :vertical)
  "open an horizontal draft" n <space>ds (select-filetype-and-open-draft :horizontal)
  "open a markdown draft" n <space>dm (create-draft-buffer :tab :markdown))
