(require-macros :macros.vim)

(local {: autoload} (require :nfnl.module))
(local watcher (autoload :rz.lib.watcher))
(local path (autoload :rz.lib.path))

(fn reload-colors []
  (let [notify (require :rz.lib.notify)
        colors (require :rz.lib.colors)]
    (notify.dismiss)
    (colors.reload)))

(fn safe-reload-colors [count]
  (when (and (> count 0)
             (not (pcall reload-colors)))
    (safe-reload-colors (- count 1))))

(define-command ReloadColors []
  (reload-colors))

(define-command SafeReloadColors []
  (safe-reload-colors 3))

(-> (path.expand "~/.Xresources")
    (watcher.watch-file-with-debounce 500
                                      #(safe-reload-colors 3)))
