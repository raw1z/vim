(require-macros :macros.vim)
(local {: autoload} (require :nfnl.module))
(local kitty (autoload :rz.lib.kitty))

(define-augroup rz_kitty_reload
  (define-autocmd BufWritePost [:kitty.conf :kitty-font.conf :*/kitty-fonts/*.conf]
    (kitty.reload-conf)))
