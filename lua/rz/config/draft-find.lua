-- [nfnl] Compiled from lua/rz/config/draft-find.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local core = autoload("nfnl.core")
local buffer = autoload("rz.lib.buffer")
local draft = autoload("rz.lib.draft")
local text = autoload("rz.lib.text")
local function draft_files()
  return vim.fn.reverse(vim.fn.sort(vim.fn.split(vim.fn.globpath(draft.folder(), "**/*.*"))))
end
local function display_name(path)
  return text.clear(path, core.str(draft.folder(), "/"))
end
local function name_parts(item)
  local _, _0, date, time_, name = string.find(display_name(item.path), "(%d+/%d+/%d+)/(%d+_%d+_%d+)-(.+)")
  return date, time_, name
end
local function make_display(item)
  local entry_display = require("telescope.pickers.entry_display")
  local date, time_, name = name_parts(item)
  local displayer = entry_display.create({separator = "", items = {{width = 11}, {width = 9}, {remaining = true}}})
  return displayer({{date, "RZDraftFindDate"}, {text["replace-all"](time_, "_", ":"), "RZDraftFindTime"}, name})
end
local function make_entry(item)
  local ordinal = display_name(item)
  return {value = item, ordinal = ordinal, display = make_display, path = item}
end
local function finder()
  local finders = require("telescope.finders")
  return finders.new_table({results = draft_files(), entry_maker = make_entry})
end
local function edit(item, direction)
  if (direction == "horizontal") then
    vim.cmd(("new " .. item.path))
  elseif (direction == "vertical") then
    vim.cmd(("vnew " .. item.path))
  elseif (direction == "tab") then
    vim.cmd(("tabnew " .. item.path))
  else
  end
  return buffer["enable-autowrite"](buffer.current())
end
local function attach_mappings(prompt_bufnr)
  do
    local actions = require("telescope.actions")
    local actions_state = require("telescope.actions.state")
    local function _3_()
      actions.close(prompt_bufnr)
      return edit(actions_state.get_selected_entry(), "tab")
    end
    actions.select_default:replace(_3_)
    local function _4_()
      actions.close(prompt_bufnr)
      return edit(actions_state.get_selected_entry(), "tab")
    end
    actions.select_tab:replace(_4_)
    local function _5_()
      actions.close(prompt_bufnr)
      return edit(actions_state.get_selected_entry(), "horizontal")
    end
    actions.select_horizontal:replace(_5_)
    local function _6_()
      actions.close(prompt_bufnr)
      return edit(actions_state.get_selected_entry(), "vertical")
    end
    actions.select_vertical:replace(_6_)
  end
  return true
end
local function build_picker()
  local opts = {}
  local conf = require("telescope.config").values
  local pickers = require("telescope.pickers")
  return pickers.new(opts, {finder = finder(), previewer = conf.file_previewer(opts), sorter = conf.file_sorter(opts), attach_mappings = attach_mappings})
end
local function pick_draft()
  return build_picker():find()
end
local function _7_()
  return pick_draft()
end
vim.api.nvim_create_user_command("DraftFind", _7_, {force = true})
return vim.keymap.set({"n"}, "<space>dd", pick_draft, {buffer = false, desc = "pick a draft", silent = true})
