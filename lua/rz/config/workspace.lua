-- [nfnl] Compiled from lua/rz/config/workspace.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local workspace = autoload("rz.lib.workspace")
local ui = autoload("rz.lib.ui")
local function _2_(_241)
  local function _3_(theme)
    return workspace["change-theme"](theme)
  end
  return _3_(_241.args)
end
vim.api.nvim_create_user_command("WorkspaceTheme", _2_, {complete = workspace["filter-themes"], force = true, nargs = 1})
local function _4_(_241)
  local function _5_(font)
    return workspace["change-font"](font)
  end
  return _5_(_241.args)
end
vim.api.nvim_create_user_command("WorkspaceFont", _4_, {complete = workspace["filter-fonts"], force = true, nargs = 1})
local function pick_theme()
  return ui.select(workspace.themes(), {prompt = "Select a theme", ["on-select"] = workspace["change-theme"], ["selected-item"] = workspace["current-theme"]()})
end
vim.keymap.set({"n"}, "<leader><leader>wt", pick_theme, {buffer = false, desc = "pick a theme", silent = true})
local function pick_font()
  return ui.select(workspace.fonts(), {prompt = "Select a font", ["on-select"] = workspace["change-font"], ["selected-item"] = workspace["current-font"]()})
end
return vim.keymap.set({"n"}, "<leader><leader>wf", pick_font, {buffer = false, desc = "pick a font", silent = true})
