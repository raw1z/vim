-- [nfnl] Compiled from lua/rz/config/quickfix.fnl by https://github.com/Olical/nfnl, do not edit.
local function open_trouble()
  return vim.cmd("Trouble qflist toggle focus=true")
end
local _1_ = vim.api.nvim_create_augroup("rz-quickfix", {clear = true})
return {vim.api.nvim_create_autocmd({"QuickfixCmdPost"}, {callback = open_trouble, group = _1_, pattern = {"vimgrep"}})}
