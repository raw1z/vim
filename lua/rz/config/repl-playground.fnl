(require-macros :macros.core)
(require-macros :macros.vim)
(local {: autoload} (require :nfnl.module))
(local repl-playground (autoload :rz.lib.repl-playground))
(local draft (autoload :rz.lib.draft))
(local list (autoload :rz.lib.list))

(define-command ReplPlayground [filetype scratch-suffix command]
  (let [playground (repl-playground.create filetype command {: scratch-suffix})]
    (playground.start)))

(fn sanitize-command [command]
  (vim.fn.join (list.flatten [command])))

(fn guess-command [filetype]
  (match filetype
    :elixir "iex -S mix"
    :fennel :fennel
    :ruby :irb
    :shen :shen
    :janet :janet
    :prolog :swipl
    :clojure "lein repl"
    :racket :racket
    :scheme :guile
    :lfe "rebar3 lfe repl"))

(define-command ReplThis [command?]
  (let [filetype (get-opt filetype)
        command (if (blank? command)
                  (guess-command filetype)
                  (sanitize-command command))
        playground (repl-playground.create filetype command)]
    (playground.attach-current-buffer)))

(define-command ReplType [filetype?] {:complete :filetype}
  (let [filetype (if (blank? filetype?)
                  (get-opt filetype)
                  filetype?)
        playground (->> (guess-command filetype)
                        (repl-playground.create filetype))]
    (vim.print [filetype])
    (draft.create :tab filetype)
    (playground.attach-current-buffer)))

(define-command ReplFile [filename] {:complete :file}
  (do
    (vim.cmd (.. "tabnew " filename))
    (let [filetype (get-opt filetype)
          command (guess-command filetype)
          playground (repl-playground.create filetype command)]
      (playground.attach-current-buffer))))
