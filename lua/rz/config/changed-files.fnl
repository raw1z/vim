(require-macros :macros.core)
(require-macros :macros.vim)
(local {: autoload} (require :nfnl.module))
(local git (autoload :rz.lib.git))
(local path (autoload :rz.lib.path))

(fn changed-files [base]
  (let [base (or base (git.guess-trunk-branch))]
    (->> (git.current-branch)
         (git.changed-files-between-branches base))))

(fn make-display [item]
  (let [entry_display (require :telescope.pickers.entry_display)
        displayer (entry_display.create {:separator " "
                                         :items [{:width 10}
                                                 {:remaining true}]})]
    (displayer [item.value.status
                item.value.path])))

(fn make-entry [item]
  {:value item
   :ordinal (.. item.status " " item.path)
   :path (path.join (git.root-dir) item.path)
   :display make-display})

(fn finder [base]
  (let [finders (require :telescope.finders)]
    (finders.new_table {:results (changed-files base)
                        :entry_maker make-entry})))

(fn full-path [item]
  (-> (git.root-dir)
      (path.join item.value.path)))

(fn show-file [path direction]
  (match direction
    :horizontal (vim.cmd (.. "new " path))
    :vertical   (vim.cmd (.. "vnew " path))
    :tab        (vim.cmd (.. "tabnew " path))))

(fn activate-git-diff [base]
  (let [base (or base (git.guess-trunk-branch))]
    (-> (string.format "Gitsigns change_base %s" base)
        (vim.cmd))))

(fn show-diff [item base]
  (-> (full-path item)
      (show-file :tab))
  (activate-git-diff base))

(fn attach-mappings [base prompt-bufnr]
  (let [actions (require :telescope.actions)
        actions-state (require :telescope.actions.state)]
    (actions.select_default:replace
      #(do
         (actions.close prompt-bufnr)
         (-> (actions-state.get_selected_entry)
             (show-diff base))))
    (actions.select_horizontal:replace
      #(do
         (actions.close prompt-bufnr)
         (-> (actions-state.get_selected_entry)
             (full-path)
             (show-file :horizontal))))
    (actions.select_vertical:replace
      #(do
         (actions.close prompt-bufnr)
         (-> (actions-state.get_selected_entry)
             (full-path)
             (show-file :vertical))))
    (actions.select_tab:replace
      #(do
         (actions.close prompt-bufnr)
         (-> (actions-state.get_selected_entry)
             (full-path)
             (show-file :tab)))))
  true)

(fn build-picker [base]
  (let [opts {}
        conf (. (require :telescope.config) :values)
        pickers (require :telescope.pickers)]
    (pickers.new opts {:finder (finder base)
                       :previewer (conf.file_previewer opts)
                       :sorter (conf.generic_sorter opts)
                       :attach_mappings #(attach-mappings base $1)})))

(fn list-bases []
  (vim.fn.systemlist ["git" "rev-parse" "--symbolic" "--branches" "--tags" "--remotes"]))

(define-command ChangedFiles [base?] {:complete list-bases}
  (: (build-picker base?) :find))
