(require-macros :macros.core)
(require-macros :macros.vim)

(define-augroup rz_layout_autofit
  (define-autocmd VimResized *
    (%> wincmd =))
  (define-autocmd TabEnter *
    (%> wincmd =)))
