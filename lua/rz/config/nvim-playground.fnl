(require-macros :macros.core)
(require-macros :macros.vim)

(local {: autoload} (require :nfnl.module))
(local nvim-playground (autoload :rz.lib.nvim-playground))
(local buffer (autoload :rz.lib.buffer))

(define-command SPlay []
  (nvim-playground.open :top))

(define-command VPlay []
  (nvim-playground.open :right))

(define-command TPlay []
  (nvim-playground.open :tab))

(define-command FPlay []
  (nvim-playground.open :float))

(define-command PlayThis []
  (nvim-playground.setup (buffer.current)))

(def-key "open an nvim playground on top"
  n <space>dp (%> SPlay))

(def-key "open an nvim playground in new tab"
  n <space>dP (%> TPlay))
