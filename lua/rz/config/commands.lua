-- [nfnl] Compiled from lua/rz/config/commands.fnl by https://github.com/Olical/nfnl, do not edit.
local function _1_()
  local buffer = require("rz.lib.buffer")
  return buffer["enable-autowrite"](buffer.current())
end
vim.api.nvim_create_user_command("AutoWriteEnable", _1_, {force = true})
local function _2_()
  local buffer = require("rz.lib.buffer")
  return buffer["disable-autowrite"](buffer.current())
end
vim.api.nvim_create_user_command("AutoWriteDisable", _2_, {force = true})
local function _3_()
  local buffer = require("rz.lib.buffer")
  return buffer["toggle-autowrite"](buffer.current())
end
vim.api.nvim_create_user_command("AutoWriteToggle", _3_, {force = true})
local function _4_(_241)
  local function _5_(_3fopt)
    local buffer = require("rz.lib.buffer")
    return vim.fn.setreg("+", buffer["copy-path"](buffer.current(), false, ("--full" == _3fopt)), "c")
  end
  return _5_(_241.args)
end
vim.api.nvim_create_user_command("CopyThisPath", _4_, {force = true, nargs = "?"})
local function _6_(_241)
  local function _7_(_3fopt)
    local buffer = require("rz.lib.buffer")
    return vim.fn.setreg("+", buffer["copy-path"](buffer.current(), true, ("--full" == _3fopt)), "c")
  end
  return _7_(_241.args)
end
vim.api.nvim_create_user_command("CopyThisLine", _6_, {force = true, nargs = "?"})
local function _8_(_241)
  local function _9_(message_3f)
    local git = require("rz.lib.git")
    return git["save-changes"](__fnl_global__messages_3f)
  end
  return _9_(_241.args)
end
vim.api.nvim_create_user_command("SaveChanges", _8_, {force = true, nargs = "?"})
local function _10_(_241)
  local function _11_(message_3f)
    local git = require("rz.lib.git")
    return git["save-staged-changes"](__fnl_global__messages_3f)
  end
  return _11_(_241.args)
end
vim.api.nvim_create_user_command("SaveStagedChanges", _10_, {force = true, nargs = "?"})
local function _12_()
  local git = require("rz.lib.git")
  return git["restore-changes"]()
end
vim.api.nvim_create_user_command("RestoreChanges", _12_, {force = true})
local function _13_()
  local git = require("rz.lib.git")
  return git["continue-rebase"]()
end
vim.api.nvim_create_user_command("RebaseContinue", _13_, {force = true})
local function _14_()
  local git = require("rz.lib.git")
  return git["abort-rebase"]()
end
vim.api.nvim_create_user_command("RebaseAbort", _14_, {force = true})
local function _15_()
  local terminal = require("rz.lib.terminal")
  return terminal.start("lazygit", {name = "lazygit", ["start-insert?"] = true, ["auto-close?"] = true, direction = "tab"})
end
vim.api.nvim_create_user_command("Lazygit", _15_, {force = true})
local function configure_grpc_playground(bufnr)
  vim.api.nvim_buf_set_var(bufnr, "is_special_buffer", true)
  vim.api.nvim_buf_set_var(bufnr, "special_buffer_name", "Language Shell - grpc")
  local function _16_()
    return vim.cmd("Grpc")
  end
  return vim.keymap.set({"n"}, "<Enter>", _16_, {buffer = bufnr, desc = "run current line", silent = true})
end
local function _17_()
  local draft = require("rz.lib.draft")
  local bufnr = draft.create("tab", "grpcnvim", {suffix = ".grpc", ["scratch?"] = false})
  return configure_grpc_playground(bufnr)
end
vim.api.nvim_create_user_command("GrpcPlayground", _17_, {force = true})
local function _18_()
  local buffer = require("rz.lib.buffer")
  return configure_grpc_playground(buffer.current())
end
vim.api.nvim_create_user_command("GrpcPlaygroundThis", _18_, {force = true})
local function configure_http_playground(bufnr)
  vim.api.nvim_buf_set_var(bufnr, "is_special_buffer", true)
  vim.api.nvim_buf_set_var(bufnr, "special_buffer_name", "Language Shell - http")
  return vim.keymap.set({"n"}, "<Enter>", "<Plug>RestNvim", {buffer = bufnr, desc = "run current line", silent = true})
end
local function _19_()
  local draft = require("rz.lib.draft")
  local bufnr = draft.create("tab", "http", {suffix = ".http", ["scratch?"] = false})
  return configure_http_playground(bufnr)
end
vim.api.nvim_create_user_command("HttpPlayground", _19_, {force = true})
local function _20_()
  local buffer = require("rz.lib.buffer")
  return configure_http_playground(buffer.current())
end
vim.api.nvim_create_user_command("HttpPlaygroundThis", _20_, {force = true})
local function _21_()
  local path = require("rz.lib.path")
  local config_file = path.join(vim.fn["projectroot#guess"](), ".nfnl.fnl")
  local project_file = path.join(vim.fn["projectroot#guess"](), ".nvim.fnl")
  path.create(config_file, "{:compiler-options {:compiler-env _G}\n:fennel-macro-path (.. (vim.fn.stdpath :config) :/?.fnl)}")
  path.create(project_file, "(require-macros :macros.core)\n(require-macros :macros.vim)\n; (local {: autoload} (require :nfnl.module))\n; (local terminal (autoload :rz.lib.terminal))\n\n; (load-plugins vim-bundler\n;               vim-rake)\n\n; (apply-strategies ruby)\n\n; (define-command RunIt []\n;   (terminal.direnv-exec\n;     \"<command>\"\n;     {:start-insert? true\n;      :direction :bottom\n;      :name :run-it}))\n\n; (def-keys\n;   \"run\" n <space>mm (%> RunIt))\n")
  return vim.cmd("tabnew .nvim.fnl")
end
vim.api.nvim_create_user_command("ProjectInit", _21_, {force = true})
local function _22_()
  return vim.cmd("tabnew .nvim.fnl")
end
vim.api.nvim_create_user_command("ProjectEdit", _22_, {force = true})
local function _23_(_241)
  local function _24_(path)
    local dir = require("rz.lib.dir")
    return dir["go-to-folder-in-new-tab"](path)
  end
  return _24_(_241.args)
end
vim.api.nvim_create_user_command("TabGo", _23_, {complete = "dir", force = true, nargs = 1})
local function update_plugins()
  local lazy = require("lazy")
  return lazy.update()
end
local function update_plugins_headless()
  local function _25_()
    local function _26_()
      return vim.cmd("quitall")
    end
    return vim.defer_fn(_26_, 1000)
  end
  vim.api.nvim_create_autocmd({"User"}, {callback = _25_, pattern = {"LazyUpdate"}})
  return update_plugins()
end
local function _27_()
  return update_plugins()
end
vim.api.nvim_create_user_command("PluginsUpdate", _27_, {force = true})
local function _28_()
  return update_plugins_headless()
end
return vim.api.nvim_create_user_command("PluginsHeadlessUpdate", _28_, {force = true})
