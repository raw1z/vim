-- [nfnl] Compiled from lua/rz/config/changed-files.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local git = autoload("rz.lib.git")
local path = autoload("rz.lib.path")
local function changed_files(base)
  local base0 = (base or git["guess-trunk-branch"]())
  return git["changed-files-between-branches"](base0, git["current-branch"]())
end
local function make_display(item)
  local entry_display = require("telescope.pickers.entry_display")
  local displayer = entry_display.create({separator = " ", items = {{width = 10}, {remaining = true}}})
  return displayer({item.value.status, item.value.path})
end
local function make_entry(item)
  return {value = item, ordinal = (item.status .. " " .. item.path), path = path.join(git["root-dir"](), item.path), display = make_display}
end
local function finder(base)
  local finders = require("telescope.finders")
  return finders.new_table({results = changed_files(base), entry_maker = make_entry})
end
local function full_path(item)
  return path.join(git["root-dir"](), item.value.path)
end
local function show_file(path0, direction)
  if (direction == "horizontal") then
    return vim.cmd(("new " .. path0))
  elseif (direction == "vertical") then
    return vim.cmd(("vnew " .. path0))
  elseif (direction == "tab") then
    return vim.cmd(("tabnew " .. path0))
  else
    return nil
  end
end
local function activate_git_diff(base)
  local base0 = (base or git["guess-trunk-branch"]())
  return vim.cmd(string.format("Gitsigns change_base %s", base0))
end
local function show_diff(item, base)
  show_file(full_path(item), "tab")
  return activate_git_diff(base)
end
local function attach_mappings(base, prompt_bufnr)
  do
    local actions = require("telescope.actions")
    local actions_state = require("telescope.actions.state")
    local function _3_()
      actions.close(prompt_bufnr)
      return show_diff(actions_state.get_selected_entry(), base)
    end
    actions.select_default:replace(_3_)
    local function _4_()
      actions.close(prompt_bufnr)
      return show_file(full_path(actions_state.get_selected_entry()), "horizontal")
    end
    actions.select_horizontal:replace(_4_)
    local function _5_()
      actions.close(prompt_bufnr)
      return show_file(full_path(actions_state.get_selected_entry()), "vertical")
    end
    actions.select_vertical:replace(_5_)
    local function _6_()
      actions.close(prompt_bufnr)
      return show_file(full_path(actions_state.get_selected_entry()), "tab")
    end
    actions.select_tab:replace(_6_)
  end
  return true
end
local function build_picker(base)
  local opts = {}
  local conf = require("telescope.config").values
  local pickers = require("telescope.pickers")
  local function _7_(_241)
    return attach_mappings(base, _241)
  end
  return pickers.new(opts, {finder = finder(base), previewer = conf.file_previewer(opts), sorter = conf.generic_sorter(opts), attach_mappings = _7_})
end
local function list_bases()
  return vim.fn.systemlist({"git", "rev-parse", "--symbolic", "--branches", "--tags", "--remotes"})
end
local function _8_(_241)
  local function _9_(base_3f)
    return build_picker(base_3f):find()
  end
  return _9_(_241.args)
end
return vim.api.nvim_create_user_command("ChangedFiles", _8_, {complete = list_bases, force = true, nargs = "?"})
