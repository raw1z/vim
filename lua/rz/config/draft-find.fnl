(require-macros :macros.vim)

(local {: autoload} (require :nfnl.module))
(local core (autoload :nfnl.core))
(local buffer (autoload :rz.lib.buffer))
(local draft (autoload :rz.lib.draft))
(local text (autoload :rz.lib.text))

(fn draft-files []
  (-> (draft.folder)
      (vim.fn.globpath "**/*.*")
      (vim.fn.split)
      (vim.fn.sort)
      (vim.fn.reverse)))

(fn display-name [path]
  (->> (core.str (draft.folder) :/)
       (text.clear path)))

(fn name-parts [item]
  (let [(_ _ date time_ name) (-> (display-name item.path)
                                  (string.find "(%d+/%d+/%d+)/(%d+_%d+_%d+)-(.+)"))]
    (values date time_ name)))

(fn make-display [item]
  (let [entry_display (require :telescope.pickers.entry_display)
        (date time_ name) (name-parts item)
        displayer (entry_display.create {:separator ""
                                         :items [{:width 11}
                                                 {:width 9}
                                                 {:remaining true}]})]
    (displayer [[date :RZDraftFindDate]
                [(text.replace-all time_ :_ ":") :RZDraftFindTime]
                name])))

(fn make-entry [item]
  (let [ordinal (display-name item)]
    {:value item
     : ordinal
     :display make-display
     :path item}))

(fn finder []
  (let [finders (require :telescope.finders)]
    (finders.new_table {:results (draft-files)
                        :entry_maker make-entry})))

(fn edit [item direction]
  (match direction
    :horizontal (vim.cmd (.. "new " item.path))
    :vertical   (vim.cmd (.. "vnew " item.path))
    :tab        (vim.cmd (.. "tabnew " item.path)))
  (buffer.enable-autowrite (buffer.current)))

(fn attach-mappings [prompt-bufnr]
  (let [actions (require :telescope.actions)
        actions-state (require :telescope.actions.state)]
    (actions.select_default:replace
      #(do
         (actions.close prompt-bufnr)
         (-> (actions-state.get_selected_entry)
             (edit :tab))))
    (actions.select_tab:replace
      #(do
         (actions.close prompt-bufnr)
         (-> (actions-state.get_selected_entry)
             (edit :tab))))
    (actions.select_horizontal:replace
      #(do
         (actions.close prompt-bufnr)
         (-> (actions-state.get_selected_entry)
             (edit :horizontal))))
    (actions.select_vertical:replace
      #(do
         (actions.close prompt-bufnr)
         (-> (actions-state.get_selected_entry)
             (edit :vertical)))))
  true)

(fn build-picker []
  (let [opts {}
        conf (. (require :telescope.config) :values)
        pickers (require :telescope.pickers)]
    (pickers.new opts {:finder (finder)
                       :previewer (conf.file_previewer opts)
                       :sorter (conf.file_sorter opts)
                       :attach_mappings attach-mappings})))

(fn pick-draft []
  (: (build-picker) :find))

(define-command DraftFind []
  (pick-draft))

(def-key "pick a draft"
  n <space>dd pick-draft)
