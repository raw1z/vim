-- [nfnl] Compiled from lua/rz/config/repl-playground.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local repl_playground = autoload("rz.lib.repl-playground")
local draft = autoload("rz.lib.draft")
local list = autoload("rz.lib.list")
do
  local text_28_auto = require("rz.lib.text")
  local function _2_(_241)
    local function _3_(filetype, scratch_suffix, command)
      local playground = repl_playground.create(filetype, command, {["scratch-suffix"] = scratch_suffix})
      return playground.start()
    end
    return _3_(unpack(text_28_auto.split(_241.args, "\\s", 3)))
  end
  vim.api.nvim_create_user_command("ReplPlayground", _2_, {force = true, nargs = "+"})
end
local function sanitize_command(command)
  return vim.fn.join(list.flatten({command}))
end
local function guess_command(filetype)
  if (filetype == "elixir") then
    return "iex -S mix"
  elseif (filetype == "fennel") then
    return "fennel"
  elseif (filetype == "ruby") then
    return "irb"
  elseif (filetype == "shen") then
    return "shen"
  elseif (filetype == "janet") then
    return "janet"
  elseif (filetype == "prolog") then
    return "swipl"
  elseif (filetype == "clojure") then
    return "lein repl"
  elseif (filetype == "racket") then
    return "racket"
  elseif (filetype == "scheme") then
    return "guile"
  elseif (filetype == "lfe") then
    return "rebar3 lfe repl"
  else
    return nil
  end
end
local function _5_(_241)
  local function _6_(command_3f)
    local filetype
    do
      local opt_value_11_auto = vim.opt.filetype
      filetype = opt_value_11_auto:get()
    end
    local command
    if (1 == vim.fn.empty(command)) then
      command = guess_command(filetype)
    else
      command = sanitize_command(command)
    end
    local playground = repl_playground.create(filetype, command)
    return playground["attach-current-buffer"]()
  end
  return _6_(_241.args)
end
vim.api.nvim_create_user_command("ReplThis", _5_, {force = true, nargs = "?"})
local function _8_(_241)
  local function _9_(filetype_3f)
    local filetype
    if (1 == vim.fn.empty(filetype_3f)) then
      local opt_value_11_auto = vim.opt.filetype
      filetype = opt_value_11_auto:get()
    else
      filetype = filetype_3f
    end
    local playground = repl_playground.create(filetype, guess_command(filetype))
    vim.print({filetype})
    draft.create("tab", filetype)
    return playground["attach-current-buffer"]()
  end
  return _9_(_241.args)
end
vim.api.nvim_create_user_command("ReplType", _8_, {complete = "filetype", force = true, nargs = "?"})
local function _11_(_241)
  local function _12_(filename)
    vim.cmd(("tabnew " .. filename))
    local filetype
    do
      local opt_value_11_auto = vim.opt.filetype
      filetype = opt_value_11_auto:get()
    end
    local command = guess_command(filetype)
    local playground = repl_playground.create(filetype, command)
    return playground["attach-current-buffer"]()
  end
  return _12_(_241.args)
end
return vim.api.nvim_create_user_command("ReplFile", _11_, {complete = "file", force = true, nargs = 1})
