(require-macros :macros.vim)

(local {: autoload} (require :nfnl.module))
(local core (autoload :nfnl.core))
(local dir (autoload :rz.lib.dir))
(local path (autoload :rz.lib.path))
(local text (autoload :rz.lib.text))
(local job (autoload :rz.lib.job))

(local plugin-folder (-> (vim.fn.stdpath :data)
                         (path.join "lazy")))

(fn plugins []
  (let [scan (require :plenary.scandir)]
    (scan.scan_dir plugin-folder {:depth 1
                                  :only_dirs true
                                  :hidden false})))

(fn make-entry [plugin-path]
  (let [plugin-name (path.basename plugin-path)]
    {:value plugin-path
     :ordinal plugin-name
     :display plugin-name}))

(fn finder []
  (let [finders (require :telescope.finders)]
    (finders.new_table {:results (plugins)
                        :entry_maker make-entry})))

(fn browse [plugin]
  (let [path plugin.value]
    (dir.go-to-folder-in-new-tab path)))

(fn parse-url [output]
  (let [browser (require :rz.lib.browser)
        matching-line (->> (core.filter #(text.contains? $1 :fetch) output)
                           (core.first))]
    (-> (vim.fn.split matching-line "\\s")
        (. 2)
        (browser.open))))

(fn browse-url [plugin]
  (let [cwd plugin.value]
    (job.run-with-timeout [:git :remote :-v]
                          1000
                          {: cwd
                           :on-stdout parse-url})))

(fn attach-mappings [prompt-bufnr]
  (let [actions (require :telescope.actions)
        actions-state (require :telescope.actions.state)]
    (actions.select_default:replace
      (fn []
        (actions.close prompt-bufnr)
        (-> (actions-state.get_selected_entry)
            (browse-url))))
    (actions.select_tab:replace
      (fn []
        (actions.close prompt-bufnr)
        (-> (actions-state.get_selected_entry)
            (browse)))))
  true)

(fn build-picker []
  (let [themes (require :telescope.themes)
        opts (themes.get_dropdown)
        conf (. (require :telescope.config) :values)
        pickers (require :telescope.pickers)]
    (pickers.new opts {:finder (finder)
                       :sorter (conf.generic_sorter opts)
                       :attach_mappings attach-mappings})))

(fn show-plugins []
  (: (build-picker) :find))

(def-key "show installed plugins"
  n :<space>pp show-plugins)
