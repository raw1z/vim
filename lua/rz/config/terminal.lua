-- [nfnl] Compiled from lua/rz/config/terminal.fnl by https://github.com/Olical/nfnl, do not edit.
local function open_term(command, direction)
  local terminal = require("rz.lib.terminal")
  return terminal.start(command, {direction = direction, ["start-insert?"] = true, ["auto-close?"] = false})
end
local function _1_(_241)
  local function _2_(command_3f)
    return open_term(command_3f, "horizontal")
  end
  return _2_(_241.args)
end
vim.api.nvim_create_user_command("Sterm", _1_, {force = true, nargs = "?"})
local function _3_(_241)
  local function _4_(command_3f)
    return open_term(command_3f, "vertical")
  end
  return _4_(_241.args)
end
vim.api.nvim_create_user_command("Vterm", _3_, {force = true, nargs = "?"})
local function _5_(_241)
  local function _6_(command_3f)
    return open_term(command_3f, "tab")
  end
  return _6_(_241.args)
end
vim.api.nvim_create_user_command("Tterm", _5_, {force = true, nargs = "?"})
local function _7_(_241)
  local function _8_(command_3f)
    return open_term(command_3f, "float")
  end
  return _8_(_241.args)
end
return vim.api.nvim_create_user_command("Fterm", _7_, {force = true, nargs = "?"})
