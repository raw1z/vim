-- [nfnl] Compiled from lua/rz/config/kitty.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local kitty = autoload("rz.lib.kitty")
local _2_ = vim.api.nvim_create_augroup("rz_kitty_reload", {clear = true})
local function _3_()
  return kitty["reload-conf"]()
end
return {vim.api.nvim_create_autocmd({"BufWritePost"}, {callback = _3_, group = _2_, pattern = {"kitty.conf", "kitty-font.conf", "*/kitty-fonts/*.conf"}})}
