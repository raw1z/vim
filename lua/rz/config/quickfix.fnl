(require-macros :macros.core)
(require-macros :macros.vim)

(fn open-trouble []
  (vim.cmd "Trouble qflist toggle focus=true"))

(define-augroup rz-quickfix
  (define-autocmd QuickfixCmdPost vimgrep open-trouble))
