(require-macros :macros.core)
(require-macros :macros.vim)

; AutoWriteEnable AutoWriteDisable AutoWriteToggle {{{
(define-command AutoWriteEnable []
  (let [buffer (require :rz.lib.buffer)]
    (buffer.enable-autowrite (buffer.current))))

(define-command AutoWriteDisable []
  (let [buffer (require :rz.lib.buffer)]
    (buffer.disable-autowrite (buffer.current))))

(define-command AutoWriteToggle []
  (let [buffer (require :rz.lib.buffer)]
    (buffer.toggle-autowrite (buffer.current))))
; }}}
; CopyThisPath CopyThisLine {{{
(define-command CopyThisPath [?opt]
  (let [buffer (require :rz.lib.buffer)]
    (vim.fn.setreg :+ (buffer.copy-path (buffer.current)
                                        false
                                        (= :--full ?opt)) :c)))

(define-command CopyThisLine [?opt]
  (let [buffer (require :rz.lib.buffer)]
    (vim.fn.setreg :+ (buffer.copy-path (buffer.current)
                                        true
                                        (= :--full ?opt)) :c)))
; }}}
; Git {{{
(define-command SaveChanges [message?]
  (let [git (require :rz.lib.git)]
    (git.save-changes messages?)))

(define-command SaveStagedChanges [message?]
  (let [git (require :rz.lib.git)]
    (git.save-staged-changes messages?)))

(define-command RestoreChanges []
  (let [git (require :rz.lib.git)]
    (git.restore-changes)))

(define-command RebaseContinue []
  (let [git (require :rz.lib.git)]
    (git.continue-rebase)))

(define-command RebaseAbort []
  (let [git (require :rz.lib.git)]
    (git.abort-rebase)))
; }}}
; Lazygit {{{
(define-command Lazygit []
  (let [terminal (require :rz.lib.terminal)]
    (terminal.start
      "lazygit"
      {:name :lazygit
      :start-insert? true
      :auto-close? true
      :direction :tab})))
; }}}
; GrpcPlayground {{{
(fn configure-grpc-playground [bufnr]
  (vim.api.nvim_buf_set_var bufnr :is_special_buffer true)
  (vim.api.nvim_buf_set_var bufnr :special_buffer_name "Language Shell - grpc")
  (def-key/buffer "run current line"
    bufnr n <Enter> (%> Grpc)))

(define-command GrpcPlayground []
  (let [draft (require :rz.lib.draft)
        bufnr (draft.create :tab :grpcnvim {:scratch? false
                                            :suffix :.grpc})]
    (configure-grpc-playground bufnr)))

(define-command GrpcPlaygroundThis []
  (let [buffer (require :rz.lib.buffer)]
    (-> (buffer.current)
        (configure-grpc-playground))))
; }}}
; HttpPlayground {{{
(fn configure-http-playground [bufnr]
  (vim.api.nvim_buf_set_var bufnr :is_special_buffer true)
  (vim.api.nvim_buf_set_var bufnr :special_buffer_name "Language Shell - http")
  (def-key/buffer "run current line"
    bufnr n <Enter> :<Plug>RestNvim))

(define-command HttpPlayground []
  (let [draft (require :rz.lib.draft)
        bufnr (draft.create :tab :http {:scratch? false
                                        :suffix :.http})]
    (configure-http-playground bufnr)))

(define-command HttpPlaygroundThis []
  (let [buffer (require :rz.lib.buffer)]
    (-> (buffer.current)
        (configure-http-playground))))
; }}}
; ProjectInit ProjectEdit {{{
(define-command ProjectInit []
  (let [path (require :rz.lib.path)
        config-file (path.join (vim.fn.projectroot#guess)
                               ".nfnl.fnl")
        project-file (path.join (vim.fn.projectroot#guess)
                                ".nvim.fnl")]
    (path.create config-file
                 "{:compiler-options {:compiler-env _G}\n:fennel-macro-path (.. (vim.fn.stdpath :config) :/?.fnl)}")
    (path.create project-file
                 "(require-macros :macros.core)
(require-macros :macros.vim)
; (local {: autoload} (require :nfnl.module))
; (local terminal (autoload :rz.lib.terminal))

; (load-plugins vim-bundler
;               vim-rake)

; (apply-strategies ruby)

; (define-command RunIt []
;   (terminal.direnv-exec
;     \"<command>\"
;     {:start-insert? true
;      :direction :bottom
;      :name :run-it}))

; (def-keys
;   \"run\" n <space>mm (%> RunIt))
")
    (%> tabnew .nvim.fnl)))

(define-command ProjectEdit []
  (%> tabnew .nvim.fnl))
; }}}
; TabGo {{{
(define-command TabGo [path] {:complete :dir}
  (let [dir (require :rz.lib.dir)]
    (dir.go-to-folder-in-new-tab path)))
; }}}
; PluginsUpdate PluginsHeadlessUpdate {{{
(fn update-plugins []
  (let [lazy (require :lazy)]
    (lazy.update)))

(fn update-plugins-headless []
  ; after lazy updates we wait a little bit of time
  ; in order for lazy to create the lazy-lock.json
  ; file before we close neovim
  (define-autocmd User LazyUpdate
    (vim.defer_fn #(%> quitall) 1000))
  (update-plugins))

(define-command PluginsUpdate []
  (update-plugins))
(define-command PluginsHeadlessUpdate []
  (update-plugins-headless))
; }}}
