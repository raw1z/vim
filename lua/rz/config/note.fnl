(require-macros :macros.core)
(require-macros :macros.vim)

(local {: autoload} (require :nfnl.module))
(local note (autoload :rz.lib.note))

(define-command NoteEdit [arg?]
  (if (= arg? :--root)
    (note.edit-root)
    (note.edit)))

(def-key "edit note"
  n <space>ii (%> NoteEdit))

(def-key "edit root note"
  n <space>ir (%> NoteEdit --root))
