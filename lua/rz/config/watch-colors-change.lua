-- [nfnl] Compiled from lua/rz/config/watch-colors-change.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local watcher = autoload("rz.lib.watcher")
local path = autoload("rz.lib.path")
local function reload_colors()
  local notify = require("rz.lib.notify")
  local colors = require("rz.lib.colors")
  notify.dismiss()
  return colors.reload()
end
local function safe_reload_colors(count)
  if ((count > 0) and not pcall(reload_colors)) then
    return safe_reload_colors((count - 1))
  else
    return nil
  end
end
local function _3_()
  return reload_colors()
end
vim.api.nvim_create_user_command("ReloadColors", _3_, {force = true})
local function _4_()
  return safe_reload_colors(3)
end
vim.api.nvim_create_user_command("SafeReloadColors", _4_, {force = true})
local function _5_()
  return safe_reload_colors(3)
end
return watcher["watch-file-with-debounce"](path.expand("~/.Xresources"), 500, _5_)
