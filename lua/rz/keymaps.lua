-- [nfnl] Compiled from lua/rz/keymaps.fnl by https://github.com/Olical/nfnl, do not edit.
vim.keymap.set({"i", "c"}, "kj", "<esc>", {buffer = false, silent = true})
vim.keymap.set({"c"}, "<C-p>", "<Up>", {buffer = false, silent = true})
vim.keymap.set({"c"}, "<C-n>", "<Down>", {buffer = false, silent = true})
vim.keymap.set({"n"}, "<C-l>", "<C-]>", {buffer = false, remap = true, silent = true})
vim.keymap.set({"n", "i", "v", "c", "t", "s"}, "<leader>)", "]", {buffer = false, remap = true, silent = true})
vim.keymap.set({"n", "i", "v", "c", "t", "s"}, "<leader>(", "[", {buffer = false, remap = true, silent = true})
vim.keymap.set({"n", "i", "v", "c", "t", "s"}, "<leader><leader>)", "}", {buffer = false, remap = true, silent = true})
vim.keymap.set({"n", "i", "v", "c", "t", "s"}, "<leader><leader>(", "{", {buffer = false, remap = true, silent = true})
vim.keymap.set({"n", "i", "v", "c", "t", "s"}, "<leader>n", "~", {buffer = false, remap = true, silent = true})
vim.keymap.set({"n", "i", "v", "c", "t", "s"}, "<leader>/", "\\", {buffer = false, remap = true, silent = true})
vim.keymap.set({"n", "i", "v", "c", "t", "s"}, "<leader>!", "|", {buffer = false, remap = true, silent = true})
vim.keymap.set({"n", "i", "v", "c", "t", "s"}, "<leader>'", "`", {buffer = false, remap = true, silent = true})
vim.keymap.set({"n", "i", "v", "c", "t", "s"}, "<leader>n", "~", {buffer = false, remap = true, silent = true})
vim.keymap.set({"n", "i", "v", "c", "t", "s"}, "<leader>q", "<", {buffer = false, remap = true, silent = true})
vim.keymap.set({"n", "i", "v", "c", "t", "s"}, "<leader><leader>q", ">", {buffer = false, remap = true, silent = true})
vim.keymap.set({"i"}, "<leader>-", "^", {buffer = false, remap = true, silent = true})
vim.keymap.set({"i", "c", "s"}, "<localleader>f", "(", {buffer = false, remap = true, silent = true})
vim.keymap.set({"i", "c", "s"}, "<localleader>d", "[", {buffer = false, remap = true, silent = true})
vim.keymap.set({"i", "c", "s"}, "<localleader>s", "{", {buffer = false, remap = true, silent = true})
vim.keymap.set({"i", "c", "s"}, "<localleader>r", ")", {buffer = false, remap = true, silent = true})
vim.keymap.set({"i", "c", "s"}, "<localleader>e", "]", {buffer = false, remap = true, silent = true})
vim.keymap.set({"i", "c", "s"}, "<localleader>z", "}", {buffer = false, remap = true, silent = true})
vim.keymap.set({"i", "c", "s"}, "<localleader>!", "!", {buffer = false, remap = true, silent = true})
local function _1_()
  return vim.cmd("set wrap! linebreak nolist")
end
vim.keymap.set({"n"}, "<leader>W", _1_, {buffer = false, desc = "toggle wrap", silent = true})
vim.keymap.set({"n"}, "j", "gj", {buffer = false, silent = true})
vim.keymap.set({"n"}, "k", "gk", {buffer = false, silent = true})
vim.keymap.set({"n"}, "<Up>", "<C-w>+", {buffer = false, silent = true})
vim.keymap.set({"n"}, "<Down>", "<C-w>-", {buffer = false, silent = true})
vim.keymap.set({"n"}, "<Left>", "<C-w><", {buffer = false, silent = true})
vim.keymap.set({"n"}, "<Right>", "<C-w>>", {buffer = false, silent = true})
vim.keymap.set({"n"}, "<leader>=", "<C-w>=", {buffer = false, silent = true})
local function _2_()
  return vim.cmd("tabnew")
end
vim.keymap.set({"n"}, "<C-w><C-t>", _2_, {buffer = false, desc = "create a new tab", silent = true})
local function _3_()
  return vim.cmd("tabclose")
end
local function _4_()
  return vim.cmd("tabonly")
end
local function _5_()
  return vim.cmd("only|tabonly")
end
do local _ = {vim.keymap.set({"n"}, "<C-w>C", _3_, {buffer = false, desc = "close current tab", silent = true}), vim.keymap.set({"n"}, "<C-w>O", _4_, {buffer = false, desc = "close all other tabs", silent = true}), vim.keymap.set({"n"}, "<C-w><C-o>", _5_, {buffer = false, desc = "close everything except current window", silent = true})} end
local function _6_()
  return vim.cmd("update")
end
vim.keymap.set({"n"}, "<leader>s", _6_, {buffer = false, desc = "write the whole buffer to the current file if modified", silent = true})
local function _7_()
  return vim.cmd("exit")
end
vim.keymap.set({"n"}, "<leader>x", _7_, {buffer = false, desc = "write current file, if modified, and close the current window", silent = true})
local function _8_()
  return vim.cmd("tabclose")
end
vim.keymap.set({"n"}, "<leader>X", _8_, {buffer = false, desc = "close current tab", silent = true})
local function _9_()
  local buffer = require("rz.lib.buffer")
  return buffer["select-last-pasted-text"]()
end
vim.keymap.set({"n"}, "gp", _9_, {buffer = false, desc = "select last pasted text", silent = true})
vim.keymap.set({"n"}, "Y", "\"+yy", {buffer = false, silent = true})
vim.keymap.set({"v"}, "Y", "\"+y", {buffer = false, silent = true})
vim.keymap.set({"n", "v"}, "<leader>p", "\"+p", {buffer = false, silent = true})
vim.keymap.set({"n", "v"}, "<leader>P", "\"+P", {buffer = false, silent = true})
vim.keymap.set({"n", "v"}, "<localleader>p", "\"0p", {buffer = false, silent = true})
vim.keymap.set({"n", "v"}, "<localleader>P", "\"0P", {buffer = false, silent = true})
do local _ = {vim.keymap.set({"n"}, "<localleader>o", "o<esc>", {buffer = false, desc = "insert a new line below in normal mode", silent = true}), vim.keymap.set({"n"}, "<localleader>O", "O<esc>", {buffer = false, desc = "insert a new line above in normal mode", silent = true})} end
vim.keymap.set({"n"}, "n", "nzzzv", {buffer = false, silent = true})
vim.keymap.set({"n"}, "N", "Nzzzv", {buffer = false, silent = true})
vim.keymap.set({"i"}, ",", ",<c-g>u", {buffer = false, silent = true})
vim.keymap.set({"i"}, ".", ".<c-g>u", {buffer = false, silent = true})
vim.keymap.set({"i"}, "[", "[<c-g>u", {buffer = false, silent = true})
vim.keymap.set({"i"}, "?", "?<c-g>u", {buffer = false, silent = true})
local function reload_nvim_config()
  local config_loader = require("rz.lib.config-loader")
  local notify = require("rz.lib.notify")
  config_loader["reload-all"]()
  vim.cmd("ReloadColors")
  return notify.trace("Neovim configuration reloaded")
end
vim.keymap.set({"n"}, "<leader><leader>R", reload_nvim_config, {buffer = false, desc = "reload nvim config", silent = true})
vim.keymap.set({"v"}, "<TAB>", ">gv", {buffer = false, silent = true})
vim.keymap.set({"v"}, "<S-TAB>", "<gv", {buffer = false, silent = true})
vim.keymap.set({"t"}, "<C-o>", "<C-\\><C-n>", {buffer = false, silent = true})
vim.keymap.set({"n"}, "<c-w>z", "<c-w>|<c-w>_", {buffer = false, desc = "make current window fullscreen", silent = true})
vim.keymap.set({"n"}, "<leader>w", "<C-w>", {buffer = false, silent = true})
vim.keymap.set({"t"}, "<leader>w", "<C-\\><C-n><C-w>", {buffer = false, silent = true})
vim.keymap.set({"t"}, "<C-w>", "<C-\\><C-n><C-w>", {buffer = false, remap = true, silent = true})
local function _10_()
  return Snacks.notifier.hide()
end
vim.keymap.set({"n"}, "<space>j", _10_, {buffer = false, desc = "dismiss notifications", silent = true})
local function go_to_closed_fold(direction)
  local cmd = ("norm!z" .. direction)
  local view = vim.fn.winsaveview()
  local l0 = 0
  local l = view.lnum
  local open = 1
  while ((l0 ~= l) and open) do
    vim.api.nvim_command(cmd)
    l0 = l
    l = vim.fn.line(".")
    open = (vim.fn.foldclosed(l) < 0)
  end
  if open then
    return vim.fn.winrestview(view)
  else
    return nil
  end
end
local function _12_()
  return go_to_closed_fold("j")
end
vim.keymap.set({"n"}, "<leader>zj", _12_, {buffer = false, desc = "go to next closed fold", silent = true})
local function _13_()
  return go_to_closed_fold("k")
end
vim.keymap.set({"n"}, "<leader>zk", _13_, {buffer = false, desc = "go to previous closed fold", silent = true})
local function _14_()
  return vim.cmd("nohlsearch")
end
vim.keymap.set({"n"}, "<leader>/", _14_, {buffer = false, desc = "disable search highlight", silent = true})
local function toggle_relative_numbers()
  local current
  do
    local opt_value_11_auto = vim.opt.relativenumber
    current = opt_value_11_auto:get()
  end
  vim["opt"]["relativenumber"] = not current
  return nil
end
vim.keymap.set({"n"}, "<space>n", toggle_relative_numbers, {buffer = false, desc = "toggle relative numbers", silent = true})
local function _15_()
  return vim.cmd("Lazy update")
end
local function _16_()
  return vim.cmd("Lazy restore")
end
local function _17_()
  return vim.cmd("Lazy install")
end
local function _18_()
  return vim.cmd("Lazy install")
end
local function _19_()
  return vim.cmd("Lazy")
end
return {vim.keymap.set({"n"}, "<space>pu", _15_, {buffer = false, desc = "update plugins", silent = true}), vim.keymap.set({"n"}, "<space>ps", _16_, {buffer = false, desc = "sync plugins with lockfile", silent = true}), vim.keymap.set({"n"}, "<space>pi", _17_, {buffer = false, desc = "install plugins", silent = true}), vim.keymap.set({"n"}, "<space>pi", _18_, {buffer = false, desc = "restore plugins", silent = true}), vim.keymap.set({"n"}, "<space>pl", _19_, {buffer = false, desc = "open lazy", silent = true})}
