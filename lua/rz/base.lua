-- [nfnl] Compiled from lua/rz/base.fnl by https://github.com/Olical/nfnl, do not edit.
vim["opt"]["autoindent"] = true
vim["opt"]["autoread"] = true
vim["opt"]["backspace"] = {"indent", "eol", "start"}
vim["opt"]["backupcopy"] = "yes"
vim["opt"]["cmdheight"] = 0
vim["opt"]["complete"] = {".", "w", "b", "u", "t"}
vim["opt"]["completeopt"] = {"menu", "menuone", "noselect"}
vim["opt"]["cursorline"] = true
vim["opt"]["diffopt"] = "vertical,filler,algorithm:histogram,indent-heuristic"
do
  local opt_value_11_auto = vim.opt.display
  opt_value_11_auto:prepend("truncate")
end
vim["opt"]["expandtab"] = true
vim["opt"]["exrc"] = true
vim["opt"]["fillchars"] = "vert:\226\148\130,fold: ,foldopen:-,foldclose:+,foldsep:\226\148\130,diff:\226\149\140,eob: ,wbr:\226\148\128"
vim["opt"]["foldcolumn"] = "auto:1"
do
  local opt_value_11_auto = vim.opt.formatoptions
  opt_value_11_auto:prepend("j")
end
vim["opt"]["grepprg"] = "rg --vimgrep --hidden --no-heading --smart-case"
vim["opt"]["history"] = 10000
vim["opt"]["ignorecase"] = true
vim["opt"]["incsearch"] = true
vim["opt"]["laststatus"] = 3
vim["opt"]["more"] = true
vim["opt"]["nrformats"] = {"bin", "hex"}
vim["opt"]["number"] = true
vim["opt"]["relativenumber"] = true
vim["opt"]["ruler"] = false
vim["opt"]["scrolloff"] = 1
vim["opt"]["shada"] = "!,'1000,<50,s10,h"
vim["opt"]["shiftround"] = true
vim["opt"]["shiftwidth"] = 2
vim["opt"]["shortmess"] = "acoOtWICF"
vim["opt"]["showcmdloc"] = "statusline"
vim["opt"]["showmode"] = false
vim["opt"]["sidescroll"] = 1
vim["opt"]["sidescrolloff"] = 2
vim["opt"]["signcolumn"] = "yes"
vim["opt"]["smartcase"] = true
vim["opt"]["smarttab"] = true
vim["opt"]["splitright"] = true
vim["opt"]["swapfile"] = false
vim["opt"]["tabstop"] = 2
vim["opt"]["termguicolors"] = true
vim["opt"]["title"] = true
vim["opt"]["undofile"] = true
vim["opt"]["updatetime"] = 300
vim["opt"]["viewoptions"] = {"cursor", "folds"}
do
  local opt_value_11_auto = vim.opt.wildignore
  opt_value_11_auto:append({"*.o", "*.obj", ".git", "*.rbc", "*.class", ".svn", "tmp/*", "*.so", "*.swp", "*.zip", "*.png", "*.jpg", "*.gif", "*.jpeg", "*/vendor/gems/*", "*/vendor/cache/*", "Gemfile.lock", "*/log/*", ".sass-cache", "*/coverage/*", "*.pyc", "*.pyo", "*/node_modules/*", "yarn.lock", "package-lock.json", "*/elm-stuff/*"})
end
vim["opt"]["wildignorecase"] = true
vim["opt"]["wildmenu"] = true
vim["opt"]["wrap"] = false
if (nil == vim.g.rz_base_init) then
  vim.cmd("colorscheme rz")
  vim.g.rz_base_init = true
else
end
vim["opt"]["guicursor"] = {"n-v-c-r:block-Cursor", "i-ci-ve:ver25-ICursor", "r-cr-o:block-RCursor", "a:blinkwait700-blinkoff400-blinkon250"}
vim["opt"]["sessionoptions"] = {"blank", "curdir", "folds", "help", "globals", "tabpages", "winsize", "slash", "unix"}
vim.g.editorconfig = false
do
  local _2_ = vim.api.nvim_create_augroup("NoNumbersForTerminal", {clear = true})
  do local _ = {vim.api.nvim_create_autocmd({"TermOpen"}, {command = ":setlocal nonumber norelativenumber nocursorline", group = _2_, pattern = {"*"}})} end
end
vim.filetype.add({extension = {grpc = "grpcnvim", http = "http", pl = "prolog", plt = "prolog", ua = "uiua", lfe = "lfe", nu = "nu"}})
vim.wo.foldtext = "v:lua.require('rz.lib.folds').foldtext()"
return nil
