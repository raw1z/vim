(require-macros :macros.vim)

(set vim.g.mapleader :ç)
(set vim.g.maplocalleader :!)

; disable unused vim providers
(set vim.g.loaded_node_provider 0)
(set vim.g.loaded_perl_provider 0)
(set vim.g.loaded_python3_provider 0)
(set vim.g.loaded_ruby_provider 0)

(local lazypath (.. (vim.fn.stdpath :data)
                    :/lazy/lazy.nvim))

(when (not (vim.uv.fs_stat lazypath))
  (vim.fn.system [:git
                  :clone
                  "--filter=blob:none"
                  "https://github.com/folke/lazy.nvim.git"
                  :--branch=stable
                  lazypath]))

(vim.opt.rtp:prepend lazypath)

(define-augroup rz_init
  (define-autocmd User LazyDone
    (let [config-loader (require :rz.lib.config-loader)]
      (config-loader.load-all))))

(let [lazy (require :lazy)]
  (lazy.setup :plugins {:defaults {:lazy true}
                        :dev {:path vim.env.PROJECTS_PATH}}))
