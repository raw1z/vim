nvim_folder="$(pwd)/nvim"

docker run --rm -it \
  -v "$nvim_folder:/root/.config/nvim" \
  neovim
