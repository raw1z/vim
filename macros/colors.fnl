;; [nfnl-macro]

(local M {})

(fn get-color [kind c]
  (when (not= c (sym :-))
    (let [color (tostring c)]
      (if (not= -1 (vim.fn.match color "\\v^\\u(.+)"))
        `(. (vim.api.nvim_get_hl_by_name ,color true) ,kind)
        `((. (require :rz.lib.colors) :get-color) ,color)))))

(fn get-style [s]
  (when (not= (tostring s) :-)
    (tostring s)))

(fn highlight [group foreground background style]
  (let [real-group (vim.fn.substitute (tostring group) :__ "@" "")
        opts {:foreground (get-color :foreground foreground)
              :background (get-color :background background)}
        style (get-style style)]
    (when (not= nil style)
      (tset opts style true))
    `(do
       (vim.api.nvim_set_hl 0 ,real-group ,opts)
       [,real-group ,opts])))

(fn sanitize-h [str pattern]
  (if (not (table? pattern))
    (vim.fn.substitute str pattern "" "g")
    (accumulate [acc str
                 _ item (ipairs pattern)]
      (sanitize-h acc item))))

(fn sanitize [item]
  (-> (tostring item)
      (sanitize-h [:║ :╔ :═ :╗ :╝ :╚ :╤ :╧ :│ :╟ :─])))

(fn prepare [input]
  (->> input
       (vim.tbl_map sanitize)
       (vim.tbl_filter #(not= 0 (length $1)))
       (vim.tbl_map sym)))

(fn concat [t1 t2]
  (var buffer [])
  (each [_ value (ipairs t1)]
    (table.insert buffer value))
  (each [_ value (ipairs t2)]
    (table.insert buffer value))
  buffer)

(fn last [list]
  (. list (length list)))

(fn chunk-list [list count]
  (last
    (accumulate [[acc results] [[] []]
                 _ item (ipairs list)]
      (let [new-acc (concat acc [item])]
        (if (< (length acc) (- count 1))
          [new-acc results]
          [[] (concat results [new-acc])])))))

(fn M.define-highlight [...]
  (let [chunks (chunk-list (prepare [...]) 4)]
    (icollect [_ chunk (ipairs chunks)]
      (highlight (unpack (-> (vim.iter chunk)
                             (: :flatten 10)
                             (: :totable)))))))

(fn M.define-highlight-context [name ...]
  (let [method-name (-> :-context
                        (.. (tostring name))
                        (sym))]
    `(do
       (when (= nil vim.g.rz_colors)
         (let [colors# (require :rz.lib.colors)]
           (colors#.set)))
       (when (not (vim.tbl_isempty vim.g.rz_colors))
         (fn ,method-name [] (define-highlight ,...))
         (,method-name)))))

(fn M.define-diagnostic-icon [kind icon]
  `(let [inflector# (require :rz.lib.inflector)
         sign-name# (.. :DiagnosticSign (inflector#.capitalize ,(tostring kind)))
         highlight-group# (.. :Diagnostic (inflector#.capitalize ,(tostring kind)))]
     (vim.fn.sign_define sign-name# {:text ,icon
                                     :texthl highlight-group#})))

M
