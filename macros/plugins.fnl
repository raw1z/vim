;; [nfnl-macro]

(local M {})

(fn M.plug [url opts]
  `(let [plugins# (require :rz.lib.plugins)]
       (plugins#.declare ,(tostring url) ,opts)))

(fn M.plug-key [mode lhs rhs opts]
  `(let [plugins# (require :rz.lib.plugins)]
       (plugins#.key ,(vim.split (tostring mode) :|)
                     ,(tostring lhs)
                     ,rhs
                     ,opts)))

M
