;; [nfnl-macro]

(local M {})

(fn M.%fmt [pattern ...]
  `(fmt ,pattern [,...]))

(fn M.%c [index ...]
  `(c ,index [,...]))

(fn M.%s [trigger ...]
  `(s ,(tostring trigger) ,...))

(fn M.%sfmt [trigger pattern ...]
  `(s ,(tostring trigger)
      (fmt ,pattern [,...])))

(fn M.%p [trigger pattern]
  `(parse ,(tostring trigger) ,pattern))

M
