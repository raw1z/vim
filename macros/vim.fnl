;; [nfnl-macro]

(local M {})

; helpers {{{
(fn prepare-list [input]
  (if (table? input)
    input
    (vim.split (tostring input) :|)))

(fn prepare-body [...]
  (if (list? ...)
    `(fn [] ,...)
    ...))

(fn concat [t1 t2]
  (var buffer [])
  (each [_ value (ipairs t1)]
    (table.insert buffer value))
  (each [_ value (ipairs t2)]
    (table.insert buffer value))
  buffer)

(fn last [list]
  (. list (length list)))

(fn chunk-list [list count]
  (last
    (accumulate [[acc results] [[] []]
                 _ item (ipairs list)]
      (let [new-acc (concat acc [item])]
        (if (< (length acc) (- count 1))
          [new-acc results]
          [[] (concat results [new-acc])])))))
; }}}
; options {{{
(fn opt-method [scope action]
  (-> [action scope :opt]
      (vim.fn.join :-)
      (vim.fn.substitute :-- :- "")))

(fn opt-id [scope]
  (-> #(not= 0 (length $1))
      (vim.tbl_filter [:opt scope])
      (vim.fn.join :_)))

(fn sanitize-sym [key]
  (if (= nil (in-scope? key))
      (tostring key)
      key))

(each [_ scope (ipairs ["" :local :global])]
  (tset M (opt-method scope :set)
    (fn [key value]
      `(tset vim ,(opt-id scope) ,(sanitize-sym key) ,value))))

(each [_ scope (ipairs ["" :local :global])]
  (each [_ action (ipairs [:get :prepend :append :remove])]
    (tset M (opt-method scope action)
      (fn [key value]
        `(let [opt-value# (. vim ,(opt-id scope) ,(sanitize-sym key))]
           (: opt-value# ,action ,value))))))
; }}}
; autocommands {{{
(fn M.define-augroup [name ...]
  (let [group (gensym)
        name (if (sym? name) (tostring name) name)
        autocmd-definitions (icollect [_ definition (ipairs [...])]
                              `(->> ,group ,definition))]
    `(do
       (local ,group (vim.api.nvim_create_augroup ,name {:clear true}))
       ,autocmd-definitions)))


(fn build-options [body group custom-opts]
  (let [opts {: group}
        body (prepare-body body)]
    (if (= :string (type body))
      (tset opts :command body)
      (tset opts :callback body))

    (->> (or custom-opts {})
         (vim.tbl_extend :force opts))))

(fn M.define-autocmd [event pattern body group]
  (if (= :_ (tostring pattern))
    `(vim.api.nvim_create_autocmd
         ,(prepare-list event)
         ,(build-options body group {}))
    `(vim.api.nvim_create_autocmd
         ,(prepare-list event)
         ,(build-options body group {:pattern (prepare-list pattern)}))))

(fn M.define-autocmd/nested [event pattern body group]
  (if (= :_ (tostring pattern))
    `(vim.api.nvim_create_autocmd
         ,(prepare-list event)
         ,(build-options body group {:nested true}))
    `(vim.api.nvim_create_autocmd
         ,(prepare-list event)
         ,(build-options body group {:pattern (prepare-list pattern)
                                     :nested true}))))

(fn M.define-autocmd/buffer [event buffer body group]
  `(vim.api.nvim_create_autocmd
     ,(prepare-list event)
     ,(build-options body group {:buffer buffer})))

(fn M.define-autocmd/this-buffer [event body group]
  `(vim.api.nvim_create_autocmd
     ,(prepare-list event)
     ,(build-options body group {:buffer 0})))

; }}}
; keymaps {{{
(fn keymap-opts [custom-opts]
  (let [default-opts {:buffer false :silent true}]
    (vim.tbl_extend :force default-opts custom-opts)))

(fn create-key [mode key custom-opts ...]
  `(vim.keymap.set ,(prepare-list mode)
                   ,(tostring key)
                   ,(prepare-body ...)
                   ,(keymap-opts custom-opts)))

(fn M.define-key [mode key ...]
  (create-key mode key {} ...))

(fn M.def-key [desc mode key ...]
  (create-key mode key {: desc} ...))

(fn M.define-key/expr [mode key ...]
  (create-key mode key {:expr true} ...))

(fn M.def-key/expr [desc mode key ...]
  (create-key mode key {: desc
                        :expr true} ...))

(fn M.define-keys [...]
  (let [chunks (chunk-list [...] 3)]
    (icollect [_ chunk (ipairs chunks)]
      (M.define-key (unpack chunk)))))

(fn M.def-keys [...]
  (let [chunks (chunk-list [...] 4)]
    (icollect [_ chunk (ipairs chunks)]
      (M.def-key (unpack chunk)))))

(fn M.define-key/buffer [buffer mode key ...]
  (create-key mode key {: buffer} ...))

(fn M.def-key/buffer [desc buffer mode key ...]
  (create-key mode key {: buffer
                        : desc} ...))

(fn M.define-keys/buffer [buffer ...]
  (let [chunks (chunk-list [...] 3)]
    (icollect [_ chunk (ipairs chunks)]
      (M.define-key/buffer buffer (unpack chunk)))))

(fn M.def-keys/buffer [buffer ...]
  (let [chunks (chunk-list [...] 4)]
    (icollect [_ [desc & rest] (ipairs chunks)]
      (M.def-key/buffer desc buffer (unpack rest)))))

(fn M.define-key/this-buffer [mode key ...]
  (create-key mode key {:buffer true} ...))

(fn M.def-key/this-buffer [desc mode key ...]
  (create-key mode key {:buffer true : desc} ...))

(fn M.define-keys/this-buffer [...]
  (let [chunks (chunk-list [...] 3)]
    (icollect [_ chunk (ipairs chunks)]
      (M.define-key/this-buffer (unpack chunk)))))

(fn M.def-keys/this-buffer [...]
  (let [chunks (chunk-list [...] 4)]
    (icollect [_ chunk (ipairs chunks)]
      (M.def-key/this-buffer (unpack chunk)))))

(fn M.remap-key [mode key ...]
  (create-key mode key {:remap true} ...))

(fn M.remap-keys/buffer [...]
  (let [chunks (chunk-list [...] 4)]
    (icollect [_ chunk (ipairs chunks)]
      (M.remap-key/buffer (unpack chunk)))))

(fn M.remap-key/buffer [buffer mode key ...]
  (create-key mode key {: buffer
                        :remap true} ...))

(fn M.remap-keys/buffer [...]
  (let [chunks (chunk-list [...] 4)]
    (icollect [_ chunk (ipairs chunks)]
      (M.remap-key/buffer (unpack chunk)))))

(fn M.remap-key/this-buffer [mode key ...]
  (create-key mode key {:buffer true
                        :remap true} ...))

(fn M.remap-keys/this-buffer [...]
  (let [chunks (chunk-list [...] 3)]
    (icollect [_ chunk (ipairs chunks)]
      (M.remap-key/this-buffer (unpack chunk)))))
; }}}
; commands {{{
(fn define-command-as-function-without-args [name opts ...]
  `(vim.api.nvim_create_user_command
      ,(tostring name)
      #(do ,...)
      ,opts))

(fn define-command-as-function-with-one-arg [name args opts ...]
  (let [arg-name (tostring (. args 1))
        optional? (not= -1 (vim.fn.stridx arg-name :?))
        nargs (if optional? :? 1)]
    `(vim.api.nvim_create_user_command
        ,(tostring name)
        #((fn ,args ,...) $1.args)
        ,(vim.tbl_extend :force opts {:nargs nargs}))))

(fn define-command-as-function-with-many-args [name args opts ...]
  `(let [text# (require :rz.lib.text)]
     (vim.api.nvim_create_user_command
        ,(tostring name)
        #((fn ,args ,...) (unpack (text#.split $1.args :\s ,(length args))))
        ,(vim.tbl_extend :force opts {:nargs :+}))))

(fn define-command-as-function [name args ...]
  (let [[first & rest] [...]
        opts {:force true}
        opts (if (table? first)
               (vim.tbl_extend :force first opts)
               opts)
        body (if (table? first)
               (unpack rest)
               ...)]
    (if
      (= 0 (length args))
        (define-command-as-function-without-args name opts body)
      (= 1 (length args))
        (define-command-as-function-with-one-arg name args opts body)
      (define-command-as-function-with-many-args name args opts body))))

(fn define-command-as-string [name args]
  `(vim.api.nvim_create_user_command
      ,(tostring name)
      ,args
      {:force true}))

(fn M.define-command [name args ...]
  (if
    (= :table (type args))
      (define-command-as-function name args ...)
    (= :string (type args))
      (define-command-as-string name args)))
; }}}
; operator {{{
(fn M.define-operator [mode key on-triggered]
  `(let [vim# (require :rz.lib.vim)]
     (vim#.add-operator
       ,(prepare-list mode)
       ,(tostring key)
       ,(keymap-opts {})
       ,on-triggered)))

(fn M.define-operator/buffer [buffer mode key on-triggered]
  `(let [vim# (require :rz.lib.vim)]
     (vim#.add-operator
       ,(prepare-list mode)
       ,(tostring key)
       ,(keymap-opts {: buffer})
       ,on-triggered)))

(fn M.define-operator/this-buffer [mode key on-triggered]
  `(let [vim# (require :rz.lib.vim)]
     (vim#.add-operator
       ,(prepare-list mode)
       ,(tostring key)
       ,(keymap-opts {:buffer true})
       ,on-triggered)))
;}}}
; log {{{
(fn M.rlog [input]
  `(let [log# (require :rz.lib.log)]
     (log#.rlog ,input)))

(fn M.flog [input]
  `(let [log# (require :rz.lib.log)]
     (log#.flog ,input)))
; }}}
; projects {{{
(fn M.apply-strategies [...]
  (icollect [_ strategy (ipairs [...])]
    `(require ,(.. "rz.lib.project-strategy." (tostring strategy)))))

(fn M.load-plugins [...]
  `(let [lazy# (require :lazy)]
    (lazy#.load {:plugins ,(vim.tbl_map tostring [...])})))
; }}}
; misc {{{
(fn M.autoformat-code []
  `(define-autocmd/this-buffer BufWritePre
      (let [lsp# (require :rz.lib.lsp)]
        (lsp#.autoformat-code))))

(fn M.lint-code [event]
  `(define-autocmd/this-buffer ,event
      (let [lint# (require :lint)]
        (lint#.try_lint))))
; }}}

M
