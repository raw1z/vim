;; [nfnl-macro]

(local M {})

(fn M.%w [...]
  (vim.tbl_map tostring [...]))

(fn M.%f [...]
  `#(do ,...))

(fn M.%> [...]
  (let [command (->> [...]
                     (vim.tbl_map tostring)
                     (vim.fn.join))]
    `(vim.cmd ,command)))

; set a vim variable only if it is not already initialized
(fn M.%g [var-name ...]
  `(when (= nil ,var-name)
     (set ,var-name ,...)))

; support lua style mixed tables
(fn M.%o [key opts]
  `(vim.tbl_extend :force ,opts {1 ,key}))

; copied from the code of nfnl: https://github.com/Olical/nfnl/blob/9e82c0e88584d842c5dcb4637c11f848799b4123/fnl/nfnl/macros.fnl
(fn conditional-let [branch bindings ...]
  (assert (= 2 (length bindings)) "expected a single binding pair")

  (let [[bind-expr value-expr] bindings]
    (if
      ;; Simple symbols
      ;; [foo bar]
      (sym? bind-expr)
      `(let [,bind-expr ,value-expr]
         (,branch ,bind-expr ,...))

      ;; List / values destructure
      ;; [(a b) c]
      (list? bind-expr)
      (do
        ;; Even if the user isn't using the first slot, we will.
        ;; [(_ val) (pcall #:foo)]
        ;;  => [(bindGENSYM12345 val) (pcall #:foo)]
        (when (= '_ (. bind-expr 1))
          (tset bind-expr 1 (gensym "bind")))

        `(let [,bind-expr ,value-expr]
           (,branch ,(. bind-expr 1) ,...)))

      ;; Sequential and associative table destructure
      ;; [[a b] c]
      ;; [{: a : b} c]
      (table? bind-expr)
      `(let [value# ,value-expr
             ,bind-expr (or value# {})]
         (,branch value# ,...))

      ;; We should never get here, but just in case.
      (assert (.. "unknown bind-expr type: " (type bind-expr))))))

(fn M.if-let [bindings ...]
  (assert (<= (length [...]) 2) (.. "if-let does not support more than two branches"))
  (conditional-let 'if bindings ...))

(fn M.when-let [bindings ...]
  (conditional-let 'when bindings ...))

(fn M.blank? [x]
  `(= 1 (vim.fn.empty ,x)))

M
