
let s:is_windows = has('win32') || has('win64')

function! g:RunCommandInTerminal(command) abort "{{{
  execute 'tabnew'
  nmap <buffer> q :q<CR>
  call termopen(a:command)
endfunction "}}}
function! g:IsWindows() abort "{{{
  return s:is_windows
endfunction "}}}
function! g:IsMac() abort "{{{
  return !s:is_windows && !has('win32unix')
      \ && (has('mac') || has('macunix') || has('gui_macvim')
      \     || (!executable('xdg-open') && system('uname') =~? '^darwin'))
endfunction "}}}
function! g:RemoveFilesReadOnlyAttribute(files) abort "{{{
  if has("win32")
    let s:cmd = "silent ! attrib -R ".a:files
  else
    let s:cmd = "silent !chmod u+w ".a:files
  end

  execute s:cmd
endfunction "}}}
function! g:RemoveCurrentFileReadOnly() abort "{{{
  let filename = expand("%")
  call g:RemoveFilesReadOnlyAttribute(filename)
endfunction "}}}
function! g:AutoTabularize() abort "{{{
  let p = '^\s*|\s.*\s|\s*$'
  if exists(':Tabularize') && getline('.') =~# '^\s*|' && (getline(line('.')-1) =~# p || getline(line('.')+1) =~# p)
    let column = strlen(substitute(getline('.')[0:col('.')],'[^|]','','g'))
    let position = strlen(matchstr(getline('.')[0:col('.')],'.*|\s*\zs.*'))
    Tabularize/|/l1
    normal! 0
    call search(repeat('[^|]*|',column).'\s\{-\}'.repeat('.',position),'ce',line('.'))
  endif
endfunction "}}}
function! g:CreateTermBuffer() "{{{
  new
  let g:current_term_buffer = bufnr('%')
  normal K
endfunction "}}}
function! g:DeleteTermBuffer() "{{{
  if exists("g:current_term_buffer")
    if bufexists(g:current_term_buffer)
      execute 'silent ' . g:current_term_buffer . 'bdelete!'
    endif
  end
endfunction "}}}
function! g:OpenTermBuffer(command) "{{{
  call g:DeleteTermBuffer()
  wall
  call g:CreateTermBuffer()
  setlocal nonu
  nnoremap <buffer>q :quit!<CR>
  call termopen(a:command)
endfunction "}}}
function! g:UpdateRPlugin(info) " {{{
  if has('nvim')
    silent UpdateRemotePlugins
    echomsg 'rplugin updated: ' . a:info['name'] . ', restart vim for changes'
  endif
endfunction " }}}
function! g:CreateDisplayBuffer() "{{{
  new
  let g:display_buffer = bufnr('%')

  normal K
  setlocal buftype=nofile bufhidden=wipe noswapfile nobuflisted nomodified
  nmap <buffer> q :q<CR>
  nmap <buffer> <c-o> :echo "noop"<cr>
  nmap <buffer> <c-i> :echo "noop"<cr>
  nmap <buffer> <c-l> :echo "noop"<cr>
endfunction "}}}
function! g:DeleteDisplayBuffer() "{{{
  if exists("g:display_buffer")
    if bufexists(g:display_buffer)
      execute 'silent ' . g:display_buffer . 'bdelete!'
    endif
  end
endfunction "}}}

" run a shell command in background and display its output if its exit code is not 0 {{{
let g:Shell = {}

function g:Shell.on_stdout(_job_id, data, _event)
  let self.output += a:data
endfunction

let g:Shell.on_stderr = function(g:Shell.on_stdout)

function g:Shell.on_exit(_job_id, exit_code, _event)
  if a:exit_code == 0
    echo 'succeeded!'
    return
  endif

  call g:CreateDisplayBuffer()

  for line in self.output
    call append(line('$'), line)
  endfor

  exe ":%s//\r/g"
  normal gg
endfunction

function g:Shell.new(name, cmd)
  let object = extend(copy(g:Shell), {'name': a:name})
  let object.cmd = ['sh', '-c', a:cmd]
  let object.output = []
  let object.id = jobstart(object.cmd, object)

  call g:DeleteDisplayBuffer()

  return object
endfunction

function g:RunShellCommandInBackground(name, command)
  call g:Shell.new(a:name, a:command)
endfunction
" }}}
