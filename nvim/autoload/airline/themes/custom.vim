let g:airline#themes#custom#palette = {}
let s:gui00 = "NONE"
let s:gui04 = "#c678dd"
let s:gui05 = "#c8ccd4"
let s:gui08 = "#e06c75"
let s:gui0A = "#e5c07b"
let s:gui0B = "#8bbf56"
let s:gui0C = "#282c34"
let s:gui0D = "#abb2bf"
let s:gui0E = "#568bbf"

" Terminal color definitions
let s:cterm00 = 00
let s:cterm03 = 08
let s:cterm05 = 07
let s:cterm07 = 15
let s:cterm08 = 01
let s:cterm0A = 03
let s:cterm0B = 02
let s:cterm0C = 06
let s:cterm0D = 04
let s:cterm0E = 05
if exists('base16colorspace') && base16colorspace == "256"
    let s:cterm01 = 18
    let s:cterm02 = 19
    let s:cterm04 = 20
    let s:cterm06 = 21
    let s:cterm09 = 16
    let s:cterm0F = 17
else
    let s:cterm01 = 10
    let s:cterm02 = 11
    let s:cterm04 = 12
    let s:cterm06 = 13
    let s:cterm09 = 09
    let s:cterm0F = 14
endif

let s:N1   = [ s:gui0D, s:gui00, s:cterm0B, s:cterm0B ]
let s:N2   = [ s:gui0C, s:gui00, s:cterm02, s:cterm02 ]
let s:N3   = [ s:gui05, s:gui00, s:cterm01, s:cterm01 ]
let g:airline#themes#custom#palette.normal = airline#themes#generate_color_map(s:N1, s:N2, s:N3)

let s:I1   = [ s:gui0B, s:gui00, s:cterm0D, 'NONE' ]
let s:I2   = [ s:gui0C, s:gui00, s:cterm02, 'NONE' ]
let s:I3   = [ s:gui05, s:gui00, s:cterm01, 'NONE' ]
let g:airline#themes#custom#palette.insert = airline#themes#generate_color_map(s:I1, s:I2, s:I3)

let s:R1   = [ s:gui08, s:gui00, s:cterm08, 'NONE' ]
let s:R2   = [ s:gui0C, s:gui00, s:cterm02, 'NONE' ]
let s:R3   = [ s:gui05, s:gui00, s:cterm01, 'NONE' ]
let g:airline#themes#custom#palette.replace = airline#themes#generate_color_map(s:R1, s:R2, s:R3)

let s:V1   = [ s:gui0E, s:gui00, s:cterm0E, 'NONE' ]
let s:V2   = [ s:gui0C, s:gui00, s:cterm02, 'NONE' ]
let s:V3   = [ s:gui05, s:gui00, s:cterm01, 'NONE' ]
let g:airline#themes#custom#palette.visual = airline#themes#generate_color_map(s:V1, s:V2, s:V3)

let s:IA1   = [ s:gui04, s:gui00, s:cterm01, 'NONE' ]
let s:IA2   = [ s:gui04, s:gui00, s:cterm01, 'NONE' ]
let s:IA3   = [ s:gui0D, s:gui00, s:cterm01, 'NONE' ]
let g:airline#themes#custom#palette.inactive = airline#themes#generate_color_map(s:IA1, s:IA2, s:IA3)

" Warning Section
let g:airline#themes#custom#palette.normal.airline_warning = [
            \ s:gui0A, s:gui00, s:cterm0A, 'NONE'
            \ ]

let g:airline#themes#custom#palette.insert.airline_warning =
            \ g:airline#themes#custom#palette.normal.airline_warning

let g:airline#themes#custom#palette.visual.airline_warning =
            \ g:airline#themes#custom#palette.normal.airline_warning

let g:airline#themes#custom#palette.replace.airline_warning =
            \ g:airline#themes#custom#palette.normal.airline_warning

" Error Section
let g:airline#themes#custom#palette.normal.airline_error = [
            \ s:gui08, s:gui00, s:cterm08, 'NONE'
            \ ]

let g:airline#themes#custom#palette.insert.airline_error =
            \ g:airline#themes#custom#palette.normal.airline_error

let g:airline#themes#custom#palette.visual.airline_error =
            \ g:airline#themes#custom#palette.normal.airline_error

let g:airline#themes#custom#palette.replace.airline_error =
            \ g:airline#themes#custom#palette.normal.airline_error
