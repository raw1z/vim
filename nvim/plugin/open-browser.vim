nmap gx <Plug>(openbrowser-smart-search)
vmap gx <Plug>(openbrowser-smart-search)
let g:netrw_nogx = 1 " disable netrw's gx mapping.
let g:openbrowser_default_search = "duckduckgo"
