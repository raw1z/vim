let g:UltiSnipsEditSplit = "horizontal"
let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"

try
  let s:Vital = vital#of('vital')
  let s:Filepath = s:Vital.import('System.Filepath')
  let s:vim_dirname = s:Filepath.dirname($MYVIMRC)
catch
endtry

if exists('s:vim_dirname')
  let g:UltiSnipsSnippetsDir = s:Filepath.join(s:vim_dirname, 'UltiSnips')
endif

