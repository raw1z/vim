let g:colorizer_disable_bufleave = 1

if has('nvim')
  let g:colorizer_use_virtual_text = 1
endif

