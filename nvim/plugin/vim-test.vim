let test#strategy = "neovim"
let test#neovim#term_position = "top"
let test#ruby#rspec#options = "--no-profile"
let test#ruby#use_spring_binstub = 1

function! g:RunTestCommand(command)"{{{
  silent! wall
  exe a:command
  nmap <buffer> q :quit<CR>
  tnoremap <buffer> gg <C-\><C-n>gg
endfunction
"}}}
function! g:RunTestNearest()"{{{
  call g:RunTestCommand("TestNearest")
endfunction
"}}}
function! g:RunTestFile()"{{{
  call g:RunTestCommand("TestFile")
endfunction
"}}}
function! g:RunTestLast()"{{{
  call g:RunTestCommand("TestLast")
endfunction
"}}}

nmap <silent> <leader>r :call g:RunTestNearest()<CR>
nmap <silent> <leader><leader>r :call g:RunTestFile()<CR>
nmap <silent> <leader>l :call g:RunTestLast()<CR>

