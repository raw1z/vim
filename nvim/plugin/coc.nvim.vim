" if hidden is not set, TextEdit might fail.
set hidden

" Some servers have issues with backup files, see #649
set nobackup
set nowritebackup

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()"{{{
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction"}}}

" Mappings for CoCList {{{

" Show all actions.
nnoremap <silent><nowait> <space>a  :<C-u>CocAction --normal<cr>

" Show all diagnostics.
nnoremap <silent><nowait> <space>d  :<C-u>CocList --normal diagnostics<cr>

" }}}
