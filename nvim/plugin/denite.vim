if has('nvim')
  try
    " Define mappings {{{
    autocmd FileType denite call s:denite_my_settings()
    function! s:denite_my_settings() abort
      nnoremap <silent><buffer><expr> <CR> denite#do_map('do_action')
      nnoremap <silent><buffer><expr> d denite#do_map('do_action', 'delete')
      nnoremap <silent><buffer><expr> p denite#do_map('do_action', 'preview')
      nnoremap <silent><buffer><expr> q denite#do_map('quit')
      nnoremap <silent><buffer><expr> <ESC> denite#do_map('quit')
      nnoremap <silent><buffer><expr> i denite#do_map('open_filter_buffer')
      nnoremap <silent><buffer><expr> <Space> denite#do_map('toggle_select').'j'
      nnoremap <silent><buffer><expr> <TAB> denite#do_map('choose_action')
      nnoremap <silent><buffer><expr> s denite#do_map('do_action', 'split')
      nnoremap <silent><buffer><expr> v denite#do_map('do_action', 'vsplit')
      nnoremap <silent><buffer><expr> t denite#do_map('do_action', 'tabopen')
    endfunction

    autocmd FileType denite-filter call s:denite_filter_my_settings()
    function! s:denite_filter_my_settings() abort
      inoremap <silent><buffer><expr> <C-c> denite#do_map('quit')
      nnoremap <silent><buffer><expr> <C-c> denite#do_map('quit')
    endfunction "}}}
    " Change default prompt {{{
    call denite#custom#option('default', 'prompt', '>')
    "}}}
    " Configure highlighting{{{
    call denite#custom#option('_', 'highlight_mode_insert', 'CursorLine')
    call denite#custom#option('_', 'highlight_matched_range', 'None')
    call denite#custom#option('_', 'highlight_matched_char', 'None')
    "}}}
    " Change matchers.{{{
    call denite#custom#source(
    \ 'file_mru', 'matchers', ['matcher/fuzzy', 'matcher/project_files'])
    call denite#custom#source(
    \ 'file/rec', 'matchers', ['matcher/fuzzy'])
    "}}}
    " Change sorters.{{{
    call denite#custom#source(
    \ 'file/rec', 'sorters', ['sorter/sublime'])
    "}}}
    " use rg for file/rec command.{{{
    call denite#custom#var('file/rec', 'command',
          \ ['rg', '--files', '--glob', '!.git', '--glob', '!node_modules'])
    "}}}
    " Ripgrep command on grep source{{{
    call denite#custom#var('grep', 'command', ['rg', '--glob', '!.git', '--glob', '!node_modules'])
    call denite#custom#var('grep', 'default_opts', ['-i', '--vimgrep', '--no-heading'])
    call denite#custom#var('grep', 'recursive_opts', [])
    call denite#custom#var('grep', 'pattern_opt', ['--regexp'])
    call denite#custom#var('grep', 'separator', ['--'])
    call denite#custom#var('grep', 'final_opts', [])
    "}}}
    " Customize filters{{{
    call denite#custom#filter('matcher_ignore_globs', 'ignore_globs',
          \ [ '.git/', '.ropeproject/', '__pycache__/',
          \   'venv/', 'images/', '*.min.*', 'img/', 'fonts/', 'node_modules'])
    " }}}
    " denite keymaps {{{
    nnoremap <silent> <space>b :Denite -no-empty -buffer-name=buffers buffer<cr>
    nnoremap <silent> <space>c :Denite -no-empty -buffer-name=colorschemes colorscheme<cr>
    nnoremap <silent> <space>f :Denite -no-empty -buffer-name=files file/rec<cr>
    nnoremap <silent> <space>h :Denite -no-empty -buffer-name=help help<cr>
    nnoremap <silent> <space>j :Denite -no-empty -buffer-name=junkfile junkfile<cr>
    nnoremap <silent> <space>k :Denite -no-empty -buffer-name=search grep:.<cr>
    nnoremap <silent> <space>o :Denite -no-empty -buffer-name=outline outline<cr>
    nnoremap <silent> <space>t :Denite -no-empty -buffer-name=tags tag<cr>
    nnoremap <silent> <space>q :Denite -no-empty -buffer-name=quickfix quickfix<cr>
    " }}}
  catch
  endtry
end

