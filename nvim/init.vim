let g:polyglot_disabled = ['go', 'clojure', 'haskell', 'elm']

" Use a different shell {{{
if &shell =~# 'fish$'
  set shell=/bin/bash
endif
"}}}

let mapleader="ç"

" Automatic installation of vim-plug {{{
if empty(glob(expand('$HOME/.vim/autoload')))
  call mkdir(expand('$HOME/.vim/autoload'), 'p')
endif

if has('nvim')
  if empty(glob(expand('$HOME/.local/share/nvim/site/autoload/plug.vim')))
    silent !curl -fLo $HOME/.local/share/nvim/site/autoload/plug.vim --create-dirs
          \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif
else
  if empty(glob(expand('$HOME/.vim/autoload/plug.vim')))
    silent !curl -fLo $HOME/.vim/autoload/plug.vim --create-dirs
          \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif
endif
" }}}

" Utils {{{
function! g:AbsPath(file) abort
  return resolve(expand('$HOME/.config/nvim/' . a:file . '.rc.vim'))
endfunction

function! g:SourceConfigFile(config_file) abort
  let abspath = AbsPath(a:config_file)
  execute 'source' fnameescape(abspath)
endfunction
" }}}

call SourceConfigFile('global_functions')

if has('nvim')
  call SourceConfigFile('neovim')
endif

call SourceConfigFile('plugins')
call SourceConfigFile('base')
call SourceConfigFile('commands')

let custom_configuration_file = expand('$HOME/.vimrc.custom')
if filereadable(custom_configuration_file)
  exe 'source '.custom_configuration_file
endif

