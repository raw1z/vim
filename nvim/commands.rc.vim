" open terminal {{{
function s:open_terminal(command) "{{{
  let fish_path = exepath("fish")
  if len(a:command) == 0
    call termopen(fish_path)
  else
    call termopen(fish_path . ' -i -c "' . a:command . '"')
  endif

  normal A
endfunction "}}}
command! -nargs=* Vterm call s:open_terminal_in_vertically_splitted_window("<args>") "{{{
function! s:open_terminal_in_vertically_splitted_window(command)
  vnew
  call s:open_terminal(a:command)
endfunction "}}}
command! -nargs=* Sterm call s:open_terminal_in_splitted_window("<args>") "{{{
function! s:open_terminal_in_splitted_window(command)
  new
  call s:open_terminal(a:command)
endfunction "}}}
command! -nargs=* Tterm call s:open_terminal_in_new_tab("<args>") "{{{
function! s:open_terminal_in_new_tab(command)
  tabnew
  call s:open_terminal(a:command)
endfunction "}}}
"}}}
" cd into a directory and source given file {{{
function! s:change_directory_and_source(path, filename)
  execute('lcd '.a:path)
  execute('source'.a:filename)
endfunction

if g:IsWindows()
  command! -nargs=1 Xcd call s:change_directory_and_source(<q-args>, "_exrc")
else
  command! -nargs=1 Xcd call s:change_directory_and_source(<q-args>, ".exrc")
endif
" }}}
" allow to open a folder in a new tab {{{
function! g:GotoFolderInNewTab(path)
  execute 'tabnew ' a:path
  execute 'lcd ' a:path
  execute "e ."
endfunction

command! -nargs=1 -complete=dir TabGo call GotoFolderInNewTab(<q-args>)
" }}}
" Grep Improved {{{
fun! s:escape_search(search)
  let s:V = vital#of("vital")
  let s:S = s:V.import("Data.String")

  let sanitizedSearch = s:S.replace(a:search, "\\<", "")
  let sanitizedSearch = s:S.replace(sanitizedSearch, "\\>", "")
  let sanitizedSearch = s:S.chomp(sanitizedSearch)
  let sanitizedSearch = s:S.trim(sanitizedSearch)
  let sanitizedSearch = s:S.replace(sanitizedSearch, " ", '\\ ')

  return sanitizedSearch
endf

function! s:grep(search)
  let sanitizedSearch = s:escape_search(a:search)
  let path = expand('%', 'p')
  exe "Denite -buffer-name=search -input=" . sanitizedSearch . " grep:".path
endfunction

function! s:grep_last_search()
  call s:grep(@/)
endfunction

function! s:grep_current_word()
  let @/ = expand('<cword>')
  call s:grep_last_search()
endfunction

function! GlobalGrep(search)
  let sanitizedSearch = s:escape_search(a:search)
  exe "Denite -buffer-name=search -input=" . sanitizedSearch . " grep:."
endfunction

function! GlobalGrepLastSearch()
  call GlobalGrep(@/)
endfunction

function! GlobalGrepCurrentWord()
  let @/ = expand('<cword>')
  call GlobalGrepLastSearch()
endfunction

function! GetVisuallySelectedText()
  let [lnum1, col1] = getpos("'<")[1:2]
  let [lnum2, col2] = getpos("'>")[1:2]
  let lines = getline(lnum1, lnum2)
  let lines[-1] = lines[-1][: col2 - (&selection == 'inclusive' ? 1 : 2)]
  let lines[0] = lines[0][col1 - 1:]
  return join(lines, "\n")
endfunction

function! GrepVisuallySelectedText(isGlobal)
  let selectedText = GetVisuallySelectedText()
  let sanitizedSearch = s:escape_search(selectedText)
  let @/ = sanitizedSearch
  if a:isGlobal == 0
    call s:grep(sanitizedSearch)
  else
    call GlobalGrep(sanitizedSearch)
  endif
endfunction

command! -nargs=1 Grep :call s:grep(<q-args>)
command! GrepLastSearch :call s:grep_last_search()
command! GrepCurrentWord :call s:grep_current_word()
command! GGrepLastSearch :call GlobalGrepLastSearch()
command! GGrepCurrentWord :call GlobalGrepCurrentWord()
command! -range GrepVisualSelection :call GrepVisuallySelectedText(0)
command! -range GGrepVisualSelection :call GrepVisuallySelectedText(1)

nmap <leader>n :GrepLastSearch<CR>
nmap <leader>f :GrepCurrentWord<CR>
nmap <leader>N :GGrepLastSearch<CR>
nmap <leader>F :GGrepCurrentWord<CR>
vnorem <leader>n :GrepVisualSelection<CR>
vnorem <leader>N :GGrepVisualSelection<CR>

" }}}
" manage vim sessions {{{
function! s:SaveSession(...) "{{{
  let filename = a:1
  if strlen(filename) == 0
    let filename = "./session.vim"
  endif

  let filename = fnamemodify(filename, ":p")

  execute ':mksession! ' filename
endfunction "}}}
function! s:RestoreSession(...) "{{{
  let filename = a:1
  if strlen(filename) == 0
    let filename = "./session.vim"
  endif

  let filename = fnamemodify(filename, ":p")

  execute '%bdelete!'
  let g:loading_session = 1
  execute ':source  ' filename
  let g:loading_session = 0
endfunction "}}}
function! s:AutoRestoreSession() "{{{
  let g:loading_session = 0
  if argc() == 0
    let filename = "./session.vim"
    let filename = fnamemodify(filename, ":p")
    if filereadable(filename)
      call s:RestoreSession(filename)
    endif
  endif
endfunction "}}}
function! s:AutoSaveSession() "{{{
  if exists("v:this_session")
    let filename = v:this_session
    if filereadable(filename)
      call s:SaveSession(filename)
    endif
  endif
endfunction "}}}
function! s:DeleteInactiveBuffers() " {{{
  "From tabpagebuflist() help, get a list of all buffers in all tabs
  let tablist = []
  for i in range(tabpagenr('$'))
    call extend(tablist, tabpagebuflist(i + 1))
  endfor

  "Below originally inspired by Hara Krishna Dara and Keith Roberts
  "http://tech.groups.yahoo.com/group/vim/message/56425
  let nWipeouts = 0
  for i in range(1, bufnr('$'))
    if bufexists(i) && !getbufvar(i,"&mod") && index(tablist, i) == -1
      "bufno exists AND isn't modified AND isn't in the list of buffers open in windows and tabs
      silent exec 'bdelete' i
      let nWipeouts = nWipeouts + 1
    endif
  endfor
endfunction " }}}

command! DeleteInactiveBuffers call s:DeleteInactiveBuffers()
command! -nargs=? -complete=file SaveSession call s:SaveSession(<q-args>)
command! -nargs=? -complete=file RestoreSession call s:RestoreSession(<q-args>)
nmap <leader><leader>r :silent RestoreSession<CR>
autocmd VimLeavePre * silent call s:AutoSaveSession()
autocmd FocusLost * silent call s:AutoSaveSession()
autocmd VimEnter * nested silent call s:AutoRestoreSession()
" }}}
" open current directory in Git Tower {{{
nmap <leader>t :silent !gittower .<CR>
"}}}
" Open current buffer file in new tab {{{
function! s:tab_this()
  let currentBuffer = bufname("%")
  execute 'hide'
  execute 'tabnew ' . currentBuffer
endfunction
command! TabThis call s:tab_this()
"}}}
" Show current folder's git history {{{
function! s:show_git_history(current_branch_only)
  redir => output
  if a:current_branch_only
    execute "!git log develop.. --oneline"
  else
    execute "!git log --oneline"
  endif
  redir END

  tabnew
  setlocal buftype=nofile bufhidden=wipe noswapfile nobuflisted nomodified
  nmap <buffer> q :tabclose <bar> tabprevious<CR>
  silent put =output
  normal gg4dd
endfunction
command! ShowGitHistory call s:show_git_history(0)
command! ShowCurrentBranchGitHistory call s:show_git_history(1)
nmap <leader>h :silent ShowGitHistory<CR>
nmap <leader>H :silent ShowCurrentBranchGitHistory<CR>
" }}}
" Show current buffer git diff {{{
nmap <leader>d :silent Gvdiff<CR>
" }}}
" File explorer commands {{{
" Open current buffer file dirname {{{
function! s:open_current_buffer_file_dirname()
  let s:Vital = vital#of('vital')
  let s:Filepath = s:Vital.import('System.Filepath')
  let filepath = expand('%:p')
  let dirname = s:Filepath.dirname(filepath)
  if len(dirname) > 0
    Defx `expand('%:p:h')` -search=`expand('%:p')`
  else
    Defx `expand('%:p:h')`
  endif
endfunction

fun! s:split_current_buffer_file_dirname()
  split
  call s:open_current_buffer_file_dirname()
endf

fun! s:vsplit_current_buffer_file_dirname()
  vsplit
  call s:open_current_buffer_file_dirname()
endf

fun! s:tabopen_current_buffer_file_dirname()
  tabnew %
  call s:open_current_buffer_file_dirname()
endf

command! OpenCurrentBufferFileDirname call s:open_current_buffer_file_dirname()
command! SOpenCurrentBufferFileDirname call s:split_current_buffer_file_dirname()
command! VOpenCurrentBufferFileDirname call s:vsplit_current_buffer_file_dirname()
command! TOpenCurrentBufferFileDirname call s:tabopen_current_buffer_file_dirname()
nmap -- :OpenCurrentBufferFileDirname<CR>
nmap -s :SOpenCurrentBufferFileDirname<CR>
nmap -v :VOpenCurrentBufferFileDirname<CR>
nmap -t :TOpenCurrentBufferFileDirname<CR>
"}}}
" Open root directory {{{
fun s:open_root_project_directory()
  Defx `projectroot#guess()`
endf

fun! s:split_root_project_directory()
  split
  call s:open_root_project_directory()
endf

fun! s:vsplit_root_project_directory()
  vsplit
  call s:open_root_project_directory()
endf

fun! s:tabopen_root_project_directory()
  tabnew %
  call s:open_root_project_directory()
endf

command! OpenRootDirectory call s:open_root_project_directory()
command! SOpenRootDirectory call s:split_root_project_directory()
command! VOpenRootDirectory call s:vsplit_root_project_directory()
command! TOpenRootDirectory call s:tabopen_root_project_directory()
nmap <leader>-- :OpenRootDirectory<CR>
nmap <leader>-s :SOpenRootDirectory<CR>
nmap <leader>-v :VOpenRootDirectory<CR>
nmap <leader>-t :TOpenRootDirectory<CR>
"}}}
" }}}
" open a draft buffer with the given filetype {{{
function! g:StartDraftBuffer(...)
  " if the filetype isnt set then
  " use current buffer's filetype
  let draft_filetype = &ft
  if len(a:1) > 0
    let draft_filetype = a:1
  endif

  vnew
  nmap <buffer> <c-o> :echo "noop"<cr>
  nmap <buffer> <c-i> :echo "noop"<cr>

  exe 'set ft='.draft_filetype
  setlocal buftype=nofile bufhidden=wipe noswapfile nobuflisted nomodified
endfunction
command! -nargs=? Draft call g:StartDraftBuffer(<q-args>)
" }}}
" colorscheme management {{{
function! s:customize_airline_colors() "{{{
  let g:airline#themes#custom#palette = {}
  let s:gui00 = color#Darken(g:terminal_color_background, '0.1')
  let s:gui04 = g:terminal_color_8
  let s:gui05 = g:terminal_color_15
  let s:gui08 = g:terminal_color_4 " replace
  let s:gui0A = g:terminal_color_7
  let s:gui0B = g:terminal_color_7 " normal
  let s:gui0C = color#Lighten(g:terminal_color_background, '0.1')
  let s:gui0D = g:terminal_color_2 " insert
  let s:gui0E = g:terminal_color_3 " visual

  " Terminal color definitions
  let s:cterm00 = 00
  let s:cterm03 = 08
  let s:cterm05 = 07
  let s:cterm07 = 15
  let s:cterm08 = 01
  let s:cterm0A = 03
  let s:cterm0B = 02
  let s:cterm0C = 06
  let s:cterm0D = 04
  let s:cterm0E = 05
  if exists('base16colorspace') && base16colorspace == "256"
    let s:cterm01 = 18
    let s:cterm02 = 19
    let s:cterm04 = 20
    let s:cterm06 = 21
    let s:cterm09 = 16
    let s:cterm0F = 17
  else
    let s:cterm01 = 10
    let s:cterm02 = 11
    let s:cterm04 = 12
    let s:cterm06 = 13
    let s:cterm09 = 09
    let s:cterm0F = 14
  endif

  let s:N1   = [ s:gui0B, s:gui00, s:cterm0B, s:cterm0B]
  let s:N2   = [ s:gui0C, s:gui00, s:cterm02, s:cterm02]
  let s:N3   = [ s:gui05, s:gui00, s:cterm01, s:cterm01]
  let g:airline#themes#custom#palette.normal = airline#themes#generate_color_map(s:N1, s:N2, s:N3)

  let s:I1   = [ s:gui0D, s:gui00, s:cterm0D, 'NONE' ]
  let s:I2   = [ s:gui0C, s:gui00, s:cterm02, 'NONE' ]
  let s:I3   = [ s:gui05, s:gui00, s:cterm01, 'NONE' ]
  let g:airline#themes#custom#palette.insert = airline#themes#generate_color_map(s:I1, s:I2, s:I3)

  let s:R1   = [ s:gui08, s:gui00, s:cterm08, 'NONE' ]
  let s:R2   = [ s:gui0C, s:gui00, s:cterm02, 'NONE' ]
  let s:R3   = [ s:gui05, s:gui00, s:cterm01, 'NONE' ]
  let g:airline#themes#custom#palette.replace = airline#themes#generate_color_map(s:R1, s:R2, s:R3)

  let s:V1   = [ s:gui0E, s:gui00, s:cterm0E, 'NONE' ]
  let s:V2   = [ s:gui0C, s:gui00, s:cterm02, 'NONE' ]
  let s:V3   = [ s:gui05, s:gui00, s:cterm01, 'NONE' ]
  let g:airline#themes#custom#palette.visual = airline#themes#generate_color_map(s:V1, s:V2, s:V3)

  let s:IA1   = [ s:gui04, s:gui00, s:cterm01, 'NONE' ]
  let s:IA2   = [ s:gui04, s:gui00, s:cterm01, 'NONE' ]
  let s:IA3   = [ s:gui04, s:gui00, s:cterm01, 'NONE' ]
  let g:airline#themes#custom#palette.inactive = airline#themes#generate_color_map(s:IA1, s:IA2, s:IA3)

  " Warning Section
  let g:airline#themes#custom#palette.normal.airline_warning = [
        \ s:gui0A, s:gui00, s:cterm0A, 'NONE'
        \ ]

  let g:airline#themes#custom#palette.insert.airline_warning =
        \ g:airline#themes#custom#palette.normal.airline_warning

  let g:airline#themes#custom#palette.visual.airline_warning =
        \ g:airline#themes#custom#palette.normal.airline_warning

  let g:airline#themes#custom#palette.replace.airline_warning =
        \ g:airline#themes#custom#palette.normal.airline_warning

  " Error Section
  let g:airline#themes#custom#palette.normal.airline_error = [
        \ s:gui08, s:gui00, s:cterm08, 'NONE'
        \ ]

  let g:airline#themes#custom#palette.insert.airline_error =
        \ g:airline#themes#custom#palette.normal.airline_error

  let g:airline#themes#custom#palette.visual.airline_error =
        \ g:airline#themes#custom#palette.normal.airline_error

  let g:airline#themes#custom#palette.replace.airline_error =
        \ g:airline#themes#custom#palette.normal.airline_error

  call airline#load_theme()
endfunction
"}}}
function! s:customize_colors() abort "{{{
  if exists('g:terminal_color_background')
    " coc.nvim color changes
    hi link CocErrorSign WarningMsg
    hi link CocWarningSign Number
    hi link CocInfoSign Type

    " Make background transparent for many things
    exec "hi Normal ctermbg=NONE guibg=" . g:terminal_color_background
    exec "hi NonText ctermbg=NONE guibg=" . g:terminal_color_background
    exec "hi LineNr ctermfg=NONE guibg=" . g:terminal_color_background
    exec "hi FoldColumn ctermfg=NONE guibg=" . g:terminal_color_background
    exec "hi Folded ctermfg=NONE guibg=" . g:terminal_color_background
    exec "hi SignColumn ctermfg=NONE guibg=" . g:terminal_color_background
    exec "hi StatusLineNC guibg=" . g:terminal_color_background
    exec "hi VertSplit guibg=" . g:terminal_color_background
    exec "hi TabLineSel guibg=" . g:terminal_color_background
    exec "hi TabLine guibg=" . color#Darken(g:terminal_color_background, '0.1')
    exec "hi TabLineFill guibg=" . color#Darken(g:terminal_color_background, '0.1')

    " Make background color transparent for git changes
    exec "hi SignifySignAdd guibg=" . g:terminal_color_background
    exec "hi SignifySignDelete guibg=" . g:terminal_color_background
    exec "hi SignifySignChange guibg=" . g:terminal_color_background

    call s:customize_airline_colors()
  endif
endfunction
"}}}
function! s:update_colorscheme() "{{{
  try
    let colors_file = expand('~/.vimrc_background')
    if filereadable(colors_file)
      exec "source " . colors_file
      call s:customize_colors()
    else
      colorscheme darkblue
    endif
  catch 
    colorscheme darkblue
  endtry
endfunction
"}}}

autocmd VimEnter * call s:update_colorscheme()
autocmd FocusGained * call s:update_colorscheme()
autocmd ColorScheme * call s:customize_colors()

" }}}
" load output of a command in a new tab{{{
function! ShowCommandOutput(cmd)
  redir => message
  silent execute a:cmd
  redir END
  if empty(message)
    echoerr "no output"
  else
    tabnew
    setlocal buftype=nofile bufhidden=wipe noswapfile nobuflisted nomodified
    silent put=message
  endif
endfunction
command! -nargs=+ -complete=command ShowCommandOutput call ShowCommandOutput(<q-args>)
"}}}
" display the list of keymaps {{{
function! ListKeymaps()
  call ShowCommandOutput('silent verbose map')
endfunction
command! ListKeymaps call ListKeymaps()
"}}}
" edit fish aliases file {{{
function! FishAliases()
  let s:V = vital#of("vital")
  let s:F = s:V.import("System.Filepath")
  exe 'tabnew ' . s:F.join(getcwd(), '.aliases.fish')
endfunction
command! FishAliases call FishAliases()
" }}}
" Copy current buffer file path {{{
function! s:copy_this_path()
  let @+ = expand("%:p")
endfunction
command! CopyThisPath call s:copy_this_path()
" }}}
" Open current file in macdown {{{
function! s:open_in_macdown()
  exec "silent !open -a MacDown %"
endfunction
command! MarkdownPreview call s:open_in_macdown()
" }}}
