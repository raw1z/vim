call plug#begin('$HOME/.vim/bundle')

" misc {{{
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-bundler'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-jdaddy'
Plug 'tpope/vim-projectionist'
Plug 'tpope/vim-rails'
Plug 'tpope/vim-rake'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-rsi'
Plug 'tpope/vim-scriptease'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-abolish'

Plug 'vim-jp/vital.vim'
Plug 'bronson/vim-trailing-whitespace'
Plug 'godlygeek/tabular', { 'on': ['Tabularize'] }
Plug 'AndrewRadev/linediff.vim', { 'on': ['Linediff'] }
Plug 'easymotion/vim-easymotion', { 'on': ['<Plug>(easymotion-s2)'] }
Plug 'mhinz/vim-signify'
Plug 'mattn/emmet-vim'
Plug 'ludovicchabant/vim-gutentags'
Plug 'neomake/neomake'
Plug 'dietsche/vim-lastplace'
Plug 'AndrewRadev/splitjoin.vim', {'on': ['SplitjoinSplit', 'SplitjoinJoin']}
Plug 'mbbill/undotree', { 'on': ['UndotreeToggle'] }
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'raw1z/vim-repl', { 'on': ['OpenREPL', 'SendToREPL', 'SendVisualSelectionToREPL'] }
Plug 'slashmili/alchemist.vim', { 'for': 'elixir' }
Plug 'junegunn/gv.vim', { 'on': ['GV!'] }
Plug 'tyru/open-browser.vim', { 'on': ['<Plug>(openbrowser-smart-search)'] }
Plug 'ervandew/supertab'
Plug 'nixprime/cpsm'
Plug 'obreitwi/vim-sort-folds'
Plug 'wsdjeg/vim-fetch'
Plug 'vim-scripts/AnsiEsc.vim'
Plug 'mattn/webapi-vim'
Plug 'tmhedberg/SimpylFold'
Plug 'jiangmiao/auto-pairs'
Plug 'machakann/vim-highlightedyank'
Plug 'cohama/agit.vim'
Plug 'pechorin/any-jump.vim', { 'on': ['AnyJump'] }
Plug 'psf/black', { 'branch': 'stable', 'for': 'python' }
Plug 'ruby-formatter/rufo-vim', { 'for': 'ruby' }
Plug 'chrisbra/Colorizer'
Plug 'romgrk/lib.kom'
Plug 'ryanoasis/vim-devicons'
Plug 'kristijanhusak/defx-icons'
Plug 'dbakker/vim-projectroot'
" }}}
" vim text objects {{{
Plug 'kana/vim-textobj-user'
  Plug 'michaeljsmith/vim-indent-object'
  Plug 'kana/vim-textobj-fold'
  Plug 'rhysd/vim-textobj-anyblock'
  Plug 'thinca/vim-textobj-between'
  Plug 'vimtaku/vim-textobj-keyvalue'
  Plug 'coderifous/textobj-word-column.vim'
" }}}
" vim operators {{{
Plug 'kana/vim-operator-user'
  Plug 'kana/vim-operator-replace'
  Plug 'tyru/operator-camelize.vim'
  Plug 'rhysd/vim-operator-surround'
" }}}
" colorschemes {{{
Plug 'chriskempson/base16-vim'
" }}}
" languages {{{
Plug 'fatih/vim-go'
Plug 'neovimhaskell/haskell-vim'
" Plug 'alx741/vim-hindent'
Plug 'sheerun/vim-polyglot'
Plug 'vim-jp/syntax-vim-ex'
Plug 'simpsonjulian/cypher-vim-syntax'
Plug 'dag/vim-fish'
Plug 'guns/vim-sexp'
Plug 'tpope/vim-sexp-mappings-for-regular-people'
Plug 'tpope/vim-fireplace'
Plug 'tpope/vim-salve'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'ElmCast/elm-vim'
" }}}
" nvim only {{{
if has('nvim')
  Plug 'Shougo/defx.nvim', { 'do': ':UpdateRemotePlugins' }
  Plug 'Shougo/denite.nvim', { 'do': function('UpdateRPlugin') }
    Plug 'Shougo/junkfile.vim'
    Plug 'chemzqm/denite-extra'

  Plug 'SirVer/ultisnips'
    Plug 'honza/vim-snippets'

  Plug 'janko-m/vim-test'
  Plug 'mhinz/vim-grepper', { 'on': ['Grepper', '<plug>(GrepperOperator)'] }
endif
" }}}

call plug#end()
