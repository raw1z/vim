" sensible defaults {{{
set nu
set noshowmode
set noshowcmd
set noruler
set nowrap
set tabstop=2 shiftwidth=2 expandtab autoindent
set ignorecase smartcase
set showcmd
set foldcolumn=3
set splitright
set updatetime=300
set backupcopy=yes

if has("nvim")
  set wildignorecase
endif

set exrc

if has("persistent_undo")
  set undodir=$HOME/.undodir/
  set undofile
endif

set viewoptions=cursor,folds
set shortmess=acoOt
set title
set diffopt=vertical,filler

" Some files we just don't want to see here
set wildignore+=*.o,*.obj,.git,*.rbc,*.class,.svn,tmp/*,*.so,*.swp,*.zip " general stuff
set wildignore+=*.png,*.jpg,*.gif,*.jpeg " images
set wildignore+=*/vendor/gems/*,*/vendor/cache/*,Gemfile.lock,*/log/*,.sass-cache,*/coverage/* " Ruby related files
set wildignore+=*.pyc,*.pyo " Python
set wildignore+=*/node_modules/****************" " NodeJS

" configure mksession
set sessionoptions=blank,curdir,folds,globals,tabpages,winsize

" extend matchpairs
set matchpairs+=<:>
" }}}
" vim default keys remap {{{
map <C-L> <C-]>
inoremap kj <Esc>
cnoremap kj <C-c>
cmap <C-P> <Up>
cmap <C-N> <Down>
" }}}
" jump to next nearest closed fold {{{
function! s:goto_next_closed_fold(dir)
    let cmd = 'norm!z' . a:dir
    let view = winsaveview()
    let [l0, l, open] = [0, view.lnum, 1]
    while l != l0 && open
        exe cmd
        let [l0, l] = [l, line('.')]
        let open = foldclosed(l) < 0
    endwhile
    if open
        call winrestview(view)
    endif
endfunction

nnoremap <silent> <leader>zj :call s:goto_next_closed_fold('j')<cr>
nnoremap <silent> <leader>zk :call s:goto_next_closed_fold('k')<cr>

" }}}
" disable search highlights {{{
nmap <silent> <leader>/ :nohl<CR>
" }}}
" define shortcuts for inserting backslash, tilde, brackets and square brackets, lt and gt {{{
imap <leader>) ]
imap <leader>( [
imap <leader><leader>) }
imap <leader><leader>( {
imap <leader>n ~
imap <leader>/ \
imap <leader>! \|
imap <leader>' `
imap <leader>- ^

cmap <leader>) ]
cmap <leader>( [
cmap <leader><leader>) }
cmap <leader><leader>( {
cmap <leader>n ~
cmap <leader>/ \
cmap <leader>! \|
cmap <leader>' `
cmap <leader>- ^

map <leader>q >
map <leader><leader>q <
map! <leader>q >
map! <leader><leader>q <
tmap <leader>q >
tmap <leader><leader>q <
" }}}
" auto source exrc files {{{
if has("autocmd")
  augroup ConfigChangeDetect
    autocmd BufWritePost .vimrc.custom source %
    autocmd BufWritePost [_.]exrc,exrc source %
  augroup END
endif
" }}}
" keyboard map for editing the configuration files {{{
function! s:goto_vim_folder()
  let s:Vital = vital#of('vital')
  let s:Filepath = s:Vital.import('System.Filepath')
  let dirname = s:Filepath.dirname($MYVIMRC)
  call GotoFolderInNewTab(dirname)
  e $MYVIMRC
endfunction
nmap <leader>vd :call <SID>goto_vim_folder()<CR>
nmap <leader>vv :tabnew $MYVIMRC<CR>
nmap <leader>vc :tabnew $HOME/.vimrc.custom<CR>

if IsWindows()
  nmap <leader>ve :tabnew _exrc<CR>
else
  nmap <leader>ve :tabnew .exrc<CR>
endif
" }}}
" keyboard map for toggling wrap mode {{{
nmap <leader>W :set wrap! linebreak nolist<CR>
nmap <leader><space> :only<CR>
" }}}
" mapping to make movements operate on 1 screen line in wrap mode {{{
:map j gj
:map k gk
" }}}
" diff mode keymap {{{
function! MyDiffput(visualModeEnabled)
  if a:visualModeEnabled == 0
    exe ':diffput'
  else
    exe ":'<,'>diffput"
  endif

  exe ':diffupdate'
  call feedkeys(']c')
endfunction

function! MyDiffget(visualModeEnabled)
  if a:visualModeEnabled == 0
    exe ':diffget'
  else
    exe ":'<,'>diffget"
  endif

  exe ':diffupdate'
  call feedkeys(']c')
endfunction

function! EnableDiffKeyMaps()
  if &diff
    nmap <buffer> <leader>dj ]c
    nmap <buffer> <leader>dk [c
    nmap <buffer> J ]c
    nmap <buffer> K [c
    nmap <buffer> <silent> <leader>dp :call MyDiffput(0)<CR>
    nmap <buffer> <silent> <leader>dg :call MyDiffget(0)<CR>
    nmap <buffer> <silent> <leader>du :diffupdate<CR>
    vmap <buffer> <silent> <leader>dp :call MyDiffput(1)<CR>
    vmap <buffer> <silent> <leader>dg :call MyDiffget(1)<CR>
  endif
endfunction

augroup DiffModeMaps
  autocmd! FilterWritePre * call EnableDiffKeyMaps()
augroup END
"}}}
" easily moves between opened windows {{{
nmap <leader><leader>h <C-w>h
nmap <leader><leader>j <C-w>j
nmap <leader><leader>k <C-w>k
nmap <leader><leader>l <C-w>l
tmap <leader><leader>h <C-o><C-w>h
tmap <leader><leader>j <C-o><C-w>j
tmap <leader><leader>k <C-o><C-w>k
tmap <leader><leader>l <C-o><C-w>l
nmap <C-j> gT
nmap <C-k> gt
nmap <space><space> <C-w><C-w>
"}}}
" easily resize and split windows {{{
nmap <Up> <C-w>+
nmap <Down> <C-w>-
nmap <Left> <C-w><
nmap <Right> <C-w>>
nmap <leader>= <C-w>=
nmap <C-t> :tabnew<CR>
nmap <space>s :split<CR>
nmap <space>v :vsplit<CR>
" }}}
" easy save and quit {{{
nmap <silent> <leader>s :update<CR>
nmap <silent> <leader>x :x<CR>
"}}}
" ensure autoread when an opened file has been changed outside vim {{{
if has("autocmd")
  augroup ChangeDetect
    au FocusGained,BufEnter * :silent! !
  augroup END
endif
"}}}
" file type specific configuration {{{
" deface support {{{
augroup filetypedetect
  au BufNewFile,BufRead *.html.erb.deface set ft=eruby
augroup END
" }}}
" SASS/SCSS better word detection {{{
autocmd BufNewFile,BufRead *.sass setl iskeyword+=#,-
autocmd BufNewFile,BufRead *.scss setl iskeyword+=#,-
" }}}
" ruby better keyword detection {{{
autocmd BufNewFile,BufRead *.rb setl iskeyword+=?,!,@,$
autocmd BufNewFile,BufRead *.rbx setl iskeyword+=?,!,@,$ "}}}
autocmd BufNewFile,BufRead *.exs set ft=elixir
autocmd BufNewFile,BufRead .zshrc set ft=zsh
autocmd BufNewFile,BufRead *.eex set ft=eelixir
autocmd BufNewFile,BufRead *.kubeconfig set ft=yaml
"}}}
" enter terminal pane in insert mode {{{
function! s:start_insert()
  if exists('g:loading_session') && g:loading_session == 0
    startinsert!
    redraw
  endif
endfunction

function! s:stop_insert()
  if exists('g:loading_session') && g:loading_session == 0
    stopinsert
    redraw
  endif
endfunction

autocmd BufEnter term://* call s:start_insert()
autocmd BufLeave term://* call s:stop_insert()

"}}}
" select last pasted text {{{
nnoremap <expr> gp '`[' . strpart(getregtype(), 0, 1) . '`]'
" }}}
" customize folds appearance{{{
set foldtext=FoldText()
function! FoldText()
  let l:lpadding = &fdc
  redir => l:signs
    execute 'silent sign place buffer='.bufnr('%')
  redir End
  let l:lpadding += l:signs =~ 'id=' ? 2 : 0

  if exists("+relativenumber")
    if (&number)
      let l:lpadding += max([&numberwidth, strlen(line('$'))]) + 1
    elseif (&relativenumber)
      let l:lpadding += max([&numberwidth, strlen(v:foldstart - line('w0')), strlen(line('w$') - v:foldstart), strlen(v:foldstart)]) + 1
    endif
  else
    if (&number)
      let l:lpadding += max([&numberwidth, strlen(line('$'))]) + 1
    endif
  endif

  let s:Vital = vital#of('vital')
  let s:String = s:Vital.import('Data.String')

  let l:start = getline(v:foldstart)
  let l:commentstring = split(&commentstring, '%s')[0]
  let l:start = substitute(l:start, l:commentstring, '', 'g')
  if &foldmethod == "marker"
    let l:start = s:String.truncate(l:start, strlen(l:start) - 3)
    let l:start = s:String.trim(l:start)
  endif
  let l:start = substitute(l:start, '\t', repeat(' ', &tabstop), 'g')

  let l:line_count = string(v:foldend - v:foldstart)
  let l:padded_line_count = s:String.pad_right(l:line_count, 3)

  let l:info = '===x' . l:padded_line_count
  let l:infolen = strlen(substitute(l:info, '.', 'x', 'g'))
  let l:width = winwidth(0) - l:lpadding - l:infolen + 1

  let l:separator = '…'
  let l:separatorlen = strlen(substitute(l:separator, '.', 'x', 'g'))
  let l:start = strpart(l:start , 0, l:width - l:separatorlen)
  let l:text = l:start . l:separator

  return l:text . repeat(' ', l:width - strlen(substitute(l:text, ".", "x", "g"))) . l:info
endfunction
"}}}
" keymap for copying directly to system clipboard {{{
nmap Y "+yy
vmap Y "+y
" }}}
" keymap for inserting new lines without leaving normal mode {{{
nnoremap <Leader>o o<Esc>
nnoremap <Leader>O O<Esc>
"}}}
