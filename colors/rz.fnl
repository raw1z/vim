(require-macros :macros.core)
(require-macros :macros.colors)

(define-highlight-context base
  ╔════════════════╤════════════╤════════════╤═══════════╗
  ║ Normal         │ fg/8       │ bg/8       │ -         ║
  ║ Cursor         │ bg/8       │ magenta/8  │ -         ║
  ║ ICursor        │ -          │ yellow/5   │ -         ║
  ║ RCursor        │ -          │ green/5    │ -         ║
  ║ CursorLine     │ -          │ bg/7       │ -         ║
  ║ LineNr         │ bg/1|fg/15 │ -          │ -         ║
  ║ CursorLineNr   │ blue/8     │ -          │ -         ║
  ║ CursorLineSign │ -          │ -          │ -         ║
  ║ WinSeparator   │ bg/8       │ -          │ -         ║
  ║ FoldColumn     │ bg/1|fg/15 │ -          │ -         ║
  ║ Folded         │ bg/1|fg/15 │ -          │ italic    ║
  ║ MatchParen     │ -          │ -          │ underline ║
  ║ Visual         │ -          │ bg/5       │ -         ║
  ║ VisualNOS      │ -          │ Visual     │ -         ║
  ║ WildMenu       │ yellow/5   │ -          │ -         ║
  ║ SignColumn     │ -          │ bg/8       │ -         ║
  ║ ColorColumn    │ -          │ bg/8       │ -         ║
  ║ CursorColumn   │ -          │ bg/7       │ -         ║
  ║ QuickFixLine   │ -          │ bg/3       │ -         ║
  ║ Search         │ bg/8       │ blue/8     │ -         ║
  ║ IncSearch      │ bg/8       │ cyan/8     │ -         ║
  ║ Susbstitute    │ bg/8       │ cyan/5     │ -         ║
  ║ Directory      │ blue/5     │ -          │ bold      ║
  ║ ErrorMsg       │ red/5      │ -          │ -         ║
  ║ WarningMsg     │ yellow/8   │ -          │ -         ║
  ║ Question       │ bg/1|fg/15 │ -          │ italic    ║
  ║ SpecialKey     │ bg/1|fg/15 │ -          │ italic    ║
  ║ Title          │ magenta/8  │ -          │ -         ║
  ║ Macro          │ yellow/8   │ -          │ -         ║
  ╚════════════════╧════════════╧════════════╧═══════════╝
  )

(define-highlight-context standard
  ╔══════════════╤════════════╤═══╤═══════════╗
  ║ Bold         │ -          │ - │ bold      ║
  ║ Boolean      │ cyan/8     │ - │ -         ║
  ║ Character    │ red/8      │ - │ -         ║
  ║ Comment      │ bg/1|fg/15 │ - │ italic    ║
  ║ Conditional  │ magenta/8  │ - │ -         ║
  ║ Constant     │ blue/9     │ - │ bold      ║
  ║ Define       │ magenta/8  │ - │ -         ║
  ║ Delimiter    │ Comment    │ - │ -         ║
  ║ Float        │ yellow/8   │ - │ -         ║
  ║ Function     │ cyan/1     │ - │ -         ║
  ║ Identifier   │ blue/15    │ - │ -         ║
  ║ Include      │ blue/8     │ - │ -         ║
  ║ Italic       │ -          │ - │ italic    ║
  ║ Keyword      │ magenta/8  │ - │ -         ║
  ║ Label        │ yellow/10  │ - │ -         ║
  ║ Number       │ yellow/8   │ - │ -         ║
  ║ Operator     │ Comment    │ - │ -         ║
  ║ PreProc      │ yellow/11  │ - │ -         ║
  ║ Repeat       │ yellow/8   │ - │ -         ║
  ║ Special      │ cyan/8     │ - │ -         ║
  ║ SpecialChar  │ yellow/8   │ - │ -         ║
  ║ Statement    │ red/8      │ - │ -         ║
  ║ StorageClass │ yellow/8   │ - │ -         ║
  ║ String       │ green/8    │ - │ -         ║
  ║ Structure    │ Constant   │ - │ -         ║
  ║ Tag          │ yellow/8   │ - │ -         ║
  ║ Todo         │ yellow/8   │ - │ -         ║
  ║ Type         │ cyan/8     │ - │ -         ║
  ║ Underline    │ -          │ - │ underline ║
  ║ Variable     │ blue/15    │ - │ -         ║
  ╚══════════════╧════════════╧═══╧═══════════╝
  )

(define-highlight-context treesitter
  ╔═══════════════════════╤═════════════╤════════════╤═══════════╗
  ║ TSAnnotation          │ blue/8      │ -          │ -         ║
  ║ TSAttribute           │ cyan/8      │ -          │ -         ║
  ║ TSBoolean             │ Boolean     │ Boolean    │ -         ║
  ║ TSCharacter           │ yellow/5    │ -          │ -         ║
  ║ TSComment             │ Comment     │ -          │ italic    ║
  ║ TSConditional         │ Conditional │ -          │ -         ║
  ║ TSConstBuiltin        │ red/8       │ -          │ -         ║
  ║ TSConstMacro          │ red/8       │ -          │ -         ║
  ║ TSConstant            │ Constant    │ Constant   │ -         ║
  ║ TSConstructor         │ blue/10     │ -          │ -         ║
  ║ TSCurrentScope        │ -           │ -          │ -         ║
  ║ TSDefinition          │ bg/8        │ yellow/8   │ -         ║
  ║ TSDefinitionUsage     │ bg/8        │ yellow/8   │ -         ║
  ║ TSEmphasis            │ yellow/10   │ -          │ -         ║
  ║ TSError               │ red/8       │ -          │ -         ║
  ║ TSException           │ red/5       │ -          │ -         ║
  ║ TSField               │ magenta/8   │ -          │ -         ║
  ║ TSFloat               │ Float       │ -          │ -         ║
  ║ TSFuncBuiltin         │ cyan/8      │ -          │ -         ║
  ║ TSFuncMacro           │ magenta/8   │ -          │ -         ║
  ║ TSFunction            │ Function    │ -          │ -         ║
  ║ TSFunctionBuiltin     │ green/10    │ -          │ -         ║
  ║ TSIdentifier          │ Identifier  │ -          │ -         ║
  ║ TSInclude             │ Include     │ Include    │ -         ║
  ║ TSKeyword             │ Keyword     │ Keyword    │ -         ║
  ║ TSKeywordFunction     │ cyan/5      │ -          │ -         ║
  ║ TSLabel               │ Label       │ Label      │ -         ║
  ║ TSLiteral             │ yellow/3    │ -          │ -         ║
  ║ TSMethod              │ Function    │ Function   │ -         ║
  ║ TSNameSpace           │ Function    │ Function   │ -         ║
  ║ TSNumber              │ Number      │ Number     │ -         ║
  ║ TSOperator            │ Number      │ Number     │ -         ║
  ║ TSParameter           │ yellow/2    │ -          │ -         ║
  ║ TSParameterReference  │ green/3     │ -          │ -         ║
  ║ TSProperty            │ cyan/5      │ -          │ -         ║
  ║ TSPunctBracket        │ blue/10     │ -          │ bold      ║
  ║ TSPunctDelimiter      │ Delimiter   │ Delimiter  │ -         ║
  ║ TSPunctSpectial       │ red/1       │ -          │ -         ║
  ║ TSRepeat              │ Repeat      │ Repeat     │ -         ║
  ║ TSString              │ String      │ String     │ -         ║
  ║ TSStringEscape        │ green/3     │ -          │ -         ║
  ║ TSStringRegex         │ green/5     │ -          │ -         ║
  ║ TSStrong              │ yellow/5    │ -          │ -         ║
  ║ TSStructure           │ Structure   │ Structure  │ -         ║
  ║ TSTag                 │ Tag         │ Tag        │ -         ║
  ║ TSTagDelimiter        │ Delimiter   │ Delimiter  │ -         ║
  ║ TSTitle               │ SpecialKey  │ SpecialKey │ -         ║
  ║ TSType                │ cyan/10     │ -          │ bold      ║
  ║ TSTypeBuiltin         │ yellow/5    │ -          │ bold      ║
  ║ TSURI                 │ green/3     │ -          │ italic    ║
  ║ TSUnderline           │ -           │ -          │ undercurl ║
  ║ TSVariable            │ Identifier  │ -          │ -         ║
  ║ TSVariableBuiltin     │ yellow/8    │ -          │ -         ║
  ║ __symbol              │ yellow/12   │ -          │ -         ║
  ║ __variable            │ fg/8        │ -          │ -         ║
  ║ __keyword.repeat      │ yellow/8    │ -          │ -         ║
  ║ __keyword.conditional │ Conditional │ -          │ -         ║
  ╚═══════════════════════╧═════════════╧════════════╧═══════════╝
  )

(define-highlight-context treesitter-ruby
  ╔═══════════════════════════╤════════════╤════════════╤═══════════╗
  ║ __function.ruby           │ red/1      │ -          │ bold      ║
  ║ __variable.parameter.ruby │ blue/8     │ -          │ -         ║
  ╚═══════════════════════════╧════════════╧════════════╧═══════════╝
  )

(define-highlight-context treesitter-go
  ╔═══════════════════════════╤════════════╤════════════╤═══════════╗
  ║ __function.go             │ red/1      │ -          │ bold      ║
  ║ __module.go               │ Constant   │ -          │ bold      ║
  ║ __function.method.go      │ red/1      │ -          │ bold      ║
  ╚═══════════════════════════╧════════════╧════════════╧═══════════╝
  )

(define-highlight-context treesitter-fennel
  ╔═══════════════════════════╤════════════╤════════════╤═══════════╗
  ║ __function.fennel         │ red/1      │ -          │ bold      ║
  ╚═══════════════════════════╧════════════╧════════════╧═══════════╝
  )

(define-highlight-context treesitter-custom
  ╔══════════════════════════════╤═══════════════════╤═══╤══════╗
  ║ __AniseedDefine              │ TSKeyword         │ - │ -    ║
  ║ __AniseedImportAlias         │ TSVariableBuiltin │ - │ -    ║
  ║ __AniseedImportLib           │ TSVariable        │ - │ -    ║
  ║ __AniseedModule              │ TSKeyword         │ - │ -    ║
  ║ __AniseedModuleAutoload      │ Macro             │ - │ -    ║
  ║ __AniseedModuleMacro         │ TSVariableBuiltin │ - │ -    ║
  ║ __AniseedModuleName          │ TSFunction        │ - │ -    ║
  ║ __AniseedModuleRequireMacros │ Macro             │ - │ -    ║
  ║ __RZBeancountAssets          │ magenta/15        │ - │ -    ║
  ║ __RZBeancountDate            │ Macro             │ - │ -    ║
  ║ __RZBeancountExpenses        │ red/8             │ - │ -    ║
  ║ __RZBeancountIncome          │ green/13          │ - │ -    ║
  ║ __RZBeancountLiabilities     │ magenta/8         │ - │ -    ║
  ║ __RZBeancountNarration       │ Comment           │ - │ -    ║
  ║ __RZBeancountPayee           │ blue/8            │ - │ -    ║
  ║ __RZColorsSpecBorder         │ bg/3|fg/1         │ - │ -    ║
  ║ __RZColorsSpecGroup          │ blue/8            │ - │ -    ║
  ║ __RZColorsSpecGroupRef       │ cyan/8            │ - │ -    ║
  ║ __RZColorsSpecGui            │ yellow/8          │ - │ -    ║
  ║ __RZFennelInterpolation      │ red/8             │ - │ -    ║
  ║ __RZFennelKeymap             │ cyan/8            │ - │ -    ║
  ║ __RZFennelPlugName           │ TSFunction        │ - │ -    ║
  ║ __RZFennelSpecialString      │ green/14          │ - │ -    ║
  ║ __RZFennelTableKey           │ cyan/8            │ - │ -    ║
  ║ __RZFennelVim                │ yellow/8          │ - │ -    ║
  ║ __RZFennelVimApi             │ yellow/8          │ - │ -    ║
  ║ __RZFennelVimApiMethod       │ blue/8            │ - │ -    ║
  ║ __RZFishBackslash            │ Comment           │ - │ -    ║
  ║ __RZFishCommandArgument      │ yellow/8          │ - │ -    ║
  ║ __RZFishCommandName          │ yellow/15         │ - │ -    ║
  ║ __RZFishFunctionName         │ TSFunction        │ - │ -    ║
  ║ __RZFishTools                │ cyan/8            │ - │ -    ║
  ║ __RZFishVariable             │ Identifier        │ - │ -    ║
  ║ __RZFunctionName             │ TSFunction        │ - │ -    ║
  ║ __RZKeyword                  │ TSKeyword         │ - │ -    ║
  ║ __RZMacro                    │ Macro             │ - │ -    ║
  ║ __RZRailsDsl                 │ TSFuncMacro       │ - │ bold ║
  ║ __RZRailsDslArgument         │ TSFunction        │ - │ -    ║
  ║ __RZRspecDeclaration         │ TSKeyword         │ - │ -    ║
  ║ __RZRspecDeclarationDescribe │ magenta/8         │ - │ -    ║
  ║ __RZRspecGroup               │ magenta/15        │ - │ bold ║
  ║ __RZRspecHelper              │ blue/1            │ - │ bold ║
  ║ __RZRspecHelperName          │ cyan/8            │ - │ -    ║
  ║ __RZRspecHook                │ yellow/8          │ - │ bold ║
  ║ __RZRspecSpec                │ cyan/10           │ - │ bold ║
  ║ __RZRubyCall                 │ blue/8            │ - │ -    ║
  ║ __RZRubyDsl                  │ TSKeyword         │ - │ -    ║
  ║ __RZRubyBlockParamShort      │ magenta/8         │ - │ -    ║
  ║ __RZRubyBlockParam           │ yellow/1          │ - │ -    ║
  ║ __RZRubyHashKeySymbol        │ yellow/5          │ - │ -    ║
  ║ __RZRubyInstanceVariable     │ red/8             │ - │ -    ║
  ║ __RZSnippet                  │ magenta/8         │ - │ -    ║
  ║ __RZVimOption                │ TSKeyword         │ - │ -    ║
  ║ __RZVimOptionName            │ TSVariable        │ - │ -    ║
  ║ __RZVueBuiltin               │ TSKeyword         │ - │ -    ║
  ║ __RZVueCustomTagName         │ TSFuncMacro       │ - │ -    ║
  ║ __RZVueScript                │ yellow/8          │ - │ -    ║
  ║ __RZVueSpecialElement        │ magenta/15        │ - │ -    ║
  ║ __RZVueStyle                 │ yellow/8          │ - │ -    ║
  ║ __RZVueTagName               │ TSIdentifier      │ - │ -    ║
  ║ __RZVueTemplate              │ yellow/8          │ - │ -    ║
  ╚══════════════════════════════╧═══════════════════╧═══╧══════╝
  )

(define-highlight-context diff
  ╔═════════════════════════════╤══════╤═══════════╤═══╗
  ║ DiffAdd                     │ bg/7 │ green/5   │ - ║
  ║ DiffChange                  │ -    │ -         │ - ║
  ║ DiffDelete                  │ bg/7 │ red/5     │ - ║
  ║ DiffText                    │ bg/7 │ blue/5    │ - ║
  ║ DiffConflictStartMarker     │ bg/7 │ yellow/5  │ - ║
  ║ DiffConflictEndMarker       │ bg/7 │ yellow/5  │ - ║
  ║ DiffConflictParentMarker    │ bg/7 │ yellow/12 │ - ║
  ║ DiffConflictSeparatorMarker │ bg/7 │ yellow/12 │ - ║
  ╚═════════════════════════════╧══════╧═══════════╧═══╝
  )

(define-highlight-context diagnostics
  ╔════════════════════════════╤═════════════════╤═══╤═══════════╗
  ║ DiagnosticError            │ ErrorMsg        │ - │ -         ║
  ║ DiagnosticWarn             │ WarningMsg      │ - │ -         ║
  ║ DiagnosticInfo             │ Comment         │ - │ -         ║
  ║ DiagnosticHint             │ Comment         │ - │ -         ║
  ║ DiagnosticVirtualTextError │ DiagnosticError │ - │ -         ║
  ║ DiagnosticVirtualTextWarn  │ DiagnosticWarn  │ - │ -         ║
  ║ DiagnosticVirtualTextInfo  │ DiagnosticInfo  │ - │ -         ║
  ║ DiagnosticVirtualTextHint  │ DiagnosticHint  │ - │ -         ║
  ║ DiagnosticUnderlineError   │ -               │ - │ undercurl ║
  ║ DiagnosticUnderlineWarn    │ -               │ - │ undercurl ║
  ║ DiagnosticUnderlineInfo    │ -               │ - │ undercurl ║
  ║ DiagnosticUnderlineHint    │ -               │ - │ undercurl ║
  ╚════════════════════════════╧═════════════════╧═══╧═══════════╝
  )

(define-highlight-context statusline
  ╔══════════════════════════════════╤══════════════════════════════╤══════════════════════════════╤═══╗
  ║ StatusLinePillLeftBorder         │ bg/6                         │ bg/8                         │ - ║
  ║ StatusLinePillLeft               │ fg/8                         │ bg/6                         │ - ║
  ║ StatusLinePillSeparator          │ bg/6                         │ bg/7                         │ - ║
  ║ StatusLinePillRight              │ fg/8                         │ bg/7                         │ - ║
  ║ StatusLinePillRightBorder        │ bg/7                         │ bg/8                         │ - ║
  ║ StatusLine                       │ bg/8                         │ bg/8                         │ - ║
  ║ StatusLineAutowrite              │ fg/8                         │ bg/7                         │ - ║
  ║ StatusLineAutowriteLeft          │ bg/7                         │ bg/8                         │ - ║
  ║ StatusLineAutowriteRight         │ StatusLineAutowriteLeft      │ StatusLineAutowriteLeft      │ - ║
  ║ StatusLineBranch                 │ fg/8                         │ bg/7                         │ - ║
  ║ StatusLineBranchLeft             │ bg/7                         │ bg/8                         │ - ║
  ║ StatusLineBranchRight            │ StatusLineBranchLeft         │ StatusLineBranchLeft         │ - ║
  ║ StatusLineCommand                │ fg/8                         │ bg/7                         │ - ║
  ║ StatusLineCommandLeft            │ bg/7                         │ bg/8                         │ - ║
  ║ StatusLineCommandRight           │ StatusLineCommandLeft        │ bg/8                         │ - ║
  ║ StatusLineDap                    │ black/8                      │ cyan/8                       │ - ║
  ║ StatusLineDapLeft                │ cyan/8                       │ bg/8                         │ - ║
  ║ StatusLineDapRight               │ cyan/8                       │ bg/7                         │ - ║
  ║ StatusLineFileType               │ white/8                      │ cyan/13                      │ - ║
  ║ StatusLineFileTypeLeft           │ cyan/13                      │ bg/8                         │ - ║
  ║ StatusLineFileTypeRight          │ cyan/13                      │ bg/7                         │ - ║
  ║ StatusLineFileTypeRightAlt       │ cyan/13                      │ bg/8                         │ - ║
  ║ StatusLineLineInfo               │ fg/8                         │ bg/6                         │ - ║
  ║ StatusLineLineInfoLeft           │ bg/6                         │ bg/8                         │ - ║
  ║ StatusLineLinePercent            │ fg/8                         │ bg/7                         │ - ║
  ║ StatusLineLinePercentLeft        │ bg/7                         │ bg/6                         │ - ║
  ║ StatusLineLinePercentRight       │ StatusLineLinePercentLeft    │ bg/8                         │ - ║
  ║ StatusLineLsp                    │ fg/8                         │ bg/7                         │ - ║
  ║ StatusLineLspLeft                │ bg/7                         │ bg/8                         │ - ║
  ║ StatusLineLspRight               │ bg/7                         │ bg/8                         │ - ║
  ║ StatusLineSession                │ fg/8                         │ bg/7                         │ - ║
  ║ StatusLineSessionLeft            │ bg/7                         │ bg/8                         │ - ║
  ║ StatusLineSessionRight           │ StatusLineSessionLeft        │ StatusLineSessionLeft        │ - ║
  ║ StatusLineTest                   │ bg/8                         │ magenta/8                    │ - ║
  ║ StatusLineTestLeft               │ magenta/8                    │ bg/8                         │ - ║
  ║ StatusLineTestRight              │ magenta/8                    │ bg/7                         │ - ║
  ║ StatusLineRecording              │ bg/8                         │ red/8                        │ - ║
  ║ StatusLineRecordingLeft          │ red/8                        │ bg/8                         │ - ║
  ║ StatusLineRecordingRight         │ red/8                        │ bg/7                         │ - ║
  ║ StatusLineViModeLeftCommand      │ red/7                        │ bg/8                         │ - ║
  ║ StatusLineViModeLeftInsert       │ yellow/5                     │ bg/8                         │ - ║
  ║ StatusLineViModeLeftNormal       │ bg/6                         │ bg/8                         │ - ║
  ║ StatusLineViModeLeftReplace      │ green/5                      │ bg/8                         │ - ║
  ║ StatusLineViModeLeftSelect       │ magenta/5                    │ bg/8                         │ - ║
  ║ StatusLineViModeLeftTerminal     │ cyan/5                       │ bg/8                         │ - ║
  ║ StatusLineViModeLeftVisual       │ blue/7                       │ bg/8                         │ - ║
  ║ StatusLineViModeMiddleCommand    │ bg/8                         │ red/7                        │ - ║
  ║ StatusLineViModeMiddleInsert     │ bg/8                         │ yellow/5                     │ - ║
  ║ StatusLineViModeMiddleNormal     │ fg/8                         │ bg/6                         │ - ║
  ║ StatusLineViModeMiddleReplace    │ bg/8                         │ green/5                      │ - ║
  ║ StatusLineViModeMiddleSelect     │ bg/8                         │ magenta/5                    │ - ║
  ║ StatusLineViModeMiddleTerminal   │ bg/8                         │ cyan/5                       │ - ║
  ║ StatusLineViModeMiddleVisual     │ bg/8                         │ blue/7                       │ - ║
  ║ StatusLineViModeRightCommand     │ StatusLineViModeLeftCommand  │ bg/7                         │ - ║
  ║ StatusLineViModeRightInsert      │ StatusLineViModeLeftInsert   │ bg/7                         │ - ║
  ║ StatusLineViModeRightNormal      │ StatusLineViModeLeftNormal   │ bg/7                         │ - ║
  ║ StatusLineViModeRightReplace     │ StatusLineViModeLeftReplace  │ bg/7                         │ - ║
  ║ StatusLineViModeRightSelect      │ StatusLineViModeLeftSelect   │ bg/7                         │ - ║
  ║ StatusLineViModeRightTerminal    │ StatusLineViModeLeftTerminal │ bg/7                         │ - ║
  ║ StatusLineViModeRightVisual      │ StatusLineViModeLeftVisual   │ bg/7                         │ - ║
  ╚══════════════════════════════════╧══════════════════════════════╧══════════════════════════════╧═══╝
  )

(define-highlight-context tabline
  ╔═══════════════════════════╤════════╤════════╤══════╗
  ║ TabLine                   │ fg/8   │ bg/6   │ -    ║
  ║ TabLineFill               │ fg/8   │ bg/8   │ -    ║
  ║ TabLineSel                │ fg/8   │ bg/6   │ -    ║
  ║ TabLineBorder             │ bg/8   │ -      │ -    ║
  ║ TabLineTabLeftBorder      │ bg/4   │ bg/8   │ -    ║
  ║ TabLineTabLeftBorderSel   │ blue/8 │ bg/8   │ -    ║
  ║ TabLineTabRightBorder     │ bg/6   │ bg/8   │ -    ║
  ║ TabLineTabRightBorderSel  │ bg/6   │ bg/8   │ -    ║
  ║ TabLineNumberSel          │ bg/8   │ blue/8 │ bold ║
  ║ TabLineNumberPreviousSel  │ blue/8 │ bg/4   │ bold ║
  ║ TabLineNumber             │ fg/1   │ bg/4   │ -    ║
  ║ TabLineNumberSeparatorSel │ blue/8 │ bg/6   │ -    ║
  ║ TabLineNumberSeparator    │ bg/4   │ bg/6   │ -    ║
  ╚═══════════════════════════╧════════╧════════╧══════╝
  )

(define-highlight-context winbar
  ╔══════════╤══════════╤═══╤══════╗
  ║ WinBar   │ blue/5   │ - │ bold ║
  ║ WinBarNC │ Comment  │ - │ bold ║
  ╚══════════╧══════════╧═══╧══════╝
  )

(define-highlight-context float-window
  ╔═════════════╤════════╤══════╤═══╗
  ║ NormalFloat │ fg/8   │ bg/7 │ - ║
  ║ FloatBorder │ bg/7   │ bg/7 │ - ║
  ╚═════════════╧════════╧══════╧═══╝
  )

(define-highlight-context popup
  ╔════════════╤═════════╤══════╤═══╗
  ║ Pmenu      │ fg/8    │ bg/6 │ - ║
  ║ PmenuSel   │ -       │ bg/3 │ - ║
  ║ PmenuSbar  │ bg/6    │ bg/6 │ - ║
  ║ PmenuThumb │ -       │ bg/7 │ - ║
  ╚════════════╧═════════╧══════╧═══╝
  )

(define-highlight-context git
  ╔════════════════════════╤═══════════╤═══╤═══╗
  ║ gitcommitOverflow      │ red/8     │ - │ - ║
  ║ gitcommitSummary       │ green/8   │ - │ - ║
  ║ gitcommitComment       │ Comment   │ - │ - ║
  ║ gitcommitUntracked     │ bg/5      │ - │ - ║
  ║ gitcommitDiscarded     │ bg/5      │ - │ - ║
  ║ gitcommitSelected      │ bg/5      │ - │ - ║
  ║ gitcommitHeader        │ magenta/8 │ - │ - ║
  ║ gitcommitSelectedType  │ blue/8    │ - │ - ║
  ║ gitcommitUnmergedType  │ blue/8    │ - │ - ║
  ║ gitcommitDiscardedType │ blue/8    │ - │ - ║
  ║ gitcommitBranch        │ yellow/8  │ - │ - ║
  ║ gitcommitUntrackedFile │ yellow/8  │ - │ - ║
  ║ gitcommitUnmergedFile  │ red/8     │ - │ - ║
  ║ gitcommitDiscardedFile │ red/8     │ - │ - ║
  ║ gitcommitSelectedFile  │ green/8   │ - │ - ║
  ╚════════════════════════╧═══════════╧═══╧═══╝
  )

(define-highlight-context linear
  ╔══════════════════════╤═══════════╤═══╤═══╗
  ║ LinearIdentifier     │ magenta/7 │ - │ - ║
  ║ LinearStateCompleted │ green/5   │ - │ - ║
  ║ LinearStateUnstarted │ blue/5    │ - │ - ║
  ║ LinearStateStarted   │ yellow/5  │ - │ - ║
  ╚══════════════════════╧═══════════╧═══╧═══╝
  )

(define-highlight-context dirbuf
  ╔═════════════════╤═══════════╤═══════════╤════════╗
  ║ DirbufHash      │ Comment   │ -         │ italic ║
  ║ DirbufDirectory │ Directory │ Directory │ bold   ║
  ╚═════════════════╧═══════════╧═══════════╧════════╝
  )

(define-highlight-context telescope
  ╔═════════════════════════╤══════════════╤══════╤═══╗
  ║ TelescopeBorder         │ bg/7         │ bg/7 │ - ║
  ║ TelescopeNormal         │ -            │ bg/7 │ - ║
  ║ TelescopePromptNormal   │ -            │ bg/7 │ - ║
  ║ TelescopePromptCounter  │ bg/7         │ bg/7 │ - ║
  ║ TelescopePreviewNormal  │ -            │ bg/7 │ - ║
  ║ TelescopePreviewBorder  │ bg/7         │ bg/7 │ - ║
  ║ TelescopePreviewTitle   │ bg/7         │ bg/7 │ - ║
  ║ TelescopeResultsNormal  │ -            │ bg/7 │ - ║
  ║ TelescopeSelection      │ yellow/8     │ -    │ - ║
  ║ TelescopeSelectionCaret │ yellow/8     │ -    │ - ║
  ║ TelescopePromptPrefix   │ yellow/8     │ -    │ - ║
  ║ TelescopeMatching       │ yellow/8     │ -    │ - ║
  ╚═════════════════════════╧══════════════╧══════╧═══╝
  )

(define-highlight-context gitsigns
  ╔═══════════════════╤══════════╤═══╤═══╗
  ║ GitSignsAdd       │ green/5  │ - │ - ║
  ║ GitSignsDelete    │ red/5    │ - │ - ║
  ║ GitSignsChange    │ blue/5   │ - │ - ║
  ║ GitSignsUntracked │ yellow/5 │ - │ - ║
  ╚═══════════════════╧══════════╧═══╧═══╝
  )

(define-highlight-context nvim-cmp
  ╔═══════════════════════╤═══════════╤═══╤═══════════════╗
  ║ CmpItemAbbr           │ fg/8      │ - │ -             ║
  ║ CmpItemAbbrDeprecated │ green/5   │ - │ strikethrough ║
  ║ CmpItemAbbrMatch      │ WildMenu  │ - │ -             ║
  ║ CmpItemAbbrMatchFuzzy │ WildMenu  │ - │ -             ║
  ║ CmpItemKindVariable   │ cyan/5    │ - │ -             ║
  ║ CmpItemKindInterface  │ cyan/5    │ - │ -             ║
  ║ CmpItemKindText       │ cyan/5    │ - │ -             ║
  ║ CmpItemKindFunction   │ yellow/5  │ - │ -             ║
  ║ CmpItemKindMethod     │ yellow/5  │ - │ -             ║
  ║ CmpItemKindClass      │ green/5   │ - │ -             ║
  ║ CmpItemKindModule     │ green/5   │ - │ -             ║
  ║ CmpItemKindSnippet    │ magenta/5 │ - │ -             ║
  ╚═══════════════════════╧═══════════╧═══╧═══════════════╝
  )

(define-highlight-context vim-notify
  ╔═══════════════════╤═════════════════╤════════════════╤═══╗
  ║ NotifyERRORBody   │ red/8           │ bg/6           │ - ║
  ║ NotifyERRORBorder │ bg/6            │ bg/6           │ - ║
  ║ NotifyERRORIcon   │ red/8           │ bg/6           │ - ║
  ║ NotifyERRORTitle  │ red/8           │ bg/6           │ - ║
  ║ NotifyINFOBody    │ blue/8          │ bg/6           │ - ║
  ║ NotifyINFOBorder  │ bg/6            │ bg/6           │ - ║
  ║ NotifyINFOIcon    │ blue/8          │ bg/6           │ - ║
  ║ NotifyINFOTitle   │ blue/8          │ bg/6           │ - ║
  ║ NotifyWARNBody    │ yellow/8        │ bg/6           │ - ║
  ║ NotifyWARNBorder  │ bg/6            │ bg/6           │ - ║
  ║ NotifyWARNIcon    │ yellow/8        │ bg/6           │ - ║
  ║ NotifyWARNTitle   │ yellow/8        │ bg/6           │ - ║
  ╚═══════════════════╧═════════════════╧════════════════╧═══╝
  )

(define-highlight-context diffview
  ╔══════════════════════════╤══════════╤═══╤═══╗
  ║ DiffviewFilePanelTitle   │ yellow/7 │ - │ - ║
  ║ DiffviewFilePanelCounter │ yellow/3 │ - │ - ║
  ╚══════════════════════════╧══════════╧═══╧═══╝
  )

(define-highlight-context lsp
  ╔═══════════════════════════╤══════════╤══════╤═══════════╗
  ║ LspReferenceText          │ -        │ bg/6 │ -         ║
  ║ LspReferenceRead          │ -        │ -    │ underline ║
  ║ LspReferenceWrite         │ -        │ bg/6 │ italic    ║
  ║ __lsp.type.namespace.ruby │ Constant │ -    │ -         ║
  ║ __lsp.type.parameter.ruby │ blue/8   │ -    │ -         ║
  ╚═══════════════════════════╧══════════╧══════╧═══════════╝
)

(define-highlight-context noice
  ╔═════════════════════════╤═══════════╤═══════════╤═══╗
  ║ NoiceLspProgressClient  │ green/5   │ -         │ - ║
  ║ NoiceLspProgressSpinner │ red/5     │ -         │ - ║
  ║ NoiceLspProgressTitle   │ blue/5    │ -         │ - ║
  ║ NoiceMini               │ -         │ bg/8      │ - ║
  ╚═════════════════════════╧═══════════╧═══════════╧═══╝
  )

(define-highlight-context neotest
  ╔════════════════╤══════════╤══════╤═══╗
  ║ NeotestPassed  │ green/8  │ bg/8 │ - ║
  ║ NeotestRunning │ yellow/8 │ bg/8 │ - ║
  ║ NeotestFailed  │ red/8    │ bg/8 │ - ║
  ║ NeotestSkipped │ bg/6     │ bg/8 │ - ║
  ╚════════════════╧══════════╧══════╧═══╝
  )

(define-highlight-context marks
  ╔════════════╤═════════╤═══╤═══╗
  ║ MarkSignHL │ green/5 │ - │ - ║
  ╚════════════╧═════════╧═══╧═══╝
  )

(define-highlight-context which-key
  ╔════════════════╤══════╤══════╤═══╗
  ║ WhichKeyBorder │ bg/8 │ bg/8 │ - ║
  ╚════════════════╧══════╧══════╧═══╝
  )

(define-highlight-context yanky
  ╔═════════════╤═════════╤══════════╤═══╗
  ║ YankyPut    │ black/8 │ yellow/8 │ - ║
  ║ YankyYanked │ black/8 │ yellow/4 │ - ║
  ╚═════════════╧═════════╧══════════╧═══╝
  )

(define-highlight-context custom
  ╔════════════════════════════════╤══════════════════╤══════════╤════════╗
  ║ RZDashboardButtonShortcut      │ magenta/8        │ -        │ -      ║
  ║ RZDashboardDate                │ Comment          │ -        │ -      ║
  ║ RZDashboardHeader              │ yellow/8         │ -        │ -      ║
  ║ RZDashboardMiscelaniousInfos   │ Comment          │ -        │ -      ║
  ║ RZDashboardSectionTitleText    │ cyan/8           │ -        │ -      ║
  ║ RZDashboardSectionTitleSep     │ Comment          │ -        │ -      ║
  ║ RZDashboardTruthText           │ blue/5           │ -        │ -      ║
  ║ RZDashboardTruthReference      │ blue/15          │ -        │ -      ║
  ║ RZDraftFindDate                │ blue/8           │ -        │ -      ║
  ║ RZDraftFindTime                │ cyan/8           │ -        │ -      ║
  ║ RZGithubStatusAuthor           │ magenta/10       │ -        │ italic ║
  ║ RZGithubStatusCommitContext    │ cyan/8           │ -        │ -      ║
  ║ RZGithubStatusCommitKind       │ yellow/10        │ -        │ -      ║
  ║ RZGithubStatusCommitMessage    │ Normal           │ -        │ -      ║
  ║ RZGithubStatusCommitTag        │ magenta/10       │ -        │ -      ║
  ║ RZGithubStatusCurrentBranch    │ blue/10          │ -        │ -      ║
  ║ RZGithubStatusDelimiter        │ TSPunctDelimiter │ -        │ -      ║
  ║ RZGithubStatusExtMark          │ fg/8             │ bg/5     │ -      ║
  ║ RZGithubStatusExtMarkInv       │ bg/5             │ bg/7     │ -      ║
  ║ RZGithubStatusFailure          │ DiagnosticError  │ -        │ -      ║
  ║ RZGithubStatusFoldOpened       │ Comment          │ -        │ -      ║
  ║ RZGithubStatusFoldClosed       │ Comment          │ -        │ -      ║
  ║ RZGithubStatusGroup            │ blue/5           │ -        │ -      ║
  ║ RZGithubStatusMessage          │ Comment          │ -        │ italic ║
  ║ RZGithubStatusNormal           │ fg/8             │ bg/7     │ -      ║
  ║ RZGithubStatusPending          │ DiagnosticWarn   │ -        │ -      ║
  ║ RZGithubStatusPrNumber         │ yellow/10        │ -        │ -      ║
  ║ RZGithubStatusPrNumberDraft    │ Comment          │ -        │ italic ║
  ║ RZGithubStatusSuccess          │ green/8          │ -        │ -      ║
  ║ RZGithubStatusTitle            │ bg/7             │ yellow/8 │ -      ║
  ║ RZGithusStatusBorder           │ bg/7             │ bg/7     │ -      ║
  ║ RZPopupNormal                  │ fg/8             │ bg/7     │ -      ║
  ║ RZPopupBorder                  │ bg/7             │ bg/7     │ -      ║
  ║ RZTableBorder                  │ Comment          │ -        │ -      ║
  ║ RZTableFallback                │ bg/6             │ -        │ -      ║
  ║ RZWinbarLeftDelimiter          │ blue/8           │ bg/8     │ -      ║
  ║ RZWinbarLeftDelimiterInactive  │ bg/5             │ bg/8     │ -      ║
  ║ RZWinbarRightDelimiter         │ bg/6             │ bg/8     │ -      ║
  ║ RZWinbarRightDelimiterInactive │ bg/6             │ bg/8     │ -      ║
  ║ RZWinbarText                   │ blue/8           │ bg/8     │ -      ║
  ║ RZWinbarTextInactive           │ fg/7             │ bg/8     │ -      ║
  ║ RZWinbarIcon                   │ bg/8             │ blue/8   │ -      ║
  ║ RZWinbarIconInactive           │ fg/8             │ bg/5     │ -      ║
  ║ RZWinbarIconDelimiter          │ blue/8           │ bg/6     │ -      ║
  ║ RZWinbarIconDelimiterInactive  │ bg/5             │ bg/6     │ -      ║
  ║ RZWinbarFlag                   │ blue/8           │ bg/6     │ bold   ║
  ║ RZWinbarFlagInactive           │ fg/7             │ bg/6     │ bold   ║
  ║ RZWinbarSpacer                 │ blue/8           │ bg/8     │ -      ║
  ║ RZWinbarSpacerInactive         │ bg/6             │ bg/8     │ -      ║
  ║ RZWinbarTestPassed             │ bg/8             │ green/8  │ -      ║
  ║ RZWinbarTestFailed             │ bg/8             │ red/8    │ -      ║
  ║ RZWinbarTestPassedDelimiter    │ green/8          │ bg/8     │ -      ║
  ║ RZWinbarTestFailedDelimiter    │ red/8            │ bg/8     │ -      ║
  ╚════════════════════════════════╧══════════════════╧══════════╧════════╝
)

(define-diagnostic-icon error :E)
(define-diagnostic-icon warn  :W)
(define-diagnostic-icon info  :I)
(define-diagnostic-icon hint  :H)

(set vim.g.colors_name :rz)
