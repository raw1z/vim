-- [nfnl] Compiled from colors/rz.fnl by https://github.com/Olical/nfnl, do not edit.
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextbase()
      local _2_
      do
        vim.api.nvim_set_hl(0, "Normal", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _2_ = {"Normal", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _3_
      do
        vim.api.nvim_set_hl(0, "Cursor", {background = require("rz.lib.colors")["get-color"]("magenta/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _3_ = {"Cursor", {background = require("rz.lib.colors")["get-color"]("magenta/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _4_
      do
        vim.api.nvim_set_hl(0, "ICursor", {background = require("rz.lib.colors")["get-color"]("yellow/5"), foreground = require("rz.lib.colors")["get-color"]("-")})
        _4_ = {"ICursor", {background = require("rz.lib.colors")["get-color"]("yellow/5"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _5_
      do
        vim.api.nvim_set_hl(0, "RCursor", {background = require("rz.lib.colors")["get-color"]("green/5"), foreground = require("rz.lib.colors")["get-color"]("-")})
        _5_ = {"RCursor", {background = require("rz.lib.colors")["get-color"]("green/5"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _6_
      do
        vim.api.nvim_set_hl(0, "CursorLine", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("-")})
        _6_ = {"CursorLine", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _7_
      do
        vim.api.nvim_set_hl(0, "LineNr", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/1|fg/15")})
        _7_ = {"LineNr", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/1|fg/15")}}
      end
      local _8_
      do
        vim.api.nvim_set_hl(0, "CursorLineNr", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _8_ = {"CursorLineNr", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _9_
      do
        vim.api.nvim_set_hl(0, "CursorLineSign", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-")})
        _9_ = {"CursorLineSign", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _10_
      do
        vim.api.nvim_set_hl(0, "WinSeparator", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _10_ = {"WinSeparator", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _11_
      do
        vim.api.nvim_set_hl(0, "FoldColumn", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/1|fg/15")})
        _11_ = {"FoldColumn", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/1|fg/15")}}
      end
      local _12_
      do
        vim.api.nvim_set_hl(0, "Folded", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/1|fg/15"), italic = true})
        _12_ = {"Folded", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/1|fg/15"), italic = true}}
      end
      local _13_
      do
        vim.api.nvim_set_hl(0, "MatchParen", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-"), underline = true})
        _13_ = {"MatchParen", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-"), underline = true}}
      end
      local _14_
      do
        vim.api.nvim_set_hl(0, "Visual", {background = require("rz.lib.colors")["get-color"]("bg/5"), foreground = require("rz.lib.colors")["get-color"]("-")})
        _14_ = {"Visual", {background = require("rz.lib.colors")["get-color"]("bg/5"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _15_
      do
        vim.api.nvim_set_hl(0, "VisualNOS", {background = vim.api.nvim_get_hl_by_name("Visual", true).background, foreground = require("rz.lib.colors")["get-color"]("-")})
        _15_ = {"VisualNOS", {background = vim.api.nvim_get_hl_by_name("Visual", true).background, foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _16_
      do
        vim.api.nvim_set_hl(0, "WildMenu", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/5")})
        _16_ = {"WildMenu", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/5")}}
      end
      local _17_
      do
        vim.api.nvim_set_hl(0, "SignColumn", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("-")})
        _17_ = {"SignColumn", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _18_
      do
        vim.api.nvim_set_hl(0, "ColorColumn", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("-")})
        _18_ = {"ColorColumn", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _19_
      do
        vim.api.nvim_set_hl(0, "CursorColumn", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("-")})
        _19_ = {"CursorColumn", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _20_
      do
        vim.api.nvim_set_hl(0, "QuickFixLine", {background = require("rz.lib.colors")["get-color"]("bg/3"), foreground = require("rz.lib.colors")["get-color"]("-")})
        _20_ = {"QuickFixLine", {background = require("rz.lib.colors")["get-color"]("bg/3"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _21_
      do
        vim.api.nvim_set_hl(0, "Search", {background = require("rz.lib.colors")["get-color"]("blue/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _21_ = {"Search", {background = require("rz.lib.colors")["get-color"]("blue/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _22_
      do
        vim.api.nvim_set_hl(0, "IncSearch", {background = require("rz.lib.colors")["get-color"]("cyan/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _22_ = {"IncSearch", {background = require("rz.lib.colors")["get-color"]("cyan/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _23_
      do
        vim.api.nvim_set_hl(0, "Susbstitute", {background = require("rz.lib.colors")["get-color"]("cyan/5"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _23_ = {"Susbstitute", {background = require("rz.lib.colors")["get-color"]("cyan/5"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _24_
      do
        vim.api.nvim_set_hl(0, "Directory", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("blue/5")})
        _24_ = {"Directory", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("blue/5")}}
      end
      local _25_
      do
        vim.api.nvim_set_hl(0, "ErrorMsg", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/5")})
        _25_ = {"ErrorMsg", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/5")}}
      end
      local _26_
      do
        vim.api.nvim_set_hl(0, "WarningMsg", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _26_ = {"WarningMsg", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _27_
      do
        vim.api.nvim_set_hl(0, "Question", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/1|fg/15"), italic = true})
        _27_ = {"Question", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/1|fg/15"), italic = true}}
      end
      local _28_
      do
        vim.api.nvim_set_hl(0, "SpecialKey", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/1|fg/15"), italic = true})
        _28_ = {"SpecialKey", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/1|fg/15"), italic = true}}
      end
      local _29_
      do
        vim.api.nvim_set_hl(0, "Title", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")})
        _29_ = {"Title", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")}}
      end
      local function _30_()
        vim.api.nvim_set_hl(0, "Macro", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        return {"Macro", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      return {_2_, _3_, _4_, _5_, _6_, _7_, _8_, _9_, _10_, _11_, _12_, _13_, _14_, _15_, _16_, _17_, _18_, _19_, _20_, _21_, _22_, _23_, _24_, _25_, _26_, _27_, _28_, _29_, _30_()}
    end
    _contextbase()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextstandard()
      local _33_
      do
        vim.api.nvim_set_hl(0, "Bold", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("-")})
        _33_ = {"Bold", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _34_
      do
        vim.api.nvim_set_hl(0, "Boolean", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")})
        _34_ = {"Boolean", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")}}
      end
      local _35_
      do
        vim.api.nvim_set_hl(0, "Character", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")})
        _35_ = {"Character", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")}}
      end
      local _36_
      do
        vim.api.nvim_set_hl(0, "Comment", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/1|fg/15"), italic = true})
        _36_ = {"Comment", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/1|fg/15"), italic = true}}
      end
      local _37_
      do
        vim.api.nvim_set_hl(0, "Conditional", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")})
        _37_ = {"Conditional", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")}}
      end
      local _38_
      do
        vim.api.nvim_set_hl(0, "Constant", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("blue/9")})
        _38_ = {"Constant", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("blue/9")}}
      end
      local _39_
      do
        vim.api.nvim_set_hl(0, "Define", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")})
        _39_ = {"Define", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")}}
      end
      local _40_
      do
        vim.api.nvim_set_hl(0, "Delimiter", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground})
        _40_ = {"Delimiter", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground}}
      end
      local _41_
      do
        vim.api.nvim_set_hl(0, "Float", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _41_ = {"Float", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _42_
      do
        vim.api.nvim_set_hl(0, "Function", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/1")})
        _42_ = {"Function", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/1")}}
      end
      local _43_
      do
        vim.api.nvim_set_hl(0, "Identifier", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/15")})
        _43_ = {"Identifier", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/15")}}
      end
      local _44_
      do
        vim.api.nvim_set_hl(0, "Include", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _44_ = {"Include", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _45_
      do
        vim.api.nvim_set_hl(0, "Italic", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-"), italic = true})
        _45_ = {"Italic", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-"), italic = true}}
      end
      local _46_
      do
        vim.api.nvim_set_hl(0, "Keyword", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")})
        _46_ = {"Keyword", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")}}
      end
      local _47_
      do
        vim.api.nvim_set_hl(0, "Label", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/10")})
        _47_ = {"Label", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/10")}}
      end
      local _48_
      do
        vim.api.nvim_set_hl(0, "Number", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _48_ = {"Number", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _49_
      do
        vim.api.nvim_set_hl(0, "Operator", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground})
        _49_ = {"Operator", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground}}
      end
      local _50_
      do
        vim.api.nvim_set_hl(0, "PreProc", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/11")})
        _50_ = {"PreProc", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/11")}}
      end
      local _51_
      do
        vim.api.nvim_set_hl(0, "Repeat", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _51_ = {"Repeat", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _52_
      do
        vim.api.nvim_set_hl(0, "Special", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")})
        _52_ = {"Special", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")}}
      end
      local _53_
      do
        vim.api.nvim_set_hl(0, "SpecialChar", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _53_ = {"SpecialChar", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _54_
      do
        vim.api.nvim_set_hl(0, "Statement", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")})
        _54_ = {"Statement", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")}}
      end
      local _55_
      do
        vim.api.nvim_set_hl(0, "StorageClass", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _55_ = {"StorageClass", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _56_
      do
        vim.api.nvim_set_hl(0, "String", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/8")})
        _56_ = {"String", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/8")}}
      end
      local _57_
      do
        vim.api.nvim_set_hl(0, "Structure", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Constant", true).foreground})
        _57_ = {"Structure", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Constant", true).foreground}}
      end
      local _58_
      do
        vim.api.nvim_set_hl(0, "Tag", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _58_ = {"Tag", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _59_
      do
        vim.api.nvim_set_hl(0, "Todo", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _59_ = {"Todo", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _60_
      do
        vim.api.nvim_set_hl(0, "Type", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")})
        _60_ = {"Type", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")}}
      end
      local _61_
      do
        vim.api.nvim_set_hl(0, "Underline", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-"), underline = true})
        _61_ = {"Underline", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-"), underline = true}}
      end
      local function _62_()
        vim.api.nvim_set_hl(0, "Variable", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/15")})
        return {"Variable", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/15")}}
      end
      return {_33_, _34_, _35_, _36_, _37_, _38_, _39_, _40_, _41_, _42_, _43_, _44_, _45_, _46_, _47_, _48_, _49_, _50_, _51_, _52_, _53_, _54_, _55_, _56_, _57_, _58_, _59_, _60_, _61_, _62_()}
    end
    _contextstandard()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contexttreesitter()
      local _65_
      do
        vim.api.nvim_set_hl(0, "TSAnnotation", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _65_ = {"TSAnnotation", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _66_
      do
        vim.api.nvim_set_hl(0, "TSAttribute", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")})
        _66_ = {"TSAttribute", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")}}
      end
      local _67_
      do
        vim.api.nvim_set_hl(0, "TSBoolean", {background = vim.api.nvim_get_hl_by_name("Boolean", true).background, foreground = vim.api.nvim_get_hl_by_name("Boolean", true).foreground})
        _67_ = {"TSBoolean", {background = vim.api.nvim_get_hl_by_name("Boolean", true).background, foreground = vim.api.nvim_get_hl_by_name("Boolean", true).foreground}}
      end
      local _68_
      do
        vim.api.nvim_set_hl(0, "TSCharacter", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/5")})
        _68_ = {"TSCharacter", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/5")}}
      end
      local _69_
      do
        vim.api.nvim_set_hl(0, "TSComment", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground, italic = true})
        _69_ = {"TSComment", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground, italic = true}}
      end
      local _70_
      do
        vim.api.nvim_set_hl(0, "TSConditional", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Conditional", true).foreground})
        _70_ = {"TSConditional", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Conditional", true).foreground}}
      end
      local _71_
      do
        vim.api.nvim_set_hl(0, "TSConstBuiltin", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")})
        _71_ = {"TSConstBuiltin", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")}}
      end
      local _72_
      do
        vim.api.nvim_set_hl(0, "TSConstMacro", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")})
        _72_ = {"TSConstMacro", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")}}
      end
      local _73_
      do
        vim.api.nvim_set_hl(0, "TSConstant", {background = vim.api.nvim_get_hl_by_name("Constant", true).background, foreground = vim.api.nvim_get_hl_by_name("Constant", true).foreground})
        _73_ = {"TSConstant", {background = vim.api.nvim_get_hl_by_name("Constant", true).background, foreground = vim.api.nvim_get_hl_by_name("Constant", true).foreground}}
      end
      local _74_
      do
        vim.api.nvim_set_hl(0, "TSConstructor", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/10")})
        _74_ = {"TSConstructor", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/10")}}
      end
      local _75_
      do
        vim.api.nvim_set_hl(0, "TSCurrentScope", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-")})
        _75_ = {"TSCurrentScope", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _76_
      do
        vim.api.nvim_set_hl(0, "TSDefinition", {background = require("rz.lib.colors")["get-color"]("yellow/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _76_ = {"TSDefinition", {background = require("rz.lib.colors")["get-color"]("yellow/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _77_
      do
        vim.api.nvim_set_hl(0, "TSDefinitionUsage", {background = require("rz.lib.colors")["get-color"]("yellow/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _77_ = {"TSDefinitionUsage", {background = require("rz.lib.colors")["get-color"]("yellow/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _78_
      do
        vim.api.nvim_set_hl(0, "TSEmphasis", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/10")})
        _78_ = {"TSEmphasis", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/10")}}
      end
      local _79_
      do
        vim.api.nvim_set_hl(0, "TSError", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")})
        _79_ = {"TSError", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")}}
      end
      local _80_
      do
        vim.api.nvim_set_hl(0, "TSException", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/5")})
        _80_ = {"TSException", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/5")}}
      end
      local _81_
      do
        vim.api.nvim_set_hl(0, "TSField", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")})
        _81_ = {"TSField", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")}}
      end
      local _82_
      do
        vim.api.nvim_set_hl(0, "TSFloat", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Float", true).foreground})
        _82_ = {"TSFloat", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Float", true).foreground}}
      end
      local _83_
      do
        vim.api.nvim_set_hl(0, "TSFuncBuiltin", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")})
        _83_ = {"TSFuncBuiltin", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")}}
      end
      local _84_
      do
        vim.api.nvim_set_hl(0, "TSFuncMacro", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")})
        _84_ = {"TSFuncMacro", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")}}
      end
      local _85_
      do
        vim.api.nvim_set_hl(0, "TSFunction", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Function", true).foreground})
        _85_ = {"TSFunction", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Function", true).foreground}}
      end
      local _86_
      do
        vim.api.nvim_set_hl(0, "TSFunctionBuiltin", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/10")})
        _86_ = {"TSFunctionBuiltin", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/10")}}
      end
      local _87_
      do
        vim.api.nvim_set_hl(0, "TSIdentifier", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Identifier", true).foreground})
        _87_ = {"TSIdentifier", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Identifier", true).foreground}}
      end
      local _88_
      do
        vim.api.nvim_set_hl(0, "TSInclude", {background = vim.api.nvim_get_hl_by_name("Include", true).background, foreground = vim.api.nvim_get_hl_by_name("Include", true).foreground})
        _88_ = {"TSInclude", {background = vim.api.nvim_get_hl_by_name("Include", true).background, foreground = vim.api.nvim_get_hl_by_name("Include", true).foreground}}
      end
      local _89_
      do
        vim.api.nvim_set_hl(0, "TSKeyword", {background = vim.api.nvim_get_hl_by_name("Keyword", true).background, foreground = vim.api.nvim_get_hl_by_name("Keyword", true).foreground})
        _89_ = {"TSKeyword", {background = vim.api.nvim_get_hl_by_name("Keyword", true).background, foreground = vim.api.nvim_get_hl_by_name("Keyword", true).foreground}}
      end
      local _90_
      do
        vim.api.nvim_set_hl(0, "TSKeywordFunction", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/5")})
        _90_ = {"TSKeywordFunction", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/5")}}
      end
      local _91_
      do
        vim.api.nvim_set_hl(0, "TSLabel", {background = vim.api.nvim_get_hl_by_name("Label", true).background, foreground = vim.api.nvim_get_hl_by_name("Label", true).foreground})
        _91_ = {"TSLabel", {background = vim.api.nvim_get_hl_by_name("Label", true).background, foreground = vim.api.nvim_get_hl_by_name("Label", true).foreground}}
      end
      local _92_
      do
        vim.api.nvim_set_hl(0, "TSLiteral", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/3")})
        _92_ = {"TSLiteral", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/3")}}
      end
      local _93_
      do
        vim.api.nvim_set_hl(0, "TSMethod", {background = vim.api.nvim_get_hl_by_name("Function", true).background, foreground = vim.api.nvim_get_hl_by_name("Function", true).foreground})
        _93_ = {"TSMethod", {background = vim.api.nvim_get_hl_by_name("Function", true).background, foreground = vim.api.nvim_get_hl_by_name("Function", true).foreground}}
      end
      local _94_
      do
        vim.api.nvim_set_hl(0, "TSNameSpace", {background = vim.api.nvim_get_hl_by_name("Function", true).background, foreground = vim.api.nvim_get_hl_by_name("Function", true).foreground})
        _94_ = {"TSNameSpace", {background = vim.api.nvim_get_hl_by_name("Function", true).background, foreground = vim.api.nvim_get_hl_by_name("Function", true).foreground}}
      end
      local _95_
      do
        vim.api.nvim_set_hl(0, "TSNumber", {background = vim.api.nvim_get_hl_by_name("Number", true).background, foreground = vim.api.nvim_get_hl_by_name("Number", true).foreground})
        _95_ = {"TSNumber", {background = vim.api.nvim_get_hl_by_name("Number", true).background, foreground = vim.api.nvim_get_hl_by_name("Number", true).foreground}}
      end
      local _96_
      do
        vim.api.nvim_set_hl(0, "TSOperator", {background = vim.api.nvim_get_hl_by_name("Number", true).background, foreground = vim.api.nvim_get_hl_by_name("Number", true).foreground})
        _96_ = {"TSOperator", {background = vim.api.nvim_get_hl_by_name("Number", true).background, foreground = vim.api.nvim_get_hl_by_name("Number", true).foreground}}
      end
      local _97_
      do
        vim.api.nvim_set_hl(0, "TSParameter", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/2")})
        _97_ = {"TSParameter", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/2")}}
      end
      local _98_
      do
        vim.api.nvim_set_hl(0, "TSParameterReference", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/3")})
        _98_ = {"TSParameterReference", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/3")}}
      end
      local _99_
      do
        vim.api.nvim_set_hl(0, "TSProperty", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/5")})
        _99_ = {"TSProperty", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/5")}}
      end
      local _100_
      do
        vim.api.nvim_set_hl(0, "TSPunctBracket", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("blue/10")})
        _100_ = {"TSPunctBracket", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("blue/10")}}
      end
      local _101_
      do
        vim.api.nvim_set_hl(0, "TSPunctDelimiter", {background = vim.api.nvim_get_hl_by_name("Delimiter", true).background, foreground = vim.api.nvim_get_hl_by_name("Delimiter", true).foreground})
        _101_ = {"TSPunctDelimiter", {background = vim.api.nvim_get_hl_by_name("Delimiter", true).background, foreground = vim.api.nvim_get_hl_by_name("Delimiter", true).foreground}}
      end
      local _102_
      do
        vim.api.nvim_set_hl(0, "TSPunctSpectial", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/1")})
        _102_ = {"TSPunctSpectial", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/1")}}
      end
      local _103_
      do
        vim.api.nvim_set_hl(0, "TSRepeat", {background = vim.api.nvim_get_hl_by_name("Repeat", true).background, foreground = vim.api.nvim_get_hl_by_name("Repeat", true).foreground})
        _103_ = {"TSRepeat", {background = vim.api.nvim_get_hl_by_name("Repeat", true).background, foreground = vim.api.nvim_get_hl_by_name("Repeat", true).foreground}}
      end
      local _104_
      do
        vim.api.nvim_set_hl(0, "TSString", {background = vim.api.nvim_get_hl_by_name("String", true).background, foreground = vim.api.nvim_get_hl_by_name("String", true).foreground})
        _104_ = {"TSString", {background = vim.api.nvim_get_hl_by_name("String", true).background, foreground = vim.api.nvim_get_hl_by_name("String", true).foreground}}
      end
      local _105_
      do
        vim.api.nvim_set_hl(0, "TSStringEscape", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/3")})
        _105_ = {"TSStringEscape", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/3")}}
      end
      local _106_
      do
        vim.api.nvim_set_hl(0, "TSStringRegex", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/5")})
        _106_ = {"TSStringRegex", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/5")}}
      end
      local _107_
      do
        vim.api.nvim_set_hl(0, "TSStrong", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/5")})
        _107_ = {"TSStrong", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/5")}}
      end
      local _108_
      do
        vim.api.nvim_set_hl(0, "TSStructure", {background = vim.api.nvim_get_hl_by_name("Structure", true).background, foreground = vim.api.nvim_get_hl_by_name("Structure", true).foreground})
        _108_ = {"TSStructure", {background = vim.api.nvim_get_hl_by_name("Structure", true).background, foreground = vim.api.nvim_get_hl_by_name("Structure", true).foreground}}
      end
      local _109_
      do
        vim.api.nvim_set_hl(0, "TSTag", {background = vim.api.nvim_get_hl_by_name("Tag", true).background, foreground = vim.api.nvim_get_hl_by_name("Tag", true).foreground})
        _109_ = {"TSTag", {background = vim.api.nvim_get_hl_by_name("Tag", true).background, foreground = vim.api.nvim_get_hl_by_name("Tag", true).foreground}}
      end
      local _110_
      do
        vim.api.nvim_set_hl(0, "TSTagDelimiter", {background = vim.api.nvim_get_hl_by_name("Delimiter", true).background, foreground = vim.api.nvim_get_hl_by_name("Delimiter", true).foreground})
        _110_ = {"TSTagDelimiter", {background = vim.api.nvim_get_hl_by_name("Delimiter", true).background, foreground = vim.api.nvim_get_hl_by_name("Delimiter", true).foreground}}
      end
      local _111_
      do
        vim.api.nvim_set_hl(0, "TSTitle", {background = vim.api.nvim_get_hl_by_name("SpecialKey", true).background, foreground = vim.api.nvim_get_hl_by_name("SpecialKey", true).foreground})
        _111_ = {"TSTitle", {background = vim.api.nvim_get_hl_by_name("SpecialKey", true).background, foreground = vim.api.nvim_get_hl_by_name("SpecialKey", true).foreground}}
      end
      local _112_
      do
        vim.api.nvim_set_hl(0, "TSType", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("cyan/10")})
        _112_ = {"TSType", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("cyan/10")}}
      end
      local _113_
      do
        vim.api.nvim_set_hl(0, "TSTypeBuiltin", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("yellow/5")})
        _113_ = {"TSTypeBuiltin", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("yellow/5")}}
      end
      local _114_
      do
        vim.api.nvim_set_hl(0, "TSURI", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/3"), italic = true})
        _114_ = {"TSURI", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/3"), italic = true}}
      end
      local _115_
      do
        vim.api.nvim_set_hl(0, "TSUnderline", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-"), undercurl = true})
        _115_ = {"TSUnderline", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-"), undercurl = true}}
      end
      local _116_
      do
        vim.api.nvim_set_hl(0, "TSVariable", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Identifier", true).foreground})
        _116_ = {"TSVariable", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Identifier", true).foreground}}
      end
      local _117_
      do
        vim.api.nvim_set_hl(0, "TSVariableBuiltin", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _117_ = {"TSVariableBuiltin", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _118_
      do
        vim.api.nvim_set_hl(0, "@symbol", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/12")})
        _118_ = {"@symbol", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/12")}}
      end
      local _119_
      do
        vim.api.nvim_set_hl(0, "@variable", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _119_ = {"@variable", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _120_
      do
        vim.api.nvim_set_hl(0, "@keyword.repeat", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _120_ = {"@keyword.repeat", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local function _121_()
        vim.api.nvim_set_hl(0, "@keyword.conditional", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Conditional", true).foreground})
        return {"@keyword.conditional", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Conditional", true).foreground}}
      end
      return {_65_, _66_, _67_, _68_, _69_, _70_, _71_, _72_, _73_, _74_, _75_, _76_, _77_, _78_, _79_, _80_, _81_, _82_, _83_, _84_, _85_, _86_, _87_, _88_, _89_, _90_, _91_, _92_, _93_, _94_, _95_, _96_, _97_, _98_, _99_, _100_, _101_, _102_, _103_, _104_, _105_, _106_, _107_, _108_, _109_, _110_, _111_, _112_, _113_, _114_, _115_, _116_, _117_, _118_, _119_, _120_, _121_()}
    end
    _contexttreesitter()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contexttreesitter_ruby()
      local _124_
      do
        vim.api.nvim_set_hl(0, "@function.ruby", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("red/1")})
        _124_ = {"@function.ruby", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("red/1")}}
      end
      local function _125_()
        vim.api.nvim_set_hl(0, "@variable.parameter.ruby", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        return {"@variable.parameter.ruby", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      return {_124_, _125_()}
    end
    _contexttreesitter_ruby()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contexttreesitter_go()
      local _128_
      do
        vim.api.nvim_set_hl(0, "@function.go", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("red/1")})
        _128_ = {"@function.go", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("red/1")}}
      end
      local _129_
      do
        vim.api.nvim_set_hl(0, "@module.go", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = vim.api.nvim_get_hl_by_name("Constant", true).foreground})
        _129_ = {"@module.go", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = vim.api.nvim_get_hl_by_name("Constant", true).foreground}}
      end
      local function _130_()
        vim.api.nvim_set_hl(0, "@function.method.go", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("red/1")})
        return {"@function.method.go", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("red/1")}}
      end
      return {_128_, _129_, _130_()}
    end
    _contexttreesitter_go()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contexttreesitter_fennel()
      local function _133_()
        vim.api.nvim_set_hl(0, "@function.fennel", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("red/1")})
        return {"@function.fennel", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("red/1")}}
      end
      return {_133_()}
    end
    _contexttreesitter_fennel()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contexttreesitter_custom()
      local _136_
      do
        vim.api.nvim_set_hl(0, "@AniseedDefine", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSKeyword", true).foreground})
        _136_ = {"@AniseedDefine", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSKeyword", true).foreground}}
      end
      local _137_
      do
        vim.api.nvim_set_hl(0, "@AniseedImportAlias", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSVariableBuiltin", true).foreground})
        _137_ = {"@AniseedImportAlias", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSVariableBuiltin", true).foreground}}
      end
      local _138_
      do
        vim.api.nvim_set_hl(0, "@AniseedImportLib", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSVariable", true).foreground})
        _138_ = {"@AniseedImportLib", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSVariable", true).foreground}}
      end
      local _139_
      do
        vim.api.nvim_set_hl(0, "@AniseedModule", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSKeyword", true).foreground})
        _139_ = {"@AniseedModule", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSKeyword", true).foreground}}
      end
      local _140_
      do
        vim.api.nvim_set_hl(0, "@AniseedModuleAutoload", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Macro", true).foreground})
        _140_ = {"@AniseedModuleAutoload", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Macro", true).foreground}}
      end
      local _141_
      do
        vim.api.nvim_set_hl(0, "@AniseedModuleMacro", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSVariableBuiltin", true).foreground})
        _141_ = {"@AniseedModuleMacro", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSVariableBuiltin", true).foreground}}
      end
      local _142_
      do
        vim.api.nvim_set_hl(0, "@AniseedModuleName", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSFunction", true).foreground})
        _142_ = {"@AniseedModuleName", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSFunction", true).foreground}}
      end
      local _143_
      do
        vim.api.nvim_set_hl(0, "@AniseedModuleRequireMacros", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Macro", true).foreground})
        _143_ = {"@AniseedModuleRequireMacros", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Macro", true).foreground}}
      end
      local _144_
      do
        vim.api.nvim_set_hl(0, "@RZBeancountAssets", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/15")})
        _144_ = {"@RZBeancountAssets", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/15")}}
      end
      local _145_
      do
        vim.api.nvim_set_hl(0, "@RZBeancountDate", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Macro", true).foreground})
        _145_ = {"@RZBeancountDate", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Macro", true).foreground}}
      end
      local _146_
      do
        vim.api.nvim_set_hl(0, "@RZBeancountExpenses", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")})
        _146_ = {"@RZBeancountExpenses", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")}}
      end
      local _147_
      do
        vim.api.nvim_set_hl(0, "@RZBeancountIncome", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/13")})
        _147_ = {"@RZBeancountIncome", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/13")}}
      end
      local _148_
      do
        vim.api.nvim_set_hl(0, "@RZBeancountLiabilities", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")})
        _148_ = {"@RZBeancountLiabilities", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")}}
      end
      local _149_
      do
        vim.api.nvim_set_hl(0, "@RZBeancountNarration", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground})
        _149_ = {"@RZBeancountNarration", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground}}
      end
      local _150_
      do
        vim.api.nvim_set_hl(0, "@RZBeancountPayee", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _150_ = {"@RZBeancountPayee", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _151_
      do
        vim.api.nvim_set_hl(0, "@RZColorsSpecBorder", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/3|fg/1")})
        _151_ = {"@RZColorsSpecBorder", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/3|fg/1")}}
      end
      local _152_
      do
        vim.api.nvim_set_hl(0, "@RZColorsSpecGroup", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _152_ = {"@RZColorsSpecGroup", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _153_
      do
        vim.api.nvim_set_hl(0, "@RZColorsSpecGroupRef", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")})
        _153_ = {"@RZColorsSpecGroupRef", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")}}
      end
      local _154_
      do
        vim.api.nvim_set_hl(0, "@RZColorsSpecGui", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _154_ = {"@RZColorsSpecGui", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _155_
      do
        vim.api.nvim_set_hl(0, "@RZFennelInterpolation", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")})
        _155_ = {"@RZFennelInterpolation", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")}}
      end
      local _156_
      do
        vim.api.nvim_set_hl(0, "@RZFennelKeymap", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")})
        _156_ = {"@RZFennelKeymap", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")}}
      end
      local _157_
      do
        vim.api.nvim_set_hl(0, "@RZFennelPlugName", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSFunction", true).foreground})
        _157_ = {"@RZFennelPlugName", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSFunction", true).foreground}}
      end
      local _158_
      do
        vim.api.nvim_set_hl(0, "@RZFennelSpecialString", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/14")})
        _158_ = {"@RZFennelSpecialString", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/14")}}
      end
      local _159_
      do
        vim.api.nvim_set_hl(0, "@RZFennelTableKey", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")})
        _159_ = {"@RZFennelTableKey", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")}}
      end
      local _160_
      do
        vim.api.nvim_set_hl(0, "@RZFennelVim", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _160_ = {"@RZFennelVim", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _161_
      do
        vim.api.nvim_set_hl(0, "@RZFennelVimApi", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _161_ = {"@RZFennelVimApi", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _162_
      do
        vim.api.nvim_set_hl(0, "@RZFennelVimApiMethod", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _162_ = {"@RZFennelVimApiMethod", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _163_
      do
        vim.api.nvim_set_hl(0, "@RZFishBackslash", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground})
        _163_ = {"@RZFishBackslash", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground}}
      end
      local _164_
      do
        vim.api.nvim_set_hl(0, "@RZFishCommandArgument", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _164_ = {"@RZFishCommandArgument", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _165_
      do
        vim.api.nvim_set_hl(0, "@RZFishCommandName", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/15")})
        _165_ = {"@RZFishCommandName", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/15")}}
      end
      local _166_
      do
        vim.api.nvim_set_hl(0, "@RZFishFunctionName", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSFunction", true).foreground})
        _166_ = {"@RZFishFunctionName", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSFunction", true).foreground}}
      end
      local _167_
      do
        vim.api.nvim_set_hl(0, "@RZFishTools", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")})
        _167_ = {"@RZFishTools", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")}}
      end
      local _168_
      do
        vim.api.nvim_set_hl(0, "@RZFishVariable", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Identifier", true).foreground})
        _168_ = {"@RZFishVariable", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Identifier", true).foreground}}
      end
      local _169_
      do
        vim.api.nvim_set_hl(0, "@RZFunctionName", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSFunction", true).foreground})
        _169_ = {"@RZFunctionName", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSFunction", true).foreground}}
      end
      local _170_
      do
        vim.api.nvim_set_hl(0, "@RZKeyword", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSKeyword", true).foreground})
        _170_ = {"@RZKeyword", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSKeyword", true).foreground}}
      end
      local _171_
      do
        vim.api.nvim_set_hl(0, "@RZMacro", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Macro", true).foreground})
        _171_ = {"@RZMacro", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Macro", true).foreground}}
      end
      local _172_
      do
        vim.api.nvim_set_hl(0, "@RZRailsDsl", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = vim.api.nvim_get_hl_by_name("TSFuncMacro", true).foreground})
        _172_ = {"@RZRailsDsl", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = vim.api.nvim_get_hl_by_name("TSFuncMacro", true).foreground}}
      end
      local _173_
      do
        vim.api.nvim_set_hl(0, "@RZRailsDslArgument", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSFunction", true).foreground})
        _173_ = {"@RZRailsDslArgument", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSFunction", true).foreground}}
      end
      local _174_
      do
        vim.api.nvim_set_hl(0, "@RZRspecDeclaration", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSKeyword", true).foreground})
        _174_ = {"@RZRspecDeclaration", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSKeyword", true).foreground}}
      end
      local _175_
      do
        vim.api.nvim_set_hl(0, "@RZRspecDeclarationDescribe", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")})
        _175_ = {"@RZRspecDeclarationDescribe", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")}}
      end
      local _176_
      do
        vim.api.nvim_set_hl(0, "@RZRspecGroup", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("magenta/15")})
        _176_ = {"@RZRspecGroup", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("magenta/15")}}
      end
      local _177_
      do
        vim.api.nvim_set_hl(0, "@RZRspecHelper", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("blue/1")})
        _177_ = {"@RZRspecHelper", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("blue/1")}}
      end
      local _178_
      do
        vim.api.nvim_set_hl(0, "@RZRspecHelperName", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")})
        _178_ = {"@RZRspecHelperName", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")}}
      end
      local _179_
      do
        vim.api.nvim_set_hl(0, "@RZRspecHook", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _179_ = {"@RZRspecHook", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _180_
      do
        vim.api.nvim_set_hl(0, "@RZRspecSpec", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("cyan/10")})
        _180_ = {"@RZRspecSpec", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("cyan/10")}}
      end
      local _181_
      do
        vim.api.nvim_set_hl(0, "@RZRubyCall", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _181_ = {"@RZRubyCall", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _182_
      do
        vim.api.nvim_set_hl(0, "@RZRubyDsl", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSKeyword", true).foreground})
        _182_ = {"@RZRubyDsl", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSKeyword", true).foreground}}
      end
      local _183_
      do
        vim.api.nvim_set_hl(0, "@RZRubyBlockParamShort", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")})
        _183_ = {"@RZRubyBlockParamShort", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")}}
      end
      local _184_
      do
        vim.api.nvim_set_hl(0, "@RZRubyBlockParam", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/1")})
        _184_ = {"@RZRubyBlockParam", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/1")}}
      end
      local _185_
      do
        vim.api.nvim_set_hl(0, "@RZRubyHashKeySymbol", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/5")})
        _185_ = {"@RZRubyHashKeySymbol", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/5")}}
      end
      local _186_
      do
        vim.api.nvim_set_hl(0, "@RZRubyInstanceVariable", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")})
        _186_ = {"@RZRubyInstanceVariable", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")}}
      end
      local _187_
      do
        vim.api.nvim_set_hl(0, "@RZSnippet", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")})
        _187_ = {"@RZSnippet", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")}}
      end
      local _188_
      do
        vim.api.nvim_set_hl(0, "@RZVimOption", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSKeyword", true).foreground})
        _188_ = {"@RZVimOption", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSKeyword", true).foreground}}
      end
      local _189_
      do
        vim.api.nvim_set_hl(0, "@RZVimOptionName", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSVariable", true).foreground})
        _189_ = {"@RZVimOptionName", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSVariable", true).foreground}}
      end
      local _190_
      do
        vim.api.nvim_set_hl(0, "@RZVueBuiltin", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSKeyword", true).foreground})
        _190_ = {"@RZVueBuiltin", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSKeyword", true).foreground}}
      end
      local _191_
      do
        vim.api.nvim_set_hl(0, "@RZVueCustomTagName", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSFuncMacro", true).foreground})
        _191_ = {"@RZVueCustomTagName", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSFuncMacro", true).foreground}}
      end
      local _192_
      do
        vim.api.nvim_set_hl(0, "@RZVueScript", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _192_ = {"@RZVueScript", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _193_
      do
        vim.api.nvim_set_hl(0, "@RZVueSpecialElement", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/15")})
        _193_ = {"@RZVueSpecialElement", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/15")}}
      end
      local _194_
      do
        vim.api.nvim_set_hl(0, "@RZVueStyle", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _194_ = {"@RZVueStyle", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _195_
      do
        vim.api.nvim_set_hl(0, "@RZVueTagName", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSIdentifier", true).foreground})
        _195_ = {"@RZVueTagName", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSIdentifier", true).foreground}}
      end
      local function _196_()
        vim.api.nvim_set_hl(0, "@RZVueTemplate", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        return {"@RZVueTemplate", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      return {_136_, _137_, _138_, _139_, _140_, _141_, _142_, _143_, _144_, _145_, _146_, _147_, _148_, _149_, _150_, _151_, _152_, _153_, _154_, _155_, _156_, _157_, _158_, _159_, _160_, _161_, _162_, _163_, _164_, _165_, _166_, _167_, _168_, _169_, _170_, _171_, _172_, _173_, _174_, _175_, _176_, _177_, _178_, _179_, _180_, _181_, _182_, _183_, _184_, _185_, _186_, _187_, _188_, _189_, _190_, _191_, _192_, _193_, _194_, _195_, _196_()}
    end
    _contexttreesitter_custom()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextdiff()
      local _199_
      do
        vim.api.nvim_set_hl(0, "DiffAdd", {background = require("rz.lib.colors")["get-color"]("green/5"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _199_ = {"DiffAdd", {background = require("rz.lib.colors")["get-color"]("green/5"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _200_
      do
        vim.api.nvim_set_hl(0, "DiffChange", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-")})
        _200_ = {"DiffChange", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _201_
      do
        vim.api.nvim_set_hl(0, "DiffDelete", {background = require("rz.lib.colors")["get-color"]("red/5"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _201_ = {"DiffDelete", {background = require("rz.lib.colors")["get-color"]("red/5"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _202_
      do
        vim.api.nvim_set_hl(0, "DiffText", {background = require("rz.lib.colors")["get-color"]("blue/5"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _202_ = {"DiffText", {background = require("rz.lib.colors")["get-color"]("blue/5"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _203_
      do
        vim.api.nvim_set_hl(0, "DiffConflictStartMarker", {background = require("rz.lib.colors")["get-color"]("yellow/5"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _203_ = {"DiffConflictStartMarker", {background = require("rz.lib.colors")["get-color"]("yellow/5"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _204_
      do
        vim.api.nvim_set_hl(0, "DiffConflictEndMarker", {background = require("rz.lib.colors")["get-color"]("yellow/5"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _204_ = {"DiffConflictEndMarker", {background = require("rz.lib.colors")["get-color"]("yellow/5"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _205_
      do
        vim.api.nvim_set_hl(0, "DiffConflictParentMarker", {background = require("rz.lib.colors")["get-color"]("yellow/12"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _205_ = {"DiffConflictParentMarker", {background = require("rz.lib.colors")["get-color"]("yellow/12"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local function _206_()
        vim.api.nvim_set_hl(0, "DiffConflictSeparatorMarker", {background = require("rz.lib.colors")["get-color"]("yellow/12"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        return {"DiffConflictSeparatorMarker", {background = require("rz.lib.colors")["get-color"]("yellow/12"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      return {_199_, _200_, _201_, _202_, _203_, _204_, _205_, _206_()}
    end
    _contextdiff()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextdiagnostics()
      local _209_
      do
        vim.api.nvim_set_hl(0, "DiagnosticError", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("ErrorMsg", true).foreground})
        _209_ = {"DiagnosticError", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("ErrorMsg", true).foreground}}
      end
      local _210_
      do
        vim.api.nvim_set_hl(0, "DiagnosticWarn", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("WarningMsg", true).foreground})
        _210_ = {"DiagnosticWarn", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("WarningMsg", true).foreground}}
      end
      local _211_
      do
        vim.api.nvim_set_hl(0, "DiagnosticInfo", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground})
        _211_ = {"DiagnosticInfo", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground}}
      end
      local _212_
      do
        vim.api.nvim_set_hl(0, "DiagnosticHint", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground})
        _212_ = {"DiagnosticHint", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground}}
      end
      local _213_
      do
        vim.api.nvim_set_hl(0, "DiagnosticVirtualTextError", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("DiagnosticError", true).foreground})
        _213_ = {"DiagnosticVirtualTextError", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("DiagnosticError", true).foreground}}
      end
      local _214_
      do
        vim.api.nvim_set_hl(0, "DiagnosticVirtualTextWarn", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("DiagnosticWarn", true).foreground})
        _214_ = {"DiagnosticVirtualTextWarn", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("DiagnosticWarn", true).foreground}}
      end
      local _215_
      do
        vim.api.nvim_set_hl(0, "DiagnosticVirtualTextInfo", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("DiagnosticInfo", true).foreground})
        _215_ = {"DiagnosticVirtualTextInfo", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("DiagnosticInfo", true).foreground}}
      end
      local _216_
      do
        vim.api.nvim_set_hl(0, "DiagnosticVirtualTextHint", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("DiagnosticHint", true).foreground})
        _216_ = {"DiagnosticVirtualTextHint", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("DiagnosticHint", true).foreground}}
      end
      local _217_
      do
        vim.api.nvim_set_hl(0, "DiagnosticUnderlineError", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-"), undercurl = true})
        _217_ = {"DiagnosticUnderlineError", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-"), undercurl = true}}
      end
      local _218_
      do
        vim.api.nvim_set_hl(0, "DiagnosticUnderlineWarn", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-"), undercurl = true})
        _218_ = {"DiagnosticUnderlineWarn", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-"), undercurl = true}}
      end
      local _219_
      do
        vim.api.nvim_set_hl(0, "DiagnosticUnderlineInfo", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-"), undercurl = true})
        _219_ = {"DiagnosticUnderlineInfo", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-"), undercurl = true}}
      end
      local function _220_()
        vim.api.nvim_set_hl(0, "DiagnosticUnderlineHint", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-"), undercurl = true})
        return {"DiagnosticUnderlineHint", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-"), undercurl = true}}
      end
      return {_209_, _210_, _211_, _212_, _213_, _214_, _215_, _216_, _217_, _218_, _219_, _220_()}
    end
    _contextdiagnostics()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextstatusline()
      local _223_
      do
        vim.api.nvim_set_hl(0, "StatusLinePillLeftBorder", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/6")})
        _223_ = {"StatusLinePillLeftBorder", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/6")}}
      end
      local _224_
      do
        vim.api.nvim_set_hl(0, "StatusLinePillLeft", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _224_ = {"StatusLinePillLeft", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _225_
      do
        vim.api.nvim_set_hl(0, "StatusLinePillSeparator", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("bg/6")})
        _225_ = {"StatusLinePillSeparator", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("bg/6")}}
      end
      local _226_
      do
        vim.api.nvim_set_hl(0, "StatusLinePillRight", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _226_ = {"StatusLinePillRight", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _227_
      do
        vim.api.nvim_set_hl(0, "StatusLinePillRightBorder", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _227_ = {"StatusLinePillRightBorder", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _228_
      do
        vim.api.nvim_set_hl(0, "StatusLine", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _228_ = {"StatusLine", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _229_
      do
        vim.api.nvim_set_hl(0, "StatusLineAutowrite", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _229_ = {"StatusLineAutowrite", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _230_
      do
        vim.api.nvim_set_hl(0, "StatusLineAutowriteLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _230_ = {"StatusLineAutowriteLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _231_
      do
        vim.api.nvim_set_hl(0, "StatusLineAutowriteRight", {background = vim.api.nvim_get_hl_by_name("StatusLineAutowriteLeft", true).background, foreground = vim.api.nvim_get_hl_by_name("StatusLineAutowriteLeft", true).foreground})
        _231_ = {"StatusLineAutowriteRight", {background = vim.api.nvim_get_hl_by_name("StatusLineAutowriteLeft", true).background, foreground = vim.api.nvim_get_hl_by_name("StatusLineAutowriteLeft", true).foreground}}
      end
      local _232_
      do
        vim.api.nvim_set_hl(0, "StatusLineBranch", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _232_ = {"StatusLineBranch", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _233_
      do
        vim.api.nvim_set_hl(0, "StatusLineBranchLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _233_ = {"StatusLineBranchLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _234_
      do
        vim.api.nvim_set_hl(0, "StatusLineBranchRight", {background = vim.api.nvim_get_hl_by_name("StatusLineBranchLeft", true).background, foreground = vim.api.nvim_get_hl_by_name("StatusLineBranchLeft", true).foreground})
        _234_ = {"StatusLineBranchRight", {background = vim.api.nvim_get_hl_by_name("StatusLineBranchLeft", true).background, foreground = vim.api.nvim_get_hl_by_name("StatusLineBranchLeft", true).foreground}}
      end
      local _235_
      do
        vim.api.nvim_set_hl(0, "StatusLineCommand", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _235_ = {"StatusLineCommand", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _236_
      do
        vim.api.nvim_set_hl(0, "StatusLineCommandLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _236_ = {"StatusLineCommandLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _237_
      do
        vim.api.nvim_set_hl(0, "StatusLineCommandRight", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = vim.api.nvim_get_hl_by_name("StatusLineCommandLeft", true).foreground})
        _237_ = {"StatusLineCommandRight", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = vim.api.nvim_get_hl_by_name("StatusLineCommandLeft", true).foreground}}
      end
      local _238_
      do
        vim.api.nvim_set_hl(0, "StatusLineDap", {background = require("rz.lib.colors")["get-color"]("cyan/8"), foreground = require("rz.lib.colors")["get-color"]("black/8")})
        _238_ = {"StatusLineDap", {background = require("rz.lib.colors")["get-color"]("cyan/8"), foreground = require("rz.lib.colors")["get-color"]("black/8")}}
      end
      local _239_
      do
        vim.api.nvim_set_hl(0, "StatusLineDapLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")})
        _239_ = {"StatusLineDapLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")}}
      end
      local _240_
      do
        vim.api.nvim_set_hl(0, "StatusLineDapRight", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")})
        _240_ = {"StatusLineDapRight", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")}}
      end
      local _241_
      do
        vim.api.nvim_set_hl(0, "StatusLineFileType", {background = require("rz.lib.colors")["get-color"]("cyan/13"), foreground = require("rz.lib.colors")["get-color"]("white/8")})
        _241_ = {"StatusLineFileType", {background = require("rz.lib.colors")["get-color"]("cyan/13"), foreground = require("rz.lib.colors")["get-color"]("white/8")}}
      end
      local _242_
      do
        vim.api.nvim_set_hl(0, "StatusLineFileTypeLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("cyan/13")})
        _242_ = {"StatusLineFileTypeLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("cyan/13")}}
      end
      local _243_
      do
        vim.api.nvim_set_hl(0, "StatusLineFileTypeRight", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("cyan/13")})
        _243_ = {"StatusLineFileTypeRight", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("cyan/13")}}
      end
      local _244_
      do
        vim.api.nvim_set_hl(0, "StatusLineFileTypeRightAlt", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("cyan/13")})
        _244_ = {"StatusLineFileTypeRightAlt", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("cyan/13")}}
      end
      local _245_
      do
        vim.api.nvim_set_hl(0, "StatusLineLineInfo", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _245_ = {"StatusLineLineInfo", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _246_
      do
        vim.api.nvim_set_hl(0, "StatusLineLineInfoLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/6")})
        _246_ = {"StatusLineLineInfoLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/6")}}
      end
      local _247_
      do
        vim.api.nvim_set_hl(0, "StatusLineLinePercent", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _247_ = {"StatusLineLinePercent", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _248_
      do
        vim.api.nvim_set_hl(0, "StatusLineLinePercentLeft", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _248_ = {"StatusLineLinePercentLeft", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _249_
      do
        vim.api.nvim_set_hl(0, "StatusLineLinePercentRight", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = vim.api.nvim_get_hl_by_name("StatusLineLinePercentLeft", true).foreground})
        _249_ = {"StatusLineLinePercentRight", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = vim.api.nvim_get_hl_by_name("StatusLineLinePercentLeft", true).foreground}}
      end
      local _250_
      do
        vim.api.nvim_set_hl(0, "StatusLineLsp", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _250_ = {"StatusLineLsp", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _251_
      do
        vim.api.nvim_set_hl(0, "StatusLineLspLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _251_ = {"StatusLineLspLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _252_
      do
        vim.api.nvim_set_hl(0, "StatusLineLspRight", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _252_ = {"StatusLineLspRight", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _253_
      do
        vim.api.nvim_set_hl(0, "StatusLineSession", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _253_ = {"StatusLineSession", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _254_
      do
        vim.api.nvim_set_hl(0, "StatusLineSessionLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _254_ = {"StatusLineSessionLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _255_
      do
        vim.api.nvim_set_hl(0, "StatusLineSessionRight", {background = vim.api.nvim_get_hl_by_name("StatusLineSessionLeft", true).background, foreground = vim.api.nvim_get_hl_by_name("StatusLineSessionLeft", true).foreground})
        _255_ = {"StatusLineSessionRight", {background = vim.api.nvim_get_hl_by_name("StatusLineSessionLeft", true).background, foreground = vim.api.nvim_get_hl_by_name("StatusLineSessionLeft", true).foreground}}
      end
      local _256_
      do
        vim.api.nvim_set_hl(0, "StatusLineTest", {background = require("rz.lib.colors")["get-color"]("magenta/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _256_ = {"StatusLineTest", {background = require("rz.lib.colors")["get-color"]("magenta/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _257_
      do
        vim.api.nvim_set_hl(0, "StatusLineTestLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")})
        _257_ = {"StatusLineTestLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")}}
      end
      local _258_
      do
        vim.api.nvim_set_hl(0, "StatusLineTestRight", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")})
        _258_ = {"StatusLineTestRight", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")}}
      end
      local _259_
      do
        vim.api.nvim_set_hl(0, "StatusLineRecording", {background = require("rz.lib.colors")["get-color"]("red/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _259_ = {"StatusLineRecording", {background = require("rz.lib.colors")["get-color"]("red/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _260_
      do
        vim.api.nvim_set_hl(0, "StatusLineRecordingLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("red/8")})
        _260_ = {"StatusLineRecordingLeft", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("red/8")}}
      end
      local _261_
      do
        vim.api.nvim_set_hl(0, "StatusLineRecordingRight", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("red/8")})
        _261_ = {"StatusLineRecordingRight", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("red/8")}}
      end
      local _262_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeLeftCommand", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("red/7")})
        _262_ = {"StatusLineViModeLeftCommand", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("red/7")}}
      end
      local _263_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeLeftInsert", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("yellow/5")})
        _263_ = {"StatusLineViModeLeftInsert", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("yellow/5")}}
      end
      local _264_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeLeftNormal", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/6")})
        _264_ = {"StatusLineViModeLeftNormal", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/6")}}
      end
      local _265_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeLeftReplace", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("green/5")})
        _265_ = {"StatusLineViModeLeftReplace", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("green/5")}}
      end
      local _266_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeLeftSelect", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("magenta/5")})
        _266_ = {"StatusLineViModeLeftSelect", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("magenta/5")}}
      end
      local _267_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeLeftTerminal", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("cyan/5")})
        _267_ = {"StatusLineViModeLeftTerminal", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("cyan/5")}}
      end
      local _268_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeLeftVisual", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("blue/7")})
        _268_ = {"StatusLineViModeLeftVisual", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("blue/7")}}
      end
      local _269_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeMiddleCommand", {background = require("rz.lib.colors")["get-color"]("red/7"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _269_ = {"StatusLineViModeMiddleCommand", {background = require("rz.lib.colors")["get-color"]("red/7"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _270_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeMiddleInsert", {background = require("rz.lib.colors")["get-color"]("yellow/5"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _270_ = {"StatusLineViModeMiddleInsert", {background = require("rz.lib.colors")["get-color"]("yellow/5"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _271_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeMiddleNormal", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _271_ = {"StatusLineViModeMiddleNormal", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _272_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeMiddleReplace", {background = require("rz.lib.colors")["get-color"]("green/5"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _272_ = {"StatusLineViModeMiddleReplace", {background = require("rz.lib.colors")["get-color"]("green/5"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _273_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeMiddleSelect", {background = require("rz.lib.colors")["get-color"]("magenta/5"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _273_ = {"StatusLineViModeMiddleSelect", {background = require("rz.lib.colors")["get-color"]("magenta/5"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _274_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeMiddleTerminal", {background = require("rz.lib.colors")["get-color"]("cyan/5"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _274_ = {"StatusLineViModeMiddleTerminal", {background = require("rz.lib.colors")["get-color"]("cyan/5"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _275_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeMiddleVisual", {background = require("rz.lib.colors")["get-color"]("blue/7"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _275_ = {"StatusLineViModeMiddleVisual", {background = require("rz.lib.colors")["get-color"]("blue/7"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _276_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeRightCommand", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = vim.api.nvim_get_hl_by_name("StatusLineViModeLeftCommand", true).foreground})
        _276_ = {"StatusLineViModeRightCommand", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = vim.api.nvim_get_hl_by_name("StatusLineViModeLeftCommand", true).foreground}}
      end
      local _277_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeRightInsert", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = vim.api.nvim_get_hl_by_name("StatusLineViModeLeftInsert", true).foreground})
        _277_ = {"StatusLineViModeRightInsert", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = vim.api.nvim_get_hl_by_name("StatusLineViModeLeftInsert", true).foreground}}
      end
      local _278_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeRightNormal", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = vim.api.nvim_get_hl_by_name("StatusLineViModeLeftNormal", true).foreground})
        _278_ = {"StatusLineViModeRightNormal", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = vim.api.nvim_get_hl_by_name("StatusLineViModeLeftNormal", true).foreground}}
      end
      local _279_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeRightReplace", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = vim.api.nvim_get_hl_by_name("StatusLineViModeLeftReplace", true).foreground})
        _279_ = {"StatusLineViModeRightReplace", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = vim.api.nvim_get_hl_by_name("StatusLineViModeLeftReplace", true).foreground}}
      end
      local _280_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeRightSelect", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = vim.api.nvim_get_hl_by_name("StatusLineViModeLeftSelect", true).foreground})
        _280_ = {"StatusLineViModeRightSelect", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = vim.api.nvim_get_hl_by_name("StatusLineViModeLeftSelect", true).foreground}}
      end
      local _281_
      do
        vim.api.nvim_set_hl(0, "StatusLineViModeRightTerminal", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = vim.api.nvim_get_hl_by_name("StatusLineViModeLeftTerminal", true).foreground})
        _281_ = {"StatusLineViModeRightTerminal", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = vim.api.nvim_get_hl_by_name("StatusLineViModeLeftTerminal", true).foreground}}
      end
      local function _282_()
        vim.api.nvim_set_hl(0, "StatusLineViModeRightVisual", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = vim.api.nvim_get_hl_by_name("StatusLineViModeLeftVisual", true).foreground})
        return {"StatusLineViModeRightVisual", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = vim.api.nvim_get_hl_by_name("StatusLineViModeLeftVisual", true).foreground}}
      end
      return {_223_, _224_, _225_, _226_, _227_, _228_, _229_, _230_, _231_, _232_, _233_, _234_, _235_, _236_, _237_, _238_, _239_, _240_, _241_, _242_, _243_, _244_, _245_, _246_, _247_, _248_, _249_, _250_, _251_, _252_, _253_, _254_, _255_, _256_, _257_, _258_, _259_, _260_, _261_, _262_, _263_, _264_, _265_, _266_, _267_, _268_, _269_, _270_, _271_, _272_, _273_, _274_, _275_, _276_, _277_, _278_, _279_, _280_, _281_, _282_()}
    end
    _contextstatusline()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contexttabline()
      local _285_
      do
        vim.api.nvim_set_hl(0, "TabLine", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _285_ = {"TabLine", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _286_
      do
        vim.api.nvim_set_hl(0, "TabLineFill", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _286_ = {"TabLineFill", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _287_
      do
        vim.api.nvim_set_hl(0, "TabLineSel", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _287_ = {"TabLineSel", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _288_
      do
        vim.api.nvim_set_hl(0, "TabLineBorder", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _288_ = {"TabLineBorder", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _289_
      do
        vim.api.nvim_set_hl(0, "TabLineTabLeftBorder", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/4")})
        _289_ = {"TabLineTabLeftBorder", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/4")}}
      end
      local _290_
      do
        vim.api.nvim_set_hl(0, "TabLineTabLeftBorderSel", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _290_ = {"TabLineTabLeftBorderSel", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _291_
      do
        vim.api.nvim_set_hl(0, "TabLineTabRightBorder", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/6")})
        _291_ = {"TabLineTabRightBorder", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/6")}}
      end
      local _292_
      do
        vim.api.nvim_set_hl(0, "TabLineTabRightBorderSel", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/6")})
        _292_ = {"TabLineTabRightBorderSel", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/6")}}
      end
      local _293_
      do
        vim.api.nvim_set_hl(0, "TabLineNumberSel", {background = require("rz.lib.colors")["get-color"]("blue/8"), bold = true, foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _293_ = {"TabLineNumberSel", {background = require("rz.lib.colors")["get-color"]("blue/8"), bold = true, foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _294_
      do
        vim.api.nvim_set_hl(0, "TabLineNumberPreviousSel", {background = require("rz.lib.colors")["get-color"]("bg/4"), bold = true, foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _294_ = {"TabLineNumberPreviousSel", {background = require("rz.lib.colors")["get-color"]("bg/4"), bold = true, foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _295_
      do
        vim.api.nvim_set_hl(0, "TabLineNumber", {background = require("rz.lib.colors")["get-color"]("bg/4"), foreground = require("rz.lib.colors")["get-color"]("fg/1")})
        _295_ = {"TabLineNumber", {background = require("rz.lib.colors")["get-color"]("bg/4"), foreground = require("rz.lib.colors")["get-color"]("fg/1")}}
      end
      local _296_
      do
        vim.api.nvim_set_hl(0, "TabLineNumberSeparatorSel", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _296_ = {"TabLineNumberSeparatorSel", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local function _297_()
        vim.api.nvim_set_hl(0, "TabLineNumberSeparator", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("bg/4")})
        return {"TabLineNumberSeparator", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("bg/4")}}
      end
      return {_285_, _286_, _287_, _288_, _289_, _290_, _291_, _292_, _293_, _294_, _295_, _296_, _297_()}
    end
    _contexttabline()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextwinbar()
      local _300_
      do
        vim.api.nvim_set_hl(0, "WinBar", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("blue/5")})
        _300_ = {"WinBar", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = require("rz.lib.colors")["get-color"]("blue/5")}}
      end
      local function _301_()
        vim.api.nvim_set_hl(0, "WinBarNC", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground})
        return {"WinBarNC", {background = require("rz.lib.colors")["get-color"]("-"), bold = true, foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground}}
      end
      return {_300_, _301_()}
    end
    _contextwinbar()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextfloat_window()
      local _304_
      do
        vim.api.nvim_set_hl(0, "NormalFloat", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _304_ = {"NormalFloat", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local function _305_()
        vim.api.nvim_set_hl(0, "FloatBorder", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        return {"FloatBorder", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      return {_304_, _305_()}
    end
    _contextfloat_window()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextpopup()
      local _308_
      do
        vim.api.nvim_set_hl(0, "Pmenu", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _308_ = {"Pmenu", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _309_
      do
        vim.api.nvim_set_hl(0, "PmenuSel", {background = require("rz.lib.colors")["get-color"]("bg/3"), foreground = require("rz.lib.colors")["get-color"]("-")})
        _309_ = {"PmenuSel", {background = require("rz.lib.colors")["get-color"]("bg/3"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _310_
      do
        vim.api.nvim_set_hl(0, "PmenuSbar", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("bg/6")})
        _310_ = {"PmenuSbar", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("bg/6")}}
      end
      local function _311_()
        vim.api.nvim_set_hl(0, "PmenuThumb", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("-")})
        return {"PmenuThumb", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      return {_308_, _309_, _310_, _311_()}
    end
    _contextpopup()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextgit()
      local _314_
      do
        vim.api.nvim_set_hl(0, "gitcommitOverflow", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")})
        _314_ = {"gitcommitOverflow", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")}}
      end
      local _315_
      do
        vim.api.nvim_set_hl(0, "gitcommitSummary", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/8")})
        _315_ = {"gitcommitSummary", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/8")}}
      end
      local _316_
      do
        vim.api.nvim_set_hl(0, "gitcommitComment", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground})
        _316_ = {"gitcommitComment", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground}}
      end
      local _317_
      do
        vim.api.nvim_set_hl(0, "gitcommitUntracked", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/5")})
        _317_ = {"gitcommitUntracked", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/5")}}
      end
      local _318_
      do
        vim.api.nvim_set_hl(0, "gitcommitDiscarded", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/5")})
        _318_ = {"gitcommitDiscarded", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/5")}}
      end
      local _319_
      do
        vim.api.nvim_set_hl(0, "gitcommitSelected", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/5")})
        _319_ = {"gitcommitSelected", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/5")}}
      end
      local _320_
      do
        vim.api.nvim_set_hl(0, "gitcommitHeader", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")})
        _320_ = {"gitcommitHeader", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")}}
      end
      local _321_
      do
        vim.api.nvim_set_hl(0, "gitcommitSelectedType", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _321_ = {"gitcommitSelectedType", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _322_
      do
        vim.api.nvim_set_hl(0, "gitcommitUnmergedType", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _322_ = {"gitcommitUnmergedType", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _323_
      do
        vim.api.nvim_set_hl(0, "gitcommitDiscardedType", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _323_ = {"gitcommitDiscardedType", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _324_
      do
        vim.api.nvim_set_hl(0, "gitcommitBranch", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _324_ = {"gitcommitBranch", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _325_
      do
        vim.api.nvim_set_hl(0, "gitcommitUntrackedFile", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _325_ = {"gitcommitUntrackedFile", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _326_
      do
        vim.api.nvim_set_hl(0, "gitcommitUnmergedFile", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")})
        _326_ = {"gitcommitUnmergedFile", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")}}
      end
      local _327_
      do
        vim.api.nvim_set_hl(0, "gitcommitDiscardedFile", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")})
        _327_ = {"gitcommitDiscardedFile", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/8")}}
      end
      local function _328_()
        vim.api.nvim_set_hl(0, "gitcommitSelectedFile", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/8")})
        return {"gitcommitSelectedFile", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/8")}}
      end
      return {_314_, _315_, _316_, _317_, _318_, _319_, _320_, _321_, _322_, _323_, _324_, _325_, _326_, _327_, _328_()}
    end
    _contextgit()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextlinear()
      local _331_
      do
        vim.api.nvim_set_hl(0, "LinearIdentifier", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/7")})
        _331_ = {"LinearIdentifier", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/7")}}
      end
      local _332_
      do
        vim.api.nvim_set_hl(0, "LinearStateCompleted", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/5")})
        _332_ = {"LinearStateCompleted", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/5")}}
      end
      local _333_
      do
        vim.api.nvim_set_hl(0, "LinearStateUnstarted", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/5")})
        _333_ = {"LinearStateUnstarted", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/5")}}
      end
      local function _334_()
        vim.api.nvim_set_hl(0, "LinearStateStarted", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/5")})
        return {"LinearStateStarted", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/5")}}
      end
      return {_331_, _332_, _333_, _334_()}
    end
    _contextlinear()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextdirbuf()
      local _337_
      do
        vim.api.nvim_set_hl(0, "DirbufHash", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground, italic = true})
        _337_ = {"DirbufHash", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground, italic = true}}
      end
      local function _338_()
        vim.api.nvim_set_hl(0, "DirbufDirectory", {background = vim.api.nvim_get_hl_by_name("Directory", true).background, bold = true, foreground = vim.api.nvim_get_hl_by_name("Directory", true).foreground})
        return {"DirbufDirectory", {background = vim.api.nvim_get_hl_by_name("Directory", true).background, bold = true, foreground = vim.api.nvim_get_hl_by_name("Directory", true).foreground}}
      end
      return {_337_, _338_()}
    end
    _contextdirbuf()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contexttelescope()
      local _341_
      do
        vim.api.nvim_set_hl(0, "TelescopeBorder", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _341_ = {"TelescopeBorder", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _342_
      do
        vim.api.nvim_set_hl(0, "TelescopeNormal", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("-")})
        _342_ = {"TelescopeNormal", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _343_
      do
        vim.api.nvim_set_hl(0, "TelescopePromptNormal", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("-")})
        _343_ = {"TelescopePromptNormal", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _344_
      do
        vim.api.nvim_set_hl(0, "TelescopePromptCounter", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _344_ = {"TelescopePromptCounter", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _345_
      do
        vim.api.nvim_set_hl(0, "TelescopePreviewNormal", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("-")})
        _345_ = {"TelescopePreviewNormal", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _346_
      do
        vim.api.nvim_set_hl(0, "TelescopePreviewBorder", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _346_ = {"TelescopePreviewBorder", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _347_
      do
        vim.api.nvim_set_hl(0, "TelescopePreviewTitle", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _347_ = {"TelescopePreviewTitle", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _348_
      do
        vim.api.nvim_set_hl(0, "TelescopeResultsNormal", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("-")})
        _348_ = {"TelescopeResultsNormal", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _349_
      do
        vim.api.nvim_set_hl(0, "TelescopeSelection", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _349_ = {"TelescopeSelection", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _350_
      do
        vim.api.nvim_set_hl(0, "TelescopeSelectionCaret", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _350_ = {"TelescopeSelectionCaret", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _351_
      do
        vim.api.nvim_set_hl(0, "TelescopePromptPrefix", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _351_ = {"TelescopePromptPrefix", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local function _352_()
        vim.api.nvim_set_hl(0, "TelescopeMatching", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        return {"TelescopeMatching", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      return {_341_, _342_, _343_, _344_, _345_, _346_, _347_, _348_, _349_, _350_, _351_, _352_()}
    end
    _contexttelescope()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextgitsigns()
      local _355_
      do
        vim.api.nvim_set_hl(0, "GitSignsAdd", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/5")})
        _355_ = {"GitSignsAdd", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/5")}}
      end
      local _356_
      do
        vim.api.nvim_set_hl(0, "GitSignsDelete", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/5")})
        _356_ = {"GitSignsDelete", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/5")}}
      end
      local _357_
      do
        vim.api.nvim_set_hl(0, "GitSignsChange", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/5")})
        _357_ = {"GitSignsChange", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/5")}}
      end
      local function _358_()
        vim.api.nvim_set_hl(0, "GitSignsUntracked", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/5")})
        return {"GitSignsUntracked", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/5")}}
      end
      return {_355_, _356_, _357_, _358_()}
    end
    _contextgitsigns()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextnvim_cmp()
      local _361_
      do
        vim.api.nvim_set_hl(0, "CmpItemAbbr", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _361_ = {"CmpItemAbbr", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _362_
      do
        vim.api.nvim_set_hl(0, "CmpItemAbbrDeprecated", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/5"), strikethrough = true})
        _362_ = {"CmpItemAbbrDeprecated", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/5"), strikethrough = true}}
      end
      local _363_
      do
        vim.api.nvim_set_hl(0, "CmpItemAbbrMatch", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("WildMenu", true).foreground})
        _363_ = {"CmpItemAbbrMatch", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("WildMenu", true).foreground}}
      end
      local _364_
      do
        vim.api.nvim_set_hl(0, "CmpItemAbbrMatchFuzzy", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("WildMenu", true).foreground})
        _364_ = {"CmpItemAbbrMatchFuzzy", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("WildMenu", true).foreground}}
      end
      local _365_
      do
        vim.api.nvim_set_hl(0, "CmpItemKindVariable", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/5")})
        _365_ = {"CmpItemKindVariable", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/5")}}
      end
      local _366_
      do
        vim.api.nvim_set_hl(0, "CmpItemKindInterface", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/5")})
        _366_ = {"CmpItemKindInterface", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/5")}}
      end
      local _367_
      do
        vim.api.nvim_set_hl(0, "CmpItemKindText", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/5")})
        _367_ = {"CmpItemKindText", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/5")}}
      end
      local _368_
      do
        vim.api.nvim_set_hl(0, "CmpItemKindFunction", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/5")})
        _368_ = {"CmpItemKindFunction", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/5")}}
      end
      local _369_
      do
        vim.api.nvim_set_hl(0, "CmpItemKindMethod", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/5")})
        _369_ = {"CmpItemKindMethod", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/5")}}
      end
      local _370_
      do
        vim.api.nvim_set_hl(0, "CmpItemKindClass", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/5")})
        _370_ = {"CmpItemKindClass", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/5")}}
      end
      local _371_
      do
        vim.api.nvim_set_hl(0, "CmpItemKindModule", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/5")})
        _371_ = {"CmpItemKindModule", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/5")}}
      end
      local function _372_()
        vim.api.nvim_set_hl(0, "CmpItemKindSnippet", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/5")})
        return {"CmpItemKindSnippet", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/5")}}
      end
      return {_361_, _362_, _363_, _364_, _365_, _366_, _367_, _368_, _369_, _370_, _371_, _372_()}
    end
    _contextnvim_cmp()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextvim_notify()
      local _375_
      do
        vim.api.nvim_set_hl(0, "NotifyERRORBody", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("red/8")})
        _375_ = {"NotifyERRORBody", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("red/8")}}
      end
      local _376_
      do
        vim.api.nvim_set_hl(0, "NotifyERRORBorder", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("bg/6")})
        _376_ = {"NotifyERRORBorder", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("bg/6")}}
      end
      local _377_
      do
        vim.api.nvim_set_hl(0, "NotifyERRORIcon", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("red/8")})
        _377_ = {"NotifyERRORIcon", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("red/8")}}
      end
      local _378_
      do
        vim.api.nvim_set_hl(0, "NotifyERRORTitle", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("red/8")})
        _378_ = {"NotifyERRORTitle", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("red/8")}}
      end
      local _379_
      do
        vim.api.nvim_set_hl(0, "NotifyINFOBody", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _379_ = {"NotifyINFOBody", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _380_
      do
        vim.api.nvim_set_hl(0, "NotifyINFOBorder", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("bg/6")})
        _380_ = {"NotifyINFOBorder", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("bg/6")}}
      end
      local _381_
      do
        vim.api.nvim_set_hl(0, "NotifyINFOIcon", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _381_ = {"NotifyINFOIcon", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _382_
      do
        vim.api.nvim_set_hl(0, "NotifyINFOTitle", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _382_ = {"NotifyINFOTitle", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _383_
      do
        vim.api.nvim_set_hl(0, "NotifyWARNBody", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _383_ = {"NotifyWARNBody", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _384_
      do
        vim.api.nvim_set_hl(0, "NotifyWARNBorder", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("bg/6")})
        _384_ = {"NotifyWARNBorder", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("bg/6")}}
      end
      local _385_
      do
        vim.api.nvim_set_hl(0, "NotifyWARNIcon", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _385_ = {"NotifyWARNIcon", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local function _386_()
        vim.api.nvim_set_hl(0, "NotifyWARNTitle", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        return {"NotifyWARNTitle", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      return {_375_, _376_, _377_, _378_, _379_, _380_, _381_, _382_, _383_, _384_, _385_, _386_()}
    end
    _contextvim_notify()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextdiffview()
      local _389_
      do
        vim.api.nvim_set_hl(0, "DiffviewFilePanelTitle", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/7")})
        _389_ = {"DiffviewFilePanelTitle", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/7")}}
      end
      local function _390_()
        vim.api.nvim_set_hl(0, "DiffviewFilePanelCounter", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/3")})
        return {"DiffviewFilePanelCounter", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/3")}}
      end
      return {_389_, _390_()}
    end
    _contextdiffview()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextlsp()
      local _393_
      do
        vim.api.nvim_set_hl(0, "LspReferenceText", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("-")})
        _393_ = {"LspReferenceText", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      local _394_
      do
        vim.api.nvim_set_hl(0, "LspReferenceRead", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-"), underline = true})
        _394_ = {"LspReferenceRead", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("-"), underline = true}}
      end
      local _395_
      do
        vim.api.nvim_set_hl(0, "LspReferenceWrite", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("-"), italic = true})
        _395_ = {"LspReferenceWrite", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("-"), italic = true}}
      end
      local _396_
      do
        vim.api.nvim_set_hl(0, "@lsp.type.namespace.ruby", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Constant", true).foreground})
        _396_ = {"@lsp.type.namespace.ruby", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Constant", true).foreground}}
      end
      local function _397_()
        vim.api.nvim_set_hl(0, "@lsp.type.parameter.ruby", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        return {"@lsp.type.parameter.ruby", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      return {_393_, _394_, _395_, _396_, _397_()}
    end
    _contextlsp()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextnoice()
      local _400_
      do
        vim.api.nvim_set_hl(0, "NoiceLspProgressClient", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/5")})
        _400_ = {"NoiceLspProgressClient", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/5")}}
      end
      local _401_
      do
        vim.api.nvim_set_hl(0, "NoiceLspProgressSpinner", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/5")})
        _401_ = {"NoiceLspProgressSpinner", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("red/5")}}
      end
      local _402_
      do
        vim.api.nvim_set_hl(0, "NoiceLspProgressTitle", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/5")})
        _402_ = {"NoiceLspProgressTitle", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/5")}}
      end
      local function _403_()
        vim.api.nvim_set_hl(0, "NoiceMini", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("-")})
        return {"NoiceMini", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("-")}}
      end
      return {_400_, _401_, _402_, _403_()}
    end
    _contextnoice()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextneotest()
      local _406_
      do
        vim.api.nvim_set_hl(0, "NeotestPassed", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("green/8")})
        _406_ = {"NeotestPassed", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("green/8")}}
      end
      local _407_
      do
        vim.api.nvim_set_hl(0, "NeotestRunning", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _407_ = {"NeotestRunning", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _408_
      do
        vim.api.nvim_set_hl(0, "NeotestFailed", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("red/8")})
        _408_ = {"NeotestFailed", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("red/8")}}
      end
      local function _409_()
        vim.api.nvim_set_hl(0, "NeotestSkipped", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/6")})
        return {"NeotestSkipped", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/6")}}
      end
      return {_406_, _407_, _408_, _409_()}
    end
    _contextneotest()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextmarks()
      local function _412_()
        vim.api.nvim_set_hl(0, "MarkSignHL", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/5")})
        return {"MarkSignHL", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/5")}}
      end
      return {_412_()}
    end
    _contextmarks()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextwhich_key()
      local function _415_()
        vim.api.nvim_set_hl(0, "WhichKeyBorder", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        return {"WhichKeyBorder", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      return {_415_()}
    end
    _contextwhich_key()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextyanky()
      local _418_
      do
        vim.api.nvim_set_hl(0, "YankyPut", {background = require("rz.lib.colors")["get-color"]("yellow/8"), foreground = require("rz.lib.colors")["get-color"]("black/8")})
        _418_ = {"YankyPut", {background = require("rz.lib.colors")["get-color"]("yellow/8"), foreground = require("rz.lib.colors")["get-color"]("black/8")}}
      end
      local function _419_()
        vim.api.nvim_set_hl(0, "YankyYanked", {background = require("rz.lib.colors")["get-color"]("yellow/4"), foreground = require("rz.lib.colors")["get-color"]("black/8")})
        return {"YankyYanked", {background = require("rz.lib.colors")["get-color"]("yellow/4"), foreground = require("rz.lib.colors")["get-color"]("black/8")}}
      end
      return {_418_, _419_()}
    end
    _contextyanky()
  else
  end
end
do
  if (nil == vim.g.rz_colors) then
    local colors_12_auto = require("rz.lib.colors")
    colors_12_auto.set()
  else
  end
  if not vim.tbl_isempty(vim.g.rz_colors) then
    local function _contextcustom()
      local _422_
      do
        vim.api.nvim_set_hl(0, "RZDashboardButtonShortcut", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")})
        _422_ = {"RZDashboardButtonShortcut", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/8")}}
      end
      local _423_
      do
        vim.api.nvim_set_hl(0, "RZDashboardDate", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground})
        _423_ = {"RZDashboardDate", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground}}
      end
      local _424_
      do
        vim.api.nvim_set_hl(0, "RZDashboardHeader", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")})
        _424_ = {"RZDashboardHeader", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/8")}}
      end
      local _425_
      do
        vim.api.nvim_set_hl(0, "RZDashboardMiscelaniousInfos", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground})
        _425_ = {"RZDashboardMiscelaniousInfos", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground}}
      end
      local _426_
      do
        vim.api.nvim_set_hl(0, "RZDashboardSectionTitleText", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")})
        _426_ = {"RZDashboardSectionTitleText", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")}}
      end
      local _427_
      do
        vim.api.nvim_set_hl(0, "RZDashboardSectionTitleSep", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground})
        _427_ = {"RZDashboardSectionTitleSep", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground}}
      end
      local _428_
      do
        vim.api.nvim_set_hl(0, "RZDashboardTruthText", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/5")})
        _428_ = {"RZDashboardTruthText", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/5")}}
      end
      local _429_
      do
        vim.api.nvim_set_hl(0, "RZDashboardTruthReference", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/15")})
        _429_ = {"RZDashboardTruthReference", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/15")}}
      end
      local _430_
      do
        vim.api.nvim_set_hl(0, "RZDraftFindDate", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _430_ = {"RZDraftFindDate", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _431_
      do
        vim.api.nvim_set_hl(0, "RZDraftFindTime", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")})
        _431_ = {"RZDraftFindTime", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")}}
      end
      local _432_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusAuthor", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/10"), italic = true})
        _432_ = {"RZGithubStatusAuthor", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/10"), italic = true}}
      end
      local _433_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusCommitContext", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")})
        _433_ = {"RZGithubStatusCommitContext", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("cyan/8")}}
      end
      local _434_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusCommitKind", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/10")})
        _434_ = {"RZGithubStatusCommitKind", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/10")}}
      end
      local _435_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusCommitMessage", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Normal", true).foreground})
        _435_ = {"RZGithubStatusCommitMessage", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Normal", true).foreground}}
      end
      local _436_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusCommitTag", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/10")})
        _436_ = {"RZGithubStatusCommitTag", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("magenta/10")}}
      end
      local _437_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusCurrentBranch", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/10")})
        _437_ = {"RZGithubStatusCurrentBranch", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/10")}}
      end
      local _438_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusDelimiter", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSPunctDelimiter", true).foreground})
        _438_ = {"RZGithubStatusDelimiter", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("TSPunctDelimiter", true).foreground}}
      end
      local _439_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusExtMark", {background = require("rz.lib.colors")["get-color"]("bg/5"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _439_ = {"RZGithubStatusExtMark", {background = require("rz.lib.colors")["get-color"]("bg/5"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _440_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusExtMarkInv", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("bg/5")})
        _440_ = {"RZGithubStatusExtMarkInv", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("bg/5")}}
      end
      local _441_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusFailure", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("DiagnosticError", true).foreground})
        _441_ = {"RZGithubStatusFailure", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("DiagnosticError", true).foreground}}
      end
      local _442_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusFoldOpened", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground})
        _442_ = {"RZGithubStatusFoldOpened", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground}}
      end
      local _443_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusFoldClosed", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground})
        _443_ = {"RZGithubStatusFoldClosed", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground}}
      end
      local _444_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusGroup", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/5")})
        _444_ = {"RZGithubStatusGroup", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("blue/5")}}
      end
      local _445_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusMessage", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground, italic = true})
        _445_ = {"RZGithubStatusMessage", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground, italic = true}}
      end
      local _446_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusNormal", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _446_ = {"RZGithubStatusNormal", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _447_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusPending", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("DiagnosticWarn", true).foreground})
        _447_ = {"RZGithubStatusPending", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("DiagnosticWarn", true).foreground}}
      end
      local _448_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusPrNumber", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/10")})
        _448_ = {"RZGithubStatusPrNumber", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("yellow/10")}}
      end
      local _449_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusPrNumberDraft", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground, italic = true})
        _449_ = {"RZGithubStatusPrNumberDraft", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground, italic = true}}
      end
      local _450_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusSuccess", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/8")})
        _450_ = {"RZGithubStatusSuccess", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("green/8")}}
      end
      local _451_
      do
        vim.api.nvim_set_hl(0, "RZGithubStatusTitle", {background = require("rz.lib.colors")["get-color"]("yellow/8"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _451_ = {"RZGithubStatusTitle", {background = require("rz.lib.colors")["get-color"]("yellow/8"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _452_
      do
        vim.api.nvim_set_hl(0, "RZGithusStatusBorder", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _452_ = {"RZGithusStatusBorder", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _453_
      do
        vim.api.nvim_set_hl(0, "RZPopupNormal", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _453_ = {"RZPopupNormal", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _454_
      do
        vim.api.nvim_set_hl(0, "RZPopupBorder", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("bg/7")})
        _454_ = {"RZPopupBorder", {background = require("rz.lib.colors")["get-color"]("bg/7"), foreground = require("rz.lib.colors")["get-color"]("bg/7")}}
      end
      local _455_
      do
        vim.api.nvim_set_hl(0, "RZTableBorder", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground})
        _455_ = {"RZTableBorder", {background = require("rz.lib.colors")["get-color"]("-"), foreground = vim.api.nvim_get_hl_by_name("Comment", true).foreground}}
      end
      local _456_
      do
        vim.api.nvim_set_hl(0, "RZTableFallback", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/6")})
        _456_ = {"RZTableFallback", {background = require("rz.lib.colors")["get-color"]("-"), foreground = require("rz.lib.colors")["get-color"]("bg/6")}}
      end
      local _457_
      do
        vim.api.nvim_set_hl(0, "RZWinbarLeftDelimiter", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _457_ = {"RZWinbarLeftDelimiter", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _458_
      do
        vim.api.nvim_set_hl(0, "RZWinbarLeftDelimiterInactive", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/5")})
        _458_ = {"RZWinbarLeftDelimiterInactive", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/5")}}
      end
      local _459_
      do
        vim.api.nvim_set_hl(0, "RZWinbarRightDelimiter", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/6")})
        _459_ = {"RZWinbarRightDelimiter", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/6")}}
      end
      local _460_
      do
        vim.api.nvim_set_hl(0, "RZWinbarRightDelimiterInactive", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/6")})
        _460_ = {"RZWinbarRightDelimiterInactive", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/6")}}
      end
      local _461_
      do
        vim.api.nvim_set_hl(0, "RZWinbarText", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _461_ = {"RZWinbarText", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _462_
      do
        vim.api.nvim_set_hl(0, "RZWinbarTextInactive", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("fg/7")})
        _462_ = {"RZWinbarTextInactive", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("fg/7")}}
      end
      local _463_
      do
        vim.api.nvim_set_hl(0, "RZWinbarIcon", {background = require("rz.lib.colors")["get-color"]("blue/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _463_ = {"RZWinbarIcon", {background = require("rz.lib.colors")["get-color"]("blue/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _464_
      do
        vim.api.nvim_set_hl(0, "RZWinbarIconInactive", {background = require("rz.lib.colors")["get-color"]("bg/5"), foreground = require("rz.lib.colors")["get-color"]("fg/8")})
        _464_ = {"RZWinbarIconInactive", {background = require("rz.lib.colors")["get-color"]("bg/5"), foreground = require("rz.lib.colors")["get-color"]("fg/8")}}
      end
      local _465_
      do
        vim.api.nvim_set_hl(0, "RZWinbarIconDelimiter", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _465_ = {"RZWinbarIconDelimiter", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _466_
      do
        vim.api.nvim_set_hl(0, "RZWinbarIconDelimiterInactive", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("bg/5")})
        _466_ = {"RZWinbarIconDelimiterInactive", {background = require("rz.lib.colors")["get-color"]("bg/6"), foreground = require("rz.lib.colors")["get-color"]("bg/5")}}
      end
      local _467_
      do
        vim.api.nvim_set_hl(0, "RZWinbarFlag", {background = require("rz.lib.colors")["get-color"]("bg/6"), bold = true, foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _467_ = {"RZWinbarFlag", {background = require("rz.lib.colors")["get-color"]("bg/6"), bold = true, foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _468_
      do
        vim.api.nvim_set_hl(0, "RZWinbarFlagInactive", {background = require("rz.lib.colors")["get-color"]("bg/6"), bold = true, foreground = require("rz.lib.colors")["get-color"]("fg/7")})
        _468_ = {"RZWinbarFlagInactive", {background = require("rz.lib.colors")["get-color"]("bg/6"), bold = true, foreground = require("rz.lib.colors")["get-color"]("fg/7")}}
      end
      local _469_
      do
        vim.api.nvim_set_hl(0, "RZWinbarSpacer", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("blue/8")})
        _469_ = {"RZWinbarSpacer", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("blue/8")}}
      end
      local _470_
      do
        vim.api.nvim_set_hl(0, "RZWinbarSpacerInactive", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/6")})
        _470_ = {"RZWinbarSpacerInactive", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("bg/6")}}
      end
      local _471_
      do
        vim.api.nvim_set_hl(0, "RZWinbarTestPassed", {background = require("rz.lib.colors")["get-color"]("green/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _471_ = {"RZWinbarTestPassed", {background = require("rz.lib.colors")["get-color"]("green/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _472_
      do
        vim.api.nvim_set_hl(0, "RZWinbarTestFailed", {background = require("rz.lib.colors")["get-color"]("red/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")})
        _472_ = {"RZWinbarTestFailed", {background = require("rz.lib.colors")["get-color"]("red/8"), foreground = require("rz.lib.colors")["get-color"]("bg/8")}}
      end
      local _473_
      do
        vim.api.nvim_set_hl(0, "RZWinbarTestPassedDelimiter", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("green/8")})
        _473_ = {"RZWinbarTestPassedDelimiter", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("green/8")}}
      end
      local function _474_()
        vim.api.nvim_set_hl(0, "RZWinbarTestFailedDelimiter", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("red/8")})
        return {"RZWinbarTestFailedDelimiter", {background = require("rz.lib.colors")["get-color"]("bg/8"), foreground = require("rz.lib.colors")["get-color"]("red/8")}}
      end
      return {_422_, _423_, _424_, _425_, _426_, _427_, _428_, _429_, _430_, _431_, _432_, _433_, _434_, _435_, _436_, _437_, _438_, _439_, _440_, _441_, _442_, _443_, _444_, _445_, _446_, _447_, _448_, _449_, _450_, _451_, _452_, _453_, _454_, _455_, _456_, _457_, _458_, _459_, _460_, _461_, _462_, _463_, _464_, _465_, _466_, _467_, _468_, _469_, _470_, _471_, _472_, _473_, _474_()}
    end
    _contextcustom()
  else
  end
end
do
  local inflector_13_auto = require("rz.lib.inflector")
  local sign_name_14_auto = ("DiagnosticSign" .. inflector_13_auto.capitalize("error"))
  local highlight_group_15_auto = ("Diagnostic" .. inflector_13_auto.capitalize("error"))
  vim.fn.sign_define(sign_name_14_auto, {text = "E", texthl = highlight_group_15_auto})
end
do
  local inflector_13_auto = require("rz.lib.inflector")
  local sign_name_14_auto = ("DiagnosticSign" .. inflector_13_auto.capitalize("warn"))
  local highlight_group_15_auto = ("Diagnostic" .. inflector_13_auto.capitalize("warn"))
  vim.fn.sign_define(sign_name_14_auto, {text = "W", texthl = highlight_group_15_auto})
end
do
  local inflector_13_auto = require("rz.lib.inflector")
  local sign_name_14_auto = ("DiagnosticSign" .. inflector_13_auto.capitalize("info"))
  local highlight_group_15_auto = ("Diagnostic" .. inflector_13_auto.capitalize("info"))
  vim.fn.sign_define(sign_name_14_auto, {text = "I", texthl = highlight_group_15_auto})
end
do
  local inflector_13_auto = require("rz.lib.inflector")
  local sign_name_14_auto = ("DiagnosticSign" .. inflector_13_auto.capitalize("hint"))
  local highlight_group_15_auto = ("Diagnostic" .. inflector_13_auto.capitalize("hint"))
  vim.fn.sign_define(sign_name_14_auto, {text = "H", texthl = highlight_group_15_auto})
end
vim.g.colors_name = "rz"
return nil
