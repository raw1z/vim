(require-macros :macros.core)
(require-macros :macros.snippets)

[(%sfmt fn
        "function {}\n  {}\nend"
        (i 1 :name)
        (i 0))
]
