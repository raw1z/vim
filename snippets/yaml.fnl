(require-macros :macros.snippets)

[
  (%p kus "apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
  - ${1:namespace.yaml}")

  (%p kush "apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
  - namespace.yaml
  - repo.yaml
  - release.yaml")

  (%p ns "apiVersion: v1
kind: Namespace
metadata:
  labels:
    name: ${1:namespace}
  name: $1")

  (%p repo "apiVersion: source.toolkit.fluxcd.io/v1beta1
kind: HelmRepository
metadata:
  name: ${1:repo-name}
  namespace: flux-system
spec:
  interval: 1h0m0s
  url: ${2:repo-url}")

  (%p rel "apiVersion: helm.toolkit.fluxcd.io/v2beta1
kind: HelmRelease
metadata:
  name: ${1:release}
  namespace: flux-system
spec:
  chart:
    spec:
      chart: $2
      sourceRef:
        kind: HelmRepository
        name: $3
  values:
    $5
  interval: 1h0m0s
  releaseName: $1
  targetNamespace: $4
  install:
    crds: Create
  upgrade:
    crds: CreateReplace")

  (%p sec "apiVersion: onepassword.com/v1
kind: OnePasswordItem
metadata:
  name: $1
  namespace: $2
spec:
  itemPath: \"vaults/k8s-raw1z/items/$1\"")

  (%p mysql "apiVersion: mysql.oracle.com/v2
kind: InnoDBCluster
metadata:
  name: $1
  namespace: $2
spec:
  edition: community
  secretName: $3
  tlsUseSelfSigned: true
  instances: ${4:1}
  datadirVolumeClaimTemplate:
    metadata:
      name: $1-storage
      namespace: $2
    spec:
      accessModes:
        - ReadWriteOnce
      resources:
        requests:
          storage: ${5:1Gi}
  router:
    instances: 1")

  (%p pvc "apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: $1-storage
  namespace: $2
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: ${3:1Gi}")

  (%p app "apiVersion: kustomize.toolkit.fluxcd.io/v1beta2
kind: Kustomization
metadata:
  name: $1
  namespace: flux-system
spec:
  interval: 10m0s
  sourceRef:
    kind: GitRepository
    name: flux-system
  path: ./flux/apps/$1
  prune: true
  dependsOn:
    - name: 1password")

  (%p infra "apiVersion: kustomize.toolkit.fluxcd.io/v1beta2
kind: Kustomization
metadata:
  name: $1
  namespace: flux-system
spec:
  interval: 10m0s
  sourceRef:
    kind: GitRepository
    name: flux-system
  path: ./flux/base/$1
  prune: true
  dependsOn:
    - name: 1password")

  (%p ing "apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: $1
  namespace: $2
  annotations:
    cert-manager.io/cluster-issuer: letsencrypt
    traefik.ingress.kubernetes.io/router.middlewares: traefik-redirect-https@kubernetescrd
spec:
  tls:
    - hosts:
        - $3
      secretName: $1-tls
  rules:
    - host: $3
      http:
        paths:
          - path: \"/\"
            pathType: Prefix
            backend:
              service:
                name: $4
                port:
                  name: ${5:http}")

(%p svc "apiVersion: v1
kind: Service
metadata:
  name: $1
  namespace: $2
  labels:
    app.kubernetes.io/name: $1
spec:
  selector:
    app.kubernetes.io/name: $1
  type: ClusterIP
  ports:
    - name: ${4:http}
      port: $3
      protocol: TCP
      targetPort: $4")

(%p dp "apiVersion: apps/v1
kind: Deployment
metadata:
  name: $1
  namespace: $2
  labels:
    app.kubernetes.io/name: $1
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: $1
  replicas: 1
  template:
    metadata:
      labels:
        app.kubernetes.io/name: $1
    spec:
      imagePullSecrets:
        - name: docker-registry-secret
      containers:
        - name: ${3:$1}
          image: $4
          ports:
            - containerPort: ${5:3000}
              name: http
          resources:
            requests:
              memory: 100Mi
            limits:
              memory: 200Mi
          livenessProbe:
            tcpSocket:
              port: $5
            initialDelaySeconds: 2
            timeoutSeconds: 5
          readinessProbe:
            httpGet:
              path: ${6:/health}
              port: $5
            initialDelaySeconds: 2
            timeoutSeconds: 2")

  (%p sts "apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: $1
  namespace: $2
  labels:
    app.kubernetes.io/name: $1
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: $1
  replicas: 1
  template:
    metadata:
      labels:
        app.kubernetes.io/name: $1
    spec:
      containers:
        - name: $1
          image: $3
          ports:
            - containerPort: ${4:3000}
              name: http
          volumeMounts:
            - name: ${5:data}
              mountPath: ${6:/path/to}
          resources:
            requests:
              memory: 100Mi
            limits:
              memory: 200Mi
          livenessProbe:
            tcpSocket:
              port: $4
            initialDelaySeconds: 5
            timeoutSeconds: 5
          readinessProbe:
            httpGet:
              path: /health
              port: $4
            initialDelaySeconds: 5
            timeoutSeconds: 5
  volumeClaimTemplates:
    - metadata:
        name: $5
      spec:
        accessModes:
          - ReadWriteOnce
        resources:
          requests:
            storage: ${7:1Gi}")

  (%sfmt env
         "{}"
         (%c 1
             (%fmt "- name: {}\n  value: {}"
                   (i 1)
                   (i 2))
             (%fmt "- name: {}\n  valueFrom:\n    configMapKeyRef:\n      name: {}\n      key: {}"
                   (i 1)
                   (i 2)
                   (i 3))
             (%fmt "- name: {}\n  valueFrom:\n    secretKeyRef:\n      name: {}\n      key: {}"
                   (i 1)
                   (i 2)
                   (i 3))))

  (%sfmt vol
         "{}"
         (%c 1
             (%fmt "- name: {}\n  configMap:\n    name: {}"
                   (i 1)
                   (i 2))
             (%fmt "- name: {}\n  secret:\n    secretName: {}"
                   (i 1)
                   (i 2))))

(%p inspect-volume "apiVersion: v1
kind: Pod
metadata:
  name: inspect-volume
  namespace: ${1:default}
spec:
  containers:
  - name: inspect-volume
    image: busybox:latest
    command:
      - sh
      - -c
      - 'while true; do sleep 6000; done'
    volumeMounts:
    - name: inspected-volume
      mountPath: /inspected-volume
  volumes:
  - name: inspected-volume
    persistentVolumeClaim:
      claimName: $2")
]
