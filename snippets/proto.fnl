(require-macros :macros.snippets)

[(%p syn "syntax = \"${1:proto3}\";")

 (%p pkg "package ${1:name};")

 (%p opt "option ${1:name} = \"$2\";")

 (%p im "import \"$1\";")

 (%sfmt agg
        "{} {} {{\n  {}\n}}"
        (%c 1
            (t :message)
            (t :enum)
            (t :service))
        (i 2 :Name)
        (i 3))

 (%sfmt at
        "{}{} = {};"
        (%c 1
            (fmt "{} " (i 1 :type))
            (t ""))
        (i 2 :name)
        (i 3 :1))

 (%sfmt rpc
        "rpc {} ({}) returns ({}){};"
        (i 1 :name)
        (i 2 :InputType)
        (%c 3
            (t :google.protobuf.Empty)
            (fmt "{}" (i 1 :OutputType)))
        (i 4))]
