-- [nfnl] Compiled from snippets/shen.fnl by https://github.com/Olical/nfnl, do not edit.
return {parse("df", "(define ${1:func-name}\n  % ${2:pattern} -> ${3:body})"), parse("ru", "$1 -> $2"), parse("sg", "{$1 --> $2}"), parse("cons", "[$1 | ${2:_}]")}
