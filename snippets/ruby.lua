-- [nfnl] Compiled from snippets/ruby.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local git = autoload("rz.lib.git")
local inflector = autoload("rz.lib.inflector")
local text = autoload("rz.lib.text")
local function relative_path(filepath)
  local projectroot = vim.fn["projectroot#guess"](filepath)
  local path = vim.fn.substitute(filepath, projectroot, "", "")
  if text["matches?"](path, "\\v^/lib/") then
    return vim.fn.substitute(path, "\\v^/lib/", "", "")
  elseif text["matches?"](path, "\\v^/app/") then
    return vim.fn.substitute(path, "\\v^/app/[a-z_]+/", "", "")
  else
    return path
  end
end
local function build_class_name(filepath)
  return inflector["split-map-join"](text["first-part"](relative_path(filepath), "."), inflector.pascalize, "::", {sep = "/"})
end
local function _3_(_241, _242)
  return sn(nil, i(1, build_class_name(_242.env.TM_FILEPATH)))
end
local function _4_(_241, _242)
  return sn(nil, i(1, build_class_name(_242.env.TM_FILEPATH)))
end
local function _5_()
  return sn(nil, i(1, vim.fn.substitute(git["current-branch"](), "\\v^(liv-\\d+).*", "\\1", "")))
end
return {parse("rinsp", "Rails.logger.info %(\n=================\n  #{$0}\n=================\n)"), parse("insp", "puts %(\n=================\n  #{$0}\n=================\n)"), parse("sinsp", "Sidekiq.logger.warn %(\n=================\n  #{$0}\n=================\n)"), parse("#", "#{$1}$0"), s("dbg", t("binding.pry")), s("subj", fmt("subject {}{{ {} }}", {c(1, {t(""), fmt("(:{}) ", {i(1)})}), i(0)})), s("bef", fmt("{} {}do\n  {}\nend", {c(1, {t("before"), t("after")}), c(2, {t(""), fmt("(:{}) ", {i(1, "each")})}), i(0)})), s("cont", fmt("{} \"{}\" do\n  {}\nend", {c(1, {t("context"), t("describe")}), i(2), i(0)})), s("it", fmt("it {}do\n  {}\nend", {c(1, {t(""), fmt("\"{}\" ", {i(1)})}), i(0)})), s("itexp", fmt("it {{ is_expected.to {} }}", {i(1)})), s("let", fmt("{}(:{}) {{ {} }}", {c(1, {t("let"), t("let!")}), i(2, "name"), i(0)})), s("exp", fmt("expect{}.to {}", {c(1, {fmt("({})", {i(1)}), fmt("{{ {} }}", {i(1)})}), i(0)})), s("itis", fmt("it {{ is_expected.to {} }}", {i(0)})), s("req", fmt("require \"{}\"", {i(0)})), s("reqr", t("require \"rails_helper\"")), s("cla", fmt("class {}\n  {}\nend", {d(1, _3_), i(2)})), s("mod", fmt("module {}\n  {}\nend", {d(1, _4_), i(2)})), s("def", fmt("{}{}\n  {}\nend", {c(1, {fmt("def {}", {i(1, "initialize")}), fmt("def self.{}", {i(1, "method")})}), c(2, {t(""), fmt("({})", {i(1)})}), i(0)})), parse(".ys", ".yield_self { |$1| $0 }"), parse(".ysi", ".yield_self { |obj| puts obj.inspect; obj }"), s("lsorg", fmt("\norg = FactoryBot.create(:organization, name: '{}', pricing_version: 2, users_count: {})\norg = Organization.find_by(name: '{}')\norg.owner.update!(job_level: 'level_three')\nevent = FactoryBot.create(:event_type, :drafted, organization: org, identity: org.owner, title: 'test')\nperiod = FactoryBot.create(:period, :upcoming, organization: org, event_type: event, team_members: org.team_members.shuffle.take(4))\nclip | {{ org.owner.email }}\nclip | {{ \"#Password1234#\" }}\nclip | {{ org.owner.id }}\nclip | {{ org.id }}\nclip | {{ event.id }}\nclip | {{ period.id }}\nMigrations::FillRolesAndPermissions::WorkspaceRoles::MigrateWorker.new.perform", {d(1, _5_), i(2, "10"), rep(1)})), parse("lsauth", "\norg = Organization.create(id: '$1')\nadmin_role = Role.find_by(name: 'admin')\nauth = Authorization.create(identity_id: '$2', scope: org, role: admin_role)"), parse("miginsp", "class SchemaMigration < ActiveRecord::Base; end\n    SchemaMigration.last"), parse("rlog", "system(%(fish -c 'rlog \"#{$1.inspect}\"'))"), parse("flog", "system(%(fish -c 'flog \"#{$1.inspect}\"'))"), parse("pry", "require 'pry'; binding.pry"), parse("debug", "require 'debug'; binding.break"), parse("active_model_serializer", "\noptions = { serializer: ${1:SerializerClass} }\nserializable_resource = ActiveModelSerializers::SerializableResource.new(${2:resource}, options)\nmodel_json = serializable_resource.as_json\n    "), s("rubocop_disable", fmt("\n  # rubocop:disable {}\n  # rubocop:enable {}", {i(1, "Rails/SkipsModelValidations"), rep(1)})), s("erbt", fmt("erb_template <<-ERB\n  {}\nERB", {i(1)})), s(".ea", fmt(".each do |{}|\n  {}\nend", {i(1), i(0)})), parse("explain", "\nsql = ${1:relation}.to_sql\nputs ActiveRecord::Base.connection.execute(\"EXPLAIN (ANALYZE, BUFFERS, FORMAT JSON, VERBOSE) #{sql}\").first.first\n    ")}
