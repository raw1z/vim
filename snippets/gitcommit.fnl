(require-macros :macros.snippets)

(local gitmoji-description-map [[:✨ "Introduce new features"]
                                [:🐛 "Fix a bug"]
                                [:⚡️ "Improve performance"]
                                [:♻️ "Refactor code"]
                                [:✅ "Add, update, or pass tests"]
                                [:🔥 "Remove code or files"]
                                [:👷 "Add or update CI build system"]
                                [:🚨 "Fix compiler / linter warnings"]
                                [:🎨 "Improve structure / format of the code"]
                                [:✏️ "Fix typos"]
                                [:🗃️ "Perform database related changes"]
                                [:🧑‍💻 "Improve developer experience"]
                                [:⚰️ "Remove dead code"]
                                [:🧱 "Infrastructure related changes."]])

(fn gc-choices []
  (vim.tbl_map #(t (. $1 1))
               gitmoji-description-map))

(fn gitmoji-description [gitmoji]
  (-> (vim.tbl_filter #(= (. $1 1) gitmoji)
                      gitmoji-description-map)
      (. 1 2)))

(fn update-gc-message [args]
  (sn nil (i 1 (->> (. args 1 1)
                    (gitmoji-description)))))

[
 (%sfmt cc
        "{}{}: {}\n{}"
        (%c 1
            (t :chore)
            (t :feat)
            (t :fix)
            (t :refactor)
            (t :test)
            (t :ci)
            (t :build)
            (t :docs)
            (t :perf)
            (t :style))
        (%c 2
            (t "")
            (%fmt "({})" (i 1 :scope)))
        (i 3 :message)
        (i 0))

 (%sfmt gc
        "{} {}\n{}"
        (c 1 (gc-choices))
        (d 2 update-gc-message [1])
        (i 0))

 (%sfmt cpu
        "chore(plugins): update\n\n")

 (%sfmt clu
        "chore(ledger/{}): update\n\n"
        (i 1))
 ]
