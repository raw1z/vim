(require-macros :macros.core)
(require-macros :macros.snippets)

[(%sfmt fn
        "({})"
        (%c 1
            (%fmt "defn {} [{}]\n  {}"
                  (i 1 :name)
                  (i 2)
                  (i 3))
            (%fmt "fn [{}] {}"
                  (i 1)
                  (i 2))))

 (%p let "(let [${1:var} ${2:value}]\n  $3)")

 (%p whe "(when ${1:condition}\n  $2)")

 (%p whel "(when-let [${1:var} ${2:value}]\n  $3)")

 (%p when "(when-not [${1:var} ${2:value}]\n  $3)")

 (%p whel "(when-let [${1:var} ${2:value}]\n  $3)")

 (%p if "(if ${1:condition}\n  $2\n  $3)")

 (%p ifl "(if-let [${1:var} ${2:value}]\n  $3)")

 (%p ifn "(if-not [${1:var} ${2:value}]\n  $3)")

 (%p ifs "(if-some [${1:var} ${2:value}]\n  $3)")

 (%p -> "(-> $0)")

 (%p ->> "(->> $0)")
 ]
