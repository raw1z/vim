(require-macros :macros.snippets)

[
 (%p list_column_defaults "
  select col.table_schema,
         col.table_name,
         col.column_name,
         col.column_default
    from information_schema.columns col
   where col.column_default is not null
     and col.table_schema not in('information_schema', 'pg_catalog')
     and table_name = '${1:table}'
   order by col.column_name;")

 (%p list_index "
  select indexname, indexdef
    from pg_indexes
   where schemaname = '${1:public}' and tablename = '${2:table}'
order by indexname;")

 (%p list_constraints "
     select con.*
       from pg_catalog.pg_constraint con
            inner join pg_catalog.pg_class rel
                       on rel.oid = con.conrelid
            inner join pg_catalog.pg_namespace nsp
                       on nsp.oid = connamespace
      where nsp.nspname = '${1:public}'
        and rel.relname = '${2:table}';")

 (%p help "
   -- list all databases: \\l
   -- swith to another database: \\c <db-name>
   -- list all database tables: \\dt
   -- describe a table: \\d+ <table-name>
   -- list all schemas: \\dn
   -- list users and their roles: \\du
   -- retrieve a specific user: \\du <username>
   -- list all functions: \\df
   -- list all views: \\dv
   -- save query results to a file: \\o <file-name>")]
