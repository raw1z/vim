(require-macros :macros.snippets)

[(%p df "(define ${1:func-name}
  % ${2:pattern} -> ${3:body})")

 (%p ru "$1 -> $2")

 (%p sg "{$1 --> $2}")

 (%p cons "[$1 | ${2:_}]") ]
