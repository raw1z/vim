(require-macros :macros.core)
(require-macros :macros.snippets)

[
 (%sfmt .
        "{} {{\n  {}\n}}"
        (%c 1
            (%fmt ".{}" (i 1))
            (%fmt "{}" (i 1)))
        (i 0))
]
