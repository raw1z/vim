-- [nfnl] Compiled from snippets/gitcommit.fnl by https://github.com/Olical/nfnl, do not edit.
local gitmoji_description_map = {{"\226\156\168", "Introduce new features"}, {"\240\159\144\155", "Fix a bug"}, {"\226\154\161\239\184\143", "Improve performance"}, {"\226\153\187\239\184\143", "Refactor code"}, {"\226\156\133", "Add, update, or pass tests"}, {"\240\159\148\165", "Remove code or files"}, {"\240\159\145\183", "Add or update CI build system"}, {"\240\159\154\168", "Fix compiler / linter warnings"}, {"\240\159\142\168", "Improve structure / format of the code"}, {"\226\156\143\239\184\143", "Fix typos"}, {"\240\159\151\131\239\184\143", "Perform database related changes"}, {"\240\159\167\145\226\128\141\240\159\146\187", "Improve developer experience"}, {"\226\154\176\239\184\143", "Remove dead code"}, {"\240\159\167\177", "Infrastructure related changes."}}
local function gc_choices()
  local function _1_(_241)
    return t(_241[1])
  end
  return vim.tbl_map(_1_, gitmoji_description_map)
end
local function gitmoji_description(gitmoji)
  local function _2_(_241)
    return (_241[1] == gitmoji)
  end
  return vim.tbl_filter(_2_, gitmoji_description_map)[1][2]
end
local function update_gc_message(args)
  return sn(nil, i(1, gitmoji_description(args[1][1])))
end
return {s("cc", fmt("{}{}: {}\n{}", {c(1, {t("chore"), t("feat"), t("fix"), t("refactor"), t("test"), t("ci"), t("build"), t("docs"), t("perf"), t("style")}), c(2, {t(""), fmt("({})", {i(1, "scope")})}), i(3, "message"), i(0)})), s("gc", fmt("{} {}\n{}", {c(1, gc_choices()), d(2, update_gc_message, {1}), i(0)})), s("cpu", fmt("chore(plugins): update\n\n", {})), s("clu", fmt("chore(ledger/{}): update\n\n", {i(1)}))}
