-- [nfnl] Compiled from snippets/fennel.fnl by https://github.com/Olical/nfnl, do not edit.
local core = require("nfnl.core")
local text = require("rz.lib.text")
local function _1_(_241)
  return sn(nil, {t(text.replace(_241[1][1], "collect", "pairs"))})
end
local function _2_(_241)
  return text["last-part"](_241[1][1], ".")
end
local function _3_(_241)
  if (_241[1][1] == "") then
    return ""
  else
    return ")"
  end
end
local function _5_(_241)
  return text["last-part"](_241[1][1], ".")
end
return {s("fn", fmt("(fn {})", {c(1, {fmt("{} [{}]\n  {}", {i(1, "name"), i(2), i(3)}), fmt("[{}] {}", {i(1), i(2)})})})), s("mac", fmt("(macro {} [{}]\n  {})", {i(1, "name"), i(2), i(3)})), s("ea", fmt("(each [{} {} ({} {})]\n  {})", {i(1, "_"), i(2, "value"), c(3, {t("ipairs"), t("pairs")}), i(4, "table"), i(5)})), s("col", fmt("({} [{} {} ({} {})]\n  {})", {c(1, {t("collect"), t("icollect")}), i(2, "_"), i(3, "value"), d(4, _1_, {1}), i(5, "table"), i(6)})), parse("let", "(let [${1:var} ${2:value}]\n  $3)"), parse("bind", "${1:var} ${2:value}"), parse("set", "(set ${1:var} $2)"), parse("var", "(var ${1:var} $2)"), parse("loc", "(local $1)"), parse("locm", "(local M {})\n\n$1\n\nM"), parse("whi", "(while ${1:condition}\n  $2)"), parse("whe", "(when ${1:condition}\n  $2)"), parse("whel", "(when-let [${1:var} ${2:value}]\n  $3)"), parse("if", "(if ${1:condition}\n  $2\n  $3)"), parse("ifl", "(if-let [${1:var} ${2:value}]\n  $3)"), parse("do", "(do\n  $1)"), parse("vpr", "(vim.print $1)"), s("ra", t("(local {: autoload} (require :nfnl.module))")), s("rq", fmt("{}{} ({} :{}){}", {c(1, {t(""), t("(local ")}), f(_2_, {3}), c(2, {t("require"), t("autoload")}), c(3, {fmt("{}", {i(1, "mod")}), fmt("rz.lib.{}", {i(1, "mod")})}), f(_3_, {1})})), s("letrq", fmt("(let [{} (require :{})]\n  {})", {f(_5_, {1}), c(1, {fmt("{}", {i(1, "mod")}), fmt("rz.lib.{}", {i(1, "mod")})}), i(2)})), parse("rm", "(require-macros :macros.${1:vim})"), parse("ds", "{:desc \"$1\"}"), parse("->", "(-> $0)"), parse("-?>", "(-?> $0)"), parse("->>", "(->> $0)"), parse("-?>>", "(-?>> $0)"), parse("wezm", "(import-macros {: wez} :macros)\n\n(local wezterm (require :wezterm))\n(local M {})\n\n(fn M.apply [config]\n  $0)\n\nM"), parse("hsp", "(print (hs.inspect $0))")}
