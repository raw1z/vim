(require-macros :macros.snippets)

[(%sfmt iferr
        "{}\nif err {} {{\n  {}\n}}"
        (%c 1
            (%fmt "{}, err := {}" (i 1) (i 2))
            (%fmt "{}, err = {}" (i 1) (i 2))
            (%fmt "err := {}" (i 1))
            (%fmt "err = {}" (i 1)))
        (%c 2
            (t "!= nil")
            (t "== nil")
            (%fmt "== {}" (i 1))
            (%fmt "!= {}" (i 1)))
        (%c 3
            (%fmt "return {}, {}" (i 1 :nil) (i 2 :err))
            (%fmt "return {}" (i 1 :err))
            (%fmt "{}" (i 1))))

 (%sfmt iferi
        "if {}; err {} {{\n  {}\n}}"
        (%c 1
            (%fmt "{}, err := {}"
                  (i 1)
                  (i 2))
            (%fmt "{}, err = {}"
                  (i 1)
                  (i 2))
            (%fmt "err := {}" (i 1))
            (%fmt "err = {}" (i 1)))
        (%c 2
            (t "!= nil")
            (t "== nil")
            (%fmt "!= {}" (i 1))
            (%fmt "== {}" (i 1)))
        (%c 3
            (%fmt "return {}, {}" (i 1 :nil) (i 2 :err))
            (%fmt "return {}" (i 1 :err))
            (%fmt "{}" (i 1))))

(%sfmt ifer
        "if err {} {{\n  {}\n}}"
        (%c 1
            (t "!= nil")
            (t "== nil")
            (%fmt "== {}" (i 1))
            (%fmt "!= {}" (i 1)))
        (%c 2
            (%fmt "return {}, {}" (i 1 :nil) (i 2 :err))
            (%fmt "return {}" (i 1 :err))
            (%fmt "{}" (i 1))))

 (%sfmt main "func main() {{\n  {}\n}}" (i 1))

 (%sfmt insp "fmt.Printf(\"%+v\\n\", {})" (i 1))

 (%sfmt fnt
        "func Test{}(t *testing.T) {{\n  {}\n}}"
        (i 1)
        (i 2))

 (%sfmt reqeq
        "require.Equal(t, {}, {})"
        (i 1)
        (i 2))

 (%sfmt reqe "require.Error(t, {})" (i 0 :err))

 (%sfmt reqne "require.NoError(t, {})" (i 0 :err))

 (%sfmt fn
        "func {}({}) {}{{\n  {}\n}}"
        (%c 1
            (%fmt "{}" (i 1 :name))
            (%fmt "({} *{}) {}"
                  (i 1 :t)
                  (i 2 :type)
                  (i 3 :name)))
        (i 2)
        (%c 3
            (t "")
            (%fmt "{} " (i 1 :error))
            (%fmt "({}, error) " (i 1))
            (%fmt "({}) " (i 1)))
        (i 4))

 (%p ims "import (\n  $1\n)")

 (%sfmt err "{}"
            (%c 1
                (%fmt "{}, err := {}" (i 1) (i 2))
                (%fmt "{}, err = {}" (i 1) (i 2))
                (%fmt "err := {}" (i 1))
                (%fmt "err = {}" (i 1))))]
