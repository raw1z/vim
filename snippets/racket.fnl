(require-macros :macros.core)
(require-macros :macros.snippets)

[(%sfmt fn
        "({})"
        (%c 1
            (%fmt "define ({}{})\n  {}"
                  (i 1 :name)
                  (i 2)
                  (i 3))
            (%fmt "lambda ({}){}"
                  (i 1)
                  (i 2))))

 (%p def "(define ${1:var})")

 (%p set "(set! ${1:var} $2)")

 (%p whe "(when ${1:condition}\n  $2)")

 (%p if "(if ${1:condition}\n  $2\n  $3)")

 (%sfmt let
        "({} ([{} {}]{})\n  {})"
        (%c 1
            (t :let)
            (t :let*)
            (t :letrec)
            (%fmt "let {}"
                  (i 1 :name)))
        (i 2 :var)
        (i 3 :value)
        (i 4)
        (i 5))

 (%p bind "[${1:var} ${2:value}]")]
