(require-macros :macros.core)
(require-macros :macros.snippets)

(local core (require :nfnl.core))
(local text (require :rz.lib.text))

[(%sfmt fn
        "(fn {})"
        (%c 1
            (%fmt "{} [{}]\n  {}"
                  (i 1 :name)
                  (i 2)
                  (i 3))
            (%fmt "[{}] {}"
                  (i 1)
                  (i 2))))

 (%sfmt mac
        "(macro {} [{}]\n  {})"
        (i 1 :name)
        (i 2)
        (i 3))

 (%sfmt ea
        "(each [{} {} ({} {})]\n  {})"
        (i 1 :_)
        (i 2 :value)
        (%c 3
            (t :ipairs)
            (t :pairs))
        (i 4 :table)
        (i 5))

 (%sfmt col
        "({} [{} {} ({} {})]\n  {})"
        (%c 1
            (t :collect)
            (t :icollect))
        (i 2 :_)
        (i 3 :value)
        (d 4
           #(sn nil [(t (text.replace (. $1 1 1)
                                      :collect
                                      :pairs))])
           [1])
        (i 5 :table)
        (i 6))

 (%p let "(let [${1:var} ${2:value}]\n  $3)")

 (%p bind "${1:var} ${2:value}")

 (%p set "(set ${1:var} $2)")

 (%p var "(var ${1:var} $2)")

 (%p loc "(local $1)")

 (%p locm "(local M {})\n\n$1\n\nM")

 (%p whi "(while ${1:condition}\n  $2)")

 (%p whe "(when ${1:condition}\n  $2)")

 (%p whel "(when-let [${1:var} ${2:value}]\n  $3)")

 (%p if "(if ${1:condition}\n  $2\n  $3)")

 (%p ifl "(if-let [${1:var} ${2:value}]\n  $3)")

 (%p do "(do\n  $1)")

 (%p vpr "(vim.print $1)")

 (%s ra
     (t "(local {: autoload} (require :nfnl.module))"))

 (%sfmt rq
        "{}{} ({} :{}){}"
        (%c 1
            (t "")
            (t "(local "))
         (f #(text.last-part (. $1 1 1) :.) [3])
         (%c 2
             (t :require)
             (t :autoload))
         (%c 3
             (%fmt "{}" (i 1 :mod))
             (%fmt "rz.lib.{}" (i 1 :mod)))
         (f #(if (= (. $1 1 1) "") "" ")") [1]))

 (%sfmt letrq
        "(let [{} (require :{})]\n  {})"
        (f #(text.last-part (. $1 1 1) :.) [1])
        (%c 1
            (%fmt "{}" (i 1 :mod))
            (%fmt "rz.lib.{}" (i 1 :mod)))
        (i 2))

 (%p rm "(require-macros :macros.${1:vim})")

 (%p ds "{:desc \"$1\"}")

 (%p -> "(-> $0)")

 (%p -?> "(-?> $0)")

 (%p ->> "(->> $0)")

 (%p -?>> "(-?>> $0)")

 (%p wezm "(import-macros {: wez} :macros)

(local wezterm (require :wezterm))
(local M {})

(fn M.apply [config]
  $0)

M")

 (%p hsp  "(print (hs.inspect $0))")]
