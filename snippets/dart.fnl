(require-macros :macros.core)
(require-macros :macros.snippets)

[
 (%sfmt fn
        "{} {}({}) {{\n  {}\n}}"
        (i 1 :void)
        (i 2 :name)
        (i 3)
        (i 4))

 (%sfmt fnw
        "Widget {}({}) {{\n  {}\n}}"
        (i 1 :name)
        (%c 2
            (t "BuildContext context")
            (%fmt "BuildContext context, {}"
                  (i 1)))
        (i 3))

 (%sfmt f
        "{}"
        (%c 1
            (%fmt "({}) => {}"
                  (i 1)
                  (i 2))
            (%fmt "({}) {{\n  {}\n}}"
                  (i 1)
                  (i 2))))

 (%sfmt fc
        "{}"
        (%c 1
            (%fmt "(context{}) => {}"
                  (i 1)
                  (i 2))
            (%fmt "(context{}) {{\n  {}\n}}"
                  (i 1)
                  (i 2))))

 (%sfmt gen
        "{}<{}>"
        (i 1 :List)
        (i 2))
 ]
