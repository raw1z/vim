(require-macros :macros.snippets)

(local {: autoload} (require :nfnl.module))
(local git (autoload :rz.lib.git))
(local inflector (autoload :rz.lib.inflector))
(local text (autoload :rz.lib.text))

(fn relative-path [filepath]
  (let [projectroot (vim.fn.projectroot#guess filepath)
        path (vim.fn.substitute filepath projectroot "" "")]
    (if
      (text.matches? path "\\v^/lib/")
        (vim.fn.substitute path "\\v^/lib/" "" "")
      (text.matches? path "\\v^/app/")
        (vim.fn.substitute path "\\v^/app/[a-z_]+/" "" "")
      path)))

(fn build-class-name [filepath]
  (-> (relative-path filepath)
      (text.first-part :.)
      (inflector.split-map-join inflector.pascalize "::" {:sep :/})))

[(%p rinsp "Rails.logger.info %(\n=================\n  #{$0}\n=================\n)")

 (%p insp "puts %(\n=================\n  #{$0}\n=================\n)")

 (%p sinsp "Sidekiq.logger.warn %(\n=================\n  #{$0}\n=================\n)")

 (%p # "#{$1}$0")

 (%s dbg
     (t :binding.pry))

 (%sfmt subj
        "subject {}{{ {} }}"
        (%c 1
            (t "")
            (%fmt "(:{}) " (i 1)))
        (i 0))

 (%sfmt bef
        "{} {}do\n  {}\nend"
        (%c 1
            (t "before")
            (t "after"))
        (%c 2
            (t "")
            (%fmt "(:{}) " (i 1 :each)))
        (i 0))

 (%sfmt cont
        "{} \"{}\" do\n  {}\nend"
        (%c 1
            (t "context")
            (t "describe"))
        (i 2)
        (i 0))

 (%sfmt it
        "it {}do\n  {}\nend"
        (%c 1
            (t "")
            (%fmt "\"{}\" " (i 1)))
        (i 0))

 (%sfmt itexp
        "it {{ is_expected.to {} }}"
        (i 1))

 (%sfmt let
        "{}(:{}) {{ {} }}"
        (%c 1
            (t :let)
            (t :let!))
        (i 2 :name)
        (i 0))

 (%sfmt exp
        "expect{}.to {}"
        (%c 1
            (%fmt "({})" (i 1))
            (%fmt "{{ {} }}" (i 1)))
        (i 0))

 (%sfmt itis "it {{ is_expected.to {} }}" (i 0))

 (%sfmt req "require \"{}\"" (i 0))

 (%s reqr
     (t "require \"rails_helper\""))

 (%sfmt cla
        "class {}\n  {}\nend"
        (d 1 #(sn nil (i 1 (build-class-name $2.env.TM_FILEPATH))))
        (i 2))

 (%sfmt mod
        "module {}\n  {}\nend"
        (d 1 #(sn nil (i 1 (build-class-name $2.env.TM_FILEPATH))))
        (i 2))

 (%sfmt def
        "{}{}\n  {}\nend"
        (%c 1
            (%fmt "def {}" (i 1 :initialize))
            (%fmt "def self.{}" (i 1 :method)))
        (%c 2
            (t "")
            (%fmt "({})" (i 1)))
        (i 0))

 (%p .ys ".yield_self { |$1| $0 }")

 (%p .ysi ".yield_self { |obj| puts obj.inspect; obj }")

 (%sfmt lsorg "
org = FactoryBot.create(:organization, name: '{}', pricing_version: 2, users_count: {})
org = Organization.find_by(name: '{}')
org.owner.update!(job_level: 'level_three')
event = FactoryBot.create(:event_type, :drafted, organization: org, identity: org.owner, title: 'test')
period = FactoryBot.create(:period, :upcoming, organization: org, event_type: event, team_members: org.team_members.shuffle.take(4))
clip | {{ org.owner.email }}
clip | {{ \"#Password1234#\" }}
clip | {{ org.owner.id }}
clip | {{ org.id }}
clip | {{ event.id }}
clip | {{ period.id }}
Migrations::FillRolesAndPermissions::WorkspaceRoles::MigrateWorker.new.perform"
        (d 1 #(sn nil (i 1 (vim.fn.substitute (git.current-branch) "\\v^(liv-\\d+).*" "\\1" ""))))
        (i 2 "10")
        (rep 1))

 (%p lsauth "
org = Organization.create(id: '$1')
admin_role = Role.find_by(name: 'admin')
auth = Authorization.create(identity_id: '$2', scope: org, role: admin_role)")

(%p miginsp "class SchemaMigration < ActiveRecord::Base; end
    SchemaMigration.last")

(%p rlog "system(%(fish -c 'rlog \"#{$1.inspect}\"'))")

(%p flog "system(%(fish -c 'flog \"#{$1.inspect}\"'))")

(%p pry "require 'pry'; binding.pry")

(%p debug "require 'debug'; binding.break")

(%p active_model_serializer "
options = { serializer: ${1:SerializerClass} }
serializable_resource = ActiveModelSerializers::SerializableResource.new(${2:resource}, options)
model_json = serializable_resource.as_json
    ")

(%sfmt rubocop_disable "
  # rubocop:disable {}
  # rubocop:enable {}"
  (i 1 :Rails/SkipsModelValidations)
  (rep 1))

(%sfmt erbt "erb_template <<-ERB\n  {}\nERB"
  (i 1))

(%sfmt .ea
       ".each do |{}|\n  {}\nend"
       (i 1)
       (i 0))
(%p explain "
sql = ${1:relation}.to_sql
puts ActiveRecord::Base.connection.execute(\"EXPLAIN (ANALYZE, BUFFERS, FORMAT JSON, VERBOSE) #{sql}\").first.first
    ")
]
