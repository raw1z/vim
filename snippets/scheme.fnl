(require-macros :macros.core)
(require-macros :macros.snippets)

[(%sfmt fn
        "({})"
        (%c 1
            (%fmt "define ({}{})\n  {}"
                  (i 1 :name)
                  (i 2)
                  (i 3))
            (%fmt "lambda ({}){}"
                  (i 1)
                  (i 2))))

 (%p macro "(define-macro (${1:name})\n  $2)")

 (%p syn "(define-syntax-rule (${1:name})\n  $2)")

 (%p def "(define ${1:var})")

 (%p set "(set! ${1:var} $2)")

 (%sfmt if
        "(if {}\n  {})"
        (i 1 :test)
        (%c 2
            (%fmt "{}\n  {}"
                  (i 1)
                  (i 2))
            (%fmt "{}"
                  (i 1))))

 (%sfmt cond
        "(cond\n  ({} {}){})"
        (i 1 :test)
        (i 2)
        (i 3))

 (%sfmt condb
        "({} {})"
        (i 1 :test)
        (i 2))

 (%sfmt let
        "({} (({} {}){})\n  {})"
        (%c 1
            (t :let)
            (t :let*)
            (t :letrec)
            (%fmt "let {}"
                  (i 1 :name)))
        (i 2 :var)
        (i 3 :value)
        (i 4)
        (i 5))

 (%p bind "(${1:var} ${2:value})")

 (%p lst "(list $1)")

 (%p . "($1 . $2)")]
