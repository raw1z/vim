fun! s:create_term_buffer() "{{{
  new
  let s:current_term_buffer = bufnr('%')
  normal K
endfun "}}}

fun! s:delete_term_buffer() "{{{
  if exists("s:current_term_buffer")
    if bufexists(s:current_term_buffer)
      execute 'silent ' . s:current_term_buffer . 'bdelete!'
    endif
  end
endfun "}}}

fun! s:start_term(command) "{{{
  call s:delete_term_buffer()
  wall
  call s:create_term_buffer()
  nnoremap <buffer>q :quit!<CR>
  call termopen(a:command)
endfun "}}}

fun! Start()
  call s:start_term('dkr neovim zsh')
  norm a
endfun

fun! Build()
  call s:start_term('docker build -t neovim .')
  norm G
endfun

nmap <leader>r :silent call Start()<CR>
nmap <leader>b :silent call Build()<CR>



