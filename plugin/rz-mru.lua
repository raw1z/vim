-- [nfnl] Compiled from plugin/rz-mru.fnl by https://github.com/Olical/nfnl, do not edit.
local _local_1_ = require("nfnl.module")
local autoload = _local_1_["autoload"]
local mru = autoload("rz.lib.mru")
local _2_ = vim.api.nvim_create_augroup("rz_mru", {clear = true})
return {vim.api.nvim_create_autocmd({"BufWinEnter", "BufEnter"}, {callback = mru["on-win-enter"], group = _2_, pattern = {"*"}})}
