(require-macros :macros.vim)

(local {: autoload} (require :nfnl.module))
(local mru (autoload :rz.lib.mru))

(define-augroup rz_mru
  (define-autocmd BufWinEnter|BufEnter * mru.on-win-enter))
