;; extends

[
  "define"
  "defmacro"
] @keyword.function

[
  "("
  ")"
  "["
  "]"
  "{"
  "}"
] @punctuation.bracket

[
  "->"
  "<-"
  "|"
] @punctuation.delimiter

[
  "@p"
  "@v"
  "where"
  "/."
] @define

(comment) @comment @spell
(boolean) @boolean
(number) @number
(string) @string @spell
(variable) @symbol
(symbol) @variable

(application . (symbol) @function.call)
(shen_def
  name: (symbol) @function)

((vector) @macro (#eq? @macro "<>"))
((symbol) @conditional (#any-of? @conditional "if"))
((symbol) @punctuation.special (#eq? @punctuation.special "_"))
((symbol) @define (#any-of? @define "@s" "set" "value" "let" "lambda" "freeze" "fn" "open" "error" "trap-error" "hash" "put" "eval"))

(signature
  (type) @include (#any-of? @include "number" "boolean" "string"))
(signature
  "-->" @punctuation.delimiter)
(type
  ["vector" "list"] @include)
