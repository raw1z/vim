(abstraction
  "(" @opening
  ")" @closing) @container

(application
  "(" @opening
  ")" @closing) @container

(shen_def
  "(" @opening
  ")" @closing) @container

(list
  "[" @opening
  "]" @closing) @container

(cons
  "[" @opening
  "]" @closing) @container

(tuple
  "(" @opening
  ")" @closing) @container

(vector
  "(" @opening
  ")" @closing) @container

(signature
  "{" @opening
  "}" @closing) @container

(type
  "(" @opening
  ")" @closing) @container
