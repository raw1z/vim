;; extends

(tag_name) @RZVueTagName

((tag_name) @RZVueCustomTagName (#match? @RZVueCustomTagName "^[A-Z]"))

((tag_name) @RZVueSpecialElement (#any-of? @RZVueSpecialElement "component" "slot" "template"))

((directive_name) @RZVueBuiltin (#any-of? @RZVueBuiltin "v-bind" "v-cloak" "v-else" "v-else-if" "v-for" "v-html" "v-if" "v-memo" "v-model" "v-on" "v-once" "v-pre" "v-show" "v-slot" "v-text"))

(start_tag . (tag_name) @RZVueTemplate (#eq? @RZVueTemplate "template"))
(end_tag . (tag_name) @RZVueTemplate (#eq? @RZVueTemplate "template"))

(start_tag . (tag_name) @RZVueScript (#eq? @RZVueScript "script"))
(end_tag . (tag_name) @RZVueScript (#eq? @RZVueScript "script"))

(start_tag . (tag_name) @RZVueStyle (#eq? @RZVueStyle "style"))
(end_tag . (tag_name) @RZVueStyle (#eq? @RZVueStyle "style"))
