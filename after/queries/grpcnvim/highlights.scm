;; extends

(comment) @comment

(grpc_request
  (command) @RZFishCommandName)

(grpc_request
  (command) @RZFishCommandName)

(grpc_request
  (command
    argument: (word) @RZFishTools (#any-of? @RZFishTools "describe" "list")))

(grpc_request
  (command
    argument: (word) @RZFishCommandArgument (#match? @RZFishCommandArgument "^-") .
    argument: (word) @RZFishVariable))

[
 (json_true)
 (json_false)
] @boolean

(json_null) @constant.builtin

(json_number) @number

(json_pair key: (json_string) @label)
(json_pair value: (json_string) @string)

(json_array (json_string) @string)

["," ":"] @punctuation.delimiter

[
 "[" "]"
 "{" "}"
] @punctuation.bracket
