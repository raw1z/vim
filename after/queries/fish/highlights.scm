;; extends

(function_definition name: (word) @RZFishFunctionName)

(command name: (word) @RZFishCommandName)

(command name: (word) @RZFishTools (#any-of? @RZFishTools "abbr" "alias" "argparse" "bg" "bind" "block" "breakpoint" "builtin" "cd" "cdh" "command" "commandline" "complete" "contains" "count" "dirh" "dirs" "disown" "echo" "else" "emit" "eval" "exec" "exit" "false" "fg" "fish" "fish_add_path" "fish_breakpoint_prompt" "fish_clipboard_copy" "fish_clipboard_paste" "fish_command_not_found" "fish_config" "fish_delta" "fish_git_prompt" "fish_greeting" "fish_hg_prompt" "fish_indent" "fish_is_root_user" "fish_key_reader" "fish_mode_prompt" "fish_opt" "fish_prompt" "fish_right_prompt" "fish_status_to_signal" "fish_svn_prompt" "fish_title" "fish_update_completions" "fish_vcs_prompt" "funced" "funcsave" "function" "functions" "help" "history" "isatty" "jobs" "math" "nextd" "open" "path" "popd" "prevd" "printf" "prompt_hostname" "prompt_login" "prompt_pwd" "psub" "pushd" "pwd" "random" "read" "realpath" "set" "set_color" "source" "status" "string" "string" "suspend" "test" "time" "trap" "true" "type" "ulimit" "umask" "vared" "wait" "while"))

(command name: (word) @_set (#eq? @_set "set") .
         argument: (word) @RZFishVariable)

(command name: (word) @_set (#eq? @_set "set") .
         argument: (word) @RZFishCommandArgument (#match? @RZFishCommandArgument "^-") .
         argument: (word) @RZFishVariable)

(command name: (word) @_set (#any-of? @_set "test" "type") .
         argument: (word) @RZFishCommandArgument (#match? @RZFishCommandArgument "^-"))

(variable_name) @RZFishVariable

(escape_sequence) @RZFishBackslash (#eq? @RZFishBackslash "\\")
