;; extends

(list .
  call: (symbol) @_keyword (#eq? @_keyword "%>")
  item: (_) @injection.content
  (#set! injection.language "vim")
  (#set! injection.include-children))
