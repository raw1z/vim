;; extends

;; custom macros
(list .
  (symbol) @RZMacro (#any-of? @RZMacro "%g" "%p" "%sfmt" "%s" "%fmt" "%c" "%>" "%f" "%w" "%o" "plug" "plug-key" "define-diagnostic-icon" "define-highlight-context" "define-augroup" "define-autocmd" "define-autocmd/nested" "define-autocmd/buffer" "define-autocmd/this-buffer" "define-keys" "define-keys/buffer" "define-keys/this-buffer" "remap-keys/buffer" "remap-keys/buffer" "remap-keys/this-buffer" "define-key" "define-key/expr" "define-key/buffer" "define-key/this-buffer" "remap-key" "remap-key/buffer" "remap-key/this-buffer" "define-command" "define-command/buffer" "define-command/this-buffer" "with-strategies" "when-loaded" "define-operator" "define-operator/buffer" "define-operator/this-buffer" "def-key" "def-key/expr" "def-key/buffer" "def-keys" "def-key/this-buffer" "def-keys/this-buffer" "def-keys/buffer" "apply-strategies" "load-plugins" "autoformat-code" "telescope-extension"))

(list
  call: (symbol) @_keyword (#any-of? @_keyword "define-diagnostic-icon" "define-highlight-context" "define-augroup" "define-autocmd" "define-autocmd/nested" "define-autocmd/buffer" "define-autocmd/this-buffer" "define-command" "define-command/this-buffer") .
  item: (symbol) @RZFunctionName)

(list
  call: (symbol) @RZVimOption (#any-of? @RZVimOption "get-opt" "set-opt" "set-local-opt" "set-global-opt" "append-opt" "append-local-opt" "append-global-opt" "prepend-opt" "prepend-local-opt" "prepend-global-opt" "remove-opt" "remove-local-opt" "remove-global-opt") .
  item: (symbol) @RZVimOptionName)

(list
  call: (symbol) @_keyword (#any-of? @_keyword "define-keys" "define-keys/buffer" "define-keys/this-buffer" "remap-keys/buffer" "remap-keys/buffer" "remap-keys/this-buffer") .
  item: (symbol) @function)

(list
  call: (symbol) @_keyword (#any-of? @_keyword "define-keys" "define-keys/buffer" "define-keys/this-buffer" "remap-keys/buffer" "remap-keys/buffer" "remap-keys/this-buffer" "define-key" "define-key/expr" "define-key/this-buffer" "remap-key" "remap-key/this-buffer" "define-operator" "define-operator/buffer" "define-operator/this-buffer")
  item: (symbol) @number (#match? @number "^[cinostv](\\|[cinostv])*") .
  item: [(symbol) (string)] @RZFennelKeymap)

(list
  call: (symbol) @_keyword (#any-of? @_keyword "def-keys" "def-keys/buffer" "def-keys/this-buffer")
  item: (string) @comment .
  item: (symbol) @number (#match? @number "^[cinostv](\\|[cinostv])*") .
  item: [(symbol) (string)] @RZFennelKeymap)

(list
  call: (symbol) @_keyword (#any-of? @_keyword "def-key" "def-key/buffer" "def-key/expr" "def-key/this-buffer") .
  item: (string) @comment
  item: (symbol) @number (#match? @number "^[cinostv](\\|[cinostv])*") .
  item: [(symbol) (string)] @RZFennelKeymap)

(list
  call: (symbol) @_keyword (#eq? @_keyword "plug") .
  item: [
         (multi_symbol (symbol_fragment) @RZFennelPlugName)
         (symbol)
        ] @RZFennelPlugName)

(list
  call: (symbol) @keyword.conditional (#any-of? @keyword.conditional "if-let" "when-let"))

(list
  call: (symbol) @_snip (#any-of? @_snip "%p" "%sfmt" "%s") .
  item: (symbol) @RZSnippet)

(list
  call: (symbol) @_keyword (#eq? @_keyword "plug-key") .
  item: (symbol) @number (#match? @number "^[cinostv](\\|[cinostv])*") .
  item: [(symbol) (string)] @RZFennelKeymap)

(list
  call: (symbol) @_keyword (#eq? @_keyword "%w")
  item: (symbol) @RZFennelSpecialString)

;; improved standard
(table
  (table_pair
    key: [(string) (symbol)] @RZFennelTableKey))

(list
  call: (symbol) @_keyword (#eq? @_keyword "fn") .
  item: [
         (multi_symbol
           base: (symbol_fragment)
           member: (symbol_fragment) @RZFunctionName)
         (symbol) @RZFunctionName
         ] .
  item: (sequence) .
  item: (string)? @comment .
  (_))

(multi_symbol
  base: (symbol_fragment) @RZFennelVim (#eq? @RZFennelVim "vim")
  member: (symbol_fragment) @RZFennelVimApiMethod)

(multi_symbol
  base: (symbol_fragment) @RZFennelVim (#eq? @RZFennelVim "vim") .
  member: (symbol_fragment) @RZFennelVimApi (#any-of? @RZFennelVimApi "api" "b" "bo" "env" "filetype" "fn" "fs" "g" "glob" "json" "keymap" "o" "opt" "opt_local" "opt_global" "t" "text" "treesitter" "uv" "v" "w" "wo"))

(list
  call: (symbol) @keyword.function (#eq? @keyword.function "macro") .
  item: (symbol) @RZFunctionName)

(list
  call: (symbol) @keyword.repeat (#any-of? @keyword.repeat "accumulate" "collect" "icollect"))

;; theme file
(list
  item: (symbol) @RZColorsSpecBorder (#match? @RZColorsSpecBorder ".*[═│║].*"))

(list
  item: (symbol) @_line (#eq? @_line "║") .
  item: [
         (symbol)
         (multi_symbol
           (symbol_fragment) @RZColorsSpecGroup)
        ] @RZColorsSpecGroup (#match? @RZColorsSpecGroup "^[a-zA-Z_]"))

(list
  item: (symbol) @_cell (#eq? @_cell "│") .
  item: (symbol) @RZColorsSpecGroupRef (#match? @RZColorsSpecGroupRef "^[A-Z_]"))

(list
  item: (symbol) @_cell (#eq? @_cell "│") .
  item: (symbol) @RZColorsSpecGui (#any-of? @RZColorsSpecGui "bold" "italic" "underline" "undercurl"))

;; wezterrm
(list
  call: (symbol) @RZVimOption (#any-of? @RZVimOption "wez" "wez-import") .
  item: (symbol) @RZFennelSpecialString)

((symbol) @RZFennelInterpolation (#match? @RZFennelInterpolation "^\\$\\d+"))
