;; extends

(posting
  account: (account) @RZBeancountAssets (#match? @RZBeancountAssets "^Assets:"))

(posting
  account: (account) @RZBeancountExpenses (#match? @RZBeancountExpenses "^Expenses:"))

(posting
  account: (account) @RZBeancountLiabilities (#match? @RZBeancountLiabilities "^Liabilities:"))

(posting
  account: (account) @RZBeancountIncome (#match? @RZBeancountIncome "^Income:"))

((payee) @RZBeancountPayee)

((narration) @RZBeancountNarration)

((date) @RZBeancountDate)
