;; extends

;; ruby ----------------------------------------------------
(call method: (identifier) @RZRubyDsl (#any-of? @RZRubyDsl "alias_method" "include" "extend" "attr_accessor" "attr_writer" "attr_reader" "def_delegator" "require" "require_relative"))

((call method: (identifier) @RZRubyCall) (#set! "priority" 200))
((pair key: (hash_key_symbol) @RZRubyHashKeySymbol) (#set! "priority" 200))

((identifier) @RZRubyBlockParamShort (#match? @RZRubyBlockParamShort "^_"))

((instance_variable) @RZRubyInstanceVariable)

(block_parameters (identifier) @RZRubyBlockParam)

;; rails ----------------------------------------------------
(call
  method: (identifier) @RZRailsDsl (#any-of? @RZRailsDsl "accepts_nested_attributes_for" "after_commit" "after_create" "after_destroy" "after_find" "after_initialize" "after_save" "after_touch" "after_update" "after_validation" "around_create" "around_destroy" "around_save" "around_update" "before_commit" "before_create" "before_destroy" "before_save" "before_update" "before_validation" "belongs_to" "class_attribute" "default_scope" "delegate" "enum" "has_and_belongs_to_many" "has_many" "has_one" "scope" "validate" "validates" "validates_with" "with_options")
  arguments: (argument_list [(pair key: (hash_key_symbol) @RZRailsDslArgument)
                             (simple_symbol) @RZRailsDslArgument
                             (scope_resolution)
                             (constant)]))

(call
  method: (identifier) @RZRailsDsl (#any-of? @RZRailsDsl "default_scope")
  block: (block))

;; rspec ----------------------------------------------------
((call . method: (identifier) @RZRspecGroup (#any-of? @RZRspecGroup "describe" "context")) (#set! "priority" 300))

((call . method: (identifier) @RZRspecSpec (#any-of? @RZRspecSpec "it" "specify" "scenario")) (#set! "priority" 300))

((call . method: (identifier) @RZRspecHook (#any-of? @RZRspecHook "before" "after" "around" "background")) (#set! "priority" 300))

((call . method: (identifier) @RZRspecHelper (#any-of? @RZRspecHelper "subject" "let" "let!" "given")) (#set! "priority" 300))

(call
  receiver: (constant) @RZRspecDeclaration (#eq? @RZRspecDeclaration "RSpec")
  method: (identifier) @RZRspecDeclarationDescribe)

(call .
      method: (identifier) @_method (#any-of? @_method "let" "let!")
      arguments: (argument_list (simple_symbol) @RZRspecHelperName))
