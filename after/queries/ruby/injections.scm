;; extends

(subshell
  (string_content) @injection.content
  (#set! injection.language "bash"))

(heredoc_beginning) @_k (#eq? @_k "<<-ERB")
(heredoc_body
  (heredoc_content) @injection.content
  (#set! injection.language "eruby"))
