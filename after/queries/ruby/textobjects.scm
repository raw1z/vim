;; extends

(call
  method: (identifier) @RubyContext (#any-of? @RubyContext "context")
  block: (do_block) @context.inner) @context.outer
