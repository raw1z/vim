;; extends

(macro_invocation
  macro: (identifier) @_macro (#eq? @_macro "html")
(token_tree) @injection.content
(#set! injection.language "html")
(#set! injection.include-children))
