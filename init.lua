-- [nfnl] Compiled from init.fnl by https://github.com/Olical/nfnl, do not edit.
vim.g.mapleader = "\195\167"
vim.g.maplocalleader = "!"
vim.g.loaded_node_provider = 0
vim.g.loaded_perl_provider = 0
vim.g.loaded_python3_provider = 0
vim.g.loaded_ruby_provider = 0
local lazypath = (vim.fn.stdpath("data") .. "/lazy/lazy.nvim")
if not vim.uv.fs_stat(lazypath) then
  vim.fn.system({"git", "clone", "--filter=blob:none", "https://github.com/folke/lazy.nvim.git", "--branch=stable", lazypath})
else
end
vim.opt.rtp:prepend(lazypath)
do
  local _2_ = vim.api.nvim_create_augroup("rz_init", {clear = true})
  local function _3_()
    local config_loader = require("rz.lib.config-loader")
    return config_loader["load-all"]()
  end
  do local _ = {vim.api.nvim_create_autocmd({"User"}, {callback = _3_, group = _2_, pattern = {"LazyDone"}})} end
end
local lazy = require("lazy")
return lazy.setup("plugins", {defaults = {lazy = true}, dev = {path = vim.env.PROJECTS_PATH}})
