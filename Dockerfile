FROM ubuntu:16.04

RUN apt-get update -y
RUN apt-get install -y curl git software-properties-common

RUN add-apt-repository ppa:neovim-ppa/stable
RUN apt-get update
RUN apt-get install -y neovim

RUN apt-get install -y python-dev python-pip python3-dev python3-pip
RUN pip install -q --upgrade pip
RUN pip2 install -q neovim
RUN pip3 install -q neovim

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install --no-install-recommends yarn
RUN yarn global add neovim

RUN apt-get install -y libxslt-dev libxml2-dev zlib1g-dev ruby ruby-dev
RUN gem install --no-ri --no-rdoc neovim bundler solargraph

